"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const photoController_1 = require("./Models/photoController");
const r_1 = require("./r");
//Only use for remodeling. Not part of the actual application.
class RemapStorage {
    constructor(original, replace, test, callback) {
        this.original = original;
        this.replace = replace;
        this.test = test;
        this.callback = callback;
        this.controller = new photoController_1.PhotoController();
        this.current = 0;
        this.controller.factory().FindAll({}, (err, result) => {
            if (err) {
                console.log('ERROR remap finding all ', err);
                this.callback(err, null);
                return;
            }
            this.total = result.length;
            console.log('FOUND ', this.total);
            result.forEach(e => {
                console.log(e.filePath);
            });
            if (test)
                console.log('MOCK RESULT ');
            result.forEach(e => {
                console.log('current ', this.current);
                if (this.test) {
                    this.current++;
                    console.log(this.remapOne(e).filePath);
                }
                else {
                    this.remapOne(e);
                }
            });
            console.log('FINISH');
        });
    }
    remapOne(model) {
        //update this area if any new paths are added
        model.folderPath = model.folderPath.replace(this.original, this.replace);
        model.compressPath = model.compressPath.replace(this.original, this.replace);
        model.compress2Path = model.compress2Path.replace(this.original, this.replace);
        model.mergerPath = model.mergerPath.replace(this.original, this.replace);
        model.filePath = model.filePath.replace(this.original, this.replace);
        for (let i = 0; i < model.referencePath.length; i++) {
            model.referencePath[i].clean = model.referencePath[i].clean.replace(this.original, this.replace);
            model.referencePath[i].mark = model.referencePath[i].mark.replace(this.original, this.replace);
            if (model.referencePath[i].undo)
                model.referencePath[i].undo = model.referencePath[i].undo.replace(this.original, this.replace);
        }
        if (this.test) {
            return model;
        }
        else {
            this.controller.factory().AddOrUpdate({ _id: model._id }, model, (err, result) => {
                if (err) {
                    console.log('ERROR remap one update ', err);
                    this.callback(err, null);
                    return;
                }
                console.log('updated ', model.fileName);
                this.current++;
                if (this.current === this.total) {
                    console.log('REMAP COMPLETED!');
                }
            });
            return null;
        }
    }
}
exports.RemapStorage = RemapStorage;
const arg = r_1.System.CaptureCLIArugments();
let test;
if (arg[2].arg === 'false') {
    test = false;
}
else {
    test = true;
}
if (arg[1].arg === '*') {
    arg[1].arg = '';
}
console.log(arg);
//example: node .\remapStorage.js './Storage' './Static/Storage' false
//this will replace ./Storage with ./Static/Storage, set true for whatif result
const r = new RemapStorage(arg[0].arg, arg[1].arg, test, null);
//# sourceMappingURL=remapStorage.js.map