import { PhotoController } from "./Models/photoController";
import { PhotoModel } from "./Models/photo";
import { globals } from "./globals";
import {File, GUID, GoogleMaps} from "./r"
import {Image} from './r'
import * as _ from 'lodash'
import * as async from 'async'
import { ServerController } from "./Models/ServerController";
import * as fs from 'fs'
import { KeywordController } from "./Models/keywordController";
import { mediaType } from "./Models/reference";
//use to store, compress, track in db after uploaded
export class processor{
    rotated: string; //this will be deleted, it is use because file attr is lost after rotation

    //will return a this.model in the callback
    constructor(private currentLocation: string, private model: PhotoModel, private server:ServerController, private photo:PhotoController, private keyword: KeywordController, private callback:(err, data)=>void){        
        console.log('this file currentLocation ', currentLocation)
        this.model.compressPath = this.model.folderPath + globals.compressName + this.model.fileName 
        this.model.compress2Path = this.model.folderPath + globals.compressName2 + this.model.fileName 
        this.model.mergerPath = this.model.folderPath + globals.mergerName + this.model.fileName
        this.model.lastReferencePath = 0
        this.model = PhotoModel.addReference(this.model, this.model._id) //this should only have 1 reference now
        this.rotated = this.model.folderPath + 'TEMP_ROTATE_' + GUID.create() + '.' + File.getExtension(this.model.fileName);
    }
    start(){
        //if type is video, just store this db
        if(this.model.mediaType === mediaType.video){
            this.model.referencePath[0].mediaType = mediaType.video;
            // const remodel = this.model;
            // const callback = this.callback;
            // const dbStore = this.dbStore;
            this.model.date = fs.statSync(this.model.filePath).birthtime;
            this.dbStore(this.callback);
            return;
        }

        //Assume process will be image type
        if(!_.includes(['jpg', 'png'], File.getExtension(this.model.fileName).toLowerCase())){
            this.callback('Not photo file', null)
            return
        }else{
            //autoRotate cannot be part of async because it changes the currentlocation path that is use by the others
            this.autoRotate((err, data)=>{
                console.log('rotation check complete')
                this.startAsyncs()
            })
        }
    }
    startAsyncs(){
        const t1 = (callback:(err, data)=>void)=>{
            this.compress(callback)
        }

        //this is causing the async to finish early????!!
        const t2 = (callback:(err, data)=>void)=>{
            this.geotag(callback)
        }
        const t3 = (callback:(err, data)=>void)=>{
            this.compress2(callback)
        }

        //gets the creation date
        const t4 = (callback:(err, data)=>void)=>{
            Image.creationTime(this.currentLocation, (error, data)=>{
                if(error){
                    callback(error, null)
                    return;
                }
                if(data){
                    this.model.date = data;
                    console.log('process creationtime')
                    callback(null, true)
                }
            })
        }

        const tasks = [t1, t2, t3, t4]
        async.parallel(tasks, (err, data)=>{
            console.log('photo Process all completed ', err, data)
            //create google map if autogeneratemaps is enabled
            console.log('auto map ', globals.currentServerSettings.autoGenerateMaps, ' has geo ', this.model.hasGeoData)

            //stores model in db and process keyword
            if(globals.currentServerSettings.autoGenerateMaps && this.model.hasGeoData){
                processor.createGoogleMaps(this.model, (err, result)=>{
                    console.log('dbStore for geodata or gm')
                    this.dbStore((err, data)=>{
                        if(err){
                            this.callback(err, null)
                            return;
                        }
                        this.keywordStore((err, data)=>{
                            this.callback(err, this.model);
                        })
                        this.postProcess();
                    }) 
                })
            }else{ //otherwise, reference image will be a copy of the original compressed
                console.log('dbStore for none geodata or none gm')
                this.dbStore((err, data)=>{//stores model in db and process keyword
                    if(err){
                        this.callback(err, null)
                        return;
                    }
                    this.keywordStore((err, data)=>{
                        this.callback(err, this.model);
                    })
                    this.postProcess();
                }) 
            }
        })
    }

    //this can only be use after db entry is stored
    postProcess(){
        this.removeTempFiles();

        //mark extract wil NOT have a requestId, so use SYSTEM instead
        this.server.log(this.model.requestId? this.model.requestId: 'SYSTEM', this.model.fileName + ' processed')
    }
    geotag(callback:(err,data)=>void){
        const i = new Image(this.currentLocation)
        i.GeoLocation((err, data)=>{
            if(err){
                console.log('geo error ', err)
                this.model.hasGeoData = false
                callback(err, null)
                return
            }
            if(data){
                console.log('has geo')
                this.model.latitude = data.latitude
                this.model.longitude = data.longitude
                this.model.hasGeoData = true
                callback(null, true)
            }else{
                console.log('no geo')
                this.model.hasGeoData = false
                callback(null, true)
            }
        })
    }
    compress(callback:(err, data)=>void){
        //some images that don't need rotation will not be created
        // console.log('file exist ', fs.existsSync(this.rotated), ' file: ', this.rotated, ' usage ', fs.existsSync(this.rotated)? this.rotated: this.currentLocation)
        // process.exit();
        const i = new Image(fs.existsSync(this.rotated)? this.rotated: this.currentLocation)
        i.CompressAndResize(this.model.compressPath, globals.maxWidth, (err, result)=>{
            if(err){
                this.callback(err, null)
                return
            }
            console.log('resize completed')
            //copy the compress path to reference and clean
            File.CopyFile(this.model.compressPath, this.model.referencePath[0].mark, (err, result)=>{
                if(err){
                    this.callback(err, null)
                    return
                }
                console.log('markpath file copied')
                File.CopyFile(this.model.compressPath, this.model.referencePath[0].clean, (err, result)=>{
                    if(err){
                        this.callback(err, null)
                        return
                    }
                    console.log('markcleanpath file copied')
                    callback(null, result)
                })
            })
        })
    }
    autoRotate(callback:(err, data)=>void){
        Image.autoRotate(this.currentLocation, this.rotated, (err, data)=>{
            if(err){
                this.callback(err, null)
                return
            }
            callback(null, data)
        })
    }
    compress2(callback:(err, data)=>void){
        const i = new Image(this.rotated)
        i.CompressAndResize(this.model.compress2Path, globals.maxWidth2, (err, result)=>{
            if(err){
                this.callback(err, null)
                return
            }
            console.log('compress2 completed')
            callback(null, result)
        })
    }

    //next three method are for finalizing after async.parallel, they should not run if anything else fails
    dbStore(callback:(err, data)=>void){
        this.photo.factory().AddOrUpdate({_id: this.model._id}, this.model, callback)
    }
    keywordStore(callback:(err, data)=>void){
        KeywordController.processKeyword(this.model, true, this.keyword, this.photo, callback)
    }
    removeTempFiles(){
        this.server.del(this.rotated, ()=>{})
    }
    //creates a google map for markpath and markcleanpath
    static createGoogleMaps(model: PhotoModel, callback:(err, data)=>void){
        // console.log('Creating google map for ', model.latitude, model.longitude)
        const gm = new GoogleMaps(globals.apiKey) //a google api key may not be required?
        model = PhotoModel.addReference(model, 'gm')
        const lastIndex = model.referencePath.length-1 //increment
        model.lastReferencePath = lastIndex //set google map as the lastreference visited
        console.log('google map last reference path ', lastIndex)
        gm.mapImage2(model.latitude, model.longitude, model.referencePath[lastIndex].mark, (error, data)=>{
            if(error){
                callback('error exist in mapImage2 ' + error, null)
                return
            }
            //creates a copy in the clean version
            File.CopyFile(model.referencePath[lastIndex].mark, model.referencePath[lastIndex].clean, (error, data)=>{
                if(error){
                    console.log('copy file error ', error)
                    callback(error, null)
                    return
                }
                callback(null, model)
            })
        })
    }

    //creates a image64 cache, also use to update rotation if any
    // static cache(model:PhotoModel, photo: PhotoController, callback:(err, model)=>void){
    //     if(!model.cachePath) model.cachePath = model.folderPath + '/'  + globals.cacheName + model.fileName + '.data';
    //     if(!model.rotation) model.rotation = 0;
    //     let useFile: string;
    //     const cacheNow = ()=>{
    //         Image.getImage64(useFile, (err, data)=>{
    //             if(err){
    //                 console.log('ERROR caching image64 1 ', err)
    //                 callback(err, null)
    //                 return;
    //             }
    //             fs.writeFile(model.cachePath, data, (err)=>{
    //                 if(err){
    //                     console.log('ERROR caching image64 2 ', err)
    //                     callback(err, null)
    //                     return;
    //                 }
    //                 photo.factory().AddOrUpdate({_id: model._id}, model, (err, result)=>{
    //                     if(err){
    //                         console.log('ERROR caching image64 3 ', err)
    //                         callback(err, null)
    //                         return;
    //                     }
    //                     console.log('image64 cached')
    //                     callback(null, model)
    //                 })
    //             })
    //         })
    //     }
        
    //     if(model.rotation == 0){
    //         useFile = model.filePath
    //         cacheNow();
    //     }else{
    //         File.createFolder(globals.storage + '/' + globals.requestName)
    //         useFile = globals.storage + '/' + globals.requestName + '/' + GUID.create() + '.jpg' //this file will be deleted on server restart
    //         Image.rotate(model.filePath, useFile, model.rotation, (err, success)=>{
    //             if(err){
    //                 console.log('ERROR cache rotate ', err)
    //                 callback(err, null)
    //                 return
    //             }
    //             cacheNow();
    //         })
    //     }
    // }
}