"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const server_1 = require("./server");
const globals_1 = require("../globals");
const network = require("network");
const _ = require("lodash");
// import { NetworkConfig } from "../config";
class ServerController extends r_1.MongoEntity {
    //globals.currentServerSettings has the currently loaded settings
    Index() {
        throw new Error("Method not implemented.");
    }
    constructor(callback) {
        super();
        super.init(this);
        this.checkModelExist(callback);
        setTimeout(() => {
            this.log('SYSTEM', 'Server started');
        }, 2000);
    }
    checkModelExist(callback) {
        this.factory().FindOne({ _id: 'server' }, (err, data) => {
            if (err) {
                console.log('ERROR server model find ', err);
                throw err;
            }
            console.log('server settings found ', data);
            //create new settings if non exist
            if (data === null) {
                console.log('creating new server model');
                const model = this.Model();
                this.factory().AddOrUpdate({ _id: 'server' }, this.Model(), (err, data1) => {
                    if (err) {
                        console.log('ERROR server model find ', err);
                        throw err;
                    }
                    globals_1.globals.currentServerSettings = data1;
                    console.log('NEW SERVER SETTINGS CREATED!');
                    callback(null, true);
                    return;
                });
            }
            else {
                globals_1.globals.currentServerSettings = data;
                callback(null, true);
            }
        });
    }
    Model() {
        const p = new server_1.ServerModel();
        p._id = 'server';
        p.logs = [];
        p.columnView = ['date', 'location', 'photographer', 'comment', 'fileName'];
        p.autoGenerateMaps = false;
        p.loadAllOnStart = false;
        p.disableScreenSizeWarning = false;
        p.noConfirmDelete = false;
        p.enablePreview = false;
        p.settableStampColor = false;
        p.contraintImageHeightCard = false;
        p.fieldDefinitions = [];
        p.captchaByPass = false;
        p.networkMode = 'dhcp';
        p.ssl = false;
        p.certValid = false;
        p.keyValid = false;
        p.port = 1234;
        return p;
    }
    Schema() {
        return {
            _id: { type: String },
            enablePreview: { type: Boolean },
            logs: { type: [{ date: Date, _id: String, message: String, downloadable: Boolean }] },
            autoGenerateMaps: { type: Boolean },
            loadAllOnStart: { type: Boolean },
            columnView: { type: [String] },
            disableScreenSizeWarning: { type: Boolean },
            noConfirmDelete: { type: Boolean },
            settableStampColor: { type: Boolean },
            type: { type: String },
            file: { type: String },
            contraintImageHeightCard: { type: Boolean },
            autoVideoPlay: { type: Boolean },
            fieldDefinitions: { type: [String] },
            captchaByPass: { type: Boolean },
            hostname: { type: String },
            ipv4: { type: String },
            netmask: { type: String },
            gateway: { type: String },
            networkMode: { type: String, default: 'dhcp' },
            port: { type: Number, default: '1234' },
            ssl: { type: Boolean, default: false },
            keyValid: { type: Boolean, default: false },
            certValid: { type: Boolean, default: false },
            modifiedDate: Date,
            createdDate: Date
        };
    }
    Name() {
        return 'server';
    }
    log(id, message, downloadable = false) {
        this.factory().FindOne({ _id: 'server' }, (err, data) => {
            if (err) {
                console.log('Server settings did not create!! ', err);
                throw err;
            }
            const m = data;
            if (!m.logs)
                m.logs = [];
            m.logs.push({ date: new Date(), _id: id, message: message, downloadable: downloadable });
            this.factory().AddOrUpdate({ _id: 'server' }, m, (err, data) => {
                if (err) {
                    console.log('Error in log update', err);
                    return;
                }
                console.log('log appended: ', message);
            });
        });
    }
    //removes half the log if limit is reached, this is run in maintenance
    expireLogs(limit, callback) {
        this.factory().FindOne({ _id: 'server' }, (err, data) => {
            if (err) {
                console.log('ERROR Log purge1 ', err);
                callback(err, null);
                return;
            }
            const m = data;
            if (!m.logs) {
                console.log('ERROR Log purge2 logs is null');
                callback('ERROR Log purge2 logs is null', null);
                return;
            }
            if (!m.logs.length) {
                console.log('ERROR log is not an array');
                callback('ERROR log is not an array', null);
                return;
            }
            if (m.logs.length > limit) {
                const cut = Math.floor(limit / 2); //in case limit is an odd number, take the smaller halfs to maintain array bounds
                m.logs = m.logs.splice(cut, cut);
                this.factory().AddOrUpdate({ _id: 'server' }, m, (err, data) => {
                    this.log('SYSTEM', 'Log have been purge to due limit of ' + limit + ' logs.');
                    callback(null, true);
                });
            }
            else {
                console.log('no log purge required');
                callback(null, true);
            }
        });
    }
    //add file to be deleted during next server restart
    del(filePath, callback) {
        const model = new server_1.ServerModel();
        model._id = r_1.GUID.create();
        model.type = 'delete';
        model.file = filePath;
        this.factory().AddOrUpdate({ _id: model._id }, model, (err, data) => {
            if (err) {
                console.log('Error in log update', err);
                return;
            }
            console.log('queue for delete ', filePath);
            callback(null, true);
        });
    }
    //gets the system network and set it on the db and config files
    //this should only be call on server startup
    establishIp(defaultDevice, serverModel, callback) {
        const control = this;
        const filePathDist = r_1.ExpressObj.getNgDistPath('photomapNG') + '/assets/config.json';
        const filePathDev = r_1.ExpressObj.getNgDistPath('photomapNG') + '/../src/assets/config.json';
        const configer = new r_1.NetworkConfig([filePathDev, filePathDist]);
        network.get_interfaces_list(function (err, ip) {
            const ipInfo = _.filter(ip, e => e.name === defaultDevice)[0];
            serverModel.ipv4 = ipInfo.ip_address;
            serverModel.gateway = ipInfo.gateway_ip;
            new Promise((resolve) => {
                configer.blank(() => {
                    resolve();
                });
            }).then(() => {
                return new Promise((resolve) => {
                    configer.write('ipv4', serverModel.ipv4, (err, result) => { resolve(); });
                });
            }).then(() => {
                return new Promise((resolve) => {
                    configer.write('netmask', serverModel.netmask, (err, result) => { resolve(); });
                });
            }).then(() => {
                return new Promise((resolve) => {
                    configer.write('gateway', serverModel.gateway, (err, result) => { resolve(); });
                });
            }).then(() => {
                return new Promise((resolve) => {
                    serverModel.hostname = require('os').hostname();
                    configer.write('hostname', serverModel.hostname, (err, result) => { resolve(); });
                });
            }).then(() => {
                return new Promise((resolve) => {
                    r_1.NetworkConfig.getNetmask((err, netmask) => {
                        serverModel.netmask = netmask;
                        configer.write('netmask', serverModel.netmask, (err, result) => {
                            if (err)
                                throw err;
                            resolve();
                        });
                    });
                });
            }).then(() => {
                return new Promise((resolve) => {
                    r_1.NetworkConfig.getNetworkMode((err, networkMode) => {
                        serverModel.networkMode = networkMode;
                        configer.write('networkMode', serverModel.networkMode, (err, result) => {
                            if (err)
                                throw err;
                            resolve();
                        });
                    });
                });
            }).then(() => {
                return new Promise((resolve) => {
                    configer.write('ssl', serverModel.ssl ? 'true' : 'false', (err, result) => { resolve(); });
                });
            }).then(() => {
                return new Promise((resolve) => {
                    configer.write('port', serverModel.port.toString(), (err, result) => { resolve(); });
                });
            })
                .then(() => {
                return new Promise((resolve) => {
                    control.factory().AddOrUpdate({ _id: 'server' }, serverModel, (err, result) => {
                        resolve({ error: err, result: result });
                    });
                });
            })
                .then((e) => {
                callback(e.error, e.result);
            });
        });
    }
}
exports.ServerController = ServerController;
//# sourceMappingURL=ServerController.js.map