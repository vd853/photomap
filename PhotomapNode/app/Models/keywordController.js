"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const globals_1 = require("../globals");
const _ = require("lodash");
const async = require("async");
const cache_1 = require("../cache");
class KeywordController extends r_1.MongoEntity {
    Index() {
        return {
            '$**': 'text'
        };
    }
    constructor() {
        super();
        super.init(this);
    }
    Model() {
        const p = new r_1.KeywordModel(r_1.GUID.create());
        p._id = r_1.GUID.create(true);
        p.entries = 1;
        return p;
    }
    Schema() {
        return {
            _id: String,
            value: String,
            entries: Number,
            calls: Number,
            type: String,
            associatedId: String,
            updateId: String,
            modifiedDate: Date,
            createdDate: Date,
        };
    }
    Name() {
        return 'keyword';
    }
    static processKeyword(model, isAdd, keywordController, photoController, finalCallback) {
        //find keywords associated with this photo id
        //get all keywords and type that is already stored for this photo
        //for all the photo keyword, update the stored keys with a new updateId
        //delete all old updateId from the stored
        const photoKeywords = [];
        const updateId = r_1.GUID.create(); //this is unqiue to every keyword update, otherwise they will be delete later
        //only this will be cause for removing keywords
        if (!isAdd) {
            keywordController.factory().Remove({ associatedId: model._id }, (err, result) => {
                if (err) {
                    console.log('ERROR process keyword L2 ', err);
                    finalCallback(err, null);
                    return;
                }
                cache_1.cache.updateKeyword = true;
                finalCallback(null, true);
            });
            return;
        }
        //get all the key and value from the photomodel
        const getPhotoKeyword = (callback) => {
            const keywordable_except_all = _.difference(globals_1.globals.keywordable, ['all']); //removes all from keywordable
            keywordable_except_all.forEach(e => {
                if (model[e] !== 'undefined')
                    photoKeywords.push({ value: model[e], type: e });
            });
            globals_1.globals.currentServerSettings.fieldDefinitions.forEach(f => {
                const attr = model.attr.find(e => e.name === f);
                if (attr) {
                    photoKeywords.push({ value: attr.value, type: attr.name });
                }
            });
            callback(null, true);
        };
        //update the keyword or create a new entry for it
        const updateKeyword = (value, type, callback) => {
            const k = new r_1.KeywordModel(r_1.GUID.create());
            k.associatedId = model._id;
            k.updateId = updateId;
            k.value = value;
            k.type = type;
            keywordController.factory().FindOne(k, (err, result) => {
                if (err) {
                    console.log('ERROR process keyword L1 ', err);
                    callback(err, null);
                    return;
                }
                //create new entry if none is found for this keyword
                if (!result) {
                    k._id = r_1.GUID.create();
                }
                //add to db
                keywordController.factory().AddOrUpdate({ _id: k._id }, k, (err, result) => {
                    if (err) {
                        console.log('ERROR process keyword L2 ', err);
                        callback(err, null);
                        return;
                    }
                    callback(null, true);
                });
            });
        };
        //remove all the keywords that was not updated with this current run base on updateId
        const delKeywords = (callback) => {
            keywordController.factory().Remove({ associatedId: model._id, updateId: { $ne: updateId } }, (err, result) => {
                if (err) {
                    console.log('ERROR process keyword L2 ', err);
                    callback(err, null);
                    return;
                }
                callback(null, true);
            });
        };
        //create the task and run
        getPhotoKeyword((err, success) => {
            const tasks = [];
            photoKeywords.forEach(e => {
                tasks.push(async.apply(updateKeyword, e.value, e.type)); //params are func, value, key
            });
            async.parallel(tasks, (err, result) => {
                delKeywords((err, success) => {
                    console.log('keyword process completed ', updateId);
                    if (err) {
                        console.log('ERROR keyword process final ', err);
                        finalCallback(err, null);
                        return;
                    }
                    cache_1.cache.updateKeyword = true; //signal to update the keyword cache
                    finalCallback(null, true);
                });
            });
        });
    }
}
exports.KeywordController = KeywordController;
//# sourceMappingURL=keywordController.js.map