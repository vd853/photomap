"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const r_2 = require("../r");
class VideoController extends r_2.MongoEntity {
    Index() {
        return {
            '$**': 'text'
        };
    }
    constructor() {
        super();
        super.init(this, this.Index());
    }
    Model() {
        const m = new r_1.VideoModel();
        //default and unique values are define below
        m._id = r_2.GUID.create();
        m.title = 'Untitled';
        return m;
    }
    Schema() {
        return {
            _id: { type: 'string' },
            title: { type: 'string', required: true },
            uploaderId: { type: 'string' },
            status: { type: 'string' },
            comment: { type: 'string' },
            watches: { type: 'number' },
            parentPath: { type: 'string' },
            videoPath: { type: 'string' },
            videoPathFile: { type: 'string' },
            videoQ0: { type: 'string' },
            videoQ1: { type: 'string' },
            videoQ0File: { type: 'string' },
            videoQ1File: { type: 'string' },
            hasQ0: { type: 'string' },
            hasQ1: { type: 'string' },
            thumbPath: { type: 'string' },
            thumbOriginalPath: { type: 'string' },
            thumbPreviewPath: { type: 'string' },
            fileName: { type: 'string' },
            dHorizontal: { type: 'number' },
            dVertical: { type: 'number' },
            size: { type: 'number' },
            format: { type: 'string' },
            category: [String],
            keyword: [String],
            createdDate: { type: Date },
            modifiedDate: { type: Date }
        };
    }
    Name() {
        return 'video';
    }
    removeByPhotoModel(model, callback) {
        if (!model.someId) {
            callback('No someId', null);
            return;
        }
        if (model.someId.length < 1) {
            callback('No someId data', null);
            return;
        }
        this.factory().Remove({ _id: model.someId[0] }, (err, result) => {
            callback(err, result);
        });
    }
}
exports.VideoController = VideoController;
//# sourceMappingURL=videoController.js.map