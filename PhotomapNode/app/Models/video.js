"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const r_2 = require("../r");
class Video extends r_2.MongoEntity {
    Index() {
        throw new Error("Method not implemented.");
    }
    constructor() {
        super();
        super.init(this);
    }
    Model() {
        const m = new r_1.VideoModel();
        //default and unique values are define below
        m._id = r_2.GUID.create();
        m.uploaderId = r_2.GUID.create();
        m.title = 'Untitled';
        return m;
    }
    Schema() {
        return {
            _id: { type: 'string' },
            title: { type: 'string', required: true },
            uploaderId: { type: 'string' },
            status: { type: 'string' },
            comment: { type: 'string' },
            watches: { type: 'number' },
            parentPath: { type: 'string' },
            videoPath: { type: 'string' },
            videoPathFile: { type: 'string' },
            videoQ0: { type: 'string' },
            videoQ1: { type: 'string' },
            videoQ0File: { type: 'string' },
            videoQ1File: { type: 'string' },
            hasQ0: { type: 'string' },
            hasQ1: { type: 'string' },
            thumbPath: { type: 'string' },
            thumbOriginalPath: { type: 'string' },
            thumbPreviewPath: { type: 'string' },
            fileName: { type: 'string' },
            dHorizontal: { type: 'number' },
            dVertical: { type: 'number' },
            size: { type: 'number' },
            format: { type: 'string' },
            category: [String],
            keyword: [String],
            createdDate: { type: Date },
            modifiedDate: { type: Date }
        };
    }
    Name() {
        return 'video';
    }
}
exports.Video = Video;
//# sourceMappingURL=video.js.map