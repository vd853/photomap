import {PhotoModel} from './photo'
import {RequestType, DownloadType} from './request'
import { globals } from '../globals';
interface IModify{
    filaName: string;
    photo: PhotoModel
    ids: string[]
    isBatch: boolean;
    requestType: RequestType
    downloadType: DownloadType
}

//modify becomes request in the db for download processes
export class ModifyModel implements IModify {
    downloadType: DownloadType = 0;
    isBatch: boolean = false;
    requestType: RequestType = 0;
    filaName: string = "";
    photo: PhotoModel = new PhotoModel(globals.currentServerSettings.fieldDefinitions);
    ids: string[] = [];
    constructor(){
    }
}