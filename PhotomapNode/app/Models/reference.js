"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ReferenceModel {
    constructor() {
        this.referenceId = "";
        this.clean = "";
        this.mark = "";
        this.undo = "";
        this.term = [];
        this.mediaType = 0;
    }
    //this will remove the ReferenceModel key so it will work with the schema
    getModel() {
        return JSON.parse(JSON.stringify(this));
    }
}
exports.ReferenceModel = ReferenceModel;
var mediaType;
(function (mediaType) {
    mediaType[mediaType["photo"] = 0] = "photo";
    mediaType[mediaType["video"] = 1] = "video";
})(mediaType = exports.mediaType || (exports.mediaType = {}));
//# sourceMappingURL=reference.js.map