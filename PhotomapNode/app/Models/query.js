"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var QueryType;
(function (QueryType) {
    QueryType[QueryType["sorting"] = 0] = "sorting";
    QueryType[QueryType["photoCount"] = 1] = "photoCount";
    QueryType[QueryType["googleMap"] = 2] = "googleMap";
    QueryType[QueryType["search"] = 3] = "search";
    QueryType[QueryType["rotation"] = 4] = "rotation";
    QueryType[QueryType["referencing"] = 5] = "referencing";
    QueryType[QueryType["infoStamp"] = 6] = "infoStamp";
    QueryType[QueryType["clearStamp"] = 7] = "clearStamp";
    QueryType[QueryType["sectionReference"] = 8] = "sectionReference";
    QueryType[QueryType["expandedSearch"] = 9] = "expandedSearch";
    QueryType[QueryType["account"] = 10] = "account";
    QueryType[QueryType["setPrivilege"] = 11] = "setPrivilege";
})(QueryType = exports.QueryType || (exports.QueryType = {}));
class QueryModel {
    constructor() {
        this.queryType = 0;
        this.search = "";
        this.searchId = "";
        this.searchIds = [];
        this.isDescending = false;
        this.destroyAll = false;
        this.searchFields = [];
        this.getFields = [];
    }
}
exports.QueryModel = QueryModel;
//# sourceMappingURL=query.js.map