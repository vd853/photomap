"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MarkMode;
(function (MarkMode) {
    MarkMode[MarkMode["mark"] = 0] = "mark";
    MarkMode[MarkMode["clear"] = 1] = "clear";
    MarkMode[MarkMode["delete"] = 2] = "delete";
    MarkMode[MarkMode["extract"] = 3] = "extract";
    MarkMode[MarkMode["info"] = 4] = "info";
    MarkMode[MarkMode["undo"] = 5] = "undo";
    MarkMode[MarkMode["clone"] = 6] = "clone";
    MarkMode[MarkMode["mirror"] = 7] = "mirror";
})(MarkMode = exports.MarkMode || (exports.MarkMode = {}));
class MarkModel {
    constructor(id) {
        this.canUndo = false;
        this.x = 0;
        this.y = 0;
        this.size = 0;
        this.text = "";
        this.mode = 0;
        this.number = 0;
        this.isBlack = false;
        this.id = id;
    }
    //these are all params that must be cleared until you set another, the others can be maintained for binding purpose
    reset() {
        this.mode = 0;
        this.number = 0;
        this.canUndo = false;
    }
}
exports.MarkModel = MarkModel;
//# sourceMappingURL=mark.js.map