"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const section_1 = require("./section");
class SectionController extends r_1.MongoEntity {
    Index() {
    }
    constructor() {
        super();
        super.init(this);
    }
    Model() {
        const p = new section_1.SectionModel(r_1.GUID.create());
        return p;
    }
    Schema() {
        return {
            _id: { type: String },
            referenceId: { type: String },
            startPercent: Number,
            endPercent: Number,
            comment: String,
            duration: Number,
            modifiedDate: Date,
            createdDate: Date,
        };
    }
    Name() {
        return 'section';
    }
    removeByPhotoReferenceId(id, callback) {
        this.factory().Remove({ referenceId: id }, (err, result) => {
            if (err) {
                console.log('ERROR removeByVideoReferenceId ', err);
                callback(err, null);
                return;
            }
            console.log('section removed ', result);
            callback(null, result);
        });
    }
}
exports.SectionController = SectionController;
//# sourceMappingURL=sectionController.js.map