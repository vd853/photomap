"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const request_1 = require("./request");
class RequestController extends r_1.MongoEntity {
    Index() {
    }
    constructor() {
        super();
        super.init(this);
    }
    Model() {
        const p = new request_1.RequestModel(r_1.GUID.create(true));
        p.newZipPath();
        return p;
    }
    Schema() {
        return {
            _id: { type: String },
            status: { type: String },
            outputPath: { type: String },
            expiration: Date,
            requestIds: [String],
            requestType: Number,
            downloadType: Number,
            downloadTimes: Number,
            modifiedDate: Date,
            createdDate: Date,
        };
    }
    Name() {
        return 'request';
    }
    //removes expired entries and return non expired
    deleteExpiredDb(callback) {
        this.factory().Remove({ expiration: { $lt: new Date() } }, (err, result) => {
            if (err) {
                callback(err, null);
                return;
            }
            this.factory().FindAllReturnFields({}, ['_id'], (err, result) => {
                if (err) {
                    callback(err, null);
                    return;
                }
                callback(null, result);
            });
        });
    }
}
exports.RequestController = RequestController;
//# sourceMappingURL=requestController.js.map