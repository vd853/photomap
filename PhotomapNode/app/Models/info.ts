import { ReferenceModel, mediaType } from "./reference";
import { Privilege } from "../r";

export interface IInfo{
    _id: string
    ownerId: string
    ownerName: string
    type: string //photos or reference photo
    area: string
    location: string
    campus: string
    building: string
    component: string
    conduit: string
    comment: string
    isReference: boolean
    requestId: string
    date:Date
    keyword: string[]
    title: string
    attr: CFields[] //should only be of type strings
    authenticates: Privilege[];
    lastReferencePath: number //remember last reference path selected
    referencePath: Array<ReferenceModel>
    fileName: string
    createdDate: Date;
    mediaType: mediaType;
    someId: string[]; //use to reference anything like photo to media type and user account
    dependentId: string[]; //use to prevent delete if another IInfo depends on this one
}

export interface ICFields{
    value: string;
    _id: string;
    name: string;
}

export class CFields implements ICFields {
    value: string = "";
    _id: string;
    name: string = "";
    constructor(id: string){
        this._id = id;
    }
}