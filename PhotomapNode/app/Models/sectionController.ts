import { MongoEntity, MongoController, GUID, File } from "../r";
import { globals } from "../globals";
import { SectionModel } from "./section";

export class SectionController extends MongoEntity<SectionModel> implements MongoController<SectionModel> {
    Index() {
    }
    constructor(){
        super(); super.init(this);
    }
    Model(): SectionModel {
        const p = new SectionModel(GUID.create())
        return p
    }
    Schema() {
        return{
            _id: {type: String},
            referenceId: {type: String},
            startPercent: Number,
            endPercent: Number,
            comment: String,
            duration: Number,

            modifiedDate: Date,
            createdDate: Date,
        }
    }
    Name(): string {
        return 'section'
    }
    removeByPhotoReferenceId(id: string, callback:(err, result)=>void){
        this.factory().Remove({referenceId: id}, (err, result)=>{
            if(err){
                console.log('ERROR removeByVideoReferenceId ', err)
                callback(err, null)
                return;
            }
            console.log('section removed ', result)
            callback(null, result)
        })
    }
}