export enum QueryType {
    sorting = 0, 
    photoCount = 1, 
    googleMap = 2, 
    search = 3, 
    rotation = 4, 
    referencing = 5, 
    infoStamp = 6, 
    clearStamp = 7,
    sectionReference = 8,
    expandedSearch = 9,
    account = 10,
    setPrivilege = 11
}

interface IQuery{
    search: string; //box search
    searchId: string //for single id
    searchIds: string[]
    searchFields: string[]
    getFields: string[] //use for sorting
    queryType: QueryType;
    destroyAll: boolean
    isDescending: boolean
    sessionID: string
    package: any;
}
export class QueryModel implements IQuery {
    package: any;
    sessionID: string;
    queryType: QueryType = 0;
    search: string = "";
    searchId: string = "";
    searchIds: string[] = [];
    searchFields: string[];
    getFields: string[];
    destroyAll: boolean;
    isDescending: boolean = false;
    constructor(){
        this.destroyAll = false;
        this.searchFields = []
        this.getFields = []
    }
}