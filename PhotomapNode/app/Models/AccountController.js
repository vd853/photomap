"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const AccountModel_1 = require("../../../../uNode/m/AccountModel");
class AccountController extends r_1.MongoEntity {
    constructor() {
        super();
        this.lockoutLimit = 3;
        this.lockoutDuration = 3600; //seconds
        super.init(this);
        this.checkAdminUser();
    }
    Index() {
    }
    Model() {
        const p = new r_1.AccountModel(r_1.GUID.create());
        return p;
    }
    Schema() {
        return {
            _id: { type: String },
            user: { type: String },
            email: { type: String },
            password: { type: String },
            comments: { type: [String] },
            cookieExpire: Date,
            cookieId: String,
            cookieExpireSeconds: Number,
            sessionID: String,
            authenticated: Boolean,
            isLocked: Boolean,
            timeout: Date,
            status: Number,
            level: Number,
            lockoutTime: Date,
            retries: Number,
            info: String,
            request: Number,
            modifiedDate: Date,
            createdDate: Date,
        };
    }
    Name() {
        return 'account';
    }
    checkAdminUser() {
        return __awaiter(this, void 0, void 0, function* () {
            const adminUser = yield this.factory().FindOneAsync({ user: 'admin' });
            if (adminUser)
                return;
            const newUser = new r_1.AccountModel(r_1.GUID.create());
            newUser.password = r_1.AccountModel.genPassword('admin');
            newUser.user = 'admin';
            newUser.request = AccountModel_1.AccountRequest.none;
            newUser.level = AccountModel_1.AccountLevel.root;
            this.factory().AddOrUpdateAsync({ user: 'admin' }, newUser);
        });
    }
    IsUniqueAndValid(user, callback) {
        this.factory().FindAllReturnFields({ user: user }, ['_id'], (err, result) => {
            if (err)
                throw err;
            if (result.length > 0) {
                callback(false);
            }
            else {
                callback(true);
            }
        });
    }
    cookieAuthenticate(user, sessionID) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            const model = yield this.factory().FindOneAsync({ user: user });
            if (model) {
                if (model.sessionID === sessionID) {
                    resolve(true);
                }
                else {
                    model.authenticated = false;
                    model.status = r_1.AccountStatus.logout;
                    model.sessionID = '';
                    yield this.factory().AddOrUpdateAsync({ _id: model._id }, model);
                    resolve(false);
                }
            }
            else {
                resolve(false);
            }
        }));
    }
    //password validator, this will also update the current entry
    PasswordValid(model, preventPostUpdate = false, bypassPassword = false) {
        return new Promise((resolve) => {
            const finalize = (updateModel, condition) => {
                this.factory().AddOrUpdate({ _id: updateModel._id }, updateModel, (err, result) => {
                    if (err)
                        throw err;
                    if (!result) {
                        throw new Error('Could not update password retry count.');
                    }
                    resolve(condition);
                });
            };
            this.factory().FindOne({ user: model.user }, (err, result) => {
                if (err)
                    throw err;
                if (!result) {
                    resolve({ validate: false, isFound: false });
                    return;
                }
                if (r_1.AccountModel.cipherPassword(model.password, result.password) || bypassPassword) {
                    result.retries = 0;
                    result.isLocked = false;
                    finalize(result, { validate: true, isFound: true });
                }
                else {
                    result.retries++;
                    result.authenticated = false;
                    finalize(result, { validate: false, isFound: true });
                }
                if (result.retries > this.lockoutLimit && result.user !== 'admin') {
                    const now = new Date();
                    now.setMilliseconds(now.getMilliseconds() + this.lockoutDuration * 1000);
                    result.lockoutTime = now;
                }
                if (!preventPostUpdate)
                    this.factory().AddOrUpdateAsync({ _id: model._id }, result);
                if (preventPostUpdate)
                    console.log('No post update');
            });
        });
    }
    UnlockAccount(user) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            const model = yield this.factory().FindOneAsync({ user: user });
            yield this.factory().AddOrUpdateAsync({ _id: model._id }, r_1.AccountModel.unlock(model));
            resolve(true);
        }));
    }
    Authorization(sessionID) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            const model = yield this.factory().FindOneAsync({ sessionID: sessionID });
            if (model) {
                resolve({ validate: r_1.AccountModel.isAuthorized(model, sessionID), isFound: true });
            }
            else {
                resolve({ validate: false, isFound: false });
            }
        }));
    }
    //verifies by the model known by the frontend
    AuthorizationByModel(particalModel) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            const model = yield this.factory().FindOneAsync({ sessionID: particalModel.sessionID });
            if (model) {
                if (model._id !== particalModel._id || model.user !== particalModel.user) {
                    resolve({ validate: false, isFound: false });
                }
                else {
                    resolve({ validate: r_1.AccountModel.isAuthorized(model, particalModel.sessionID), isFound: true });
                }
            }
            else {
                resolve({ validate: false, isFound: false });
            }
        }));
    }
    isAdminAuthorized(sessionID) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            const model = yield this.factory().FindOneAsync({ sessionID: sessionID });
            if (model) {
                let adminAuthorized = false;
                if (r_1.AccountModel.isAuthorized(model, sessionID) && model.level < 2) {
                    adminAuthorized = true;
                }
                resolve({ validate: adminAuthorized, isFound: true });
            }
            else {
                resolve({ validate: false, isFound: false });
            }
        }));
    }
}
exports.AccountController = AccountController;
//# sourceMappingURL=AccountController.js.map