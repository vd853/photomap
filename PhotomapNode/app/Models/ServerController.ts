import { MongoEntity, MongoController, GUID, ExpressObj, NetworkConfig } from "../r";
import { ServerModel } from "./server";
import { globals } from "../globals";
import * as moment from 'moment';
import * as network from "network";
import * as _ from 'lodash';
// import { NetworkConfig } from "../config";

export class ServerController extends MongoEntity<ServerModel> implements MongoController<ServerModel> {
    //globals.currentServerSettings has the currently loaded settings
    Index() {
        throw new Error("Method not implemented.");
    }
    constructor(callback:()=>void){
        super(); super.init(this);
        this.checkModelExist(callback)
        setTimeout(() => {
            this.log('SYSTEM', 'Server started')
        }, 2000);
    }
    checkModelExist(callback:(err, success)=>void){
        this.factory().FindOne({_id: 'server'}, (err, data)=>{
            if(err){
                console.log('ERROR server model find ', err)
                throw err
            }
            console.log('server settings found ', data);
            //create new settings if non exist
            if(data === null){
                console.log('creating new server model');
                const model = this.Model()
                this.factory().AddOrUpdate({_id: 'server'}, this.Model(), (err, data1)=>{
                    if(err){
                        console.log('ERROR server model find ', err)
                        throw err;
                    }
                    globals.currentServerSettings = data1
                    console.log('NEW SERVER SETTINGS CREATED!')
                    callback(null, true)
                    return
                })
            }else{
                globals.currentServerSettings = data
                callback(null, true)
            }
        })
    }
    Model(): ServerModel {
        const p = new ServerModel()
        p._id = 'server'
        p.logs = []
        p.columnView = ['date', 'location', 'photographer', 'comment', 'fileName']
        p.autoGenerateMaps = false;
        p.loadAllOnStart = false;
        p.disableScreenSizeWarning = false;
        p.noConfirmDelete = false;
        p.enablePreview = false;
        p.settableStampColor = false;
        p.contraintImageHeightCard = false;
        p.fieldDefinitions = [];
        p.captchaByPass = false;

        p.networkMode = 'dhcp';
        p.ssl = false;
        p.certValid = false;
        p.keyValid = false;
        p.port = 1234;
        return p
    }
    Schema() {
        return{
            _id: {type: String},
            enablePreview: {type: Boolean},
            logs: {type: [{date: Date, _id: String, message: String, downloadable: Boolean}]},
            autoGenerateMaps: {type: Boolean},
            loadAllOnStart: {type: Boolean},
            columnView: {type: [String]},
            disableScreenSizeWarning: {type: Boolean},
            noConfirmDelete: {type: Boolean},
            settableStampColor: {type: Boolean},
            type: {type: String},
            file: {type: String},
            contraintImageHeightCard: {type: Boolean},
            autoVideoPlay: {type: Boolean},
            fieldDefinitions: {type: [String]},
            captchaByPass: {type: Boolean},
            
            hostname: {type: String},
            ipv4: {type: String},
            netmask: {type: String},
            gateway: {type: String},
            networkMode: {type: String, default: 'dhcp'},
            port: {type: Number, default:'1234'},
            ssl: {type: Boolean, default: false},
            keyValid: {type: Boolean, default: false},
            certValid: {type: Boolean, default: false},

            modifiedDate: Date,
            createdDate: Date
        }
    }
    Name(): string {
        return 'server'
    }
    log(id: string, message: string, downloadable = false){
        this.factory().FindOne({_id: 'server'}, (err, data)=>{
            if(err){
                console.log('Server settings did not create!! ', err)
                throw err
            }
            const m = <ServerModel>data
            if(!m.logs) m.logs = []
            m.logs.push({date: new Date(), _id: id, message: message, downloadable: downloadable})
            this.factory().AddOrUpdate({_id: 'server'}, m, (err, data)=>{
                if(err){
                    console.log('Error in log update', err)
                    return;
                }
                console.log('log appended: ', message)
            })
        })
    }

    //removes half the log if limit is reached, this is run in maintenance
    expireLogs(limit: number, callback:(err, result)=>void){
        this.factory().FindOne({_id: 'server'}, (err, data)=>{
            if(err){
                console.log('ERROR Log purge1 ', err)
                callback(err, null)
                return;
            }
            const m = <ServerModel>data
            if(!m.logs){
                console.log('ERROR Log purge2 logs is null')
                callback('ERROR Log purge2 logs is null', null)
                return;
            }
            if(!m.logs.length){
                console.log('ERROR log is not an array')
                callback('ERROR log is not an array', null)
                return;
            }
            if(m.logs.length > limit){
                const cut = Math.floor(limit/2) //in case limit is an odd number, take the smaller halfs to maintain array bounds
                m.logs = m.logs.splice(cut, cut)
                this.factory().AddOrUpdate({_id: 'server'}, m, (err, data)=>{
                    this.log('SYSTEM', 'Log have been purge to due limit of ' + limit + ' logs.')
                    callback(null, true)
                })
            }else{
                console.log('no log purge required')
                callback(null, true)
            }
        })
    }

    //add file to be deleted during next server restart
    del(filePath: string, callback:(err, result)=>void){
        const model = new ServerModel()
        model._id = GUID.create();
        model.type = 'delete'
        model.file = filePath
        this.factory().AddOrUpdate({_id: model._id}, model, (err, data)=>{
            if(err){
                console.log('Error in log update', err)
                return;
            }
            console.log('queue for delete ', filePath)
            callback(null, true)
        })
    }

    //gets the system network and set it on the db and config files
    //this should only be call on server startup
    establishIp(defaultDevice: string, serverModel:ServerModel, callback:(err, result)=>void){
        const control = this;
        const filePathDist = ExpressObj.getNgDistPath('photomapNG') + '/assets/config.json'
        const filePathDev = ExpressObj.getNgDistPath('photomapNG') + '/../src/assets/config.json'
        const configer = new NetworkConfig([filePathDev, filePathDist])
        network.get_interfaces_list(function(err, ip) {
            const ipInfo = _.filter(ip, e=>e.name === defaultDevice)[0];
            serverModel.ipv4 = ipInfo.ip_address;
            serverModel.gateway = ipInfo.gateway_ip;
            new Promise((resolve)=>{
                configer.blank(()=>{
                    resolve();
                });
            }).then(()=>{
                return new Promise((resolve)=>{
                    configer.write('ipv4', serverModel.ipv4, (err, result)=>{resolve()});
                })
            }).then(()=>{
                return new Promise((resolve)=>{
                    configer.write('netmask', serverModel.netmask, (err, result)=>{resolve()});
                })
            }).then(()=>{
                return new Promise((resolve)=>{
                    configer.write('gateway', serverModel.gateway, (err, result)=>{resolve()});
                })
            }).then(()=>{
                return new Promise((resolve)=>{
                    serverModel.hostname = require('os').hostname();
                    configer.write('hostname', serverModel.hostname, (err, result)=>{resolve()});
                })
            }).then(()=>{
                return new Promise((resolve)=>{
                    NetworkConfig.getNetmask((err, netmask)=>{
                        serverModel.netmask = netmask;
                        configer.write('netmask', serverModel.netmask, (err, result)=>{
                            if(err)throw err
                            resolve();
                        });
                    })
                })
            }).then(()=>{
                return new Promise((resolve)=>{
                    NetworkConfig.getNetworkMode((err, networkMode)=>{
                        serverModel.networkMode = networkMode;
                        configer.write('networkMode', serverModel.networkMode, (err, result)=>{
                            if(err)throw err
                            resolve();
                        })
                    })
                })
            }).then(()=>{
                return new Promise((resolve)=>{
                    configer.write('ssl', serverModel.ssl? 'true': 'false', (err, result)=>{resolve()});
                })
            }).then(()=>{
                return new Promise((resolve)=>{
                    configer.write('port', serverModel.port.toString(), (err, result)=>{resolve()});
                })
            })

            //this must be last. writes if any new ip is found, necessary for static to dhcp
            .then(()=>{
                return new Promise((resolve)=>{
                    control.factory().AddOrUpdate({_id: 'server'}, serverModel, (err, result)=>{
                        resolve({error: err, result: result})
                    })
                })
            })
            .then((e:any)=>{
                callback(e.error, e.result)
            })
        })
    }
}