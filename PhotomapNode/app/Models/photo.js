"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GUID_1 = require("../../../../uNode/u/GUID"); //using r will cause error since this is reference by Angular
const globals_1 = require("../globals");
const _ = require("lodash");
const reference_1 = require("./reference");
const info_1 = require("./info");
const util_1 = require("util");
class PhotoModel {
    constructor(CFieldNames) {
        this.attr = [];
        CFieldNames.forEach(e => {
            const initField = new info_1.CFields(GUID_1.GUID.create());
            initField.name = e;
            initField.value = null;
            this.attr.push(initField);
        });
    }
    //only updates field value where it is different, needed for batch updates
    static updateCField(currentModel, newModel) {
        for (let i = 0; i < currentModel.attr.length; i++) {
            const currentAttr = currentModel.attr[i];
            const newAttr = newModel.attr[i];
            if (currentAttr.value !== newAttr.value && newAttr.value !== null) {
                currentAttr.value = newAttr.value;
            }
        }
        return currentModel.attr;
    }
    static getAnyFieldValueByKey(key, model) {
        const resultModel = model[key];
        if (!util_1.isNullOrUndefined(resultModel)) {
            return resultModel;
        }
        const result = model.attr.find(e => e.name === key);
        if (!util_1.isNullOrUndefined(result)) {
            return result.value;
        }
        return null;
    }
    static clone(model, CFieldNames) {
        const newModel = new PhotoModel(CFieldNames);
        globals_1.globals.clonable.forEach(e => {
            newModel[e] = model[e];
        });
        return newModel;
    }
    //creates the proper folder and push to list
    //it does NOT set the lastvisit
    static addReference(model, referenceId) {
        if (!model.referencePath)
            model.referencePath = [];
        const code = GUID_1.GUID.create(true);
        const mark = model.folderPath + '/' + code + '_' + globals_1.globals.markName + model.fileName;
        const clean = model.folderPath + '/' + code + '_' + globals_1.globals.markCleanName + model.fileName;
        const undo = model.folderPath + '/' + code + '_' + globals_1.globals.markUndoName + model.fileName;
        const reference = new reference_1.ReferenceModel();
        reference.referenceId = referenceId;
        reference.clean = clean;
        reference.mark = mark;
        reference.undo = undo;
        reference.mediaType = reference_1.mediaType.photo;
        model.referencePath.push(reference);
        return model;
    }
    //removes entire reference
    static removeReference(model, index) {
        model.referencePath.splice(index, 1);
        return model;
    }
    static addTerms(model, term = '', index, isRemove = false, isClear = false) {
        //clear all terms
        console.log('isClear ', isClear);
        if (isClear) {
            model.referencePath[index].term = [];
            console.log('terms cleared ', model.referencePath[index].term);
            return model;
        }
        //remove term
        if (isRemove) {
            let removeAt;
            for (let i = 0; i < model.referencePath[index].term.length; i++) {
                if (model.referencePath[index].term[i] === term) {
                    removeAt = i;
                }
            }
            if (removeAt) {
                model.referencePath[index].term.splice(removeAt, 1);
            }
            return model;
        }
        //add term
        if (!_.includes(model.referencePath[index].term, term))
            model.referencePath[index].term.push(term);
        return model;
    }
    addRemoveDependence(model, id, isDelete = false) {
        if (!model.dependentId)
            model.dependentId = [];
        if (!isDelete) {
            model.dependentId.push(id);
        }
        else {
            model.dependentId = _.difference(model.dependentId, [id]);
        }
        return model;
    }
    //to remove set Privilege field toRemove to true
    static modifyAuthenticates(model, authenticate) {
        if (!authenticate)
            throw new Error('authenticate is null');
        if (!model)
            throw new Error('model is null');
        if (authenticate._id === null ||
            authenticate.associatedId === null ||
            authenticate.user === null ||
            authenticate.privilege === null) {
            throw new Error('authenticate is invalid, a value is null');
        }
        if (!model.authenticates)
            model.authenticates = [];
        if (model.authenticates.length > 0) {
            if (!authenticate.toRemove) {
                let found = false;
                for (let i = 0; i < model.authenticates.length; i++) {
                    if (model.authenticates[i].associatedId === authenticate.associatedId) {
                        model.authenticates[i] = authenticate;
                        found = true;
                        i = model.authenticates.length;
                    }
                }
                if (!found) {
                    model.authenticates.push(authenticate);
                    return model;
                }
                else {
                    return model;
                }
            }
            else {
                const deleted = model.authenticates.some(e => {
                    let thisAuth = model.authenticates.find(e => e._id === authenticate._id);
                    if (thisAuth) {
                        const indexSlice = model.authenticates.indexOf(thisAuth);
                        model.authenticates.splice(indexSlice, 1);
                        return true;
                    }
                });
                if (!deleted) {
                    throw new Error('Expected authenticate to remove does not exist.');
                }
                else {
                    return model;
                }
            }
        }
        else {
            if (authenticate.toRemove) {
                throw new Error('Expected authenticate to remove on empty list.');
            }
            model.authenticates.push(authenticate);
            return model;
        }
    }
    static getPrivilageByUser(model, userId) {
        if (!model || !userId || !model.authenticates)
            throw new Error('No model or userId provided.');
        const result = model.authenticates.find(e => e.associatedId == userId);
        if (!result)
            return 0; //referencing privilagetype will cause error in ng builds, don't know why.
        return result.privilege;
    }
}
exports.PhotoModel = PhotoModel;
//# sourceMappingURL=photo.js.map