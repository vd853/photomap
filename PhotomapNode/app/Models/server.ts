interface IServer {
    _id: string
    logs:Array<{date: Date, _id: string, message: string, downloadable: boolean}>
    autoGenerateMaps: boolean
    enablePreview: boolean
    loadAllOnStart: boolean
    columnView: string[]
    noConfirmDelete: boolean;
    disableScreenSizeWarning: boolean;
    settableStampColor: boolean;
    contraintImageHeightCard: boolean; //use to limit the height of view and stamp
    autoVideoPlay: boolean;
    fieldDefinitions: string[];
    captchaByPass: boolean;

    hostname: string;
    ipv4: string;
    netmask: string;
    gateway: string;
    networkMode: string;
    port: number;
    ssl: boolean;
    keyValid: boolean;
    certValid: boolean;

    type: string;
    file: string;

    //triggers, not in db schema
    triggers: ServerTriggers;
}

const columnViewLimit = 5;

export class ServerModel implements IServer {
    captchaByPass: boolean;
    fieldDefinitions: string[] = [];
    hostname: string = "";
    keyValid: boolean = false;
    certValid: boolean = false;
    port: number = 0;
    ssl: boolean = false;
    ipv4: string = "";
    netmask: string = "";
    gateway: string = "";
    networkMode: string = "";
    triggers: ServerTriggers = 0;
    autoVideoPlay: boolean = false;
    contraintImageHeightCard: boolean = false;
    type: string = "";
    file: string = "";
    settableStampColor: boolean = false;
    loadAllOnStart: boolean = false;
    noConfirmDelete: boolean = false;
    disableScreenSizeWarning: boolean = false;
    columnView: string[] = [];
    autoGenerateMaps: boolean;
    _id: string = "server";
    logs: { date: Date; _id: string, message: string, downloadable: boolean }[] = [];
    enablePreview: boolean = false;

    constructor(){
        this.autoGenerateMaps = false;
    }
    static clean(model: ServerModel){
        if(model.columnView.length > columnViewLimit){
            model.columnView = model.columnView.slice(0, columnViewLimit);
        }
        return model;
    }
}

export enum ServerTriggers {none = -1, networkUpdate = 0, reboot = 1, fieldUpdate = 2}