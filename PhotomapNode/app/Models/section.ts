import { duration } from "moment";

interface ISection {
    _id: string 
    comment: string;
    startPercent: number
    endPercent: number
    duration: number;
    referenceId: string
}

export class SectionModel implements ISection {
    duration: number = 0;
    comment: string = "";
    _id: string;
    startPercent: number = 0;
    endPercent: number = 0;
    referenceId: string = "";
    constructor(id: string){
        this._id = id;
    }
}