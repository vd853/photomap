import { MongoEntity, MongoController, GUID, File } from "../r";
import { RequestModel } from "./request";
import { globals } from "../globals";

export class RequestController extends MongoEntity<RequestModel> implements MongoController<RequestModel> {
    Index() {
    }
    constructor(){
        super(); super.init(this);
    }
    Model(): RequestModel {
        const p = new RequestModel(GUID.create(true))
        p.newZipPath()
        return p
    }
    Schema() {
        return{
            _id: {type: String},
            status: {type: String},
            outputPath: {type: String},
            expiration: Date,
            requestIds: [String],
            requestType: Number,
            downloadType: Number,
            downloadTimes: Number,
            
            modifiedDate: Date,
            createdDate: Date,
        }
    }
    Name(): string {
        return 'request'
    }

    //removes expired entries and return non expired
    deleteExpiredDb(callback:(err, result)=>void){
        this.factory().Remove({expiration: {$lt: new Date()}}, (err, result)=>{
            if(err){
                callback(err, null)
                return;
            }
            this.factory().FindAllReturnFields({},['_id'], (err, result)=>{
                if(err){
                    callback(err, null)
                    return;
                }
                callback(null, result)
            })
        })
    }
}