"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const photo_1 = require("./photo");
const globals_1 = require("../globals");
//modify becomes request in the db for download processes
class ModifyModel {
    constructor() {
        this.downloadType = 0;
        this.isBatch = false;
        this.requestType = 0;
        this.filaName = "";
        this.photo = new photo_1.PhotoModel(globals_1.globals.currentServerSettings.fieldDefinitions);
        this.ids = [];
    }
}
exports.ModifyModel = ModifyModel;
//# sourceMappingURL=modify.js.map