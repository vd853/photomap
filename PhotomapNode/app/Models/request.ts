import { globals } from "../globals";

export enum RequestType {request = 0, lookup = 1, singleDownload = 2} //assume request to be multiple
export enum DownloadType {high = 0, reference = 1, merge = 2, requestDownload = 3, clipDownload = 4}
//lookup is use to check if request has expired

interface IRequest {
    _id: string //can be set by the modify._id in the download route
    requestIds: string[]
    outputPath: string
    expiration: Date
    requestType: RequestType;
    downloadType: DownloadType;
    status: string
    downloadTimes: number
}

export class RequestModel implements IRequest {
    _id: string;
    requestIds: string[] = [];
    outputPath: string = "";
    expiration: Date = new Date();
    requestType: RequestType = 0;
    downloadType: DownloadType = 0;
    status: string = "";
    downloadTimes: number = 0;
    constructor(id: string){
        this._id = id;
    }
    newZipPath(){
        if(!this._id){
            console.log('id is not define for zip path');
            throw 'id is not define for zip path'
        }
        this.outputPath = globals.storage + globals.requestName + '/' + this._id + '.zip'
        this.downloadTimes = 0;
    }
}