"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const globals_1 = require("../globals");
var RequestType;
(function (RequestType) {
    RequestType[RequestType["request"] = 0] = "request";
    RequestType[RequestType["lookup"] = 1] = "lookup";
    RequestType[RequestType["singleDownload"] = 2] = "singleDownload";
})(RequestType = exports.RequestType || (exports.RequestType = {})); //assume request to be multiple
var DownloadType;
(function (DownloadType) {
    DownloadType[DownloadType["high"] = 0] = "high";
    DownloadType[DownloadType["reference"] = 1] = "reference";
    DownloadType[DownloadType["merge"] = 2] = "merge";
    DownloadType[DownloadType["requestDownload"] = 3] = "requestDownload";
    DownloadType[DownloadType["clipDownload"] = 4] = "clipDownload";
})(DownloadType = exports.DownloadType || (exports.DownloadType = {}));
class RequestModel {
    constructor(id) {
        this.requestIds = [];
        this.outputPath = "";
        this.expiration = new Date();
        this.requestType = 0;
        this.downloadType = 0;
        this.status = "";
        this.downloadTimes = 0;
        this._id = id;
    }
    newZipPath() {
        if (!this._id) {
            console.log('id is not define for zip path');
            throw 'id is not define for zip path';
        }
        this.outputPath = globals_1.globals.storage + globals_1.globals.requestName + '/' + this._id + '.zip';
        this.downloadTimes = 0;
    }
}
exports.RequestModel = RequestModel;
//# sourceMappingURL=request.js.map