"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const async = require("async");
const request_1 = require("./request");
const _ = require("lodash");
const r_1 = require("../r");
const globals_1 = require("../globals");
class Batch {
    constructor(model, controllers, callback) {
        this.model = model;
        this.controllers = controllers;
        this.callback = callback;
        r_1.File.createFolder(globals_1.globals.storage + '/' + globals_1.globals.requestName);
        switch (model.downloadType) {
            case request_1.DownloadType.high:
                this.process(['filePath']);
                break;
            case request_1.DownloadType.reference:
                this.processCurrentReferences();
                break;
            case request_1.DownloadType.merge:
                this.processMerge();
                break;
            case request_1.DownloadType.clipDownload:
                this.processClips();
                break;
            default:
                break;
        }
    }
    process(types) {
        this.controllers.photo.factory().FindAllForFields('_id', this.model.requestIds, types, (err, list) => {
            if (err) {
                this.model.status = 'Batch download error in prepare ' + err.toString();
                console.log('Batch download error in prepare ', err);
                this.callback(err, this.model);
                return;
            }
            const files = _.map(list, 'filePath');
            this.zip(files);
        });
    }
    //batch zips all the current references
    processCurrentReferences() {
        this.controllers.photo.factory().FindAllForFields('_id', this.model.requestIds, ['referencePath', 'lastReferencePath'], (err, list) => {
            if (err) {
                this.model.status = 'Batch download error in prepare ' + err.toString();
                console.log('Batch download error in prepare ', err);
                this.callback(err, this.model);
                return;
            }
            const files = [];
            list.forEach(e => {
                files.push(e.referencePath[e.lastReferencePath].mark);
            });
            this.zip(files);
        });
    }
    //prepares a zip file after processing all merge. merge is process async parallel
    processMerge() {
        this.controllers.photo.factory().FindAllForFields('_id', this.model.requestIds, ['mergerPath', 'compressPath', 'referencePath', 'lastReferencePath'], (err, list) => {
            if (err) {
                this.model.status = 'Batch download error in prepare ' + err.toString();
                this.callback(err, this.model);
                return;
            }
            const files = [];
            const tasks = [];
            list.forEach(e => {
                const task = (callback1) => {
                    const mark = e.referencePath[e.lastReferencePath].mark;
                    const compress = e.compressPath;
                    const merge = e.mergerPath;
                    files.push(merge);
                    r_1.Image.imageMerge([compress, mark], merge, callback1);
                };
                tasks.push(task);
            });
            async.parallel(tasks, (err, data) => {
                if (err) {
                    this.callback(err, null);
                    return;
                }
                console.log('merge files ', files);
                this.zip(files);
            });
        });
    }
    processClips() {
        console.log('processClips');
        const tempFolder = globals_1.globals.storage + '/' + globals_1.globals.requestName + '/temp' + r_1.GUID.create() + '/';
        const tasks = [];
        r_1.File.createFolder(tempFolder);
        let count = 1;
        //finds a section, finds the video of that section, begin cropping one
        const task = (id, callback) => {
            this.controllers.section.factory().FindOne({ _id: id }, (err, sModel) => {
                console.log('finding section model');
                if (err) {
                    callback(err, null);
                    return;
                }
                this.controllers.photo.factory().FindOne({ _id: sModel.referenceId }, (err, pModel) => {
                    if (err) {
                        console.log(err);
                        callback(err, null);
                        return;
                    }
                    console.log('video model ', pModel.someId[0]);
                    this.controllers.video.factory().FindOne({ _id: pModel.someId[0] }, (err, vModel) => {
                        if (err) {
                            console.log(err);
                            callback(err, null);
                            return;
                        }
                        console.log('video path ', r_1.File.reparsePath(vModel.videoPathFile));
                        const e = new r_1.Encoder(r_1.File.reparsePath(vModel.videoPathFile)); //need this path parsing for unit test
                        const newFile = count + '_' + sModel.comment.substr(0, 5) + '_' + r_1.GUID.create(true);
                        const durationBetween = (sModel.endPercent - sModel.startPercent) * sModel.duration;
                        const start = sModel.startPercent * sModel.duration;
                        console.log('creating clip at ', newFile);
                        e.EncodeFF(tempFolder, newFile, -1, -1, start, durationBetween, 1028, null, callback);
                    });
                });
            });
        };
        this.model.requestIds.forEach(e => {
            tasks.push(async.apply(task, e));
        });
        async.parallel(tasks, (err, result) => {
            if (err) {
                this.callback(err, null);
                return;
            }
            console.log('creating zip files for ', result);
            this.zip(result);
        });
    }
    //use by other process to start the zip process
    zip(files) {
        r_1.File.zipFiles(files, this.model.outputPath, (err, data) => {
            if (err) {
                this.model.status = 'Batch download error in prepare';
                this.callback(err, this.model);
                return;
            }
            this.model.status = 'Batch download available';
            this.callback(null, this.model);
        });
    }
}
exports.Batch = Batch;
//# sourceMappingURL=batch.js.map