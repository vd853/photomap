import { MongoEntity, MongoController, GUID, KeywordModel, Privilege, IAuthenticateResponse, AccountModel } from "../r";
import { PhotoModel } from "./photo";
import { KeywordController } from "./keywordController";
import { globals } from "../globals";
import * as async from 'async'
import * as _ from 'lodash';
import { CFields } from "./info";
import { Ctrl } from "../Server/server";

export class PhotoController extends MongoEntity<PhotoModel> implements MongoController<PhotoModel> {
    constructor(){
        super(); super.init(this, this.Index());
    }
    Model(): PhotoModel {
        let fieldDefinitions = globals && 
                                globals.currentServerSettings &&
                                globals.currentServerSettings.fieldDefinitions? 
                                globals.currentServerSettings.fieldDefinitions:
                                ['fieldTest1', 'fieldTest2'];
        const p = new PhotoModel(fieldDefinitions)
        p._id = GUID.create()
        p.markerCoordinates = []
        p.authenticates = []
        p.attr = []
        return p
    }
    Index(){
        return {
            '$**': 'text'
        }
    }
    Schema() {
        return{
            _id: {type: String},
            title: {type: String},
            ownerId: {type: String},
            ownerName: {type: String},
            type: {type: String},
            requestId: {type: String},
            photographer: {type: String},
            area: {type: String},
            location: {type: String},
            campus: {type: String},
            building: {type: String},
            conduit: {type: String},
            component: {type: String},
            comment: {type: String},
            isReference: {type: Boolean},
            hasGeoData: {type: Boolean},
            latitude: {type: Number},
            longitude: {type: Number},
            keyword: {type: [String]},
            date: Date,
            rotation: Number,
            mediaType: Number,
            someId: [String],
            dependentId: [String],
            attr: Object,
            authenticates: Object,
            
            referencePath: {type: [
                {
                    clean: String, 
                    mark: String, 
                    undo: String,
                    term: [String],
                    mediaType: Number,
                    referenceId: String
                }
            ]},

            compress2Path: {type: String},
            cachePath: {type: String},
            lastReferencePath: {type: Number},
            fileName: {type: String},
            folderPath: {type: String},
            filePath: {type: String},
            compressPath: {type: String},
            mergerPath: {type: String},
            mergerPhotoId: {type: String},
            markerCoordinates: [{x: Number, y: Number}], //array

            modifiedDate: Date, //meaningless to the user since it is modified by the server
            createdDate: Date
        }
    }

    Name(): string {
        return 'photo'
    }
    
    adjustFilePath(newPath: string, id: string, callback:(err, result)=>void){
        this.factory().FindInArray('someId', [id], (err, result)=>{
            if(err){
                callback(err, null);
                return;
            }
            const result1 = result[0]
            result1.filePath = newPath;
            this.factory().AddOrUpdate({_id: result1._id}, result1, (err, result)=>{
                if(err){
                    callback(err, null);
                    return;
                }
                callback(null, result1);
            })
        })
    }

    static modifyField(model:PhotoModel, shouldBeFields: string[], controllers: Ctrl):PhotoModel{
        const currentFields = _.map(model.attr, 'name');
        const delField = _.difference(currentFields, shouldBeFields)
        const addField = _.difference(shouldBeFields, currentFields)
        addField.forEach(f=>{
            let search;
            if(model.attr.length > 0){
                search = model.attr.find(e=>e.name === f);
            }
            if(!search){
                let newField = new CFields(GUID.create());
                newField.name = f;
                newField.value = '';
                model.attr.push(newField)
            }
        })
        delField.forEach(f=>{
            let thisField = model.attr.find(e=>e.name === f);
            const indexSlice = model.attr.indexOf(thisField!);
            model.attr.splice(indexSlice, 1);
            controllers.keyword.factory().Remove({type: f}, ()=>{})
        })
        return model;
    }
    static ProfileAuthorized(model: PhotoModel, sessionID: string, controllers: Ctrl){
        return new Promise<IAuthenticateResponse>(async (resolve, reject)=>{
            if(!model || !sessionID || !controllers || !model.authenticates) reject(new Error('Params not provided or null'))
            const response = AccountModel.responseFactory();
            const user = await controllers.account.factory().FindOneAsync({sessionID: sessionID});
            if(!user){
                response.reason = 'Session expired or user not exist.'
                resolve(response);
            }
            response.package = PhotoModel.getPrivilageByUser(model, user._id);
            response.reason = "User and sessionID verified and privilage retreviable";
            response.verified = true;
            response.model = user;
            resolve(response);
        })
    }
}