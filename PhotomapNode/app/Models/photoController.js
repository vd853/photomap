"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const photo_1 = require("./photo");
const globals_1 = require("../globals");
const _ = require("lodash");
const info_1 = require("./info");
class PhotoController extends r_1.MongoEntity {
    constructor() {
        super();
        super.init(this, this.Index());
    }
    Model() {
        let fieldDefinitions = globals_1.globals &&
            globals_1.globals.currentServerSettings &&
            globals_1.globals.currentServerSettings.fieldDefinitions ?
            globals_1.globals.currentServerSettings.fieldDefinitions :
            ['fieldTest1', 'fieldTest2'];
        const p = new photo_1.PhotoModel(fieldDefinitions);
        p._id = r_1.GUID.create();
        p.markerCoordinates = [];
        p.authenticates = [];
        p.attr = [];
        return p;
    }
    Index() {
        return {
            '$**': 'text'
        };
    }
    Schema() {
        return {
            _id: { type: String },
            title: { type: String },
            ownerId: { type: String },
            ownerName: { type: String },
            type: { type: String },
            requestId: { type: String },
            photographer: { type: String },
            area: { type: String },
            location: { type: String },
            campus: { type: String },
            building: { type: String },
            conduit: { type: String },
            component: { type: String },
            comment: { type: String },
            isReference: { type: Boolean },
            hasGeoData: { type: Boolean },
            latitude: { type: Number },
            longitude: { type: Number },
            keyword: { type: [String] },
            date: Date,
            rotation: Number,
            mediaType: Number,
            someId: [String],
            dependentId: [String],
            attr: Object,
            authenticates: Object,
            referencePath: { type: [
                    {
                        clean: String,
                        mark: String,
                        undo: String,
                        term: [String],
                        mediaType: Number,
                        referenceId: String
                    }
                ] },
            compress2Path: { type: String },
            cachePath: { type: String },
            lastReferencePath: { type: Number },
            fileName: { type: String },
            folderPath: { type: String },
            filePath: { type: String },
            compressPath: { type: String },
            mergerPath: { type: String },
            mergerPhotoId: { type: String },
            markerCoordinates: [{ x: Number, y: Number }],
            modifiedDate: Date,
            createdDate: Date
        };
    }
    Name() {
        return 'photo';
    }
    adjustFilePath(newPath, id, callback) {
        this.factory().FindInArray('someId', [id], (err, result) => {
            if (err) {
                callback(err, null);
                return;
            }
            const result1 = result[0];
            result1.filePath = newPath;
            this.factory().AddOrUpdate({ _id: result1._id }, result1, (err, result) => {
                if (err) {
                    callback(err, null);
                    return;
                }
                callback(null, result1);
            });
        });
    }
    static modifyField(model, shouldBeFields, controllers) {
        const currentFields = _.map(model.attr, 'name');
        const delField = _.difference(currentFields, shouldBeFields);
        const addField = _.difference(shouldBeFields, currentFields);
        addField.forEach(f => {
            let search;
            if (model.attr.length > 0) {
                search = model.attr.find(e => e.name === f);
            }
            if (!search) {
                let newField = new info_1.CFields(r_1.GUID.create());
                newField.name = f;
                newField.value = '';
                model.attr.push(newField);
            }
        });
        delField.forEach(f => {
            let thisField = model.attr.find(e => e.name === f);
            const indexSlice = model.attr.indexOf(thisField);
            model.attr.splice(indexSlice, 1);
            controllers.keyword.factory().Remove({ type: f }, () => { });
        });
        return model;
    }
    static ProfileAuthorized(model, sessionID, controllers) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            if (!model || !sessionID || !controllers || !model.authenticates)
                reject(new Error('Params not provided or null'));
            const response = r_1.AccountModel.responseFactory();
            const user = yield controllers.account.factory().FindOneAsync({ sessionID: sessionID });
            if (!user) {
                response.reason = 'Session expired or user not exist.';
                resolve(response);
            }
            response.package = photo_1.PhotoModel.getPrivilageByUser(model, user._id);
            response.reason = "User and sessionID verified and privilage retreviable";
            response.verified = true;
            response.model = user;
            resolve(response);
        }));
    }
}
exports.PhotoController = PhotoController;
//# sourceMappingURL=photoController.js.map