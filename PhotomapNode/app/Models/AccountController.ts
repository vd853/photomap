import { MongoEntity, MongoController, GUID, AccountModel, AccountStatus, Data } from "../r";
import { AccountRequest, AccountLevel } from "../../../../uNode/m/AccountModel";
import { PhotoModel } from "./photo";

export class AccountController extends MongoEntity<AccountModel> implements MongoController<AccountModel> {
    private lockoutLimit = 3;
    private lockoutDuration = 3600; //seconds
    Index() {
    }
    constructor(){
        super(); super.init(this);
        this.checkAdminUser();
    }
    Model(): AccountModel {
        const p = new AccountModel(GUID.create())
        return p
    }
    Schema() {
        return{
            _id: {type: String},
            user: {type: String},
            email: {type: String},
            password: {type: String},
            comments: {type: [String]},
            cookieExpire: Date,
            cookieId: String,
            cookieExpireSeconds: Number,
            sessionID: String,
            authenticated: Boolean,
            isLocked: Boolean,
            timeout: Date,
            status: Number,
            level: Number,
            lockoutTime: Date,
            retries: Number,
            info: String,
            request: Number,

            modifiedDate: Date,
            createdDate: Date,
        }
    }
    Name(): string {
        return 'account'
    }

    async checkAdminUser(){
        const adminUser = await this.factory().FindOneAsync({user: 'admin'});
        if(adminUser) return;
        const newUser = new AccountModel(GUID.create());
        newUser.password = AccountModel.genPassword('admin');
        newUser.user = 'admin';
        newUser.request = AccountRequest.none;
        newUser.level = AccountLevel.root;
        this.factory().AddOrUpdateAsync({user: 'admin'}, newUser)
    }

    IsUniqueAndValid(user: string, callback:(isUnique:boolean)=>void){
        this.factory().FindAllReturnFields({user: user}, ['_id'], (err, result: string[])=>{
            if(err) throw err;
            if(result.length > 0){
                callback(false);
            }else{
                callback(true);
            }
        })
    }

    cookieAuthenticate(user: string, sessionID: string){
        return new Promise<boolean>(async (resolve)=>{
            const model = await this.factory().FindOneAsync({user: user});
            if(model){
                if(model.sessionID === sessionID){
                    resolve(true);
                }else{
                    model.authenticated = false;
                    model.status = AccountStatus.logout;
                    model.sessionID = ''
                    await this.factory().AddOrUpdateAsync({_id: model._id}, model)
                    resolve(false);
                }
            }else{
                resolve(false);
            }
        })
    }

    //password validator, this will also update the current entry
    PasswordValid(model: AccountModel, preventPostUpdate = false, bypassPassword = false){
        return new Promise<{validate: boolean, isFound: boolean}>((resolve)=>{
            const finalize = (updateModel:AccountModel, condition:{validate: boolean, isFound: boolean})=>{
                this.factory().AddOrUpdate({_id: updateModel._id}, updateModel, (err, result)=>{
                    if(err) throw err;
                    if(!result){
                        throw new Error('Could not update password retry count.')
                    }
                    resolve(condition)
                })
            }
            this.factory().FindOne({user: model.user}, (err, result:AccountModel)=>{
                if(err) throw err;
                if(!result){
                    resolve({validate: false, isFound: false})
                    return;
                }
                if(AccountModel.cipherPassword(model.password, result.password) || bypassPassword){
                    result.retries = 0;
                    result.isLocked = false;
                    finalize(result, {validate: true, isFound: true})
                }else{
                    result.retries++;
                    result.authenticated = false;
                    finalize(result, {validate: false, isFound: true})
                }

                if(result.retries > this.lockoutLimit && result.user !== 'admin'){
                    const now = new Date();
                    now.setMilliseconds(now.getMilliseconds() + this.lockoutDuration*1000);
                    result.lockoutTime = now;
                }
                
                if(!preventPostUpdate) this.factory().AddOrUpdateAsync({_id: model._id}, result);
                if(preventPostUpdate) console.log('No post update');
            })
        })
    }

    UnlockAccount(user: string){
        return new Promise(async (resolve, reject)=>{
            const model = await this.factory().FindOneAsync({user: user});
            await this.factory().AddOrUpdateAsync({_id: model._id}, AccountModel.unlock(model));
            resolve(true);
        })
    }

    Authorization(sessionID: string){
        return new Promise<{validate: boolean, isFound: boolean}>(async (resolve, reject)=>{
            const model = await this.factory().FindOneAsync({sessionID: sessionID});
            if(model){
                resolve({validate: AccountModel.isAuthorized(model, sessionID), isFound: true})
            }else{
                resolve({validate: false, isFound: false})
            }
        })
    }

    //verifies by the model known by the frontend
    AuthorizationByModel(particalModel: AccountModel){
        return new Promise<{validate: boolean, isFound: boolean}>(async (resolve, reject)=>{
            const model = await this.factory().FindOneAsync({sessionID: particalModel.sessionID});
            if(model){
                if(model._id !== particalModel._id || model.user !== particalModel.user){
                    resolve({validate: false, isFound: false})
                }else{
                    resolve({validate: AccountModel.isAuthorized(model, particalModel.sessionID), isFound: true})
                }
            }else{
                resolve({validate: false, isFound: false})
            }
        })
    }

    isAdminAuthorized(sessionID: string){
        return new Promise<{validate: boolean, isFound: boolean}>(async (resolve, reject)=>{
            const model = await this.factory().FindOneAsync({sessionID: sessionID});
            if(model){
                let adminAuthorized = false;
                if(AccountModel.isAuthorized(model, sessionID) && model.level < 2){
                    adminAuthorized = true;
                }
                resolve({validate: adminAuthorized, isFound: true})
            }else{
                resolve({validate: false, isFound: false})
            }
        })
    }
}