import { VideoModel } from "../r";
import { MongoController, GUID, MongoEntity } from "../r";
import { PhotoModel } from "./photo";

export class VideoController extends MongoEntity<VideoModel> implements MongoController<VideoModel>{
    Index() {
        return {
            '$**': 'text'
        }
    }
    constructor(){
        super(); super.init(this, this.Index());
    }
    Model(): VideoModel {
        const m = new VideoModel();
        //default and unique values are define below
        m._id = GUID.create();
        m.title = 'Untitled'
        return m
    }
    Schema() {
        return {
            _id: {type: 'string'}, //should have unique
            title: {type: 'string', required: true},
            uploaderId: {type: 'string'},
            status: {type: 'string'},
            comment: {type: 'string'},
            watches: {type: 'number'},
            
            parentPath: {type: 'string'},
            videoPath: {type: 'string'},
            videoPathFile: {type: 'string'},
            videoQ0: {type: 'string'},
            videoQ1: {type: 'string'},
            videoQ0File: {type: 'string'},
            videoQ1File: {type: 'string'},
            hasQ0: {type: 'string'},
            hasQ1: {type: 'string'},

            thumbPath: {type: 'string'},
            thumbOriginalPath: {type: 'string'},
            thumbPreviewPath: {type: 'string'},

            fileName: {type: 'string'},
            dHorizontal: {type: 'number'},
            dVertical: {type: 'number'},
            size: {type: 'number'},
            format: {type: 'string'},

            category:[String],
            keyword:[String],

            createdDate: {type: Date},
            modifiedDate: {type: Date}
        }
    }
    Name(): string {
        return 'video'
    }
    removeByPhotoModel(model:PhotoModel, callback:(err, result)=>void){
        if(!model.someId){
            callback('No someId', null);
            return;
        }
        if(model.someId.length < 1){
            callback('No someId data', null);
            return;
        }
        this.factory().Remove({_id: model.someId[0]}, (err, result)=>{
            callback(err, result);
        })
    }   
}