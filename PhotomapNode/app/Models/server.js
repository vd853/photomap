"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const columnViewLimit = 5;
class ServerModel {
    constructor() {
        this.fieldDefinitions = [];
        this.hostname = "";
        this.keyValid = false;
        this.certValid = false;
        this.port = 0;
        this.ssl = false;
        this.ipv4 = "";
        this.netmask = "";
        this.gateway = "";
        this.networkMode = "";
        this.triggers = 0;
        this.autoVideoPlay = false;
        this.contraintImageHeightCard = false;
        this.type = "";
        this.file = "";
        this.settableStampColor = false;
        this.loadAllOnStart = false;
        this.noConfirmDelete = false;
        this.disableScreenSizeWarning = false;
        this.columnView = [];
        this._id = "server";
        this.logs = [];
        this.enablePreview = false;
        this.autoGenerateMaps = false;
    }
    static clean(model) {
        if (model.columnView.length > columnViewLimit) {
            model.columnView = model.columnView.slice(0, columnViewLimit);
        }
        return model;
    }
}
exports.ServerModel = ServerModel;
var ServerTriggers;
(function (ServerTriggers) {
    ServerTriggers[ServerTriggers["none"] = -1] = "none";
    ServerTriggers[ServerTriggers["networkUpdate"] = 0] = "networkUpdate";
    ServerTriggers[ServerTriggers["reboot"] = 1] = "reboot";
    ServerTriggers[ServerTriggers["fieldUpdate"] = 2] = "fieldUpdate";
})(ServerTriggers = exports.ServerTriggers || (exports.ServerTriggers = {}));
//# sourceMappingURL=server.js.map