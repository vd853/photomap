"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SectionModel {
    constructor(id) {
        this.duration = 0;
        this.comment = "";
        this.startPercent = 0;
        this.endPercent = 0;
        this.referenceId = "";
        this._id = id;
    }
}
exports.SectionModel = SectionModel;
//# sourceMappingURL=section.js.map