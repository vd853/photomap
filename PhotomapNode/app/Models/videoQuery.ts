interface IVideoQuery{
    searchIds: string[];
    quality: string;
}
export class VideoQuery implements IVideoQuery {
    quality: string = "";
    searchIds: string[] = [];
}