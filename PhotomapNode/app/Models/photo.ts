import { GUID } from "../../../../uNode/u/GUID"; //using r will cause error since this is reference by Angular
import { globals } from "../globals";
import * as _ from 'lodash'
import { ReferenceModel, mediaType } from "./reference";
import { IInfo, ICFields, CFields } from "./info";
import { Privilege, AccountModel } from "../r";
import { isNullOrUndefined } from "util";

//adding field to title steps:
//update interface, class, controller, photoform + form contructor, all the globals arrays

interface IPhoto{
    rotation: number;
    latitude: number
    longitude: number
    hasGeoData: boolean
    photographer: string

    //these all must use p() for pointing
    compress2Path: string
    folderPath: string
    filePath: string
    compressPath: string
    mergerPath: string

    mergerPhotoId: string
    markerCoordinates: Array<{x: number, y: number}>

    //not part of db
    modifyReference: boolean //this is reset in the server after image is copied
    modifyReferenceId: string
}

export class PhotoModel implements IPhoto, IInfo {
    ownerId: string;
    ownerName: string;
    authenticates: Privilege[];
    attr: CFields[] = [];
    dependentId: string[];
    someId: string[];
    mediaType: mediaType;
    rotation: number;
    latitude: number;
    longitude: number;
    hasGeoData: boolean;
    photographer: string;
    //these all must use p() for pointing
    compress2Path: string;
    folderPath: string;
    filePath: string;
    compressPath: string;
    mergerPath: string;
    mergerPhotoId: string;
    markerCoordinates: { x: number; y: number; }[];
    //not part of db
    modifyReference: boolean;
    modifyReferenceId: string;
    _id: string;
    type: string;
    area: string;
    location: string;
    campus: string;
    building: string;
    component: string;
    conduit: string;
    comment: string; //adding field to title steps:

    isReference: boolean;
    requestId: string;
    date: Date;
    keyword: string[];
    title: string;
    lastReferencePath: number;
    referencePath: ReferenceModel[];
    fileName: string;
    createdDate : Date; //these all must use p() for pointing
 
    constructor(CFieldNames: string[]){
        CFieldNames.forEach(e=>{
            const initField = new CFields(GUID.create());
            initField.name = e;
            initField.value = null;
            this.attr.push(initField);
        })
    }

    //only updates field value where it is different, needed for batch updates
    static updateCField(currentModel: PhotoModel, newModel:PhotoModel): CFields[]{
        for(let i = 0; i < currentModel.attr.length; i++){
            const currentAttr = currentModel.attr[i];
            const newAttr = newModel.attr[i];
            if(currentAttr.value !== newAttr.value && newAttr.value !== null){
                currentAttr.value = newAttr.value;
            }
        }
        return currentModel.attr;
    }

    static getAnyFieldValueByKey(key: string, model: PhotoModel){
        const resultModel = model[key];
        if(!isNullOrUndefined(resultModel)){
            return resultModel;
        }
        const result = model.attr.find(e=>e.name === key);
        if(!isNullOrUndefined(result)){
            return result.value;
        }
        return null;
    }

    static clone(model: PhotoModel, CFieldNames: string[]):PhotoModel{
        const newModel = new PhotoModel(CFieldNames)
        globals.clonable.forEach(e=>{
            newModel[e] = model[e]
        })
        return newModel
    }
    //creates the proper folder and push to list
    //it does NOT set the lastvisit
    static addReference(model: PhotoModel, referenceId: string):PhotoModel{
        if(!model.referencePath) model.referencePath = []
        const code = GUID.create(true)
        const mark = model.folderPath + '/' + code + '_' +globals.markName + model.fileName
        const clean = model.folderPath + '/' + code + '_' +globals.markCleanName + model.fileName
        const undo = model.folderPath + '/' + code + '_' +globals.markUndoName + model.fileName
        const reference = new ReferenceModel();
        reference.referenceId = referenceId;
        reference.clean = clean;
        reference.mark = mark;
        reference.undo = undo;
        reference.mediaType = mediaType.photo;
        model.referencePath.push(reference)
        return model
    }

    //removes entire reference
    static removeReference(model:PhotoModel, index: number):PhotoModel{
        model.referencePath.splice(index,1)
        return model
    }
    static addTerms(model:PhotoModel, term:string = '', index: number, isRemove = false, isClear = false):PhotoModel{
        //clear all terms
        console.log('isClear ', isClear)
        if(isClear){
            model.referencePath[index].term = []
            console.log('terms cleared ', model.referencePath[index].term)
            return model
        }

        //remove term
        if(isRemove){
            let removeAt;
            for(let i = 0; i < model.referencePath[index].term.length; i++){
                if(model.referencePath[index].term[i] === term){
                    removeAt = i
                }
            }
            if(removeAt){
                model.referencePath[index].term.splice(removeAt, 1)
            }
            return model
        }

        //add term
        if(!_.includes(model.referencePath[index].term, term)) model.referencePath[index].term.push(term)
        return model
    }

    addRemoveDependence(model: PhotoModel, id: string, isDelete = false): PhotoModel{
        if(!model.dependentId) model.dependentId = [];
        if(!isDelete){
            model.dependentId.push(id);
        }else{
            model.dependentId = _.difference(model.dependentId, [id]);
        }
        return model;
    }

    //to remove set Privilege field toRemove to true
    static modifyAuthenticates(model: PhotoModel,authenticate: Privilege): PhotoModel{
        if(!authenticate) throw new Error('authenticate is null')
        if(!model) throw new Error('model is null')
        if(
            authenticate._id === null ||
            authenticate.associatedId === null ||
            authenticate.user === null ||
            authenticate.privilege === null
        ){
            throw new Error('authenticate is invalid, a value is null');
        }
        if(!model.authenticates) model.authenticates = [];
        if(model.authenticates.length > 0){
            if(!authenticate.toRemove){
                let found = false;
                for (let i = 0; i < model.authenticates.length; i++) {
                    if(model.authenticates[i].associatedId === authenticate.associatedId){
                        model.authenticates[i] = authenticate;
                        found = true;
                        i = model.authenticates.length;
                    }
                }
                if(!found){
                    model.authenticates.push(authenticate);
                    return model;
                }else{
                    return model;
                }
            }else{
                const deleted = model.authenticates.some(e=>{
                    let thisAuth = model.authenticates.find(e=>e._id === authenticate._id);
                    if(thisAuth){
                        const indexSlice = model.authenticates.indexOf(thisAuth);
                        model.authenticates.splice(indexSlice, 1);
                        return true;
                    }
                })
                if(!deleted){
                    throw new Error('Expected authenticate to remove does not exist.')
                }else{
                    return model;
                }
            }
        }else{
            if(authenticate.toRemove){
                throw new Error('Expected authenticate to remove on empty list.')
            }
            model.authenticates.push(authenticate);
            return model;
        }
    }
    static getPrivilageByUser(model: PhotoModel, userId: string){
        if(!model || !userId || !model.authenticates) throw new Error('No model or userId provided.');
        const result = model.authenticates.find(e=>e.associatedId == userId);
        if(!result) return 0; //referencing privilagetype will cause error in ng builds, don't know why.
        return result.privilege;
    }
}