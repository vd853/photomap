//parameters for batch uploading, not use for database

interface IUpload{
    photographer: string
    location: string
}
export class UploadModel implements IUpload {
    photographer: string = "";
    location: string = "";
}