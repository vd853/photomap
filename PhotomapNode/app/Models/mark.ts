export enum MarkMode {mark = 0, clear = 1, delete = 2, extract = 3, info = 4, undo = 5, clone = 6, mirror = 7}
//undo is allow by undoAble in the PhotoModel
interface IMark{
    id: string
    x: number
    y: number
    size: number
    text: string
    mode: MarkMode
    number: number
    isBlack: boolean
    canUndo: boolean
}
export class MarkModel implements IMark {
    canUndo: boolean = false;
    id: string;
    x: number = 0;
    y: number = 0;
    size: number = 0;
    text: string = "";
    mode: MarkMode = 0;
    number: number = 0;
    isBlack: boolean = false;
    constructor(id: string){
        this.id = id;
    }
    //these are all params that must be cleared until you set another, the others can be maintained for binding purpose
    reset(){
        this.mode = 0;
        this.number = 0;
        this.canUndo = false;
    }
}