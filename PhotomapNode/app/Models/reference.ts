export interface IReference{
    clean: string;
    mark: string;
    undo: string;
    term: string[];
    mediaType: number;
    referenceId: string; //will be 'gm' if google map
}

export class ReferenceModel implements IReference {
    referenceId: string = "";
    clean: string = "";
    mark: string = "";
    undo: string = "";
    term: string[] = [];
    mediaType: number = 0;

    //this will remove the ReferenceModel key so it will work with the schema
    getModel(){
        return JSON.parse(JSON.stringify(this));
    }
}

export enum mediaType {photo = 0, video = 1}