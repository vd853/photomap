"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//import { VideoModel } from "./m";
const r_1 = require("./r");
const globals_1 = require("./globals");
const fs = require("fs");
const path = require("path");
const async = require("async");
class videoProcessor {
    constructor(filePath, model, controller, callback) {
        this.filePath = filePath;
        this.model = model;
        this.controller = controller;
        this.callback = callback;
        this.accuProgress = 0; //calculated base on raw points
        this.actualProgress = 0; //from 0 to 100
        this.noneConversionPoints = 40; //20 for thumbnail, 20 from preview
        this.conversionPointsQ0 = 0; //these are current conversion points for q0 and q1
        this.conversionPointsQ1 = 0;
        if (!fs.existsSync(filePath)) {
            console.log('Error Video file does not exist');
            this.callback('video file does not exist', null);
            return;
        }
    }
    Start() {
        this.model.fileName = r_1.File.getFileOnlyOrParentFolderName(this.filePath);
        console.log('this.model.fileName ', this.model.fileName);
        this.model.parentPath = '.\\' + path.relative(__dirname, r_1.File.getPathOnly(this.filePath)) + '\\';
        // console.log('folderPath ', folderPath)
        // process.exit();
        //if not win32, assume it is on a linux
        this.encoder = new r_1.Encoder(this.filePath, process.platform === "win32");
        this.encoder.Emitter.on('EncodeFF_Progress', (data) => {
            console.log('progress data ', data);
            if (data.reference === 'q0') {
                this.progressUpdate(undefined, data.progress);
            }
            if (data.reference === 'q1') {
                this.progressUpdate(undefined, undefined, data.progress);
            }
        });
        if (this.model._id === null) {
            this.model._id = r_1.GUID.create();
            console.log('WARNING: you pass in a model without an ID!');
        }
        //this should include full path into the folder where the video datas are stored
        this.model.videoPath = this.model.parentPath + globals_1.globals.QOriginalFolder + '/';
        this.model.videoPathFile = this.model.videoPath + this.model.fileName;
        this.model.videoQ0 = this.model.parentPath + globals_1.globals.Q0Folder;
        this.model.videoQ1 = this.model.parentPath + globals_1.globals.Q1Folder;
        this.model.thumbOriginalPath = this.model.parentPath + globals_1.globals.thumbFolder + '/original';
        this.model.thumbPath = this.model.parentPath + globals_1.globals.thumbFolder + '/compress';
        this.model.thumbPreviewPath = this.model.parentPath + globals_1.globals.thumbFolder + '/' + globals_1.globals.thumbPreviewName;
        this.getSpecs(() => {
            //move file to Original folder
            r_1.File.move(this.filePath, this.model.videoPathFile, (err) => {
                if (err) {
                    console.log('video file move error ', err);
                    this.callback(err, null);
                    return;
                }
                else {
                    this.encoder.AbsoluteFilePath = this.model.videoPathFile;
                    setTimeout(() => {
                        this.asyncTask();
                    }, 1000);
                }
            });
        });
    }
    getSpecs(callback) {
        console.log('getting video specs');
        this.encoder.getVideoSpec((err, d) => {
            this.model.dHorizontal = d.width;
            this.model.dVertical = d.height;
            console.log('got video specs ', this.model.dHorizontal, ' ', this.model.dVertical);
            callback();
        });
    }
    asyncTask() {
        console.log('video model ', this.model);
        const Q0T = (callback) => {
            this.Q0(callback);
        };
        const Q1T = (callback) => {
            this.Q1(callback);
        };
        const ThumbT = (callback) => {
            this.thumbnail(callback);
        };
        const prevT = (callback) => {
            this.thumbPreview(callback);
        };
        //progress points 100 100 20 20 = 240
        async.parallel([Q0T, Q1T, ThumbT, prevT], (error, data) => {
            console.log('async completed ', error, data);
            this.callback(null, this.model); //final callback
        });
    }
    //Q0 will be 720p
    Q0(callback) {
        if (this.model.dHorizontal > 1280) {
            this.encoder.EncodeFF(this.model.videoQ0, '720', 1280, 720, undefined, undefined, undefined, 'q0', (err, data) => {
                this.model.videoQ0File = data;
                callback(null, true);
            });
            this.model.hasQ0 = true;
        }
        else {
            this.model.hasQ0 = false;
            callback(null, true);
            console.log('Skip due to low resolution 720p');
        }
    }
    //Q0 will be 1080p
    Q1(callback) {
        if (this.model.dHorizontal > 1920) {
            this.encoder.EncodeFF(this.model.videoQ1, '1080', 1920, 1080, undefined, undefined, undefined, 'q1', (err, data) => {
                this.model.videoQ1File = data;
                callback(null, true);
            });
            this.model.hasQ1 = true;
        }
        else {
            this.model.hasQ1 = false;
            callback(null, true);
            console.log('Skip due to low resolution 1080p');
        }
    }
    thumbnail(callback) {
        // console.log('thumbnail params ', this.model.thumbOriginalPath, ' ', this.model.dHorizontal, ' ', globals.thumbCount)
        this.encoder.screenshot(this.model.thumbOriginalPath, this.model.dHorizontal, globals_1.globals.thumbCount, () => {
            fs.readdir(this.model.thumbOriginalPath, (err, list) => {
                if (err) {
                    console.log('thumbnail error ', err);
                    callback(err, null);
                }
                let endCounter = 0;
                list.forEach(e => {
                    const i = new r_1.Image(this.model.thumbOriginalPath + '/' + e);
                    i.CompressAndResize(this.model.thumbPath + '/' + e, globals_1.globals.thumbHorizontal, () => {
                        endCounter++;
                        if (endCounter >= list.length) {
                            this.progressUpdate(20);
                            callback(null, true);
                        }
                    });
                });
            });
        }, '%00i.png', 'thumbnail');
    }
    thumbPreview(callback) {
        const tempFolder = globals_1.globals.storage + '/' + r_1.GUID.create();
        this.encoder.FramesToVideo(this.model.thumbPreviewPath, globals_1.globals.panelHorizontal, 25, tempFolder, (err, data) => { this.progressUpdate(20); callback(err, data); });
    }
    progressUpdate(points, q0, q1) {
        //base on the parameter it will set the current conversion progress and acculate the none conversions progress
        this.conversionPointsQ0 = (q0 ? q0 : this.conversionPointsQ0);
        this.conversionPointsQ1 = (q1 ? q1 : this.conversionPointsQ1);
        this.accuProgress += (points ? points : 0);
        let conversionPoints = 0;
        if (this.model.hasQ0.toString() === 'true') {
            conversionPoints = 100;
        }
        if (this.model.hasQ1.toString() === 'true') {
            console.log('I should not see this');
            conversionPoints = 100;
        }
        if (this.model.hasQ0.toString() === 'true' && this.model.hasQ1.toString() === 'true') {
            conversionPoints = 200;
        }
        console.log('conversion points ', conversionPoints);
        //this will caculate the actual progress within 0-100 percent
        this.actualProgress = (this.accuProgress + this.conversionPointsQ0 + this.conversionPointsQ1) / (this.noneConversionPoints + conversionPoints);
        console.log('Progress: ', this.actualProgress);
        this.model.status = 'progress ' + ((this.actualProgress) * 100).toString().substr(0, 5);
        this.controller.factory().AddOrUpdate({ _id: this.model._id }, this.model, (err, data) => {
            if (err) {
                console.log('Progress update error ', err);
            }
        });
    }
}
exports.videoProcessor = videoProcessor;
//# sourceMappingURL=videoProcess.js.map