"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const photo_1 = require("./Models/photo");
const globals_1 = require("./globals");
const r_1 = require("./r");
const r_2 = require("./r");
const _ = require("lodash");
const async = require("async");
const fs = require("fs");
const keywordController_1 = require("./Models/keywordController");
const reference_1 = require("./Models/reference");
//use to store, compress, track in db after uploaded
class processor {
    //will return a this.model in the callback
    constructor(currentLocation, model, server, photo, keyword, callback) {
        this.currentLocation = currentLocation;
        this.model = model;
        this.server = server;
        this.photo = photo;
        this.keyword = keyword;
        this.callback = callback;
        console.log('this file currentLocation ', currentLocation);
        this.model.compressPath = this.model.folderPath + globals_1.globals.compressName + this.model.fileName;
        this.model.compress2Path = this.model.folderPath + globals_1.globals.compressName2 + this.model.fileName;
        this.model.mergerPath = this.model.folderPath + globals_1.globals.mergerName + this.model.fileName;
        this.model.lastReferencePath = 0;
        this.model = photo_1.PhotoModel.addReference(this.model, this.model._id); //this should only have 1 reference now
        this.rotated = this.model.folderPath + 'TEMP_ROTATE_' + r_1.GUID.create() + '.' + r_1.File.getExtension(this.model.fileName);
    }
    start() {
        //if type is video, just store this db
        if (this.model.mediaType === reference_1.mediaType.video) {
            this.model.referencePath[0].mediaType = reference_1.mediaType.video;
            // const remodel = this.model;
            // const callback = this.callback;
            // const dbStore = this.dbStore;
            this.model.date = fs.statSync(this.model.filePath).birthtime;
            this.dbStore(this.callback);
            return;
        }
        //Assume process will be image type
        if (!_.includes(['jpg', 'png'], r_1.File.getExtension(this.model.fileName).toLowerCase())) {
            this.callback('Not photo file', null);
            return;
        }
        else {
            //autoRotate cannot be part of async because it changes the currentlocation path that is use by the others
            this.autoRotate((err, data) => {
                console.log('rotation check complete');
                this.startAsyncs();
            });
        }
    }
    startAsyncs() {
        const t1 = (callback) => {
            this.compress(callback);
        };
        //this is causing the async to finish early????!!
        const t2 = (callback) => {
            this.geotag(callback);
        };
        const t3 = (callback) => {
            this.compress2(callback);
        };
        //gets the creation date
        const t4 = (callback) => {
            r_2.Image.creationTime(this.currentLocation, (error, data) => {
                if (error) {
                    callback(error, null);
                    return;
                }
                if (data) {
                    this.model.date = data;
                    console.log('process creationtime');
                    callback(null, true);
                }
            });
        };
        const tasks = [t1, t2, t3, t4];
        async.parallel(tasks, (err, data) => {
            console.log('photo Process all completed ', err, data);
            //create google map if autogeneratemaps is enabled
            console.log('auto map ', globals_1.globals.currentServerSettings.autoGenerateMaps, ' has geo ', this.model.hasGeoData);
            //stores model in db and process keyword
            if (globals_1.globals.currentServerSettings.autoGenerateMaps && this.model.hasGeoData) {
                processor.createGoogleMaps(this.model, (err, result) => {
                    console.log('dbStore for geodata or gm');
                    this.dbStore((err, data) => {
                        if (err) {
                            this.callback(err, null);
                            return;
                        }
                        this.keywordStore((err, data) => {
                            this.callback(err, this.model);
                        });
                        this.postProcess();
                    });
                });
            }
            else {
                console.log('dbStore for none geodata or none gm');
                this.dbStore((err, data) => {
                    if (err) {
                        this.callback(err, null);
                        return;
                    }
                    this.keywordStore((err, data) => {
                        this.callback(err, this.model);
                    });
                    this.postProcess();
                });
            }
        });
    }
    //this can only be use after db entry is stored
    postProcess() {
        this.removeTempFiles();
        //mark extract wil NOT have a requestId, so use SYSTEM instead
        this.server.log(this.model.requestId ? this.model.requestId : 'SYSTEM', this.model.fileName + ' processed');
    }
    geotag(callback) {
        const i = new r_2.Image(this.currentLocation);
        i.GeoLocation((err, data) => {
            if (err) {
                console.log('geo error ', err);
                this.model.hasGeoData = false;
                callback(err, null);
                return;
            }
            if (data) {
                console.log('has geo');
                this.model.latitude = data.latitude;
                this.model.longitude = data.longitude;
                this.model.hasGeoData = true;
                callback(null, true);
            }
            else {
                console.log('no geo');
                this.model.hasGeoData = false;
                callback(null, true);
            }
        });
    }
    compress(callback) {
        //some images that don't need rotation will not be created
        // console.log('file exist ', fs.existsSync(this.rotated), ' file: ', this.rotated, ' usage ', fs.existsSync(this.rotated)? this.rotated: this.currentLocation)
        // process.exit();
        const i = new r_2.Image(fs.existsSync(this.rotated) ? this.rotated : this.currentLocation);
        i.CompressAndResize(this.model.compressPath, globals_1.globals.maxWidth, (err, result) => {
            if (err) {
                this.callback(err, null);
                return;
            }
            console.log('resize completed');
            //copy the compress path to reference and clean
            r_1.File.CopyFile(this.model.compressPath, this.model.referencePath[0].mark, (err, result) => {
                if (err) {
                    this.callback(err, null);
                    return;
                }
                console.log('markpath file copied');
                r_1.File.CopyFile(this.model.compressPath, this.model.referencePath[0].clean, (err, result) => {
                    if (err) {
                        this.callback(err, null);
                        return;
                    }
                    console.log('markcleanpath file copied');
                    callback(null, result);
                });
            });
        });
    }
    autoRotate(callback) {
        r_2.Image.autoRotate(this.currentLocation, this.rotated, (err, data) => {
            if (err) {
                this.callback(err, null);
                return;
            }
            callback(null, data);
        });
    }
    compress2(callback) {
        const i = new r_2.Image(this.rotated);
        i.CompressAndResize(this.model.compress2Path, globals_1.globals.maxWidth2, (err, result) => {
            if (err) {
                this.callback(err, null);
                return;
            }
            console.log('compress2 completed');
            callback(null, result);
        });
    }
    //next three method are for finalizing after async.parallel, they should not run if anything else fails
    dbStore(callback) {
        this.photo.factory().AddOrUpdate({ _id: this.model._id }, this.model, callback);
    }
    keywordStore(callback) {
        keywordController_1.KeywordController.processKeyword(this.model, true, this.keyword, this.photo, callback);
    }
    removeTempFiles() {
        this.server.del(this.rotated, () => { });
    }
    //creates a google map for markpath and markcleanpath
    static createGoogleMaps(model, callback) {
        // console.log('Creating google map for ', model.latitude, model.longitude)
        const gm = new r_1.GoogleMaps(globals_1.globals.apiKey); //a google api key may not be required?
        model = photo_1.PhotoModel.addReference(model, 'gm');
        const lastIndex = model.referencePath.length - 1; //increment
        model.lastReferencePath = lastIndex; //set google map as the lastreference visited
        console.log('google map last reference path ', lastIndex);
        gm.mapImage2(model.latitude, model.longitude, model.referencePath[lastIndex].mark, (error, data) => {
            if (error) {
                callback('error exist in mapImage2 ' + error, null);
                return;
            }
            //creates a copy in the clean version
            r_1.File.CopyFile(model.referencePath[lastIndex].mark, model.referencePath[lastIndex].clean, (error, data) => {
                if (error) {
                    console.log('copy file error ', error);
                    callback(error, null);
                    return;
                }
                callback(null, model);
            });
        });
    }
}
exports.processor = processor;
//# sourceMappingURL=process.js.map