import { ServerModel } from "./Models/server";
import { Logger } from "./r";

export class globals{
    static isProduction = false;
    static bypassNetworkCheck = true;
    static scrambleKey = 'photomap';
    static storage = './Static/Storage'
    static SSL = './Keys'
    static machineUser = {user: 'admin', password: 'admin'}
    static requestName = '/Request' //stores request zip and temp files
    static mergerName = 'merge_'
    static compressName = 'compress_'
    static compressName2 = 'compress2_'
    static markName = 'mark_'
    static markCleanName = 'markClean_'
    static markUndoName = 'markUndo_'
    static cacheName = 'cache_' //this is an image64 string of the original image or its rotated version
    static maxWidth = 640
    static maxWidth2 = 160 //smaller compression
    static apiKey = '' //use in processor.createGoogleMaps()
    static requestExpirationDays = 3;
    static maxLogs = 500 //use even numbers, half the logs will be deleted when this number is reached. ex. 30 maxLogs will purge to 15 when logs count exceeds 30
    static log: Logger;

    static QOriginalFolder = 'original'
    static Q0Folder = 'q0'
    static Q1Folder = 'q1'
    static thumbFolder = 'thumbnail'
    static thumbPreviewName = 'thumbnailVideo.mp4'
    static thumbCount = 10;
    static thumbHorizontal = 100;
    static panelHorizontal = 100;
    static encodingPollTime = 5; //seconds
    static expireVideoDatabase = 160 //seconds
    static concurrentEncodingLimit = 5;
    //Will return success if modify limit is below this number, otherwise will send 'queued' notification
    //This is set to 0, so any batch modify will be queued, prevents page for automatically reloading
    static modifyLimits = 1

    static currentServerSettings: ServerModel
    static stampColorThreshold = 0.4; //1 is the brightest, anything below this number will be colored black

    //sortables also use for info stamp
    static sortables = ['location','title','photographer', 'building','area', 'campus', 'component', 'comment', 'conduit', 'date', 'fileName', 'thumbnail', 'isReference', 'hasGeoData', 'createdDate']
    static searchables = ['location','title','photographer', 'building','area', 'campus', 'component', 'comment', 'conduit', 'date', 'fileName']
    
    //use for packaging the keyword and filter, should also contain 'all' when packaging
    static filterable = ['location','title','photographer', 'building','area', 'campus', 'component', 'comment', 'conduit', 'fileName']
    static keywordable = ['all','title','location','photographer', 'building','area', 'campus', 'component', 'conduit', 'fileName'] 
    static clonable = ['location','photographer','title', 'building', 'campus','area', 'component', 'comment', 'conduit', 'date']
    static formablesText = ['location','photographer', 'building', 'campus', 'area','component', 'comment', 'conduit', 'title'] //use in photform to blank out the undefined
    static viewables = ['title','photographer','comment','location', 'area','component','building', 'campus', 'conduit', 'fileName', 'isReference', 'hasGeoData','date', 'createdDate'] //for view as left side of stamp

    static userRetrievable = ['user', 'email', 'level', 'info']
    static managementRetrievable = ['user', 'email', 'lockoutTime', 'level', 'info', 'authenticated']

    static sectionViewables = ['comment', 'startPercent', 'endPercent']

    static shorten(text: string, max = 20){
        if(text.length > max){
          return text.substr(0, max) + '...'
        }
        return text;
    }

    
    static staticMap(filePath: string){
        return filePath.replace('./Static', '')
    }
}