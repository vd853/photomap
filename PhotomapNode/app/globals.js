"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class globals {
    static shorten(text, max = 20) {
        if (text.length > max) {
            return text.substr(0, max) + '...';
        }
        return text;
    }
    static staticMap(filePath) {
        return filePath.replace('./Static', '');
    }
}
globals.isProduction = false;
globals.bypassNetworkCheck = true;
globals.scrambleKey = 'photomap';
globals.storage = './Static/Storage';
globals.SSL = './Keys';
globals.machineUser = { user: 'admin', password: 'admin' };
globals.requestName = '/Request'; //stores request zip and temp files
globals.mergerName = 'merge_';
globals.compressName = 'compress_';
globals.compressName2 = 'compress2_';
globals.markName = 'mark_';
globals.markCleanName = 'markClean_';
globals.markUndoName = 'markUndo_';
globals.cacheName = 'cache_'; //this is an image64 string of the original image or its rotated version
globals.maxWidth = 640;
globals.maxWidth2 = 160; //smaller compression
globals.apiKey = ''; //use in processor.createGoogleMaps()
globals.requestExpirationDays = 3;
globals.maxLogs = 500; //use even numbers, half the logs will be deleted when this number is reached. ex. 30 maxLogs will purge to 15 when logs count exceeds 30
globals.QOriginalFolder = 'original';
globals.Q0Folder = 'q0';
globals.Q1Folder = 'q1';
globals.thumbFolder = 'thumbnail';
globals.thumbPreviewName = 'thumbnailVideo.mp4';
globals.thumbCount = 10;
globals.thumbHorizontal = 100;
globals.panelHorizontal = 100;
globals.encodingPollTime = 5; //seconds
globals.expireVideoDatabase = 160; //seconds
globals.concurrentEncodingLimit = 5;
//Will return success if modify limit is below this number, otherwise will send 'queued' notification
//This is set to 0, so any batch modify will be queued, prevents page for automatically reloading
globals.modifyLimits = 1;
globals.stampColorThreshold = 0.4; //1 is the brightest, anything below this number will be colored black
//sortables also use for info stamp
globals.sortables = ['location', 'title', 'photographer', 'building', 'area', 'campus', 'component', 'comment', 'conduit', 'date', 'fileName', 'thumbnail', 'isReference', 'hasGeoData', 'createdDate'];
globals.searchables = ['location', 'title', 'photographer', 'building', 'area', 'campus', 'component', 'comment', 'conduit', 'date', 'fileName'];
//use for packaging the keyword and filter, should also contain 'all' when packaging
globals.filterable = ['location', 'title', 'photographer', 'building', 'area', 'campus', 'component', 'comment', 'conduit', 'fileName'];
globals.keywordable = ['all', 'title', 'location', 'photographer', 'building', 'area', 'campus', 'component', 'conduit', 'fileName'];
globals.clonable = ['location', 'photographer', 'title', 'building', 'campus', 'area', 'component', 'comment', 'conduit', 'date'];
globals.formablesText = ['location', 'photographer', 'building', 'campus', 'area', 'component', 'comment', 'conduit', 'title']; //use in photform to blank out the undefined
globals.viewables = ['title', 'photographer', 'comment', 'location', 'area', 'component', 'building', 'campus', 'conduit', 'fileName', 'isReference', 'hasGeoData', 'date', 'createdDate']; //for view as left side of stamp
globals.userRetrievable = ['user', 'email', 'level', 'info'];
globals.managementRetrievable = ['user', 'email', 'lockoutTime', 'level', 'info', 'authenticated'];
globals.sectionViewables = ['comment', 'startPercent', 'endPercent'];
exports.globals = globals;
//# sourceMappingURL=globals.js.map