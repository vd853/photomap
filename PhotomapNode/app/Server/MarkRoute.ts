import { Routers, Ctrl } from "./server";
import { ExpressRoutes, Image, File, GUID } from "../r";
import { globals } from "../globals";
import { PhotoModel } from "../Models/photo";
import { MarkModel, MarkMode } from "../Models/mark";
import { processor } from "../process";
import { UploadRoute } from "./UploadRoute";
import * as async from 'async';
import * as moment from 'moment'

export class MarkRoute implements Routers {
    controllers: Ctrl;
    init(expressApp: any, controllers: Ctrl) {
        this.controllers = controllers;
        const r = new ExpressRoutes(expressApp, 'mark', ['id'])
        r.GET = (req, res)=>{
            console.log('parma id ', req.params.id)
            const markAny = JSON.parse(req.params.id)
            const mark = <MarkModel>markAny
            console.log('mark ', mark)

            //PhotoModel will be updated at setImage() from stamp-switch component
            this.controllers.photo.factory().FindOne({_id: mark.id},(err, model:PhotoModel)=>{
                if(err){
                    res.send({error: err})
                    return
                }
                if(!model){
                    res.send({error: 'not exist'})
                    return
                }

                //extract a reference from a photo and converts it to its own photo as a reference type
                if(mark.mode === MarkMode.extract){
                    this.extract(res, model, mark, controllers)
                    return;
                }

                if(mark.mode === MarkMode.mirror){
                    model = PhotoModel.addReference(model, model._id)
                    model.lastReferencePath = model.referencePath.length-1;
                    File.CopyFile(model.compressPath, model.referencePath[model.referencePath.length-1].mark, (err, result)=>{
                        File.CopyFile(model.compressPath, model.referencePath[model.referencePath.length-1].clean, (err, result)=>{
                            this.returnResult(res, mark, model);
                        })
                    })
                }

                // console.log('mark clean path ', model.referencePath[mark.number].clean, ' at ', mark.number)
                //clears the marked image and skips marking
                if(mark.mode === MarkMode.clear){
                    MarkRoute.clear(mark, model, (err, result)=>{
                        if(err){
                            console.log('ERROR mark clear')
                            res.send({error: err})
                            return;
                        }
                        this.returnResult(res, mark, model)
                    })
                    return
                }
                //deletes the entire reference
                if(mark.mode === MarkMode.delete){
                    this.delete(res, mark, model)
                    return;
                }

                //deletes the entire reference
                if(mark.mode === MarkMode.clone){
                    this.clone(res, mark, model)
                    return;
                }

                //marks the image and returns it as image64
                if(mark.mode === MarkMode.mark){
                    const stampNow = ()=>{ //use in conditions
                        File.CopyFile(model.referencePath[mark.number].mark, model.referencePath[mark.number].undo, (err, result)=>{
                            if(err) throw err;
                            console.log('undo copied')
                            MarkRoute.stamp(mark, markAny, model, (err, result)=>{
                                this.returnResult(res, result!.mark, result!.model)
                            })
                        })
                    }

                    //will be true if stamp color is manual
                    if(globals.currentServerSettings.settableStampColor){
                        stampNow();
                    }else{
                        MarkRoute.pixelOffsets([mark.text], mark.size, (err, offsets)=>{
                            const origin = {x: mark.x, y: mark.y}
                            const end = {x: mark.x + offsets.pixelOffsetRight, y: mark.y + offsets.pixelOffsetDown}
                            Image.shadePortion(model.referencePath[mark.number].mark, origin, end, (err, shade)=>{
                                if(shade !== null){
                                    mark.isBlack = shade > globals.stampColorThreshold? true:false
                                }
                                mark.isBlack = false;
                                console.log('shade: ', shade)
                                stampNow();
                            })
                        })
                    }
                    return
                }
                if(mark.mode === MarkMode.undo){
                    File.CopyFile(model.referencePath[mark.number].undo, model.referencePath[mark.number].mark, (err, result0)=>{
                        if(err) throw err;
                        this.returnResult(res, mark, model)
                    })
                    return
                }
                if(mark.mode === MarkMode.info){
                    MarkRoute.infoStamp(model, mark, markAny, (err, result)=>{
                        if(err) throw err;
                        this.returnResult(res, result.mark, result.model)
                    })
                    // //gets all the info from PhotoModel as a string[] for the stamp
                    // let infoText = this.getStampInfo(model)
                    
                    // const stampNow = ()=>{ //use in conditions

                    //     //copy file to create the undo version
                    //     File.CopyFile(model.referencePath[mark.number].mark, model.referencePath[mark.number].undo, (err, result0:{result: any, streams: any[]})=>{
                    //         this.stamp(mark, markAny, model, (err, result)=>{
                    //             this.returnResult(res, result.mark, result.model)
                    //         }, infoText)
                    //     })
                    // }

                    // //will be true if stamp color is manual
                    // if(globals.currentServerSettings.settableStampColor){
                    //     stampNow();
                    // }else{
                    //     this.pixelOffsets(infoText, mark.size, (err, offsets)=>{
                    //         const origin = {x: mark.x, y: mark.y}
                    //         const end = {x: mark.x + offsets.pixelOffsetRight, y: mark.y + offsets.pixelOffsetDown}
                    //         Image.shadePortion(model.referencePath[mark.number].mark, origin, end, (err, shade)=>{
                    //             mark.isBlack = shade > globals.stampColorThreshold? true:false
                    //             console.log('shade: ', shade)
                    //             stampNow();
                    //         })
                    //     })
                    // }
                    return
                }
            })
        }
    }

    static infoStamp(photo: PhotoModel, mark: MarkModel, markAny: any, callback:(err, result)=>void){
        //gets all the info from PhotoModel as a string[] for the stamp
        let infoText = this.getStampInfo(photo)
                    
        const stampNow = ()=>{ //use in conditions
            //copy file to create the undo version
            File.CopyFile(photo.referencePath[mark.number].mark, photo.referencePath[mark.number].undo, (err, result0:{result: any, streams: any[]})=>{
                if(err){
                    callback(err, null)
                    return;
                }
                this.stamp(mark, markAny, photo, (err, result)=>{
                    callback(err, {mark: mark, model: photo});
                }, infoText)
            })
        }

        //will be true if stamp color is manual
        if(globals.currentServerSettings.settableStampColor){
            stampNow();
        }else{
            this.pixelOffsets(infoText, mark.size, (err, offsets)=>{
                if(err){
                    callback(err, null)
                    return;
                }
                const origin = {x: mark.x, y: mark.y}
                const end = {x: mark.x + offsets.pixelOffsetRight, y: mark.y + offsets.pixelOffsetDown}
                Image.shadePortion(photo.referencePath[mark.number].mark, origin, end, (err, shade)=>{
                    if(err){
                        callback(err, null)
                        return;
                    }
                    if(shade !== null){
                        mark.isBlack = shade > globals.stampColorThreshold? true:false
                    }
                    mark.isBlack = false;
                    console.log('shade: ', shade)
                    stampNow();
                })
            })
        }
    }

    static pixelOffsets(text:string[], fontSize: number, callback:(err, offets: {pixelOffsetRight: number, pixelOffsetDown: number, pixelOffset: number})=>void){
        let maxHorizontal = 0;
        const maxVertical = text.length
        text.forEach(e=>{
            if(e.length > maxHorizontal) maxHorizontal = e.length;
        })
        const pixelOffset = Math.pow(2, fontSize+3) //max is 64
        const pixelOffsetRight = maxHorizontal*pixelOffset
        const pixelOffsetDown = maxVertical*pixelOffset
        console.log('pixelRIgth ', pixelOffsetRight, ' pixel down ', pixelOffsetDown, ' pixeloffset ', pixelOffset)
        callback(null, {pixelOffsetRight: pixelOffsetRight, pixelOffsetDown: pixelOffsetDown, pixelOffset: pixelOffset})
    }
    static stamp(mark: MarkModel, markAny: any, model: PhotoModel, callback:(err, result:{mark: MarkModel, model: PhotoModel} | null)=>void, texts?: string[]){
        const offset = 5
        Image.textImage(
            texts? texts: [mark.text], //if texts, it wills stamp down, otherwise just stamp the mark.text
            model.referencePath[mark.number].mark, 
            model.referencePath[mark.number].mark, 
            parseInt(markAny.size), //use this since mark will return a string
            parseInt(markAny.x)-offset,
            parseInt(markAny.y)-offset,
            mark.isBlack,
            (err, data)=>{
            if(err){
                callback(err, null)
                console.log('marked ERROR ', err)
                return
            }
            console.log('marked')
            callback(null, {mark: mark, model: model})
            //this.returnResult(res, mark, model)
        })
    }
    static getStampInfo(model: PhotoModel):string[]{
        const infoTexts:any = []
        globals.sortables.forEach(e=>{
            if((typeof model[e] !== 'boolean' && !model[e]) || model[e] === 'undefined'){
            }else{
                console.log('looking ', model[e])
                if(moment(model[e]).isValid()){
                    infoTexts.push(e.toUpperCase() + ': ' + moment(model[e]).calendar(undefined, {sameElse: 'MM/D/YYYY'}))
                }else{
                    infoTexts.push(e.toUpperCase() + ': ' + globals.shorten(model[e], 30))
                }
            }
        })
        return infoTexts;
    }
    extract(
        res: any, 
        model: PhotoModel, 
        mark: MarkModel,
        controllers: Ctrl
    ){
        const newModel = PhotoModel.clone(model, globals.currentServerSettings.fieldDefinitions)
        newModel.isReference = true;
        newModel._id = GUID.create()
        const shortGUID = GUID.create(true)
        //need this short GUID to be a unique file when doing batch downloads
        const fileName = File.getFileNameOnly(model.fileName) + '_' + shortGUID +'_EXTRACT_' + (mark.number+1) + '.' + File.getExtension(model.fileName)
        const tempFile = globals.storage + globals.requestName + shortGUID; //will be moved and rename during create

        //copy the referencePath to tempFile so orginal won't get moved
        File.CopyFile(model.referencePath[mark.number].mark, tempFile, (err, result)=>{
            if(err){
                console.log('ERROR in extract temp file copy ', err)
                res.send({error: 'ERROR in extract temp file copy ', err})
                return;
            }
            UploadRoute.create(newModel, tempFile, fileName, controllers, (err, result)=>{
                if(err){
                    controllers.server.log('SYSTEM', 'ERROR in extract '+err)
                    return; //create will res.send if there is an error
                }
            })
            res.send({result: fileName})
        })
    }

    //returns image64 of the mark image, use by both modes
    //depends on the result:PhotoModel and mark:MarkModel out here
    returnResult(res: any, mark: MarkModel, model:PhotoModel){
        Image.getImage64(model.referencePath[mark.number].mark, (err, data0)=>{
            if(err){
                res.send({error: err})
                return
            }

            //update the model terms
            if(mark.mode === MarkMode.clear){
                model = PhotoModel.addTerms(model, undefined, mark.number, undefined, true) //last params means to clear this mark.number
            }else{
                if(mark.text.length > 2){
                    model = PhotoModel.addTerms(model, mark.text, mark.number, false, false)
                }
            }

            //save the updated model
            this.controllers.photo.factory().AddOrUpdate({_id: model._id}, model, (err, data1)=>{
                if(err){
                    res.send({error: err})
                    return
                } 
                //return the model and image64
                res.send({model: data1, image64: data0})
            })
        })
    }

    clone(res: any, mark: MarkModel, model: PhotoModel){
        const fromThisIndex = model.lastReferencePath;
        const toThisIndex = model.referencePath.length-1;
        model = PhotoModel.addReference(model, model.referencePath[fromThisIndex].referenceId)
        File.CopyFile(model.referencePath[fromThisIndex].mark, model.referencePath[toThisIndex].mark, ()=>{
            File.CopyFile(model.referencePath[fromThisIndex].mark, model.referencePath[toThisIndex].clean, ()=>{
                model.lastReferencePath = model.referencePath.length-1; //set new last path to the clone
                this.returnResult(res, mark, model)
            })
        })
    }

    static clear(mark: MarkModel, model:PhotoModel, callback:(err, result)=>void){
        File.CopyFile(model.referencePath[mark.number].clean, model.referencePath[mark.number].mark, (err, result)=>{
            callback(err, result);
        })
    }

    delete(res: any, mark: MarkModel, model:PhotoModel){
        this.controllers.server.del(model.referencePath[mark.number].clean, (err)=>{
            if(err){
                res.send({result: err})
                return
            }
            console.log('clean deleted')
            this.controllers.server.del(model.referencePath[mark.number].mark, (err)=>{
                if(err){
                    res.send({result: err})
                    return
                }
                console.log('mark deleted')
                model = PhotoModel.removeReference(model, mark.number)
                console.log('reference removed')
                mark.number = model.lastReferencePath = model.referencePath.length-1 //defaults to last one back
                console.log('new default ', model.lastReferencePath)
                this.returnResult(res, mark, model) //returns the model and image64 of the last one back for update
            })
        })
    }
}