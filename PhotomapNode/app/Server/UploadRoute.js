"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const globals_1 = require("../globals");
const photo_1 = require("../Models/photo");
const fs = require("fs");
const _ = require("lodash");
const formidable = require("formidable");
const process_1 = require("../process");
const reference_1 = require("../Models/reference");
class UploadRoute {
    init(expressApp, controllers) {
        this.controllers = controllers;
        const r = new r_1.ExpressRoutes(expressApp, 'upload');
        const videoController = controllers.video;
        const create = UploadRoute.create;
        const isVideoFormat = UploadRoute.isVideoFormat;
        const createVideo = UploadRoute.createVideo;
        const isSSLFormat = UploadRoute.isSSLFormat;
        r.POST = (req, res) => {
            const form = new formidable.IncomingForm();
            form.maxFileSize = 3000000000;
            const controllers = this.controllers;
            //this is called anytime 1 file have been uploaded
            form.parse(req, function (err, fields, files) {
                return __awaiter(this, void 0, void 0, function* () {
                    if (err) {
                        console.log('formidable error ', err);
                        res.send({ error: err });
                        return;
                    }
                    const downloadPath = files.file.path;
                    const fileName = files.file.name;
                    let model = JSON.parse(fields.photoModel);
                    const accountModel = JSON.parse(fields.accountModel);
                    const isAuthorized = yield controllers.account.AuthorizationByModel(accountModel);
                    if (!isAuthorized.isFound || !isAuthorized.validate) {
                        res.send({ error: 'User not authorized or not found during upload verfication.' });
                        return;
                    }
                    //#region owner and privilege assign
                    model.ownerId = accountModel._id;
                    model.ownerName = accountModel.user;
                    model = photo_1.PhotoModel.modifyAuthenticates(model, new r_1.Privilege(r_1.PrivilegeType.owner, accountModel));
                    //#endregion
                    if (isSSLFormat(fileName)) {
                        UploadRoute.processSSL(fileName, downloadPath, fields.pass, (err, result) => {
                            if (err) {
                                res.send({ error: err, fileName: fileName });
                                return;
                            }
                            res.send({ result: result, fileName: fileName });
                        });
                        return;
                    }
                    //either video or photo
                    if (isVideoFormat(fileName)) {
                        console.log('upload is a video ');
                        model.isReference = false; //video cannot be a reference
                        UploadRoute.createVideo(model, downloadPath, fileName, controllers, (err, result) => {
                            if (err) {
                                controllers.server.log('SYSTEM', 'Error in video process ' + fileName + ': ' + err);
                            }
                            if (result !== null) {
                                let k = result;
                                result.vModel.status = 'uploaded';
                                controllers.serverMain.queue.add(result.vModel, result.pModel.filePath);
                            }
                        });
                    }
                    else {
                        const remodel = _.cloneDeep(model);
                        remodel.mediaType = reference_1.mediaType.photo;
                        UploadRoute.create(remodel, downloadPath, fileName, controllers, (err, result) => {
                            if (err) {
                                controllers.server.log('SYSTEM', 'Error in photo process ' + fileName + ': ' + err);
                            }
                        });
                    }
                    res.send({ result: 'File uploaded, processings have begun' });
                });
            });
        };
    }
    static processSSL(fileName, currentLocation, pass, callback) {
        let isKey;
        if (fileName === 'cert.pem') {
            isKey = false;
        }
        else if (fileName === 'key.pem') {
            isKey = true;
        }
        else {
            callback('invalid name ' + fileName, null);
            return;
        }
        if (isKey) {
            if (pass === '') {
                callback('passphrase incorrect', null);
                return;
            }
        }
        r_1.SSL.validatePEM('./Keys/bin', currentLocation, pass, isKey, (err, result) => {
            if (err) {
                callback('passphrase incorrect', null);
                return;
            }
            r_1.File.move2(currentLocation, './Keys/temp/' + fileName, err => {
                if (err) {
                    callback('file move error ' + err, null);
                    return;
                }
                callback(null, 'validated');
            });
        });
    }
    static isVideoFormat(fileName) {
        if (_.includes(['mp4', 'avi'], r_1.File.getExtension(fileName).toLowerCase())) {
            return true;
        }
        else {
            return false;
        }
    }
    static isSSLFormat(fileName) {
        if (_.includes(['pem'], r_1.File.getExtension(fileName).toLowerCase())) {
            return true;
        }
        else {
            return false;
        }
    }
    static createVideo(model, oldPath, fileName, controllers, callback) {
        const remodel = _.cloneDeep(model);
        remodel.mediaType = reference_1.mediaType.video;
        const vm = controllers.video.Model();
        remodel.someId = [vm._id]; //link photo to video as reference
        new Promise((resolve, reject) => {
            this.create(remodel, oldPath, fileName, controllers, (err, data) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(data);
            });
        })
            .catch(e => {
            callback(e, null);
        })
            .then(e => {
            console.log('model created for video ', vm);
            const pModel = e;
            return new Promise((resolve, reject) => {
                controllers.video.factory().AddOrUpdate({ _id: vm._id }, vm, (err, vModel) => {
                    if (err) {
                        reject(err);
                        return;
                    }
                    resolve({ pModel: pModel, vModel: vModel });
                });
            });
        })
            .catch(e => {
            // console.log('e2 ', e)
            callback(e, null);
        })
            .then((e) => {
            // console.log(e)
            callback(null, e);
        });
    }
    static create(model, oldPath, fileName, controllers, callback) {
        r_1.File.createFolder(globals_1.globals.storage); //creates folder to store if it doesn't exist
        if (!oldPath) {
            console.log('connection terminated');
            callback('connection terminated', null);
            return;
        }
        else {
            if (!fs.existsSync(oldPath)) {
                console.log('connection terminated');
                callback('connection terminated', null);
                return;
            }
        }
        const oldpath = oldPath; //old path has files, but no extension
        //model.id, fileName, folder parent path, and filepath is define outside the process. Also creating the folder
        if (!model._id)
            model._id = r_1.GUID.create();
        r_1.File.createFolder(globals_1.globals.storage + '/' + model._id);
        model.fileName = r_1.File.getFileOnlyOrParentFolderName(fileName); //name of original file
        model.folderPath = globals_1.globals.storage + '/' + model._id + '/';
        if (model.mediaType === reference_1.mediaType.video) {
            const videoAppend = model.someId[0] + '/';
            r_1.File.createFolder(model.folderPath + videoAppend);
            model.filePath = model.folderPath + videoAppend + model.fileName;
        }
        else {
            model.filePath = model.folderPath + model.fileName;
        }
        r_1.File.move2(oldpath, model.filePath, function (err) {
            if (err) {
                console.log('rename error from upload ', err);
                callback('create move ERROR' + err, null);
                return;
            }
            ;
            const p = new process_1.processor(model.filePath, model, controllers.server, controllers.photo, controllers.keyword, callback);
            p.start();
        });
    }
}
exports.UploadRoute = UploadRoute;
//# sourceMappingURL=UploadRoute.js.map