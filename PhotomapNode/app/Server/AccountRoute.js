"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const globals_1 = require("../globals");
const AccountModel_1 = require("../../../../uNode/m/AccountModel");
class AccountRoute {
    init(expressApp, controllers) {
        this.controllers = controllers;
        this.expressRoute = new r_1.ExpressRoutes(expressApp, 'account');
        this.GET();
        this.POST();
    }
    GET() {
        this.expressRoute.GET = (req, res) => __awaiter(this, void 0, void 0, function* () {
            if (req.params.id === 'users') {
                const results = yield this.controllers.account.factory().FindAllReturnFieldsAsync({}, ['user', '_id']);
                res.send({ results: results.filter(e => e.user !== 'admin') });
                return;
            }
        });
    }
    POST() {
        //key: 6LfGLl0UAAAAAI-O-OBS5irWnvZdbW79nVn9w_Bw
        this.expressRoute.POST = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const aModel = req.body; //aModel must be unscramble first
            let decrypted = null;
            //login out should never use decrypt
            if (aModel.status === r_1.AccountStatus.loggingOut) {
                this.statusChange(req, res, aModel, decrypted);
                return;
            }
            else {
                decrypted = this.unscramble(aModel);
            }
            if (decrypted.model.request && decrypted.model.request !== AccountModel_1.AccountRequest.none) {
                console.log('request mode');
                this.request(req, res, aModel, decrypted);
            }
            else {
                this.statusChange(req, res, aModel, decrypted);
            }
        });
    }
    //reference the model request only
    request(req, res, aModel, decrypted) {
        return __awaiter(this, void 0, void 0, function* () {
            let isAuthorized = null;
            let isManagement = false;
            if (decrypted.model.sessionIDManagement) {
                isAuthorized = yield this.controllers.account.isAdminAuthorized(decrypted.model.sessionIDManagement);
                isManagement = true;
            }
            else {
                isAuthorized = yield this.controllers.account.Authorization(decrypted.model.sessionID);
            }
            let response = r_1.AccountModel.responseFactory();
            if (!isAuthorized.isFound) {
                response.reason = decrypted.model.user + ' was not found for authorization.';
                res.send(response);
                return;
            }
            if (!isAuthorized.validate) {
                response.reason = decrypted.model.user + ' is not authorize.';
                res.send(response);
                return;
            }
            let model;
            if (isManagement) {
                model = yield this.controllers.account.factory().FindOneAsync({ _id: decrypted.model._id });
            }
            else {
                model = yield this.controllers.account.factory().FindOneAsync({ sessionID: decrypted.model.sessionID });
            }
            if (!model && !isManagement) {
                response.reason = decrypted.model.user + ' session is expired.';
                res.send(response);
                return;
            }
            //#region authorized actions
            switch (decrypted.model.request) {
                case AccountModel_1.AccountRequest.getUsers:
                    if (!isManagement)
                        throw new Error('only management is allow here');
                    if (isAuthorized.validate) {
                        response.package = yield this.controllers.account.factory().FindAllReturnFieldsAsync({}, globals_1.globals.managementRetrievable);
                        response.reason = "User data retrieved.";
                        response.verified = true;
                    }
                    else {
                        response.reason = "You need to be root or admin.";
                    }
                    res.send(response);
                    break;
                case AccountModel_1.AccountRequest.updateSelf:
                    yield this.controllers.account.factory().AddOrUpdateAsync({ _id: model._id }, decrypted.model);
                    response.verified = true;
                    response.reason = model.user + ' was updated.';
                    response.model = yield this.controllers.account.factory().FindOneAsync({ _id: model._id });
                    res.send(response);
                    break;
                case AccountModel_1.AccountRequest.passwordChange:
                    const changeAuthorizedAction = () => __awaiter(this, void 0, void 0, function* () {
                        let isSame = true;
                        if (model.password !== decrypted.model.passwordNew) {
                            isSame = false;
                            model.password = r_1.AccountModel.genPassword(decrypted.model.passwordNew);
                            yield this.controllers.account.factory().AddOrUpdateAsync({ _id: model._id }, model);
                        }
                        response.verified = true;
                        response.reason = (model.user + ' password') + (isSame ? ' is the same.' : ' was changed.');
                    });
                    if (isManagement) {
                        yield changeAuthorizedAction();
                    }
                    else {
                        const pwCheck = yield this.controllers.account.PasswordValid(decrypted.model);
                        if (!pwCheck.isFound) {
                            throw new Error('Password change cannot find expected user');
                        }
                        if (!pwCheck.validate) {
                            response.reason = model.user + ' password is invalid.';
                        }
                        else {
                            yield changeAuthorizedAction();
                        }
                    }
                    res.send(response);
                    break;
                case AccountModel_1.AccountRequest.delete:
                    if (!isManagement)
                        model.password = decrypted.model.password; //since db pw will be the hash version
                    response = yield this.delete(model, isManagement, req);
                    res.send(response);
                    break;
                case AccountModel_1.AccountRequest.create:
                    if (!isManagement)
                        throw new Error('only management is allow here');
                    this.create(decrypted.model, req, response => {
                        res.send(response);
                    });
                    break;
                default:
                    response.reason = 'Authorized, but no action.';
                    res.send(response);
            }
            //#endregion
        });
    }
    //manipulates the model status
    statusChange(req, res, aModel, decrypted) {
        return __awaiter(this, void 0, void 0, function* () {
            //logoff does not need to be unscramble
            if (aModel.status === r_1.AccountStatus.loggingOut) {
                const response = yield this.logOut(aModel);
                res.send(response);
                return;
            }
            const response = r_1.AccountModel.responseFactory();
            if (decrypted.model.status === r_1.AccountStatus.loggingOut) {
                const responseOut = yield this.logOut(decrypted.model);
                res.send(responseOut);
                return;
            }
            //model is null, will just response
            if (Object.keys(aModel).length === 0) {
                response.reason = 'null account model ' + ' sessionId: ' + req.sessionID;
                res.send(response);
                return;
            }
            this.captchaCheck(decrypted, req, (valid, unscrambledModel) => __awaiter(this, void 0, void 0, function* () {
                if (valid) {
                    //#region Cookie Authentication
                    if (unscrambledModel.status === r_1.AccountStatus.auto) {
                        const sessionAlive = yield this.controllers.account.cookieAuthenticate(unscrambledModel.user, req.sessionID);
                        response.model = unscrambledModel;
                        if (sessionAlive) {
                            res.send(yield this.login(unscrambledModel, req, true));
                            return;
                        }
                        else {
                            response.model.status = r_1.AccountStatus.logout;
                            response.reason = 'Your previous session has expired.';
                            res.send(response);
                            return;
                        }
                    }
                    //#endregion
                    if (unscrambledModel.status === r_1.AccountStatus.loggingIn) {
                        const response = yield this.login(unscrambledModel, req);
                        res.send(response);
                    }
                    if (unscrambledModel.status === r_1.AccountStatus.create) {
                        this.create(unscrambledModel, req, r => {
                            res.send(r);
                        });
                    }
                    return;
                }
                else {
                    const response = r_1.AccountModel.responseFactory();
                    response.model = null;
                    response.reason = 'Captcha Invalid';
                    response.verified = false;
                    res.send(response);
                    return;
                }
            }));
        });
    }
    captchaCheck(decrypted, req, callback) {
        //recaptcha => find account => validate pw ===> respond
        if (globals_1.globals.currentServerSettings.captchaByPass) {
            callback(true, decrypted.model);
            return;
        }
        const cap = new r_1.Captcha('6LfGLl0UAAAAAI-O-OBS5irWnvZdbW79nVn9w_Bw', decrypted.cap, req.connection.remoteAddress, valid => {
            if (valid) {
                callback(true, decrypted.model);
            }
            else {
                callback(false, null);
            }
        });
    }
    login(decryptedModel, req, bypassPasswordCheck = false) {
        const response = r_1.AccountModel.responseFactory();
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            const valid = yield this.controllers.account.PasswordValid(decryptedModel);
            if (!valid.isFound) {
                response.reason = decryptedModel.user + ' account does not exist.';
                resolve(response);
                return;
            }
            //#region lockout check
            const model = yield this.controllers.account.factory().FindOneAsync({ user: decryptedModel.user });
            if (model.lockoutTime > new Date()) {
                response.reason = decryptedModel.user + ' is currently locked out. Try again later.';
                resolve(response);
                return;
            }
            //#endregion
            if (valid.validate || bypassPasswordCheck) {
                const result = yield this.controllers.account.factory().FindOneAsync({ user: decryptedModel.user });
                if (!result)
                    throw new Error('Expected account does not exist.');
                result.status = r_1.AccountStatus.login;
                result.sessionID = req.sessionID;
                result.authenticated = true;
                const updateResult = yield this.controllers.account.factory().AddOrUpdateAsync({ _id: result._id }, result);
                response.model = r_1.AccountModel.sanitize(result);
                response.verified = true;
                response.reason = 'Account is logged in.';
            }
            else {
                response.reason = decryptedModel.user + ' password was incorrect.';
            }
            resolve(response);
        }));
    }
    logOut(decryptedModel) {
        const response = r_1.AccountModel.responseFactory();
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            const model = yield this.controllers.account.factory().FindOneAsync({ user: decryptedModel.user });
            if (model) {
                model.status = r_1.AccountStatus.logout;
                model.sessionID = '';
                yield this.controllers.account.factory().AddOrUpdateAsync({ _id: model._id }, model);
                response.model = r_1.AccountModel.sanitize(model);
                response.reason = model.user + ' was logged off.';
                response.verified = true;
            }
            else {
                response.reason = 'User not found. Cannot logoff.';
            }
            resolve(response);
        }));
    }
    create(model, req, callback) {
        const response = r_1.AccountModel.responseFactory();
        if (model.password == null || model.password == '') {
            response.reason = "No password provided.";
            callback(response);
            return;
        }
        this.controllers.account.IsUniqueAndValid(model.user, isUnique => {
            if (isUnique) {
                model.password = r_1.AccountModel.genPassword(model.password);
                this.controllers.account.factory().AddOrUpdate({ _id: model._id }, model, (err, result) => {
                    if (err)
                        throw err;
                    response.reason = model.user + ' was created. You can log in now.';
                    response.verified = true;
                    callback(response);
                });
            }
            else {
                response.reason = model.user + ' already exists.';
                callback(response);
            }
        });
    }
    delete(model, isManagement, req) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            const response = r_1.AccountModel.responseFactory();
            response.model = null;
            const valid = yield this.controllers.account.PasswordValid(model, true, isManagement);
            if (!valid.isFound) {
                response.reason = model.user + ' does not exist for removal.';
                resolve(response);
                return;
            }
            if (valid.validate) {
                const delResult = yield this.controllers.account.factory().RemoveAsync({ _id: model._id });
                if (delResult) {
                    response.reason = model.user + ' was removed.';
                    response.verified = true;
                }
                else {
                    response.reason = 'Error removing.';
                }
                resolve(response);
            }
            else {
                response.reason = 'Password is incorrect for removal.';
                resolve(response);
            }
        }));
    }
    unscramble(model) {
        const c = new r_1.Cipher(globals_1.globals.scrambleKey);
        const decrypted = JSON.parse(c.Decrypt(model.authenticationBundle));
        return decrypted;
    }
}
exports.AccountRoute = AccountRoute;
//# sourceMappingURL=AccountRoute.js.map