import { Routers, Ctrl } from "./server";
import { ExpressRoutes, AccountModel, Captcha, Cipher, IAuthenticateResponse, AccountStatus} from "../r";
import { globals } from "../globals";
import { AccountRequest } from "../../../../uNode/m/AccountModel";

export class AccountRoute implements Routers {
    controllers: Ctrl;
    expressRoute: ExpressRoutes;
    init(expressApp: any, controllers: Ctrl) {
        this.controllers = controllers;
        this.expressRoute = new ExpressRoutes(expressApp, 'account')
        this.GET();
        this.POST();
    }

    GET(){
        this.expressRoute.GET = async (req, res)=>{
            if(req.params.id === 'users'){
                const results = await this.controllers.account.factory().FindAllReturnFieldsAsync({}, ['user', '_id'])
                res.send({results: results.filter(e=>e.user !== 'admin')});
                return;
            }
        }
    }
    POST(){
        //key: 6LfGLl0UAAAAAI-O-OBS5irWnvZdbW79nVn9w_Bw
        this.expressRoute.POST = async (req, res)=>{
            const aModel = <AccountModel>req.body //aModel must be unscramble first
            let decrypted = null;

            //login out should never use decrypt
            if(aModel.status === AccountStatus.loggingOut){
                this.statusChange(req, res, aModel, decrypted);
                return;
            }else{
                decrypted = this.unscramble(aModel);
            }

            if(decrypted.model.request && decrypted.model.request !== AccountRequest.none){
                console.log('request mode');
                this.request(req, res, aModel, decrypted);
            }else{
                this.statusChange(req, res, aModel, decrypted)
            }
        }
    }

    //reference the model request only
    async request(req: any, res: any, aModel: AccountModel, decrypted:{model: AccountModel, cap: string}){
        let isAuthorized = null;
        let isManagement = false;
        if(decrypted.model.sessionIDManagement){
            isAuthorized = await this.controllers.account.isAdminAuthorized(decrypted.model.sessionIDManagement);
            isManagement = true;
        }else{
            isAuthorized = await this.controllers.account.Authorization(decrypted.model.sessionID);
        }
        let response:IAuthenticateResponse = AccountModel.responseFactory();
        if(!isAuthorized.isFound){
            response.reason = decrypted.model.user + ' was not found for authorization.'
            res.send(response);
            return;
        }
        if(!isAuthorized.validate){
            response.reason = decrypted.model.user + ' is not authorize.'
            res.send(response);
            return;
        }

        let model:AccountModel
        if(isManagement){
            model = await this.controllers.account.factory().FindOneAsync({_id: decrypted.model._id});
        }else{
            model = await this.controllers.account.factory().FindOneAsync({sessionID: decrypted.model.sessionID});
        }

        if(!model && !isManagement){
            response.reason = decrypted.model.user + ' session is expired.'
            res.send(response);
            return;
        }

        //#region authorized actions
        switch(decrypted.model.request){
            case AccountRequest.getUsers:
                if(!isManagement) throw new Error('only management is allow here')
                if(isAuthorized.validate){
                    response.package = await this.controllers.account.factory().FindAllReturnFieldsAsync({}, globals.managementRetrievable)
                    response.reason = "User data retrieved."
                    response.verified = true;
                }else{
                    response.reason = "You need to be root or admin."
                }
                res.send(response)
                break;
            case AccountRequest.updateSelf:
                await this.controllers.account.factory().AddOrUpdateAsync({_id: model._id}, decrypted.model);
                response.verified = true;
                response.reason = model.user + ' was updated.'
                response.model = await this.controllers.account.factory().FindOneAsync({_id: model._id})
                res.send(response)
                break;
            case AccountRequest.passwordChange:
                const changeAuthorizedAction = async ()=>{
                    let isSame = true;
                    if(model.password !== decrypted.model.passwordNew){
                        isSame = false;
                        model.password = AccountModel.genPassword(decrypted.model.passwordNew);
                        await this.controllers.account.factory().AddOrUpdateAsync({_id: model._id}, model)
                    }
                    response.verified = true;
                    response.reason = (model.user + ' password') + (isSame?' is the same.': ' was changed.')
                }
                if(isManagement){
                    await changeAuthorizedAction();
                }else{
                    const pwCheck = await this.controllers.account.PasswordValid(decrypted.model);
                    if(!pwCheck.isFound){
                        throw new Error('Password change cannot find expected user')
                    }
                    if(!pwCheck.validate){
                        response.reason = model.user + ' password is invalid.'
                    }else{
                        await changeAuthorizedAction();
                    }
                }
                res.send(response);
                break;
            case AccountRequest.delete:
                if(!isManagement) model.password = decrypted.model.password; //since db pw will be the hash version
                response = await this.delete(model, isManagement, req);
                res.send(response);
                break;
            case AccountRequest.create:
                if(!isManagement) throw new Error('only management is allow here');
                this.create(decrypted.model, req, response=>{
                    res.send(response);
                })
                break;
            default:
                response.reason = 'Authorized, but no action.'
                res.send(response);
        }
        //#endregion
    }

    //manipulates the model status
    async statusChange(req: any, res: any, aModel: AccountModel, decrypted:{model: AccountModel, cap: string}){
        //logoff does not need to be unscramble
        if(aModel.status === AccountStatus.loggingOut){
            const response = await this.logOut(aModel)
            res.send(response);
            return;
        }

        const response:IAuthenticateResponse = AccountModel.responseFactory();
        
        if(decrypted.model.status === AccountStatus.loggingOut){
            const responseOut = await this.logOut(decrypted.model);
            res.send(responseOut);
            return;
        }

        //model is null, will just response
        if(Object.keys(aModel).length === 0){
            response.reason = 'null account model ' + ' sessionId: ' + req.sessionID
            res.send(response);
            return;
        }

        this.captchaCheck(decrypted, req, async (valid, unscrambledModel:AccountModel)=>{
            if(valid){
                //#region Cookie Authentication
                if(unscrambledModel.status === AccountStatus.auto){
                    const sessionAlive = await this.controllers.account.cookieAuthenticate(unscrambledModel.user, req.sessionID)
                    response.model = unscrambledModel;
                    
                    if(sessionAlive){
                        res.send(await this.login(unscrambledModel, req, true));
                        return;
                    }else{
                        response.model.status = AccountStatus.logout;
                        response.reason = 'Your previous session has expired.'
                        res.send(response);
                        return;
                    }
                }
                //#endregion

                if(unscrambledModel.status === AccountStatus.loggingIn){
                    const response = await this.login(unscrambledModel, req)
                    res.send(response);
                }
    
                if(unscrambledModel.status === AccountStatus.create){
                    this.create(unscrambledModel, req, r=>{
                        res.send(r);
                    })
                }
                return;
            }else{
                const response = AccountModel.responseFactory();
                response.model = null;
                response.reason = 'Captcha Invalid';
                response.verified = false;
                res.send(response);
                return;
            }
        })
    }

    captchaCheck(decrypted: {model: AccountModel, cap: string}, req: any, callback:(valid, decryptedModel)=>void){
        //recaptcha => find account => validate pw ===> respond
        if(globals.currentServerSettings.captchaByPass){
            callback(true, decrypted.model)
            return;
        }
        const cap = new Captcha('6LfGLl0UAAAAAI-O-OBS5irWnvZdbW79nVn9w_Bw', decrypted.cap, req.connection.remoteAddress, valid=>{
            if(valid){
                callback(true, decrypted.model)
            }else{
                callback(false, null)
            }
        })
    }
    login(decryptedModel: AccountModel, req: any, bypassPasswordCheck = false){
        const response = AccountModel.responseFactory();
        return new Promise<IAuthenticateResponse>(async resolve=>{
            const valid = await this.controllers.account.PasswordValid(decryptedModel)
            if(!valid.isFound){
                response.reason = decryptedModel.user + ' account does not exist.'
                resolve(response);
                return;
            }
            
            //#region lockout check
            const model = await this.controllers.account.factory().FindOneAsync({user: decryptedModel.user})
            if(model.lockoutTime > new Date()){
                response.reason = decryptedModel.user + ' is currently locked out. Try again later.'
                resolve(response);
                return;
            }
            //#endregion
            
            if(valid.validate || bypassPasswordCheck){
                const result = await this.controllers.account.factory().FindOneAsync({user: decryptedModel.user});
                if(!result) throw new Error('Expected account does not exist.')
                result.status = AccountStatus.login;
                result.sessionID = req.sessionID;
                result.authenticated = true;
                const updateResult = await this.controllers.account.factory().AddOrUpdateAsync({_id: result._id}, result);
                response.model = AccountModel.sanitize(result);
                response.verified = true;
                response.reason = 'Account is logged in.'
            }else{
                response.reason = decryptedModel.user + ' password was incorrect.'
            }
            resolve(response);
        })
    }
    logOut(decryptedModel: AccountModel){
        const response = AccountModel.responseFactory();
        return new Promise<IAuthenticateResponse>(async resolve=>{
            const model = await this.controllers.account.factory().FindOneAsync({user: decryptedModel.user});
            if(model){
                model.status = AccountStatus.logout;
                model.sessionID = '';
                await this.controllers.account.factory().AddOrUpdateAsync({_id: model._id}, model)
                response.model = AccountModel.sanitize(model)
                response.reason = model.user + ' was logged off.'
                response.verified = true;
            }else{
                response.reason = 'User not found. Cannot logoff.'
            }
            resolve(response);
        })
    }
    create(model: AccountModel, req: any, callback:(response)=>void){
        const response = AccountModel.responseFactory();
        if(model.password == null || model.password == ''){
            response.reason = "No password provided."
            callback(response);
            return;
        }
        this.controllers.account.IsUniqueAndValid(model.user, isUnique=>{
            if(isUnique){
                model.password = AccountModel.genPassword(model.password);
                this.controllers.account.factory().AddOrUpdate({_id: model._id}, model, (err, result)=>{
                    if(err) throw err;
                    response.reason = model.user + ' was created. You can log in now.';
                    response.verified = true;
                    callback(response)
                })
            }else{
                response.reason = model.user + ' already exists.';
                callback(response)
            }
        })
    }
    delete(model: AccountModel, isManagement: boolean, req: any){
        return new Promise<IAuthenticateResponse>(async (resolve)=>{
            const response = AccountModel.responseFactory();
            response.model = null;
            const valid = await this.controllers.account.PasswordValid(model, true, isManagement);
            if(!valid.isFound){
                response.reason = model.user + ' does not exist for removal.';
                resolve(response)
                return;
            }
            if(valid.validate){
                const delResult = await this.controllers.account.factory().RemoveAsync({_id: model._id});
                if(delResult){
                    response.reason = model.user + ' was removed.';
                    response.verified = true;
                }else{
                    response.reason = 'Error removing.';
                }
                resolve(response)
            }else{
                response.reason = 'Password is incorrect for removal.';
                resolve(response)
            }
        })
        
    }
    unscramble(model: AccountModel):{model: AccountModel, cap: string}{
        const c = new Cipher(globals.scrambleKey);
        const decrypted:{model: AccountModel, cap: string} = JSON.parse(c.Decrypt(model.authenticationBundle))
        return decrypted;
    }
}