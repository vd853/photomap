"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const query_1 = require("../Models/query");
class SectionRoute {
    init(expressApp, controllers) {
        this.controllers = controllers;
        this.expressRoute = new r_1.ExpressRoutes(expressApp, 'section');
        this.GET();
        this.POST();
        this.DELETE();
    }
    GET() {
        this.expressRoute.GET = (req, res) => {
            const queryAny = JSON.parse(req.params.id);
            const query = queryAny;
            console.log('section query : ', query);
            switch (query.queryType) {
                case (query_1.QueryType.sectionReference):
                    this.getSectionByReferenceId(res, query);
                    break;
                default:
                    break;
            }
        };
    }
    DELETE() {
        this.expressRoute.DELETE = (req, res) => {
            const queryAny = JSON.parse(req.params.id);
            const query = queryAny;
            console.log('section query : ', query);
            //find sectionModel > remove comment from photo keyword > remove section
            this.controllers.section.factory().FindOne({ _id: query.searchId }, (err, result) => {
                if (err) {
                    res.send({ error: err });
                    return;
                }
                this.controllers.photo.factory().Array({ _id: result.referenceId }, 'keyword', result.comment, false, (err, result) => {
                    console.log(err, result);
                });
                this.controllers.section.factory().Remove({ _id: query.searchId }, (err, result) => {
                    if (err) {
                        res.send({ error: err });
                        return;
                    }
                    res.send({ result: result });
                });
            });
        };
    }
    POST() {
        this.expressRoute.POST = (req, res) => {
            const section = req.body;
            console.log('section body : ', req.body);
            this.controllers.section.factory().AddOrUpdate({ _id: section._id }, section, (err, result) => {
                if (err) {
                    res.send({ error: err });
                    return;
                }
                res.send({ result: result });
                this.controllers.photo.factory().Array({ _id: result.referenceId }, 'keyword', result.comment, true, (err, result) => {
                    console.log(err, result);
                });
            });
        };
    }
    getSectionByReferenceId(res, query) {
        this.controllers.section.factory().FindAll({ referenceId: query.searchId }, (err, result) => {
            if (err) {
                res.send({ error: err });
                return;
            }
            console.log('result for all sections ', result);
            res.send({ result: result });
        });
    }
}
exports.SectionRoute = SectionRoute;
//# sourceMappingURL=SectionRoute.js.map