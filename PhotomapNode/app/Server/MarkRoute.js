"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const globals_1 = require("../globals");
const photo_1 = require("../Models/photo");
const mark_1 = require("../Models/mark");
const UploadRoute_1 = require("./UploadRoute");
const moment = require("moment");
class MarkRoute {
    init(expressApp, controllers) {
        this.controllers = controllers;
        const r = new r_1.ExpressRoutes(expressApp, 'mark', ['id']);
        r.GET = (req, res) => {
            console.log('parma id ', req.params.id);
            const markAny = JSON.parse(req.params.id);
            const mark = markAny;
            console.log('mark ', mark);
            //PhotoModel will be updated at setImage() from stamp-switch component
            this.controllers.photo.factory().FindOne({ _id: mark.id }, (err, model) => {
                if (err) {
                    res.send({ error: err });
                    return;
                }
                if (!model) {
                    res.send({ error: 'not exist' });
                    return;
                }
                //extract a reference from a photo and converts it to its own photo as a reference type
                if (mark.mode === mark_1.MarkMode.extract) {
                    this.extract(res, model, mark, controllers);
                    return;
                }
                if (mark.mode === mark_1.MarkMode.mirror) {
                    model = photo_1.PhotoModel.addReference(model, model._id);
                    model.lastReferencePath = model.referencePath.length - 1;
                    r_1.File.CopyFile(model.compressPath, model.referencePath[model.referencePath.length - 1].mark, (err, result) => {
                        r_1.File.CopyFile(model.compressPath, model.referencePath[model.referencePath.length - 1].clean, (err, result) => {
                            this.returnResult(res, mark, model);
                        });
                    });
                }
                // console.log('mark clean path ', model.referencePath[mark.number].clean, ' at ', mark.number)
                //clears the marked image and skips marking
                if (mark.mode === mark_1.MarkMode.clear) {
                    MarkRoute.clear(mark, model, (err, result) => {
                        if (err) {
                            console.log('ERROR mark clear');
                            res.send({ error: err });
                            return;
                        }
                        this.returnResult(res, mark, model);
                    });
                    return;
                }
                //deletes the entire reference
                if (mark.mode === mark_1.MarkMode.delete) {
                    this.delete(res, mark, model);
                    return;
                }
                //deletes the entire reference
                if (mark.mode === mark_1.MarkMode.clone) {
                    this.clone(res, mark, model);
                    return;
                }
                //marks the image and returns it as image64
                if (mark.mode === mark_1.MarkMode.mark) {
                    const stampNow = () => {
                        r_1.File.CopyFile(model.referencePath[mark.number].mark, model.referencePath[mark.number].undo, (err, result) => {
                            if (err)
                                throw err;
                            console.log('undo copied');
                            MarkRoute.stamp(mark, markAny, model, (err, result) => {
                                this.returnResult(res, result.mark, result.model);
                            });
                        });
                    };
                    //will be true if stamp color is manual
                    if (globals_1.globals.currentServerSettings.settableStampColor) {
                        stampNow();
                    }
                    else {
                        MarkRoute.pixelOffsets([mark.text], mark.size, (err, offsets) => {
                            const origin = { x: mark.x, y: mark.y };
                            const end = { x: mark.x + offsets.pixelOffsetRight, y: mark.y + offsets.pixelOffsetDown };
                            r_1.Image.shadePortion(model.referencePath[mark.number].mark, origin, end, (err, shade) => {
                                if (shade !== null) {
                                    mark.isBlack = shade > globals_1.globals.stampColorThreshold ? true : false;
                                }
                                mark.isBlack = false;
                                console.log('shade: ', shade);
                                stampNow();
                            });
                        });
                    }
                    return;
                }
                if (mark.mode === mark_1.MarkMode.undo) {
                    r_1.File.CopyFile(model.referencePath[mark.number].undo, model.referencePath[mark.number].mark, (err, result0) => {
                        if (err)
                            throw err;
                        this.returnResult(res, mark, model);
                    });
                    return;
                }
                if (mark.mode === mark_1.MarkMode.info) {
                    MarkRoute.infoStamp(model, mark, markAny, (err, result) => {
                        if (err)
                            throw err;
                        this.returnResult(res, result.mark, result.model);
                    });
                    // //gets all the info from PhotoModel as a string[] for the stamp
                    // let infoText = this.getStampInfo(model)
                    // const stampNow = ()=>{ //use in conditions
                    //     //copy file to create the undo version
                    //     File.CopyFile(model.referencePath[mark.number].mark, model.referencePath[mark.number].undo, (err, result0:{result: any, streams: any[]})=>{
                    //         this.stamp(mark, markAny, model, (err, result)=>{
                    //             this.returnResult(res, result.mark, result.model)
                    //         }, infoText)
                    //     })
                    // }
                    // //will be true if stamp color is manual
                    // if(globals.currentServerSettings.settableStampColor){
                    //     stampNow();
                    // }else{
                    //     this.pixelOffsets(infoText, mark.size, (err, offsets)=>{
                    //         const origin = {x: mark.x, y: mark.y}
                    //         const end = {x: mark.x + offsets.pixelOffsetRight, y: mark.y + offsets.pixelOffsetDown}
                    //         Image.shadePortion(model.referencePath[mark.number].mark, origin, end, (err, shade)=>{
                    //             mark.isBlack = shade > globals.stampColorThreshold? true:false
                    //             console.log('shade: ', shade)
                    //             stampNow();
                    //         })
                    //     })
                    // }
                    return;
                }
            });
        };
    }
    static infoStamp(photo, mark, markAny, callback) {
        //gets all the info from PhotoModel as a string[] for the stamp
        let infoText = this.getStampInfo(photo);
        const stampNow = () => {
            //copy file to create the undo version
            r_1.File.CopyFile(photo.referencePath[mark.number].mark, photo.referencePath[mark.number].undo, (err, result0) => {
                if (err) {
                    callback(err, null);
                    return;
                }
                this.stamp(mark, markAny, photo, (err, result) => {
                    callback(err, { mark: mark, model: photo });
                }, infoText);
            });
        };
        //will be true if stamp color is manual
        if (globals_1.globals.currentServerSettings.settableStampColor) {
            stampNow();
        }
        else {
            this.pixelOffsets(infoText, mark.size, (err, offsets) => {
                if (err) {
                    callback(err, null);
                    return;
                }
                const origin = { x: mark.x, y: mark.y };
                const end = { x: mark.x + offsets.pixelOffsetRight, y: mark.y + offsets.pixelOffsetDown };
                r_1.Image.shadePortion(photo.referencePath[mark.number].mark, origin, end, (err, shade) => {
                    if (err) {
                        callback(err, null);
                        return;
                    }
                    if (shade !== null) {
                        mark.isBlack = shade > globals_1.globals.stampColorThreshold ? true : false;
                    }
                    mark.isBlack = false;
                    console.log('shade: ', shade);
                    stampNow();
                });
            });
        }
    }
    static pixelOffsets(text, fontSize, callback) {
        let maxHorizontal = 0;
        const maxVertical = text.length;
        text.forEach(e => {
            if (e.length > maxHorizontal)
                maxHorizontal = e.length;
        });
        const pixelOffset = Math.pow(2, fontSize + 3); //max is 64
        const pixelOffsetRight = maxHorizontal * pixelOffset;
        const pixelOffsetDown = maxVertical * pixelOffset;
        console.log('pixelRIgth ', pixelOffsetRight, ' pixel down ', pixelOffsetDown, ' pixeloffset ', pixelOffset);
        callback(null, { pixelOffsetRight: pixelOffsetRight, pixelOffsetDown: pixelOffsetDown, pixelOffset: pixelOffset });
    }
    static stamp(mark, markAny, model, callback, texts) {
        const offset = 5;
        r_1.Image.textImage(texts ? texts : [mark.text], //if texts, it wills stamp down, otherwise just stamp the mark.text
        model.referencePath[mark.number].mark, model.referencePath[mark.number].mark, parseInt(markAny.size), //use this since mark will return a string
        parseInt(markAny.x) - offset, parseInt(markAny.y) - offset, mark.isBlack, (err, data) => {
            if (err) {
                callback(err, null);
                console.log('marked ERROR ', err);
                return;
            }
            console.log('marked');
            callback(null, { mark: mark, model: model });
            //this.returnResult(res, mark, model)
        });
    }
    static getStampInfo(model) {
        const infoTexts = [];
        globals_1.globals.sortables.forEach(e => {
            if ((typeof model[e] !== 'boolean' && !model[e]) || model[e] === 'undefined') {
            }
            else {
                console.log('looking ', model[e]);
                if (moment(model[e]).isValid()) {
                    infoTexts.push(e.toUpperCase() + ': ' + moment(model[e]).calendar(undefined, { sameElse: 'MM/D/YYYY' }));
                }
                else {
                    infoTexts.push(e.toUpperCase() + ': ' + globals_1.globals.shorten(model[e], 30));
                }
            }
        });
        return infoTexts;
    }
    extract(res, model, mark, controllers) {
        const newModel = photo_1.PhotoModel.clone(model, globals_1.globals.currentServerSettings.fieldDefinitions);
        newModel.isReference = true;
        newModel._id = r_1.GUID.create();
        const shortGUID = r_1.GUID.create(true);
        //need this short GUID to be a unique file when doing batch downloads
        const fileName = r_1.File.getFileNameOnly(model.fileName) + '_' + shortGUID + '_EXTRACT_' + (mark.number + 1) + '.' + r_1.File.getExtension(model.fileName);
        const tempFile = globals_1.globals.storage + globals_1.globals.requestName + shortGUID; //will be moved and rename during create
        //copy the referencePath to tempFile so orginal won't get moved
        r_1.File.CopyFile(model.referencePath[mark.number].mark, tempFile, (err, result) => {
            if (err) {
                console.log('ERROR in extract temp file copy ', err);
                res.send({ error: 'ERROR in extract temp file copy ', err });
                return;
            }
            UploadRoute_1.UploadRoute.create(newModel, tempFile, fileName, controllers, (err, result) => {
                if (err) {
                    controllers.server.log('SYSTEM', 'ERROR in extract ' + err);
                    return; //create will res.send if there is an error
                }
            });
            res.send({ result: fileName });
        });
    }
    //returns image64 of the mark image, use by both modes
    //depends on the result:PhotoModel and mark:MarkModel out here
    returnResult(res, mark, model) {
        r_1.Image.getImage64(model.referencePath[mark.number].mark, (err, data0) => {
            if (err) {
                res.send({ error: err });
                return;
            }
            //update the model terms
            if (mark.mode === mark_1.MarkMode.clear) {
                model = photo_1.PhotoModel.addTerms(model, undefined, mark.number, undefined, true); //last params means to clear this mark.number
            }
            else {
                if (mark.text.length > 2) {
                    model = photo_1.PhotoModel.addTerms(model, mark.text, mark.number, false, false);
                }
            }
            //save the updated model
            this.controllers.photo.factory().AddOrUpdate({ _id: model._id }, model, (err, data1) => {
                if (err) {
                    res.send({ error: err });
                    return;
                }
                //return the model and image64
                res.send({ model: data1, image64: data0 });
            });
        });
    }
    clone(res, mark, model) {
        const fromThisIndex = model.lastReferencePath;
        const toThisIndex = model.referencePath.length - 1;
        model = photo_1.PhotoModel.addReference(model, model.referencePath[fromThisIndex].referenceId);
        r_1.File.CopyFile(model.referencePath[fromThisIndex].mark, model.referencePath[toThisIndex].mark, () => {
            r_1.File.CopyFile(model.referencePath[fromThisIndex].mark, model.referencePath[toThisIndex].clean, () => {
                model.lastReferencePath = model.referencePath.length - 1; //set new last path to the clone
                this.returnResult(res, mark, model);
            });
        });
    }
    static clear(mark, model, callback) {
        r_1.File.CopyFile(model.referencePath[mark.number].clean, model.referencePath[mark.number].mark, (err, result) => {
            callback(err, result);
        });
    }
    delete(res, mark, model) {
        this.controllers.server.del(model.referencePath[mark.number].clean, (err) => {
            if (err) {
                res.send({ result: err });
                return;
            }
            console.log('clean deleted');
            this.controllers.server.del(model.referencePath[mark.number].mark, (err) => {
                if (err) {
                    res.send({ result: err });
                    return;
                }
                console.log('mark deleted');
                model = photo_1.PhotoModel.removeReference(model, mark.number);
                console.log('reference removed');
                mark.number = model.lastReferencePath = model.referencePath.length - 1; //defaults to last one back
                console.log('new default ', model.lastReferencePath);
                this.returnResult(res, mark, model); //returns the model and image64 of the last one back for update
            });
        });
    }
}
exports.MarkRoute = MarkRoute;
//# sourceMappingURL=MarkRoute.js.map