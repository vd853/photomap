import { Routers, Ctrl } from "./server";
import { ExpressRoutes, Image, File, KeywordModel } from "../r";
import { globals } from "../globals";
import * as _ from 'lodash'
import {cache} from '../cache'
import * as async from 'async'

export class KeywordRoute implements Routers {
    controllers: Ctrl;
    init(expressApp: any, controllers: Ctrl){
        this.controllers = controllers
        const expressRoute = new ExpressRoutes(expressApp, 'keyword')
        expressRoute.GET = (req, res)=>{
            const id = req.params.id
            this.packageKeywords((err, result)=>{
                if(err){
                    res.send({error: err})
                    return;
                }
                // console.log('keyword package ', result)
                res.send({result: result})
            })
        }
    }
    packageKeywords(callbackPrime:(err, data)=>void){
        if(!cache.updateKeyword){
            callbackPrime(null, cache.keywords)
            console.log('using cache keywords')
            return;
        }
        const tasks: any = []
        const keywords = new Map()

        const combinedKeywordable = globals.keywordable.concat(globals.currentServerSettings.fieldDefinitions)

        //create task to package keywords
        combinedKeywordable.forEach(k=>{
            if(k === 'all'){
                tasks.push(
                    (callback:(err, success)=>void)=>{
                        this.controllers.keyword.factory().FindDistinct({}, 'value', (err, result)=>{
                            if(err){
                                callback(err, null)
                                return;
                            }
                            keywords[k] = []
                            result.forEach(e=>{
                                keywords[k].push(e);
                            })
                            callback(null, true)
                        })
                    }
                )      
            }else{
                tasks.push(
                    (callback:(err, success)=>void)=>{
                        this.controllers.keyword.factory().FindDistinct({type: k}, 'value', (err, result)=>{
                            if(err){
                                callback(err, null)
                                return;
                            }
                            // console.log('key found ', result)
                            keywords[k] = []
                            result.forEach(e=>{
                                keywords[k].push(e);
                            })
                            callback(null, true)
                        })
                    }
                )        
            }
        })

        async.parallel(tasks, (err, result)=>{
            if(err){
                console.log('ERROR keyword package ', err)
                callbackPrime(err, null)
            }else{
                console.log('keyword packaged ')
                // console.log(keywords)
                cache.keywords = keywords
                cache.updateKeyword = false;
                callbackPrime(null, keywords)
            }
        })
    }
    // packageKeywordsX(callback:(err, data)=>void){
    //     if(!cache.updateKeyword){
    //         callback(null, cache.keywords)
    //         console.log('using cache keywords')
    //         return;
    //     }
    //     const keywords = new Map()
    //     keywords['all'] = []
    //     this.keywordController.factory().FindAll({}, (err, result:KeywordModel[])=>{
    //         if(err){
    //             console.log('ERROR packageKeywords')
    //             callback(err, null)
    //             return;
    //         }
    //         result.forEach(e=>{
    //             if(!keywords[e.type]){
    //                 keywords[e.type] = []
    //             }
    //             //type 'all' is not part of any e.type, this is use for the full text search input
    //             if(!_.includes(keywords['all'], e.value)) keywords['all'].push(e.value) 
    //             if(!_.includes(keywords[e.type], e.value)) keywords[e.type].push(e.value)
    //             // console.log('package keyword ', e.type, ' value: ', e.value)
    //         })
    //         cache.updateKeyword = false;
    //         cache.keywords = keywords;
    //         callback(null, keywords)
    //     })
    // }
}