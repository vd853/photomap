"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("../Models/server");
const r_1 = require("../r");
const globals_1 = require("../globals");
const photoController_1 = require("../Models/photoController");
// import { NetworkConfig } from "../config";
class ServerRoute {
    init(expressApp, controllers) {
        this.controllers = controllers;
        const r = new r_1.ExpressRoutes(expressApp, 'server');
        r.POST = (req, res) => {
            let model = req.body;
            // console.log('model server update ', model);
            const trigger = model.triggers;
            model.triggers = server_1.ServerTriggers.none;
            if (model._id !== 'server')
                throw Error('Server model id is not named "server"!');
            model = server_1.ServerModel.clean(model);
            this.controllers.server.factory().AddOrUpdate({ _id: 'server' }, model, (err, data) => {
                if (err) {
                    res.send({ error: err });
                    return;
                }
                globals_1.globals.currentServerSettings = model;
                //causes all fields to be removed for every photomodel. return without waiting
                if (trigger === server_1.ServerTriggers.fieldUpdate) {
                    console.log('Field update detected');
                    this.controllers.photo.factory().FindAll({}, (err, PhotoModelList) => {
                        if (err)
                            throw err;
                        PhotoModelList.forEach(e => {
                            const modified = photoController_1.PhotoController.modifyField(e, model.fieldDefinitions, this.controllers);
                            this.controllers.photo.factory().AddOrUpdate({ _id: modified._id }, modified, (err, result) => {
                                if (err)
                                    throw err;
                            });
                        });
                    });
                    res.send({ result: 'field updated', model: model });
                    return;
                }
                if (trigger === server_1.ServerTriggers.reboot) {
                    r_1.CSystem.reboot('admin', () => { });
                    return;
                }
                if (trigger === server_1.ServerTriggers.networkUpdate) {
                    console.log('Network update detected');
                    this.setNewNetwork(model, (err, result) => {
                        if (err) {
                            res.send({ error: err + result });
                            return;
                        }
                        res.send({ result: model });
                    });
                }
                else {
                    res.send({ result: model });
                    return;
                }
            });
        };
        r.GET = (req, res) => {
            this.controllers.server.factory().FindOne({ _id: 'server' }, (err, data) => {
                if (err) {
                    res.send({ error: err });
                    return;
                }
                res.send({ result: data });
            });
        };
    }
    setNewNetwork(model, callback) {
        //port and ssl is already saved to the database and will be written to config files after process.exit()
        r_1.NetworkConfig.setSystemNetwork(model.ipv4, model.netmask, model.gateway, model.networkMode, (err, result) => {
            if (err) {
                globals_1.globals.log.error(err, 'setSystemNetwork');
                callback(err, null);
            }
            else {
                callback(null, result);
                r_1.CSystem.setHostname(model.hostname, globals_1.globals.machineUser.password, (result) => {
                    //after notifying the client, network device will restart with the new settings, 
                    //then this program will exit causing Forever to turn it back on again
                    r_1.NetworkConfig.networkRestart(globals_1.globals.machineUser.password, (result) => {
                        if (result) {
                            globals_1.globals.log.log('Network has restarted in linux environment.', 'setSystemNetwork');
                            process.exit();
                        }
                        else {
                            globals_1.globals.log.warn('no network restarting in development.', 'setSystemNetwork');
                            console.log('no network restarting in development.');
                        }
                    });
                });
            }
        });
    }
}
exports.ServerRoute = ServerRoute;
//# sourceMappingURL=serverRoute.js.map