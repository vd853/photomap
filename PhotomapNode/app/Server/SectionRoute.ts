import { Routers, Ctrl } from "./server";
import { ExpressRoutes} from "../r";
import { QueryModel, QueryType } from "../Models/query";
import { SectionModel } from "../Models/section";

export class SectionRoute implements Routers {
    controllers: Ctrl;
    expressRoute: ExpressRoutes;
    init(expressApp: any, controllers: Ctrl) {
        this.controllers = controllers;
        this.expressRoute = new ExpressRoutes(expressApp, 'section')
        this.GET();
        this.POST();
        this.DELETE();
    }
    GET(){
        this.expressRoute.GET = (req, res)=>{
            const queryAny = JSON.parse(req.params.id)
            const query = <QueryModel>queryAny
            console.log('section query : ', query)
            switch(query.queryType){
                case(QueryType.sectionReference):
                    this.getSectionByReferenceId(res, query)
                    break;
                default:
                    break;
            }
        }
    }
    DELETE(){
        this.expressRoute.DELETE = (req, res)=>{
            const queryAny = JSON.parse(req.params.id)
            const query = <QueryModel>queryAny
            console.log('section query : ', query)
            //find sectionModel > remove comment from photo keyword > remove section
            this.controllers.section.factory().FindOne({_id: query.searchId}, (err, result:SectionModel)=>{
                if(err){
                    res.send({error: err})
                    return;
                }
                this.controllers.photo.factory().Array({_id: result.referenceId}, 'keyword', result.comment, false, (err, result)=>{
                    console.log(err, result);
                })
                this.controllers.section.factory().Remove({_id: query.searchId}, (err, result)=>{
                    if(err){
                        res.send({error: err})
                        return;
                    }
                    res.send({result: result})
                })
            })
        }
    }
    POST(){
        this.expressRoute.POST = (req, res)=>{
            const section = <SectionModel>req.body
            console.log('section body : ', req.body)
            this.controllers.section.factory().AddOrUpdate({_id: section._id}, section, (err, result)=>{
                if(err){
                    res.send({error: err})
                    return;
                }
                res.send({result: result})
                this.controllers.photo.factory().Array({_id: result.referenceId}, 'keyword', result.comment, true, (err, result)=>{
                    console.log(err, result);
                })
            })
        }
    }
    getSectionByReferenceId(res: any, query: QueryModel){
        this.controllers.section.factory().FindAll({referenceId: query.searchId}, (err, result)=>{
            if(err){
                res.send({error: err})
                return;
            }
            console.log('result for all sections ', result)
            res.send({result: result})
        })
    }
}