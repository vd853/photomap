"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const _ = require("lodash");
const fs = require("fs");
class VideoRoute {
    constructor() {
        this.streams = [];
    }
    init(expressApp, controllers) {
        this.controllers = controllers;
        this.getVideo(expressApp);
        this.getVideoStats(expressApp);
    }
    //for getting video to playback
    getVideo(app) {
        // <video id="videoPlayer" controls>
        // <source src="http://localhost:1234/api/video/0" type="video/mp4">
        // </video>
        const r = new r_1.ExpressRoutes(app, 'video');
        r.GET = (req, res) => {
            const vQueryAny = JSON.parse(req.params.id);
            const vQuery = vQueryAny;
            //this actually gets the video
            const getVideo = (location) => {
                console.log('calling getVideo from ', location);
                const path = location;
                const stat = fs.statSync(path);
                const total = stat.size;
                if (req.headers['range']) {
                    const range = req.headers.range;
                    const parts = range.replace(/bytes=/, "").split("-");
                    const partialstart = parts[0];
                    const partialend = parts[1];
                    const start = parseInt(partialstart, 10);
                    const end = partialend ? parseInt(partialend, 10) : total - 1;
                    const chunksize = (end - start) + 1;
                    console.log('PARTICAL CHUNK');
                    console.log('RANGE: ' + start + ' - ' + end + ' = ' + chunksize);
                    const file = fs.createReadStream(path, { start: start, end: end });
                    res.writeHead(206, { 'Content-Range': 'bytes ' + start + '-' + end + '/' + total, 'Accept-Ranges': 'bytes', 'Content-Length': chunksize, 'Content-Type': 'video/mp4' });
                    file.pipe(res);
                    this.addStream(file);
                }
                else {
                    console.log('FULL CHUNK, ALL: ' + total);
                    res.writeHead(200, { 'Content-Length': total, 'Content-Type': 'video/mp4' });
                    const file = fs.createReadStream(path).pipe(res);
                    file.destroy();
                }
            };
            //finds the video, then calls the getVideo from above
            console.log('finding video ', vQuery.searchIds, ' quality', vQuery.quality);
            this.controllers.video.factory().FindOne({ _id: vQuery.searchIds[0] }, (err, result) => {
                if (err) {
                    console.log('error in finding video' + err);
                    return;
                }
                console.log('video found ', result._id);
                console.log('vquality ', vQuery.quality);
                //console.log('video model ', result)
                if (vQuery.quality === 'q0') {
                    if (result.hasQ0.toString() === 'true') {
                        getVideo(result.videoQ0File);
                    }
                    else {
                        getVideo(result.videoPathFile);
                    }
                }
                if (vQuery.quality === 'q1') {
                    if (result.hasQ1.toString() === 'true') {
                        getVideo(result.videoQ1File);
                    }
                    else {
                        console.log('There are no q1 quality for this video');
                        getVideo(result.videoPathFile);
                    }
                }
                if (vQuery.quality === 'preview') {
                    getVideo(result.thumbPreviewPath);
                }
            });
        };
    }
    //add stream
    addStream(stream) {
        console.log('streams length ', this.streams.length);
        this.streams.push({ stream: stream, path: stream.path });
    }
    //destroy all streams with this path
    removeStream(path, callback) {
        //close all stream in this path
        this.streams.forEach(f => {
            if (_.includes(path, f.path)) {
                console.log('Stream destroy');
                f.stream.destroy();
            }
        });
        //gets all stream index with this path
        const indexRemove = [];
        for (let i = 0; i < this.streams.length; i++) {
            if (_.includes(path, this.streams[i].path)) {
                indexRemove.push(i);
            }
        }
        //remove those indexes from streams
        indexRemove.forEach(i => {
            console.log('Stream remove');
            this.streams.splice(i, 1);
        });
        callback();
    }
    //for managing videos
    getVideoStats(app) {
        const r = new r_1.ExpressRoutes(app, 'videostats');
        //return all VideoModels
        r.GET = (req, res) => {
            const vQueryAny = JSON.parse(req.params.id);
            const vQuery = vQueryAny;
            this.controllers.video.factory().FindOne({ _id: vQuery.searchIds[0] }, (err, data) => {
                if (err || !data) {
                    res.send({ model: null });
                    return;
                }
                res.send({ model: data });
            });
        };
    }
}
exports.VideoRoute = VideoRoute;
//# sourceMappingURL=VideoRoute.js.map