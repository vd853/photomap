import { Routers, Ctrl } from "./server";
import { ExpressRoutes, Image, File, GUID, VideoModel, SSL, AccountModel, Privilege, PrivilegeType } from "../r";
import { globals } from "../globals";
import { PhotoModel } from "../Models/photo";
import * as fs from 'fs'
import * as _ from 'lodash'
import * as formidable from 'formidable'
import { processor } from "../process";
import { mediaType } from "../Models/reference";

export class UploadRoute implements Routers {
    controllers: Ctrl;
    init(expressApp: any, controllers: Ctrl) {
        this.controllers = controllers
        const r = new ExpressRoutes(expressApp, 'upload')
        const videoController = controllers.video;
        const create = UploadRoute.create
        const isVideoFormat = UploadRoute.isVideoFormat
        const createVideo = UploadRoute.createVideo;
        const isSSLFormat = UploadRoute.isSSLFormat;

        r.POST = (req, res)=>{
            const form = new formidable.IncomingForm();
            form.maxFileSize = 3000000000
            const controllers = this.controllers;

            //this is called anytime 1 file have been uploaded
            form.parse(req, async function (err, fields, files) { //field is additionalParameter from NG
                if(err){
                    console.log('formidable error ', err)
                    res.send({error: err})
                    return;
                }
                const downloadPath = files.file.path
                const fileName = files.file.name

                let model = <PhotoModel>JSON.parse(fields.photoModel);
                const accountModel = <AccountModel>JSON.parse(fields.accountModel);
                const isAuthorized = await controllers.account.AuthorizationByModel(accountModel);

                if(!isAuthorized.isFound || !isAuthorized.validate){
                    res.send({error: 'User not authorized or not found during upload verfication.'})
                    return;
                }
                
                //#region owner and privilege assign
                model.ownerId = accountModel._id;
                model.ownerName = accountModel.user;
                model = PhotoModel.modifyAuthenticates(model, new Privilege(PrivilegeType.owner, accountModel))
                //#endregion

                if(isSSLFormat(fileName)){
                    UploadRoute.processSSL(fileName, downloadPath, fields.pass, (err, result)=>{
                        if(err){
                            res.send({error: err, fileName: fileName})
                            return;
                        }
                        res.send({result: result, fileName: fileName})
                    })
                    return;
                }

                //either video or photo
                if(isVideoFormat(fileName)){
                    console.log('upload is a video ')
                    model.isReference = false; //video cannot be a reference
                    UploadRoute.createVideo(model, downloadPath, fileName, controllers, (err, result)=>{
                        if(err){
                            controllers.server.log('SYSTEM', 'Error in video process ' + fileName + ': ' + err)
                        }
                        if(result !== null){
                            let k = result;
                            result.vModel.status = 'uploaded';
                            controllers.serverMain.queue.add(result.vModel, result.pModel.filePath)
                        }
                    })
                }else{
                    const remodel:PhotoModel = _.cloneDeep(model);
                    remodel.mediaType = mediaType.photo;
                    UploadRoute.create(remodel, downloadPath, fileName, controllers, (err, result)=>{
                        if(err){
                            controllers.server.log('SYSTEM', 'Error in photo process ' + fileName + ': ' + err)
                        }
                    })
                }
                res.send({result: 'File uploaded, processings have begun'});
            });
        }
    }
    static processSSL(fileName: string, currentLocation: string, pass: string, callback:(err, result)=>void){
        let isKey: boolean;
        if(fileName === 'cert.pem'){
            isKey = false;
        }else if(fileName === 'key.pem'){
            isKey = true;
        }
        else{
            callback('invalid name ' + fileName, null)
            return;
        }

        if(isKey){
            if(pass === ''){
                callback('passphrase incorrect', null);
                return;
            }
        }

        SSL.validatePEM('./Keys/bin', currentLocation, pass, isKey, (err, result)=>{
            if(err){
                callback('passphrase incorrect', null);
                return;
            }
            File.move2(currentLocation, './Keys/temp/' + fileName, err=>{
                if(err){
                    callback('file move error ' + err, null);
                    return;
                }
                callback(null, 'validated');
            })
        })
    }
    static isVideoFormat(fileName: string){
        if(_.includes(['mp4', 'avi'], File.getExtension(fileName).toLowerCase())){
            return true;
        }else{
            return false;
        }
    }
    static isSSLFormat(fileName: string){
        if(_.includes(['pem'], File.getExtension(fileName).toLowerCase())){
            return true;
        }else{
            return false;
        }
    }
    static createVideo(
        model: PhotoModel, 
        oldPath: string, 
        fileName: string, 
        controllers: Ctrl,
        callback:(err, result:{pModel: PhotoModel, vModel: VideoModel} | null )=>void
    ){
        const remodel:PhotoModel = _.cloneDeep(model);
        remodel.mediaType = mediaType.video;
        const vm = controllers.video.Model();
        remodel.someId = [vm._id]; //link photo to video as reference
        new Promise((resolve, reject)=>{
            this.create(remodel, oldPath, fileName, controllers, (err, data)=>{
                if(err){
                    reject(err)
                    return;
                }
                resolve(data)
            });
        })
        .catch(e=>{
            callback(e, null);
        })
        //if success, add video to model for proecessing in queue
        .then(e=>{
            console.log('model created for video ', vm)
            const pModel = e;
            return new Promise((resolve, reject)=>{
                controllers.video.factory().AddOrUpdate({_id: vm._id}, vm, (err, vModel:VideoModel)=>{
                    if(err){
                        reject(err)
                        return;
                    }
                    resolve({pModel: pModel, vModel: vModel})
                })
            })
        })
        .catch(e=>{
            // console.log('e2 ', e)
            callback(e, null);
        })
        .then((e: any)=>{
            // console.log(e)
            callback(null, e);
        })
    }

    static create(
        model: PhotoModel, 
        oldPath: string, 
        fileName: string, 
        controllers: Ctrl,
        callback:(err, result)=>void
    ){
        File.createFolder(globals.storage) //creates folder to store if it doesn't exist

        if(!oldPath){
            console.log('connection terminated')
            callback('connection terminated', null)
            return;
        }else{
            if(!fs.existsSync(oldPath)){
                console.log('connection terminated')
                callback('connection terminated', null)
                return;
            }
        }

        const oldpath = oldPath; //old path has files, but no extension

        //model.id, fileName, folder parent path, and filepath is define outside the process. Also creating the folder
        if(!model._id) model._id = GUID.create()
        File.createFolder(globals.storage + '/' + model._id);
        model.fileName = File.getFileOnlyOrParentFolderName(fileName); //name of original file
        model.folderPath = globals.storage + '/' + model._id + '/'; 

        if(model.mediaType === mediaType.video){
            const videoAppend = model.someId[0] + '/';
            File.createFolder(model.folderPath + videoAppend)
            model.filePath = model.folderPath + videoAppend + model.fileName;
        }else{
            model.filePath = model.folderPath + model.fileName;
        }
        
        
        File.move2(oldpath, model.filePath, function (err) {
            if (err) {
                console.log('rename error from upload ', err)
                callback('create move ERROR' + err, null)
                return;
            };
            const p = new processor(model.filePath, model, controllers.server, controllers.photo, controllers.keyword, callback)
            p.start()
        });
    }
}