"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const globals_1 = require("../globals");
const async = require("async");
class ImageRoute {
    init(expressApp, controllers) {
        this.controllers = controllers;
        const r = new r_1.ExpressRoutes(expressApp, 'image', ['id', 'type', 'reference']);
        r.GET = (req, res) => {
            console.log('get image ', req.params);
            const id = req.params.id;
            const type = req.params.type;
            const reference = req.params.reference;
            this.controllers.photo.factory().FindOne({ _id: id }, (err, model) => {
                if (err) {
                    res.send({ error: err });
                    return;
                }
                if (!model) {
                    res.send({ result: 'not exist' });
                    return;
                }
                const PV = this.getPathAndVisit(res, model, type, reference);
                if (!PV)
                    return; //this means getAllReferences() condition was invoke and no path was return
                const fileName = model.fileName; //use in the return if successful
                const fileTitle = model.title;
                // get image of one of the three paths above 0,1,2
                r_1.Image.getImage64(PV.path, (err, data) => {
                    if (err) {
                        res.send({ result: err });
                        return;
                    }
                    //if looking at another reference, update the lastreference path for this model
                    if (type === '1') {
                        if (PV.visit !== model.lastReferencePath) {
                            model.lastReferencePath = PV.visit;
                            this.controllers.photo.factory().AddOrUpdate({ _id: model._id }, model, (err, data0) => {
                                if (err) {
                                    res.send({ result: err });
                                    return;
                                }
                                res.send({ result: data });
                            });
                        }
                        else {
                            res.send({ result: data });
                        }
                        return;
                    }
                    //use for sending high quality image from clicking on the thumbnail
                    if (type === '4') {
                        res.send({ result: data });
                        return;
                    }
                    res.send({ result: data, fileName: fileName, title: fileTitle }); //for type 0 and 2, fileName is use in modify > reference
                    return;
                });
            });
        };
    }
    //reference will not be null if user selects a reference, otherwise use the last
    //type => 0: compress, 1: mark last visited, 2: compress2, 3: get all references
    //reference is use for select which reference instead of using the last visit
    getPathAndVisit(res, model, type, reference) {
        const visitReference = reference ? reference : model.lastReferencePath;
        console.log('visit reference ', visitReference, ' image retreive type ', type);
        switch (type) {
            case '0':
                console.log('return compressPath ', globals_1.globals.staticMap(model.compressPath));
                res.send({ result: globals_1.globals.staticMap(model.compressPath) });
                return null;
            // break;
            //return {path: model.compressPath, visit: -1}
            case '1':
                // res.send({result: globals.staticMap(model.referencePath[visitReference].mark)})
                // break;
                // console.log('getting reference at ', visitReference)
                return { path: model.referencePath[visitReference].mark, visit: visitReference };
            case '2':
                res.send({ result: globals_1.globals.staticMap(model.compress2Path) });
                return null;
            // return {path: model.compress2Path, visit: -1}
            case '3':
                this.getAllReferences(res, model);
                return null;
            case '4'://popup view
                res.send({ result: globals_1.globals.staticMap(model.compressPath) });
                return null;
        }
    }
    getAllReferences(res, model) {
        const tasks = [];
        // const pathList = []
        model.referencePath.forEach(ref => {
            // pathList.push(globals.staticMap(ref.mark))
            const task = (callback) => {
                r_1.Image.getImage64(ref.mark, (err, data) => {
                    if (err) {
                        console.log('image64 error in reference ', err);
                        callback(err, null);
                        return;
                    }
                    callback(null, { referenceId: ref.referenceId, base64: data });
                });
            };
            tasks.push(task);
        });
        // console.log('getAllReferences now')
        // console.log('reference paths return ', pathList)
        // res.send({result: pathList})
        async.parallel(tasks, (err, result) => {
            if (err) {
                console.log('image64 error in reference ', err);
                res.send({ error: err });
                return;
            }
            // console.log('reference paths return ', result)
            res.send({ result: result });
            return;
        });
    }
}
exports.ImageRoute = ImageRoute;
//# sourceMappingURL=ImageRoute.js.map