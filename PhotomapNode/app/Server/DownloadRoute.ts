import { Routers, Ctrl } from "./server";
import { ExpressRoutes, Image, File } from "../r";
import { globals } from "../globals";
import { PhotoModel } from "../Models/photo";
import { ModifyModel } from "../Models/modify";
import { RequestModel, RequestType, DownloadType } from "../Models/request";
import { Batch } from "../batch";
import * as moment from 'moment';

export class DownloadRoute implements Routers {
    controllers: Ctrl;
    init(expressApp: any, controllers: Ctrl) {
        this.controllers = controllers
        const r = new ExpressRoutes(expressApp, 'download')
        r.GET = (req, res)=>{
            const modifyAny = JSON.parse(req.params.id)
            const modify = <ModifyModel>modifyAny

            console.log('modify init ', modify)

            //will return null if request doesn't exist or has expired
            //this is use to block expired request files
            if(modify.requestType === RequestType.lookup){
                this.lookup(res, modify)
                return;
            }

            //single file downloads
            if(modify.requestType === RequestType.request){
                this.request(res, modify) //no download, only request
            }else{
                this.single(res, modify) //also includes download from completed request .zip
            }
        }
    }
    lookup(res: any, modify: ModifyModel){
        this.controllers.request.factory().FindOne({_id: modify.ids[0]}, (err, result)=>{
            if(err){
                console.log('ERROR in request lookup ', err)
                res.send({error: err})
                return;
            }
            if(!result) console.log('request doesnt exist or is expired')
            res.send({result: result})
        })
    }

    //start a request process
    request(res: any, modify: ModifyModel){
        if(modify.ids.length < 1){
            res.send({error: 'no ids selected'})
            return;
        }
        const rm = this.controllers.request.Model()
        rm.expiration = moment(new Date()).add(globals.requestExpirationDays, 'd').toDate();
        rm.requestIds = modify.ids
        rm.downloadType = modify.downloadType
        //log initalizer
        rm.status = 'Batch process started'
        this.controllers.server.log(rm._id, rm.status)
        res.send({result: rm})
        
        const b = new Batch(rm, this.controllers, (err1, data)=>{
            this.controllers.request.factory().AddOrUpdate({_id: rm._id}, rm, (err2, model)=>{
                //error in request db update
                if(err2){
                    console.log('ERROR in updating request db')
                    return;
                }
                //error in batch process, log server as error without download
                if(err1){
                    console.log('err1 from return ', err1)
                    this.controllers.server.log(model._id, model.status, false)
                    return;
                }
                
                //tag log as downloadable if successful
                this.controllers.server.log(model._id, model.status, true)
            })
        })
    }

    //use for actual download
    single(res: any, modify: ModifyModel){
        this.controllers.photo.factory().FindOne({_id: modify.ids[0]}, (err, data:PhotoModel)=>{
            const res1 = res;
            if(err){
                res.send({error: err})
                return
            }
            //0 download orginial, 1 reference, 2 merge, 3 request
            switch (modify.downloadType){
                case DownloadType.high: //high resolution
                    this.send(res, data.filePath)
                    break;
                case DownloadType.reference: //reference image
                    this.send(res, data.referencePath[data.lastReferencePath].mark)
                    break;
                case DownloadType.merge: //merge image
                    const mergedPath = data.mergerPath
                    const newFileName = 'merge_' + data.fileName
                    Image.imageMerge([data.compressPath, data.referencePath[data.lastReferencePath].mark], mergedPath, (err, data)=>{
                        if(err){
                            res.send({error: err})
                            return
                        }
                        this.send(res, mergedPath)
                    })
                    break;
                case DownloadType.requestDownload: //requested complete files
                    this.requestReady(res, modify)
                    break;
                default:
                    break;
            }
        })
    }
    //use in single() when a request file is ready for download
    requestReady(res: any, modify: ModifyModel){
        if(modify.ids.length > 1){
            console.log('ERROR type 3 cannot have more than one id, also this must be a request id');
            res.send({error: 'ERROR type 3 cannot have more than one id, also this must be a request id'})
            return;
        }
        this.controllers.request.factory().FindOne({_id: modify.ids[0]}, (err, data1:RequestModel)=>{
        if(err){
            console.log('ERROR at download requested file db find', err)
            res.send({result: err})
            return
        }
        if(!data1){
            console.log('filed not found or expired')
            res.send({result: 'not'})
            return
        }
        data1.downloadTimes+=1;
        this.controllers.request.factory().AddOrUpdate({_id: modify.ids[0]}, data1, (err, data2)=>{
                if(err){
                    console.log('ERROR at download requested file db update', err)
                    res.send({result: err})
                    return
                }
                console.log('sending ', data1, ' name ', File.getFileOnlyOrParentFolderName(data1.outputPath))
                this.send(res, data1.outputPath)
            })
        })
    }
    //use in single() to start sending the file
    send(res: any, fileName: string){
        //use later on for sending file
        console.log('sending preparing ', fileName)
        const repath = File.reparsePath(__dirname + '/../' + fileName.replace('./', '/'))
        res.sendFile(repath)
    }
}