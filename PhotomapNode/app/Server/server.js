"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const globals_1 = require("../globals");
const photoController_1 = require("../Models/photoController");
const ServerController_1 = require("../Models/ServerController");
const requestController_1 = require("../Models/requestController");
const serverRoute_1 = require("./serverRoute");
const MarkRoute_1 = require("./MarkRoute");
const UploadRoute_1 = require("./UploadRoute");
const DownloadRoute_1 = require("./DownloadRoute");
const ImageRoute_1 = require("./ImageRoute");
const ProfileRoute_1 = require("./ProfileRoute");
const keywordController_1 = require("../Models/keywordController");
const KeywordRoute_1 = require("./KeywordRoute");
const maintenance_1 = require("../maintenance");
const cache_1 = require("../cache");
const path = require("path");
const videoController_1 = require("../Models/videoController");
const queue_1 = require("../queue");
const VideoRoute_1 = require("./VideoRoute");
const SectionRoute_1 = require("./SectionRoute");
const sectionController_1 = require("../Models/sectionController");
const AccountController_1 = require("../Models/AccountController");
const AccountRoute_1 = require("./AccountRoute");
class Server {
    get queue() { return this._queue; }
    ;
    constructor() {
        const server = new ServerController_1.ServerController(() => {
            cache_1.cache.controllers = {
                serverMain: this,
                server: server,
                video: new videoController_1.VideoController(),
                photo: new photoController_1.PhotoController(),
                request: new requestController_1.RequestController(),
                keyword: new keywordController_1.KeywordController(),
                section: new sectionController_1.SectionController(),
                account: new AccountController_1.AccountController()
            };
            cache_1.cache.controllers.server.factory().FindOne({ _id: 'server' }, (err, result) => {
                if (err) {
                    console.log('cannot load server configs from db');
                    throw err;
                }
                if (result === null) {
                    throw new Error('Server settings not retrieved!');
                }
                this.init(result);
            });
        });
    }
    init(serverModel) {
        let ssl = undefined;
        console.log('server settings ', serverModel);
        if (serverModel.ssl && serverModel.certValid && serverModel.keyValid) {
            ssl = new r_1.SSLConfig('./Keys/key.pem', './Keys/cert.pem', 'photo');
        }
        const sessionsMinutes = 60;
        const cors = new r_1.CORSConfig('GET, POST, DELETE', '*', 1000 * 60 * sessionsMinutes, r_1.SessionStore.NonDB);
        this.e = new r_1.ExpressObj(serverModel.port, r_1.ExpressObj.getNgDistPath('photomapNG'), '../Assets/favicon.ico', path.join(__dirname, '../Static'), cors, ssl);
        maintenance_1.Maintenance.factory(cache_1.cache.controllers, () => {
            this.createRouters();
            globals_1.globals.log = new r_1.Logger('Photomap', '../logs');
            if (!globals_1.globals.bypassNetworkCheck) {
                cache_1.cache.controllers.server.establishIp(process.platform === "win32" ? 'vEthernet (External)' : 'eth0', serverModel, (err, result) => {
                    if (!err) {
                        //during dev, this will cause NG to restart because the config.json file has been modifed in the asset folder
                        globals_1.globals.log.log('network info updated');
                    }
                    else {
                        globals_1.globals.log.error(err, 'network info update');
                    }
                });
            }
            this._queue = new queue_1.Queue(cache_1.cache.controllers.video, cache_1.cache.controllers);
            this.e.Start();
        });
    }
    createRouters() {
        const routers = [];
        routers.push(new KeywordRoute_1.KeywordRoute());
        routers.push(new serverRoute_1.ServerRoute());
        routers.push(new MarkRoute_1.MarkRoute());
        routers.push(new UploadRoute_1.UploadRoute());
        routers.push(new DownloadRoute_1.DownloadRoute());
        routers.push(new ImageRoute_1.ImageRoute());
        routers.push(new ProfileRoute_1.ProfileRoute());
        routers.push(new VideoRoute_1.VideoRoute());
        routers.push(new SectionRoute_1.SectionRoute());
        routers.push(new AccountRoute_1.AccountRoute());
        routers.forEach(e => {
            e.init(this.e.app, cache_1.cache.controllers);
        });
    }
}
exports.Server = Server;
//# sourceMappingURL=server.js.map