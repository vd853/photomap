import { ServerModel, ServerTriggers } from "../Models/server";
import { Routers, Ctrl } from "./server";
import { ExpressRoutes, File, GUID, NetworkConfig, CSystem} from "../r";
import { globals } from "../globals";
import { PhotoModel } from "../Models/photo";
import { ICFields, CFields } from "../Models/info";
import * as _ from 'lodash';
import { PhotoController } from "../Models/photoController";
// import { NetworkConfig } from "../config";

export class ServerRoute implements Routers {
    controllers: Ctrl;
    init(expressApp: any, controllers: Ctrl) {
        this.controllers = controllers
        const r = new ExpressRoutes(expressApp, 'server')
        r.POST = (req, res)=>{
            let model = <ServerModel>req.body;
            // console.log('model server update ', model);
            const trigger = model.triggers;
            model.triggers = ServerTriggers.none;
            if(model._id !== 'server') throw Error('Server model id is not named "server"!')
            model = ServerModel.clean(model);
            this.controllers.server.factory().AddOrUpdate({_id: 'server'}, model, (err, data)=>{
                if(err){
                    res.send({error: err})
                    return
                }

                globals.currentServerSettings = model

                //causes all fields to be removed for every photomodel. return without waiting
                if(trigger === ServerTriggers.fieldUpdate){
                    console.log('Field update detected');
                    this.controllers.photo.factory().FindAll({}, (err, PhotoModelList:PhotoModel[])=>{
                        if(err) throw err;
                        PhotoModelList.forEach(e=>{
                            const modified = PhotoController.modifyField(e, model.fieldDefinitions, this.controllers);
                            this.controllers.photo.factory().AddOrUpdate({_id: modified._id}, modified, (err, result)=>{
                                if(err) throw err;
                            })
                        })
                    })
                    res.send({result: 'field updated', model: model})
                    return;
                }

                if(trigger === ServerTriggers.reboot){
                    CSystem.reboot('admin', ()=>{})
                    return;
                }

                if(trigger === ServerTriggers.networkUpdate){
                    console.log('Network update detected');
                    this.setNewNetwork(model, (err, result)=>{
                        if(err){
                            res.send({error: err + result})
                            return;
                        }
                        res.send({result: model})
                    })
                }else{
                    res.send({result: model})
                    return;
                }
            });
        }
        r.GET = (req, res)=>{
            this.controllers.server.factory().FindOne({_id: 'server'}, (err, data:ServerModel)=>{
                if(err){
                    res.send({error: err})
                    return
                }
                res.send({result: data});
            })
        }
    }
    
    setNewNetwork(model: ServerModel, callback:(err, result)=>void){
        //port and ssl is already saved to the database and will be written to config files after process.exit()
        NetworkConfig.setSystemNetwork(model.ipv4, model.netmask, model.gateway, model.networkMode, (err, result)=>{
            if(err){
                globals.log.error(err, 'setSystemNetwork')
                callback(err, null);
            }else{
                callback(null, result);

                CSystem.setHostname(model.hostname, globals.machineUser.password, (result)=>{
                    //after notifying the client, network device will restart with the new settings, 
                    //then this program will exit causing Forever to turn it back on again
                    NetworkConfig.networkRestart(globals.machineUser.password, (result)=>{
                        if(result){
                            globals.log.log('Network has restarted in linux environment.', 'setSystemNetwork')
                            process.exit();
                        }else{
                            globals.log.warn('no network restarting in development.', 'setSystemNetwork')
                            console.log('no network restarting in development.')
                        }
                    });
                })
            }
        })
    }
}