"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const globals_1 = require("../globals");
const request_1 = require("../Models/request");
const batch_1 = require("../batch");
const moment = require("moment");
class DownloadRoute {
    init(expressApp, controllers) {
        this.controllers = controllers;
        const r = new r_1.ExpressRoutes(expressApp, 'download');
        r.GET = (req, res) => {
            const modifyAny = JSON.parse(req.params.id);
            const modify = modifyAny;
            console.log('modify init ', modify);
            //will return null if request doesn't exist or has expired
            //this is use to block expired request files
            if (modify.requestType === request_1.RequestType.lookup) {
                this.lookup(res, modify);
                return;
            }
            //single file downloads
            if (modify.requestType === request_1.RequestType.request) {
                this.request(res, modify); //no download, only request
            }
            else {
                this.single(res, modify); //also includes download from completed request .zip
            }
        };
    }
    lookup(res, modify) {
        this.controllers.request.factory().FindOne({ _id: modify.ids[0] }, (err, result) => {
            if (err) {
                console.log('ERROR in request lookup ', err);
                res.send({ error: err });
                return;
            }
            if (!result)
                console.log('request doesnt exist or is expired');
            res.send({ result: result });
        });
    }
    //start a request process
    request(res, modify) {
        if (modify.ids.length < 1) {
            res.send({ error: 'no ids selected' });
            return;
        }
        const rm = this.controllers.request.Model();
        rm.expiration = moment(new Date()).add(globals_1.globals.requestExpirationDays, 'd').toDate();
        rm.requestIds = modify.ids;
        rm.downloadType = modify.downloadType;
        //log initalizer
        rm.status = 'Batch process started';
        this.controllers.server.log(rm._id, rm.status);
        res.send({ result: rm });
        const b = new batch_1.Batch(rm, this.controllers, (err1, data) => {
            this.controllers.request.factory().AddOrUpdate({ _id: rm._id }, rm, (err2, model) => {
                //error in request db update
                if (err2) {
                    console.log('ERROR in updating request db');
                    return;
                }
                //error in batch process, log server as error without download
                if (err1) {
                    console.log('err1 from return ', err1);
                    this.controllers.server.log(model._id, model.status, false);
                    return;
                }
                //tag log as downloadable if successful
                this.controllers.server.log(model._id, model.status, true);
            });
        });
    }
    //use for actual download
    single(res, modify) {
        this.controllers.photo.factory().FindOne({ _id: modify.ids[0] }, (err, data) => {
            const res1 = res;
            if (err) {
                res.send({ error: err });
                return;
            }
            //0 download orginial, 1 reference, 2 merge, 3 request
            switch (modify.downloadType) {
                case request_1.DownloadType.high://high resolution
                    this.send(res, data.filePath);
                    break;
                case request_1.DownloadType.reference://reference image
                    this.send(res, data.referencePath[data.lastReferencePath].mark);
                    break;
                case request_1.DownloadType.merge://merge image
                    const mergedPath = data.mergerPath;
                    const newFileName = 'merge_' + data.fileName;
                    r_1.Image.imageMerge([data.compressPath, data.referencePath[data.lastReferencePath].mark], mergedPath, (err, data) => {
                        if (err) {
                            res.send({ error: err });
                            return;
                        }
                        this.send(res, mergedPath);
                    });
                    break;
                case request_1.DownloadType.requestDownload://requested complete files
                    this.requestReady(res, modify);
                    break;
                default:
                    break;
            }
        });
    }
    //use in single() when a request file is ready for download
    requestReady(res, modify) {
        if (modify.ids.length > 1) {
            console.log('ERROR type 3 cannot have more than one id, also this must be a request id');
            res.send({ error: 'ERROR type 3 cannot have more than one id, also this must be a request id' });
            return;
        }
        this.controllers.request.factory().FindOne({ _id: modify.ids[0] }, (err, data1) => {
            if (err) {
                console.log('ERROR at download requested file db find', err);
                res.send({ result: err });
                return;
            }
            if (!data1) {
                console.log('filed not found or expired');
                res.send({ result: 'not' });
                return;
            }
            data1.downloadTimes += 1;
            this.controllers.request.factory().AddOrUpdate({ _id: modify.ids[0] }, data1, (err, data2) => {
                if (err) {
                    console.log('ERROR at download requested file db update', err);
                    res.send({ result: err });
                    return;
                }
                console.log('sending ', data1, ' name ', r_1.File.getFileOnlyOrParentFolderName(data1.outputPath));
                this.send(res, data1.outputPath);
            });
        });
    }
    //use in single() to start sending the file
    send(res, fileName) {
        //use later on for sending file
        console.log('sending preparing ', fileName);
        const repath = r_1.File.reparsePath(__dirname + '/../' + fileName.replace('./', '/'));
        res.sendFile(repath);
    }
}
exports.DownloadRoute = DownloadRoute;
//# sourceMappingURL=DownloadRoute.js.map