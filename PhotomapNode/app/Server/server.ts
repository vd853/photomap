import {ExpressObj, CORSConfig, SSLConfig, Logger, SessionStore} from "../r";
import { globals } from "../globals";
import * as fs from 'fs'
import { PhotoController } from "../Models/photoController";
import { ServerController } from "../Models/ServerController";
import { RequestController } from "../Models/requestController";
import { ServerRoute } from "./serverRoute";
import { MarkRoute } from "./MarkRoute";
import { UploadRoute } from "./UploadRoute";
import { DownloadRoute } from "./DownloadRoute";
import { ImageRoute } from "./ImageRoute";
import { ProfileRoute } from "./ProfileRoute";
import { KeywordController } from "../Models/keywordController";
import { KeywordRoute } from "./KeywordRoute";
import { Maintenance } from "../maintenance";
import {cache} from '../cache'
import * as path from 'path'
import { VideoController } from "../Models/videoController";
import { Queue } from "../queue";
import { VideoRoute } from "./VideoRoute";
import { SectionRoute } from "./SectionRoute";
import { SectionController } from "../Models/sectionController";
import { ServerModel } from "../Models/server";
import { AccountController } from "../Models/AccountController";
import { AccountRoute } from "./AccountRoute";
export interface Routers{
    controllers:Ctrl
    init(expressApp: any, controllers:Ctrl)
}
export interface Ctrl{
    serverMain: Server;
    video: VideoController,
    photo: PhotoController, 
    server: ServerController, 
    keyword: KeywordController, 
    request: RequestController,
    section: SectionController,
    account: AccountController
}
export class Server{
    private e: ExpressObj
    private _queue: Queue;
    get queue(): Queue {return this._queue};
    constructor(){
        const server = new ServerController(()=>{
            cache.controllers = {
                serverMain: this,
                server: server,
                video: new VideoController(),
                photo: new PhotoController(),
                request: new RequestController(),
                keyword: new KeywordController(),
                section: new SectionController(),
                account: new AccountController()
            }
            cache.controllers.server.factory().FindOne({_id: 'server'}, (err, result)=>{
                if(err){
                    console.log('cannot load server configs from db')
                    throw err;
                }
                if(result === null){
                    throw new Error('Server settings not retrieved!')
                }
                this.init(result);
            })
        })
    }
    init(serverModel: ServerModel){
        let ssl:SSLConfig|undefined = undefined;
        console.log('server settings ', serverModel);
        if(serverModel.ssl && serverModel.certValid && serverModel.keyValid){
            ssl = new SSLConfig('./Keys/key.pem', './Keys/cert.pem', 'photo');
        }
        const sessionsMinutes = 60;
        const cors = new CORSConfig('GET, POST, DELETE', '*', 1000*60*sessionsMinutes, SessionStore.NonDB);
        this.e = new ExpressObj(
            serverModel.port, 
            ExpressObj.getNgDistPath('photomapNG'), 
            '../Assets/favicon.ico', 
            path.join(__dirname, '../Static'),
            cors,
            ssl
        )
        Maintenance.factory(cache.controllers, ()=>{
            this.createRouters();
            globals.log = new Logger('Photomap', '../logs');
            
            if(!globals.bypassNetworkCheck){
                cache.controllers.server.establishIp(process.platform === "win32"? 'vEthernet (External)': 'eth0', serverModel, (err, result)=>{
                    if(!err){
                        //during dev, this will cause NG to restart because the config.json file has been modifed in the asset folder
                        globals.log.log('network info updated')
                    }else{
                        globals.log.error(err, 'network info update')
                    }
                })
            }
    
            this._queue = new Queue(cache.controllers.video, cache.controllers)
            this.e.Start()
        })
    }

    createRouters(){
        const routers: Routers[] = []
        routers.push(new KeywordRoute())
        routers.push(new ServerRoute())
        routers.push(new MarkRoute())
        routers.push(new UploadRoute())
        routers.push(new DownloadRoute())
        routers.push(new ImageRoute())
        routers.push(new ProfileRoute())
        routers.push(new VideoRoute())
        routers.push(new SectionRoute())
        routers.push(new AccountRoute())
        routers.forEach(e=>{
            e.init(this.e.app, cache.controllers)
        })
    }
}
