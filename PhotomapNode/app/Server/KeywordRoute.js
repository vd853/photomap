"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const globals_1 = require("../globals");
const cache_1 = require("../cache");
const async = require("async");
class KeywordRoute {
    init(expressApp, controllers) {
        this.controllers = controllers;
        const expressRoute = new r_1.ExpressRoutes(expressApp, 'keyword');
        expressRoute.GET = (req, res) => {
            const id = req.params.id;
            this.packageKeywords((err, result) => {
                if (err) {
                    res.send({ error: err });
                    return;
                }
                // console.log('keyword package ', result)
                res.send({ result: result });
            });
        };
    }
    packageKeywords(callbackPrime) {
        if (!cache_1.cache.updateKeyword) {
            callbackPrime(null, cache_1.cache.keywords);
            console.log('using cache keywords');
            return;
        }
        const tasks = [];
        const keywords = new Map();
        const combinedKeywordable = globals_1.globals.keywordable.concat(globals_1.globals.currentServerSettings.fieldDefinitions);
        //create task to package keywords
        combinedKeywordable.forEach(k => {
            if (k === 'all') {
                tasks.push((callback) => {
                    this.controllers.keyword.factory().FindDistinct({}, 'value', (err, result) => {
                        if (err) {
                            callback(err, null);
                            return;
                        }
                        keywords[k] = [];
                        result.forEach(e => {
                            keywords[k].push(e);
                        });
                        callback(null, true);
                    });
                });
            }
            else {
                tasks.push((callback) => {
                    this.controllers.keyword.factory().FindDistinct({ type: k }, 'value', (err, result) => {
                        if (err) {
                            callback(err, null);
                            return;
                        }
                        // console.log('key found ', result)
                        keywords[k] = [];
                        result.forEach(e => {
                            keywords[k].push(e);
                        });
                        callback(null, true);
                    });
                });
            }
        });
        async.parallel(tasks, (err, result) => {
            if (err) {
                console.log('ERROR keyword package ', err);
                callbackPrime(err, null);
            }
            else {
                console.log('keyword packaged ');
                // console.log(keywords)
                cache_1.cache.keywords = keywords;
                cache_1.cache.updateKeyword = false;
                callbackPrime(null, keywords);
            }
        });
    }
}
exports.KeywordRoute = KeywordRoute;
//# sourceMappingURL=KeywordRoute.js.map