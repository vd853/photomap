"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../r");
const photoController_1 = require("../Models/photoController");
const globals_1 = require("../globals");
const photo_1 = require("../Models/photo");
const query_1 = require("../Models/query");
const async = require("async");
const process_1 = require("../process");
const keywordController_1 = require("../Models/keywordController");
const cache_1 = require("../cache");
const mark_1 = require("../Models/mark");
const MarkRoute_1 = require("./MarkRoute");
const reference_1 = require("../Models/reference");
class ProfileRoute {
    init(expressApp, controllers) {
        this.controllers = controllers;
        this.expressRoute = new r_1.ExpressRoutes(expressApp, 'profile');
        this.DELETE();
        this.GET();
        this.POST();
    }
    POST() {
        this.expressRoute.POST = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const sessionId = req.params.id;
            const ids = req.body.ids;
            const model = req.body.model; //modify using this new model
            //returns of none is selected
            if (ids.length === 0) {
                console.log('No selection');
                res.send({ error: 'No selection' });
                return;
            }
            let tasks = [];
            const controllers = this.controllers;
            //this will run full task after reference photo check
            const runTasks = () => {
                console.log('runTasks for one with tasks length ', tasks.length);
                let queued = false;
                if (tasks.length > globals_1.globals.modifyLimits) {
                    res.send({ queue: tasks.length });
                    queued = true;
                }
                async.parallel(tasks, (err, dataAll) => {
                    cache_1.cache.updateKeyword = true;
                    if (err) {
                        if (!queued)
                            res.send({ error: err });
                        return;
                    }
                    else {
                        //if there is only 1 id, return the model, so it can be updated immediately
                        //a batch will reload the page
                        console.log('updated all profiles');
                        if (!queued) {
                            // console.log('returning modify with no queue ', dataAll[0])
                            res.send({ result: ids.length < 2 ? dataAll[0] : 'batch modified' });
                        }
                        else {
                            controllers.server.log('SYSTEM', ids.length + ' photos have been modified');
                        }
                    }
                });
            };
            //check if reference photo will be added 
            //addReference contains the photoId of the REFERENCE photo and its model
            let addReference = { photoId: '', model: new photo_1.PhotoModel(globals_1.globals.currentServerSettings.fieldDefinitions) };
            if (model.modifyReference) {
                addReference.photoId = model.modifyReferenceId;
                model.modifyReference = false; //reset to false
            }
            if (addReference.photoId) {
                //get the reference photo id path
                this.controllers.photo.factory().FindOne({ _id: addReference.photoId }, (err, ref) => {
                    if (err) {
                        console.log('ERROR in finding reference photo ');
                        res.send({ error: 'error in finding reference photo ' });
                        return;
                    }
                    addReference.model = ref;
                    tasks = this.postBatchUpdate(ids, sessionId, model, addReference);
                    console.log('runTasks with add reference');
                    runTasks();
                    return;
                });
            }
            else {
                tasks = this.postBatchUpdate(ids, sessionId, model, addReference);
                console.log('runTasks without add reference');
                runTasks();
                return;
            }
        });
    }
    //post will update in batches only
    postBatchUpdate(ids, sessionId, newModel, addReference) {
        //create tasks to update each profile. modify each ids with the new model, add reference photo if needed
        //if reference photo is added, update the model again after reference is added
        const tasks = [];
        ids.forEach(e => {
            const task = (callback) => __awaiter(this, void 0, void 0, function* () {
                //update the id with new data
                const currentModel = yield this.controllers.photo.factory().FindOneAsync({ _id: e });
                //#region Authorization
                const authorization = yield photoController_1.PhotoController.ProfileAuthorized(currentModel, sessionId, this.controllers);
                const privilege = authorization.package;
                if (privilege == r_1.PrivilegeType.none || privilege == r_1.PrivilegeType.read) {
                    //not authorize to modify this photomodel
                    callback(null, null);
                }
                //#endregion
                if (!currentModel)
                    callback(null, null);
                newModel.attr = photo_1.PhotoModel.updateCField(currentModel, newModel);
                this.controllers.photo.factory().AddOrUpdate({ _id: e }, newModel, (err, data0) => {
                    if (err) {
                        console.log('error in updating batch update ', err);
                        callback('error in updating batch update ' + err, false);
                        return;
                    }
                    //get ids full model
                    this.controllers.photo.factory().FindOne({ _id: e }, (err, data) => {
                        if (err) {
                            console.log('error in updating batch update ', err);
                            callback('error in updating batch update ' + err, false);
                            return;
                        }
                        //process new reference photo if needed
                        if (addReference.photoId) {
                            //add the reference and copy the files
                            data = photo_1.PhotoModel.addReference(data, addReference.photoId);
                            data.lastReferencePath = data.referencePath.length - 1;
                            const copyPaths = [data.referencePath[data.lastReferencePath].clean, data.referencePath[data.lastReferencePath].mark];
                            r_1.File.CopyFiles(addReference.model.compressPath, copyPaths, (err1, result) => {
                                if (err1) {
                                    console.log('error in updating batch update copyfiles');
                                    callback('error in updating batch update copyfiles', false);
                                    return;
                                }
                                //update the model one more time with the full model
                                this.controllers.photo.factory().AddOrUpdate({ _id: data._id }, data, (err2, data) => {
                                    if (err2) {
                                        console.log('error in updating batch update');
                                        callback('error in updating batch update', false);
                                        return;
                                    }
                                    console.log('updating keywords');
                                    keywordController_1.KeywordController.processKeyword(data, true, this.controllers.keyword, this.controllers.photo, (err, kdata) => {
                                        if (err) {
                                            callback(err, false);
                                            return;
                                        }
                                        console.log('returnning updated photomodel ');
                                        callback(null, data); //successful callback
                                    });
                                });
                            });
                        }
                        else {
                            keywordController_1.KeywordController.processKeyword(data, true, this.controllers.keyword, this.controllers.photo, (err, kdata) => {
                                if (err) {
                                    callback(err, false);
                                    console.log('keyword processed error');
                                    return;
                                }
                                console.log('returnning updated photomodel ');
                                callback(null, data); //successful callback
                            });
                        }
                    });
                });
            });
            tasks.push(task);
        });
        return tasks;
    }
    GET() {
        this.expressRoute.GET = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const queryAny = JSON.parse(req.params.id);
            const query = queryAny;
            console.log('profile: ', query);
            //#region Single Authorization
            let singleAuthorized = false;
            let singleAuthorizedView = false;
            if (query.searchId) {
                const singleModel = yield this.controllers.photo.factory().FindOneAsync({ _id: query.searchId });
                const authorization = yield photoController_1.PhotoController.ProfileAuthorized(singleModel, query.sessionID, this.controllers);
                const privilege = authorization.package;
                if (privilege == r_1.PrivilegeType.read)
                    singleAuthorizedView = true;
                if (privilege == r_1.PrivilegeType.administrator ||
                    privilege == r_1.PrivilegeType.owner ||
                    privilege == r_1.PrivilegeType.readWrite) {
                    singleAuthorized = true;
                    singleAuthorizedView = true;
                }
            }
            const returnUnauthorized = () => res.send({ result: 'You are unauthorized to make any changes.' });
            //#endregion
            if (query.queryType === query_1.QueryType.photoCount) {
                this.getPhotoCount(res);
                return; //break out of GET
            }
            //generating ONE googlemap
            const controller = this.controllers.photo;
            if (query.queryType === query_1.QueryType.googleMap) {
                if (!singleAuthorized) {
                    returnUnauthorized();
                    return;
                }
                this.getGoogleMapCondition(res, query);
                return; //break out of GET
            }
            //getting all reference ids, only reference
            if (query.queryType === query_1.QueryType.referencing) {
                this.getAllReferences(res);
                return; //break out of GET
            }
            if (query.queryType === query_1.QueryType.rotation) {
                if (!singleAuthorized) {
                    returnUnauthorized();
                    return;
                }
                this.rotate(res, query);
                return;
            }
            if (query.queryType === query_1.QueryType.infoStamp) {
                this.batchInfoStamp(res, query);
                return;
            }
            if (query.queryType === query_1.QueryType.clearStamp) {
                this.batchClearStamp(res, query);
                return;
            }
            if (query.queryType == query_1.QueryType.setPrivilege) {
                this.batchPrivilege(res, query);
                return;
            }
            //get one of any card, this is the actual _id
            if (query.searchId) {
                if (!singleAuthorizedView) {
                    returnUnauthorized();
                    return;
                }
                this.getOne(res, query);
                return; //break out of GET
            }
            //search and sort will be called if nothing on the top is called
            //any top calls should return and not hit his part
            //this is use by the input search
            this.getSearchOrSort(query, (err, result) => {
                if (err) {
                    console.log('ERROR getSearchOrSort ', err);
                    res.send({ error: err });
                }
                else {
                    res.send({ result: result });
                }
            });
        });
    }
    batchPrivilege(res, query) {
        return __awaiter(this, void 0, void 0, function* () {
            const newPrivilege = query.package;
            let unauthorizedCount = 0;
            if (query.searchIds.length < 1) {
                res.send({ error: 'No privilege to update.' });
                return;
            }
            for (let i = 0; i < query.searchIds.length; i++) {
                const e = query.searchIds[i];
                let photoModel = yield this.controllers.photo.factory().FindOneAsync({ _id: e });
                const auth = yield photoController_1.PhotoController.ProfileAuthorized(photoModel, query.sessionID, this.controllers);
                if (auth.verified) {
                    photoModel = photo_1.PhotoModel.modifyAuthenticates(photoModel, newPrivilege);
                    yield this.controllers.photo.factory().AddOrUpdateAsync({ _id: photoModel._id }, photoModel);
                }
                else {
                    unauthorizedCount++;
                }
            }
            if (unauthorizedCount < 1) {
                res.send({ result: 'Privilege updated.' });
            }
            else {
                res.send({ result: 'Privilege updated with ' + unauthorizedCount + ' skips.' });
            }
        });
    }
    batchClearStamp(res, query) {
        //if no selection, return with error
        if (query.searchIds.length < 1) {
            console.log('no photos selected');
            res.send({ error: 'batch info has no selected photos' });
            return;
        }
        //create tasks, each task > find photo > create markmodel using lastreferencepath > stamp it
        const tasks = [];
        query.searchIds.forEach(e => {
            tasks.push((callback) => {
                this.controllers.photo.factory().FindOne({ _id: e }, (err, result) => {
                    if (err) {
                        callback(err, null);
                        return;
                    }
                    if (result.mediaType === reference_1.mediaType.video) {
                        callback(null, null);
                        return;
                    }
                    const m = new mark_1.MarkModel(r_1.GUID.create());
                    m.number = result.lastReferencePath;
                    MarkRoute_1.MarkRoute.clear(m, result, (result) => {
                        console.log('batch clear');
                        callback(null, result);
                    });
                });
            });
        });
        //run all task, return 
        res.send({ result: query.searchId }); //return notify
        async.parallel(tasks, (err, result) => {
            if (err) {
                this.controllers.server.log(query.searchId, 'Error in batch clear stamp ' + err); //if error, log
                return;
            }
            this.controllers.server.log(query.searchId, 'Batch clear stamp successful'); //if successful, log
        });
    }
    batchInfoStamp(res, query) {
        //if no selection, return with error
        if (query.searchIds.length < 1) {
            console.log('no photos selected');
            res.send({ error: 'batch info has no selected photos' });
            return;
        }
        //create tasks, each task > find photo > create markmodel using lastreferencepath > stamp it
        const tasks = [];
        query.searchIds.forEach(e => {
            tasks.push((callback) => {
                this.controllers.photo.factory().FindOne({ _id: e }, (err, result) => {
                    if (err) {
                        callback(err, null);
                        return;
                    }
                    if (result.mediaType === reference_1.mediaType.video) {
                        callback(null, null);
                        return;
                    }
                    const m = new mark_1.MarkModel(r_1.GUID.create());
                    m.x = 10;
                    m.y = 10;
                    m.size = 1;
                    m.number = result.lastReferencePath;
                    MarkRoute_1.MarkRoute.infoStamp(result, m, m, (result) => {
                        console.log('batch marked');
                        callback(null, result);
                    });
                });
            });
        });
        //run all task, return 
        res.send({ result: query.searchId }); //return notify
        async.parallel(tasks, (err, result) => {
            if (err) {
                this.controllers.server.log(query.searchId, 'Error in batch info stamp ' + err); //if error, log
                return;
            }
            this.controllers.server.log(query.searchId, 'Batch info stamp successful'); //if successful, log
        });
    }
    //get photomodel > increase rotation value > create new compressPath > apply rotation to compressPath > get rotated image64 > update photomodel > return image64 as result
    rotate(res, query) {
        let model;
        let breaker = false;
        let image64;
        const onError = (err) => {
            if (err) {
                console.log('ERROR photo rotation ', err);
                res.send({ error: err });
                breaker = true;
                return true;
            }
            else {
                false;
            }
        };
        const getModel = (callback) => {
            this.controllers.photo.factory().FindOne({ _id: query.searchId }, (err, result) => {
                if (onError(err))
                    return;
                if (!result) {
                    onError('no model found');
                    return;
                }
                console.log('model retrieved');
                model = result;
                callback();
            });
        };
        const modifyRotation = (callback) => {
            if (breaker)
                return;
            if (!model.rotation) {
                model.rotation = 90; //first triggered rotation
            }
            else {
                model.rotation += 90;
                if (model.rotation > 360)
                    model.rotation = 90;
            }
            // console.log('value rotated ', model.rotation)
            callback();
        };
        const compress = (callback) => {
            if (breaker)
                return;
            const i = new r_1.Image(model.filePath);
            i.CompressAndResize(model.compressPath, globals_1.globals.maxWidth, (err, success) => {
                if (onError(err))
                    return;
                console.log('recompress image');
                callback();
            });
        };
        const rotate = (callback) => {
            if (breaker)
                return;
            r_1.Image.rotate(model.compressPath, model.compressPath, model.rotation, (err, success) => {
                if (onError(err))
                    return;
                console.log('apply rotations');
                callback();
            });
        };
        const rotate2 = (callback) => {
            if (breaker)
                return;
            r_1.Image.rotate(model.compress2Path, model.compress2Path, model.rotation, (err, success) => {
                // if(onError(err)) return;
                // console.log('apply rotations')
                callback();
            });
        };
        const compress2 = (callback) => {
            if (breaker)
                return;
            const i = new r_1.Image(model.filePath);
            i.CompressAndResize(model.compress2Path, globals_1.globals.maxWidth2, (err, success) => {
                // if(onError(err)) return;
                // console.log('recompress2 image')
                callback();
            });
        };
        const image64p = (callback) => {
            if (breaker)
                return;
            r_1.Image.getImage64(model.compressPath, (err, data64) => {
                if (onError(err))
                    return;
                image64 = data64;
                console.log('set image64');
                callback();
            });
        };
        const update = (callback) => {
            if (breaker)
                return;
            this.controllers.photo.factory().AddOrUpdate({ _id: query.searchId }, model, (err, result) => {
                if (onError(err))
                    return;
                console.log('updated model');
                callback();
            });
        };
        async.series([
            getModel,
            modifyRotation,
            compress,
            rotate,
            image64p,
            update
        ], (err, result) => {
            if (onError(err))
                return;
            console.log('rotation succesfful');
            //these can be done after
            compress2(() => rotate2(() => { }));
            res.send({ result: image64 });
            return;
        });
    }
    getPhotoCount(res) {
        this.controllers.photo.factory().GetCount({}, (err, result) => {
            console.log('photo count ', result);
            if (err) {
                res.send({ error: err });
                return;
            }
            res.send({ result: result });
        });
    }
    getSearchOrSort(model, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            const sort = [];
            const returnFields = ['_id', 'authenticates'];
            let result;
            let err = null;
            model.getFields.forEach(e => {
                sort.push({ field: e, decending: model.isDescending });
            });
            if (model.search !== '') {
                if (model.queryType === query_1.QueryType.expandedSearch) {
                    result = yield this.controllers.photo.factory().FindTextAsync(model.search, sort, returnFields).catch(e => err = e);
                    // this.controllers.photo.factory().FindText(model.search, sort, ['_id'], (err, data)=>{
                    //     if(err){
                    //         callback(err, null)
                    //         return
                    //     }
                    //     if(!data){
                    //         callback(err, null)
                    //         return
                    //     }
                    //     callback(null, data)
                    //     return;
                    // })
                }
                else {
                    result = yield this.controllers.photo.factory().FindTextAndAsync(model.search, sort, returnFields).catch(e => err = e);
                    // this.controllers.photo.factory().FindTextAnd(model.search, sort, ['_id'], (err, data)=>{
                    //     if(err){
                    //         callback(err, null)
                    //         return
                    //     }
                    //     if(!data){
                    //         callback(err, null)
                    //         return
                    //     }
                    //     callback(null, data)
                    //     return;
                    // })
                }
            }
            else {
                result = yield this.controllers.photo.factory().FindWhereUnlimitedSortAsync({}, sort, returnFields).catch(e => err = e);
                // this.controllers.photo.factory().FindWhereUnlimitedSort({}, sort, ['_id'], (err, data)=>{
                //     if(err){
                //         callback(err, null)
                //         return
                //     }
                //     if(!data){
                //         callback(null, null)
                //         return
                //     }
                //     // console.log('return results ', data.length)
                //     // console.log('return data ', data)
                //     callback(null, data)
                //     return;
                // })
            }
            if (err) {
                callback(err, null);
            }
            else {
                const allowedResults = [];
                for (let index = 0; index < result.length; index++) {
                    const e = result[index];
                    const auth = yield photoController_1.PhotoController.ProfileAuthorized(e, model.sessionID, this.controllers).catch(e => { throw e; });
                    const privilege = auth.package;
                    if (privilege == r_1.PrivilegeType.read || privilege == r_1.PrivilegeType.readWrite || privilege == r_1.PrivilegeType.owner) {
                        allowedResults.push(e);
                    }
                }
                callback(null, allowedResults);
            }
        });
    }
    getOne(res, model) {
        this.controllers.photo.factory().FindOne({ _id: model.searchId }, (err, data) => {
            if (err) {
                res.send({ error: err });
                return;
            }
            if (!data) {
                res.send({ result: null });
                return;
            }
            res.send({ result: data });
        });
    }
    //only returns all references
    getAllReferences(res) {
        this.controllers.photo.factory().FindAllForFields('isReference', ['true'], ['_id'], (err, data) => {
            if (err) {
                res.send({ error: err });
                return;
            }
            if (!data) {
                res.send({ result: null });
                return;
            }
            res.send({ result: data });
        });
    }
    //decides of google map is a single request or batch request
    getGoogleMapCondition(res, model) {
        if (model.searchIds) {
            if (!model.searchId) {
                res.send({ error: 'You need a requestId' });
                return;
            }
            this.batchGoogleMap(res, model);
            return;
        }
        //otherwise, get one and respond
        this.getGoogleMap(model.searchId, (err, data) => {
            if (err) {
                res.send({ error: err });
                return;
            }
            res.send({ result: data });
        });
    }
    //batch generates google maps and response
    batchGoogleMap(res, model) {
        const requestId = model.searchId;
        const tasks = [];
        model.searchIds.forEach(e => {
            const task = (callback) => {
                this.getGoogleMap(e, (err, data) => {
                    if (err) {
                        callback(err, null);
                        return;
                    }
                    console.log('map generated');
                    callback(null, data);
                });
            };
            tasks.push(task);
        });
        async.parallel(tasks, (err, result) => {
            if (err) {
                console.log('ERROR in getGoogleMaps ', err);
                return;
            }
            console.log('All google maps generated');
            this.controllers.server.log(requestId, 'All Google Maps have been generated');
        });
        res.send({ result: requestId });
    }
    //returns the updated photoModel after map is generated
    getGoogleMap(id, callback) {
        this.controllers.photo.factory().FindOne({ _id: id }, (err, data) => {
            if (err) {
                callback(err, null);
                return;
            }
            if (data.mediaType === reference_1.mediaType.video) {
                callback(null, null);
                return;
            }
            //this is not an error
            if (!data.hasGeoData) {
                const msg = data.fileName + ' does not have Geodata.';
                callback(null, true);
                this.controllers.server.log('SYSTEM', msg);
                return;
            }
            process_1.processor.createGoogleMaps(data, (err, data0) => {
                if (err) {
                    callback(err, null);
                    return;
                }
                const newModel = data0;
                // console.log('google map return model id ', newModel._id)
                this.controllers.photo.factory().AddOrUpdate({ _id: newModel._id }, newModel, (err, data1) => {
                    if (err) {
                        callback(err, null);
                        return;
                    }
                    // console.log('google map return model ', data1)
                    callback(null, data1); //returns the newly updated model
                });
            });
        });
    }
    //init the delete route
    DELETE() {
        //removes by batch
        this.expressRoute.DELETE = (req, res) => {
            const queryAny = JSON.parse(req.params.id);
            const query = queryAny;
            console.log('delete query ', query);
            //delete everything even files
            if (query.destroyAll) {
                this.deleteAll(res);
                return;
            }
            //no item to delete
            if (query.searchIds.length < 1) {
                res.send({ result: 'nothing to delete' });
                return;
            }
            //delete single or batch
            //foreach id, find that model > delete the folder > remove from db
            this.deleteBatch(res, query);
        };
    }
    //does a full reset of db and remove all files
    //remove (db: photo > requests > server settings), 
    //then delete main folder with all files, 
    //then recreate server settings, 
    //then delete all keywords
    //then sends response to client
    deleteAll(res) {
        new Promise(resolve => {
            this.controllers.photo.factory().RemoveAll((err, data) => {
                if (err) {
                    console.log('Delete all error ', err);
                    res.send({ err: err });
                    return;
                }
                resolve();
            });
        }).then(() => {
            return new Promise(resolve => {
                this.controllers.keyword.factory().RemoveAll((err, data) => {
                    if (err) {
                        res.send({ err: err });
                        return;
                    }
                    resolve();
                });
            });
        }).then(() => {
            return new Promise(resolve => {
                this.controllers.server.factory().RemoveAll((err, data) => {
                    if (err) {
                        res.send({ err: err });
                        return;
                    }
                    resolve();
                });
            });
        }).then(() => {
            return new Promise(resolve => {
                r_1.File.deleteFolder(globals_1.globals.storage, (err, data) => {
                    if (err) {
                        //WARN BUT CONTINUE CANNOT FIX THIS RIGHT NOW
                        //res.send({err: err})
                        //return
                    }
                });
                resolve();
            });
        }).then(() => {
            return new Promise(resolve => {
                this.controllers.server.checkModelExist((err, success) => {
                    if (err) {
                        res.send({ err: err });
                        return;
                    }
                    resolve();
                });
            });
        }).then(() => {
            return new Promise(resolve => {
                this.controllers.keyword.factory().RemoveAll((err, kdata) => {
                    if (err) {
                        res.send({ err: err });
                        return;
                    }
                    resolve();
                });
            });
        }).then(() => {
            return new Promise(resolve => {
                r_1.NetworkConfig.setNetworkMode(globals_1.globals.isProduction ? 'dhcp' : 'static', (err, result) => {
                    resolve();
                });
            });
        }).then(() => {
            return new Promise(resolve => {
                r_1.CSystem.setHostname(globals_1.globals.isProduction ? 'centos' : 'centos02', 'admin', (result) => {
                    resolve();
                });
            });
        }).then(() => {
            return new Promise(resolve => {
                this.controllers.video.factory().RemoveAll((err, vdata) => {
                    if (err) {
                        res.send({ err: err });
                        return;
                    }
                    resolve();
                });
            });
        })
            .then(() => {
            return new Promise(resolve => {
                this.controllers.section.factory().RemoveAll((err, vdata) => {
                    if (err) {
                        res.send({ err: err });
                        return;
                    }
                    res.send({ result: 'all deleted' });
                    r_1.CSystem.reboot('admin', () => { });
                });
            });
        });
        // this.controllers.photo.factory().RemoveAll((err, data)=>{
        //     if(err){
        //         console.log('Delete all error ', err)
        //         res.send({err: err})
        //         return
        //     }
        //     this.controllers.keyword.factory().RemoveAll((err, data)=>{
        //         if(err){
        //             res.send({err: err})
        //             return
        //         }
        //         this.controllers.server.factory().RemoveAll((err, data)=>{
        //             if(err){
        //                 res.send({err: err})
        //                 return
        //             }
        //             File.deleteFolder(globals.storage, (err, data)=>{
        //                 if(err){
        //                     //WARN BUT CONTINUE CANNOT FIX THIS RIGHT NOW
        //                     //res.send({err: err})
        //                     //return
        //                 }
        //                 this.controllers.server.checkModelExist((err, success)=>{
        //                     if(err){
        //                         res.send({err: err})
        //                         return
        //                     }
        //                     this.controllers.keyword.factory().RemoveAll((err, kdata)=>{
        //                         if(err){
        //                             res.send({err: err})
        //                             return
        //                         }
        //                         this.controllers.video.factory().RemoveAll((err, vdata)=>{
        //                             if(err){
        //                                 res.send({err: err})
        //                                 return
        //                             }
        //                             res.send({result: 'all deleted'})
        //                         })
        //                     })
        //                 })
        //             })
        //         })
        //     })
        // })
    }
    //removes a batch of selected profiles, db and files
    deleteBatch(res, model) {
        const tasks = [];
        model.searchIds.forEach(e => {
            const task = (callback) => {
                console.log('finding for del ', e);
                this.controllers.photo.factory().FindOne({ _id: e }, (err, pModel) => {
                    if (err) {
                        callback(err, null);
                        return;
                    }
                    console.log('data1 ', pModel);
                    this.controllers.server.del(pModel.folderPath, (err, success) => {
                        if (err) {
                            callback(err, null);
                            return;
                        }
                        this.controllers.photo.factory().Remove({ _id: e }, (err, data) => {
                            if (err) {
                                //known error due to stamping, it will clear on server restart
                                console.log('ERROR deleting photo ', err);
                            }
                            console.log('db entry deleted ', e);
                            keywordController_1.KeywordController.processKeyword(pModel, false, this.controllers.keyword, this.controllers.photo, (err, kdata) => {
                                if (err) {
                                    callback(err, null);
                                    return;
                                }
                                console.log('keyword entry deleted ', e);
                                this.controllers.section.removeByPhotoReferenceId(pModel._id, (err, result) => {
                                    if (err) {
                                        callback(err, null);
                                        return;
                                    }
                                    this.controllers.video.removeByPhotoModel(pModel, (err, result) => {
                                        if (err) {
                                            callback(err, null);
                                            return;
                                        }
                                        callback(null, e); //successful callback
                                    });
                                });
                            });
                        });
                    });
                });
            };
            tasks.push(task);
        });
        async.parallel(tasks, (err, result) => {
            if (err) {
                console.log('delete errors ', err);
                res.send({ error: err });
                return;
            }
            res.send({ result: model.searchIds });
        });
    }
}
exports.ProfileRoute = ProfileRoute;
//# sourceMappingURL=ProfileRoute.js.map