import { PhotoController } from "./Models/photoController";
import { PhotoModel } from "./Models/photo";
import { System } from "./r";

//Only use for remodeling. Not part of the actual application.

export class RemapStorage{
    controller = new PhotoController()
    total: number
    current = 0
    constructor(private original: string, private replace: string, private test: boolean, private callback:(err, result)=>void){
        this.controller.factory().FindAll({}, (err, result: Array<PhotoModel>)=>{
            if(err){
                console.log('ERROR remap finding all ', err)
                this.callback(err, null)
                return;
            }
            this.total = result.length
            console.log('FOUND ', this.total)
            result.forEach(e=>{
                console.log(e.filePath)
            })
            if(test) console.log('MOCK RESULT ')
            result.forEach(e=>{
                console.log('current ', this.current)
                if(this.test){
                    this.current++;
                    console.log(this.remapOne(e).filePath)
                }else{
                    this.remapOne(e)
                }
            })
            console.log('FINISH')
        })
    }
    remapOne(model: PhotoModel){
        //update this area if any new paths are added
        model.folderPath = model.folderPath.replace(this.original, this.replace)
        model.compressPath = model.compressPath.replace(this.original, this.replace)
        model.compress2Path = model.compress2Path.replace(this.original, this.replace)
        model.mergerPath = model.mergerPath.replace(this.original, this.replace)
        model.filePath = model.filePath.replace(this.original, this.replace)
        for(let i = 0; i < model.referencePath.length; i++){
            model.referencePath[i].clean = model.referencePath[i].clean.replace(this.original, this.replace)
            model.referencePath[i].mark = model.referencePath[i].mark.replace(this.original, this.replace)
            if(model.referencePath[i].undo) model.referencePath[i].undo = model.referencePath[i].undo.replace(this.original, this.replace)
        }
        if(this.test){
            return model
        }else{
            this.controller.factory().AddOrUpdate({_id: model._id}, model, (err, result)=>{
                if(err){
                    console.log('ERROR remap one update ', err)
                    this.callback(err, null)
                    return;
                }
                console.log('updated ', model.fileName)
                this.current++;
                if(this.current === this.total){
                    console.log('REMAP COMPLETED!')
                }
            })
            return null
        }
    }
}
const arg = System.CaptureCLIArugments()
let test: boolean
if(arg[2].arg === 'false'){
    test = false
}else{
    test = true
}
if(arg[1].arg === '*'){
    arg[1].arg = ''
}
console.log(arg)

//example: node .\remapStorage.js './Storage' './Static/Storage' false
//this will replace ./Storage with ./Static/Storage, set true for whatif result
const r = new RemapStorage(arg[0].arg, arg[1].arg, test, null)