import { VideoModel } from "./r";
import { globals } from "./globals";
import { videoProcessor } from "./videoProcess";
import {File} from './r'
import { VideoController } from "./Models/videoController";
import * as moment from 'moment'
import { Ctrl } from "./Server/server";
export class Queue{
    private queues:QueueModel[]
    private concurrent = 0
    constructor(private controller: VideoController, private controllers: Ctrl){
        this.queues = []
        setInterval(()=>{
            this.queues.forEach(e=>{
                if(!e.isActive){
                    this.process(e)
                }
            })
            if(this.concurrent > 0){
                console.log('concurrent ', this.concurrent)
            }
            //console.log('check for queues to process ', this.queues)
        }, globals.encodingPollTime*1000)

        //memory leak issue when FindAll is call too many times
        setInterval(()=>{
            this.expireDb()
        }, 5000) //time for this is not offically set
    }
    add(model:VideoModel, filePath: string){
        if(model.status !== 'uploaded'){    
            console.log('ERROR: video was not uploaded and will not be added to queue, id ', model._id)
            return;
        }
        const q = new QueueModel(model, filePath)
        this.queues.push(q)
    }
    remove(model:VideoModel){
        for(let i = 0; i < this.queues.length; i++){
            if(this.queues[i].model._id === model._id){
                this.queues.splice(i, 1)
                console.log('Queue removed, id ', model._id)
                console.log('remaining queue ', this.queues.length)
                return;
            }
        }
        console.log('ERROR: No queue could be removed, id ', model._id)
    }

    //expires a video entry in the db that was created, but video was never uploaded
    expireDb(){
        this.controller.factory().FindAll({}, (err, data)=>{
            const list = <VideoModel[]>data
            list.forEach(e=>{
                const condition = new Date() > moment(e.createdDate).add(globals.expireVideoDatabase, 's').toDate() && !e.status
                if(condition){
                    this.controller.factory().Remove({_id: e._id}, (err, data1)=>{
                        if(err){
                            console.log('ERROR in expiring db ', err)
                            return;
                        }
                        console.log('Expired db ', e._id)
                    })
                }
            })
        })
    }
    process(queue: QueueModel){
        if(this.concurrent > globals.concurrentEncodingLimit){
            console.log('Skipping queue, concurrent limit is over ' + this.concurrent)
            return;
        }
        console.log('Queue process begin for ', queue.model._id)
        this.concurrent++;
        queue.isActive = true;
        const process = new videoProcessor(queue.filePath, queue.model, this.controller, (err, model:VideoModel)=>{ 
            if(err){
                console.log('Process error ', err)
                return
            }
            model.status = 'completed' //model is mark as ready (ready to view on site)
            //set video as ready in the db
            this.controller.factory().AddOrUpdate({_id: model._id}, model, (err, data)=>{ 
                if(err){
                    console.log('set ready error ', err)
                    return
                }
                console.log(model._id + ' has been process and is ready to be viewed')
                this.controllers.photo.adjustFilePath(model.videoPathFile, model._id, (err, result)=>{})
                this.concurrent--;
                //delete the temp folder which should be empty since the file was moved
                // File.deleteFolder(queue.fileFolder, (result)=>{
                //     this.remove(model)
                //     
                // }) 
            })
        });
        process.Start() 
    }
}
export class QueueModel{
    fileFolder: string //this folder will be deleted after process!
    isActive = false
    constructor(public model: VideoModel, public filePath: string ){
        this.fileFolder = File.getFolder(filePath)
    }
}