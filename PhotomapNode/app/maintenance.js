"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("./r");
const globals_1 = require("./globals");
const fs = require("fs");
const async = require("async");
const _ = require("lodash");
class Maintenance {
    constructor(controllers, callback) {
        this.controllers = controllers;
        this.callback = callback;
        r_1.File.createFolder(globals_1.globals.storage);
        r_1.File.createFolder(globals_1.globals.storage + globals_1.globals.requestName);
        new Promise(resolve => {
            this.postDel(() => {
                console.log('postDel');
                resolve();
            });
        }).then(() => {
            return new Promise(resolve => {
                this.clearUnknownFolders(globals_1.globals.storage, () => {
                    console.log('clearUnknownFolders');
                    resolve();
                });
            });
        }).then(() => {
            return new Promise(resolve => {
                this.clearExpiredRequest(globals_1.globals.storage + globals_1.globals.requestName, () => {
                    console.log('clearExpiredRequest');
                    resolve();
                });
            });
        }).then(() => {
            return new Promise(resolve => {
                this.controllers.server.expireLogs(globals_1.globals.maxLogs, (err) => {
                    if (err)
                        console.log(err);
                    console.log('expireLogs');
                    resolve();
                });
            });
        }).then(() => {
            return new Promise(resolve => {
                this.clearUnknownFiles(globals_1.globals.storage + '/', () => {
                    console.log('clearUnknownFiles');
                    resolve();
                });
            });
        }).then(() => {
            return new Promise(resolve => {
                this.refreshSSL(globals_1.globals.SSL + '/', () => {
                    console.log('ssl refreshed');
                    resolve();
                });
            });
        }).then(() => {
            callback();
        });
    }
    static factory(controllers, callback) {
        new Maintenance(controllers, callback);
    }
    clearExpiredRequest(requestPath, callback) {
        if (!fs.existsSync(requestPath)) {
            callback();
            return;
        }
        this.controllers.request.deleteExpiredDb((err, result) => {
            if (err) {
                console.log('ERROR expiring requests ', err);
                return;
            }
            const keepIds = _.map(result, '_id');
            const keepIdsFiles = [];
            keepIds.forEach(e => {
                keepIdsFiles.push(e + '.zip');
            });
            // console.log('none-expired entries ', keepIdsFiles)
            // console.log('request path ', requestPath)
            const actualFiles = r_1.File.getAllFiles(requestPath);
            const toDelete = _.difference(actualFiles, keepIdsFiles); //anything left that doesn't match right
            console.log('request toDelete ', toDelete);
            toDelete.forEach(e => {
                r_1.File.delIfExist(requestPath + '\\' + e, () => { });
            });
            callback();
        });
    }
    //removes any folders that doesn't have the same name the photo ids and is not part of the exclude array
    clearUnknownFolders(storagePath, callback) {
        if (!fs.existsSync(storagePath))
            return;
        const folders = r_1.File.getSubfolders(storagePath);
        this.controllers.photo.factory().FindAllReturnFields({}, ['_id'], (err, result) => {
            if (err) {
                console.log('ERROR clearUnknownFolder ', err);
            }
            const exclude = [globals_1.globals.requestName.substr(1, globals_1.globals.requestName.length)];
            console.log('puarge exluded ', exclude);
            let entries = _.map(result, '_id');
            entries = _.union(exclude, entries);
            const toDelete = _.difference(folders, entries); //anything left that doesn't match right
            console.log('toDelete ', toDelete);
            // console.log('folder ', folders)
            toDelete.forEach(e => {
                r_1.File.deleteFolder(storagePath + '\\' + e, () => { });
            });
            callback();
        });
    }
    clearUnknownFiles(storagePath, callback) {
        const files = r_1.File.getAllFiles(storagePath);
        if (files.length < 1) {
            callback();
            return;
        }
        files.forEach(e => {
            fs.unlinkSync(storagePath + e);
        });
        callback();
    }
    postDel(callback) {
        this.controllers.server.factory().FindAll({ type: 'delete' }, (err, data) => {
            if (err) {
                console.log('Server settings did not create!! ', err);
                callback(err, null);
                return;
            }
            if (!data) {
                callback(null, null);
                return;
            }
            const tasks = [];
            data.forEach(e => {
                tasks.push((callback) => {
                    r_1.File.deleteFolder(e.file, (err, result) => {
                        callback(err, result);
                    });
                });
            });
            async.parallel(tasks, (err, result) => {
                if (err) {
                    console.log('ERROR in file delete ', err);
                    callback(err, null);
                    return;
                }
                this.controllers.server.factory().Remove({ type: 'delete' }, (err, data) => {
                    if (err) {
                        console.log('Error in log update', err);
                        return;
                    }
                    callback(null, true);
                });
            });
        });
    }
    //new keys are stored in temp folder inside the key folder, and they should be moved out
    refreshSSL(keysPath, callback) {
        const files = r_1.File.getAllFiles(keysPath + 'temp/');
        if (files.length < 1) {
            console.log('No keys to refresh');
            callback(null, null);
            return;
        }
        files.forEach(e => {
            r_1.File.move2(keysPath + 'temp/' + e, keysPath + e, err => {
                if (err)
                    console.log(err);
                if (!err)
                    console.log(e, ' was moved');
            });
        });
        callback(null, null);
    }
}
exports.Maintenance = Maintenance;
//# sourceMappingURL=maintenance.js.map