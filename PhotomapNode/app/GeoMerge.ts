import {File, Image, GoogleMaps, GUID} from './r'
import * as async from 'async'
import * as fs from 'fs'
export class GeoMerge{
    nullImage = '../Assets/nullImage.jpg'
    tempPath = 'temp'
    nullImageCount = 0;
    validFolders : string[] = []
    invalidFolders : string[] = []
    matches:Array<{source: string, target: string}> = []
    constructor(private input: string, private output: string, private key: string){
        
    }
    Start(){
        if(!fs.existsSync(this.input)){
            throw 'Your input path does not exist'
        }

        if(!fs.existsSync(this.nullImage)){
            throw 'Your null image does not exist'
        }
        
        //check if any jpg exist anywhere
        this.folderValidate(this.input)

        if(this.validFolders.length === 0){
            throw 'No folder or subfolder contains .jpg'
        }

        File.createFolder(this.tempPath)
        this.createFolder()
        this.geoCreate()
        console.log(this.matches)
    }
    folderValidate(input: string, prime = true){ //prime means the first folder
        const folders = File.getSubfolders(input)
        folders.forEach(e=>{
            const actual = input + e //the actual subfolder path

            //this is use for recursive inner folders and run this function again
            if(File.getSubfolders(actual).length > 0){
                File.getSubfolders(actual).forEach(subs=>{
                    let actualSub = actual + '/' + subs + '/'
                    //console.log('subsub ', actualSub)
                    this.folderValidate(actualSub, false)
                })
            }

            //check if subfolder has jpg
            const valid = File.getAllFiles(actual).some(e1=>{
                if(File.getExtension(e1) === 'jpg'){
                    return true
                }
            })

            if(valid){
                this.validFolders.push(actual)
            }else{
                this.invalidFolders.push(actual)
            }
        })

        //check if this folder has photos
        const validPrime = File.getAllFiles(input).some(e1=>{
            if(File.getExtension(e1) === 'jpg'){
                return true
            }
        })
        if(validPrime){
            if(prime){
                this.validFolders.push(input)
            }else{
                this.validFolders.push(input)
            }
        }else{
            if(prime){
                this.invalidFolders.push(input)
            }else{
                this.invalidFolders.push(input)
            }
        }
    }
    createFolder(){
        this.validFolders.forEach(e=>{
            const actual = (this.output + e).replace(this.input, '/').replace('//', '/')
            //console.log(actual)
            File.createFolder(actual)
            this.matches.push({source: e, target: actual})
        })
    }
    geoCreate(){
        const tasks: any[] = []
        this.matches.forEach(e=>{
            const create = (callback:(err, result)=>void)=>{
                this.geoMergeBulk(e.source, e.target, callback)
            }
            tasks.push(create)
        })
        console.log('tasks count ', tasks.length)
        async.parallel(tasks, (err, data)=>{
            console.log(data)
            console.log(err)
            console.log('all process completed')
            //delete the temp folder
            File.deleteFolder(this.tempPath, r=>{
                if(this.nullImageCount>0) console.log(this.nullImageCount + ' does not have geotag data.')
                console.log('All task completed')
            })
        })
    }

    //no error callback
    geoMergeBulk(input: string, output: string, callback:(err, data)=>void){
        let taskCounter = 0;
        const subtask: any[] = []
        const files = File.getAllFiles(input)
        const taskLimit = files.length-1
        files.forEach(e=>{
            if(File.getExtension(e) === 'jpg'){
                const create = (callbackc:(err, result)=>void)=>{
                    this.geoMergeOne(input + '/' + e, output + '/' + e, e=>callbackc(e, true))
                }
                subtask.push(create)
            }
        })
        async.parallel(subtask, callback)
    }
    geoMergeOne(input: string, output: string, callback:(err)=>void){
        const i = new Image(input)
        console.log('Getting Google map for ', input)
        i.GeoLocation((err, dataGeo)=>{
            let hasGeotag = true
            if(err){
                hasGeotag = false
            }
            if(!dataGeo){
                hasGeotag = false
            }
            const tempfileResized = this.tempPath + '/' + GUID.create(true) + File.getFileOnlyOrParentFolderName(input)
            const tempfileRotate = this.tempPath + '/' + GUID.create(true) + File.getFileOnlyOrParentFolderName(input)
            console.log('compressing orginal to ',tempfileResized )
            
            const compressResizer = (image: Image, hasGeo: boolean)=>{
                image.CompressAndResize(tempfileResized, 640, (err, data1)=>{
                    if(err){
                        console.log('ERROR CompressAndResize', err)
                        callback(err)
                        throw err
                    }
                    output = output.replace('//', '/')
                    const file = GUID.create(true) + '.png'
                    const tempfile = this.tempPath + '/' + file
                    if(hasGeo && dataGeo !== null){
                        const m = new GoogleMaps(this.key)
                        m.mapImage2(dataGeo.latitude, dataGeo.longitude, tempfile, (err, dataNull1)=>{
                            if(err){
                                console.log('ERROR mapImage2 ', err)
                                callback(err)
                                throw err
                            }
                            if(fs.statSync(tempfile).size === 0){
                                console.log('ERROR mapImage2, a map image did not return')
                                throw 'ERROR mapImage2, a map image did not return. Check your internet connection. Check your API key works for Google Map Static'
                            }
                            console.log('output path ', output)
                            Image.imageMerge([tempfile, tempfileResized], output, (err, dataNull2)=>{
                                if(!err){
                                    console.log('Image merged (map)')
                                    callback(null)
                                }else{
                                    callback(err)
                                    console.log('Error in image merge (map) ', err, '. Image maybe corrupted.')
                                }
                            })
                        })
                    }else{
                        // console.log('IMAGE')
                        // console.log(this.nullImage)
                        // console.log(tempfileResized)
                        // console.log(output)
                        Image.imageMerge([this.nullImage, tempfileResized], output, (err, dataNull2)=>{
                            if(!err){
                                console.log('Image merged')
                                callback(null)
                                this.nullImageCount++
                            }else{
                                callback(err)
                                console.log('Error in image merge ', err, '. Image maybe corrupted.')
                            }
                        })
                    }
                })
            }
            
            Image.autoRotate(input, tempfileRotate, (err, data)=>{
                if(err){
                    console.log('Error in auto rotate')
                    callback(err)
                    return
                }
                const i1 = new Image(tempfileRotate)
                compressResizer(i1, hasGeotag)
            })
            
        })
    }
}