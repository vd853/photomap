import * as async from 'async'
import { RequestController } from './Models/requestController';
import { RequestModel, DownloadType } from './Models/request';
import { PhotoController } from './Models/photoController';
import * as _ from 'lodash'
import {File, Image, GUID, VideoModel, Encoder } from "./r";
import { globals } from './globals';
import { Ctrl } from './Server/server';
import { SectionModel } from './Models/section';
import { PhotoModel } from './Models/photo';
import { mediaType } from './Models/reference';
export class Batch{
    constructor(
        private model: RequestModel,
        private controllers: Ctrl, 
        private callback:(err, model:RequestModel | null)=>void
    ){
        File.createFolder(globals.storage + globals.requestName)
        switch(model.downloadType){
            case DownloadType.high:
                this.process(['filePath']);
                break;
            case DownloadType.reference:
                this.processCurrentReferences();
                break;
            case DownloadType.merge:
                this.processMerge();
                break;
            case DownloadType.clipDownload:
                this.processClips();
                break;
            default:
                break;
        }
    }
    process(types: string[]){
        this.controllers.photo.factory().FindAllForFields('_id', this.model.requestIds, types, (err, list)=>{
            if(err){
                this.model.status = 'Batch download error in prepare ' + err.toString()
                console.log('Batch download error in prepare ', err)
                this.callback(err, this.model)
                return;
            }  
            const files = _.map(list, 'filePath')
            this.zip(files)
        })
    }

    //batch zips all the current references
    processCurrentReferences(){
        this.controllers.photo.factory().FindAllForFields('_id', this.model.requestIds, ['referencePath', 'lastReferencePath', 'mediaType'], (err, list)=>{
            if(err){
                this.model.status = 'Batch download error in prepare ' + err.toString()
                console.log('Batch download error in prepare ', err)
                this.callback(err, this.model)
                return;
            }  
            const files: string[] = []
            list.forEach((e:PhotoModel)=>{
                console.log('checking ', e)
                if(e.mediaType === mediaType.photo)
                    files.push(e.referencePath[e.lastReferencePath].mark)
            })
            this.zip(files)
        })
    }

    //prepares a zip file after processing all merge. merge is process async parallel
    processMerge(){
        this.controllers.photo.factory().FindAllForFields('_id', this.model.requestIds, ['mergerPath', 'compressPath', 'referencePath', 'lastReferencePath', 'mediaType'], (err, list)=>{
            if(err){
                this.model.status = 'Batch download error in prepare ' + err.toString()
                this.callback(err, this.model)
                return;
            }  
            const files: string[] = []
            const tasks: any = []
            list.forEach((e:PhotoModel)=>{
                if(e.mediaType === mediaType.photo){
                    const task = (callback1:(err, result)=>void)=>{
                        const mark = e.referencePath[e.lastReferencePath].mark;
                        const compress = e.compressPath;
                        const merge = e.mergerPath;
                        files.push(merge);
                        Image.imageMerge([compress, mark], merge, callback1);
                    }
                    tasks.push(task)
                }
            })
            async.parallel(tasks, (err, data)=>{
                if(err){
                    this.callback(err, null)
                    return
                }
                console.log('merge files ', files)
                this.zip(files)
            })
        })
    }

    processClips(){
        const tempFolder = globals.storage + globals.requestName + '/temp' + GUID.create() + '/';
        const tasks: any = [];
        File.createFolder(tempFolder);
        let count = 1;
        //finds a section, finds the video of that section, begin cropping one
        const task = (id: string, callback:(err, data)=>void)=>{
            this.controllers.section.factory().FindOne({_id: id}, (err, sModel:SectionModel)=>{
                console.log('finding section model')
                if(err){
                    callback(err, null)
                    return;
                }
                this.controllers.photo.factory().FindOne({_id: sModel.referenceId}, (err, pModel:PhotoModel)=>{
                    if(err){
                        console.log(err)
                        callback(err, null)
                        return;
                    }
                    console.log('video model ', pModel.someId[0])
                    this.controllers.video.factory().FindOne({_id: pModel.someId[0]}, (err, vModel:VideoModel)=>{
                        if(err){
                            console.log(err)
                            callback(err, null)
                            return;
                        }
                        console.log('video path ', File.reparsePath(vModel.videoPathFile))
                        const e = new Encoder(File.reparsePath(vModel.videoPathFile)) //need this path parsing for unit test
                        const newFile = count + '_' + sModel.comment.substr(0, 5) + '_' + GUID.create(true)
                        const durationBetween = (sModel.endPercent - sModel.startPercent)  * sModel.duration;
                        const start = sModel.startPercent * sModel.duration;
                        console.log('creating clip at ', newFile)
                        e.EncodeFF(tempFolder, newFile, undefined, undefined, start, durationBetween, 1028, undefined, callback)
                    })
                })
            })
        }
        this.model.requestIds.forEach(e=>{
            tasks.push(async.apply(task, e));
        })
        async.parallel(tasks, (err, result)=>{
            if(err){
                this.callback(err, null);
                return;
            }
            console.log('creating zip files for ', result)
            this.zip(result)
        })
    }

    //use by other process to start the zip process
    zip(files: string[]){
        File.zipFiles(files, this.model.outputPath, (err, data)=>{
            if(err){
                this.model.status = 'Batch download error in prepare'
                this.callback(err, this.model)
                return;
            }   
            this.model.status = 'Batch download available'
            this.callback(null, this.model)
        })
    }
}