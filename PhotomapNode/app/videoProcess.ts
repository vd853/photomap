//import { VideoModel } from "./m";
import { Encoder, GUID, File, Image } from "./r";
import { globals } from "./globals";
import * as fs from 'fs'
import * as path from 'path'
import * as async from 'async'
// import { Video } from "./Model/Video";
import { VideoModel } from "./r";
import { VideoController } from "./Models/videoController";
export class videoProcessor{
    private accuProgress = 0 //calculated base on raw points
    private actualProgress = 0 //from 0 to 100
    private noneConversionPoints = 40 //20 for thumbnail, 20 from preview
    private conversionPointsQ0 = 0 //these are current conversion points for q0 and q1
    private conversionPointsQ1 = 0
    private encoder: Encoder
    constructor(private filePath: string, private model: VideoModel, private controller: VideoController, private callback:(err: any, data:VideoModel | null)=>void){
        if(!fs.existsSync(filePath)){
            console.log('Error Video file does not exist')
            this.callback('video file does not exist', null)
            return
        }
    }
    public Start(){
        this.model.fileName = File.getFileOnlyOrParentFolderName(this.filePath)
        console.log('this.model.fileName ', this.model.fileName)
        this.model.parentPath = '.\\' + path.relative(__dirname, File.getPathOnly(this.filePath)) + '\\'
        // console.log('folderPath ', folderPath)
        // process.exit();

        //if not win32, assume it is on a linux
        this.encoder = new Encoder(this.filePath, process.platform === "win32")
        this.encoder.Emitter.on('EncodeFF_Progress', (data: {reference: string, progress: number})=>{
            console.log('progress data ', data)
            if(data.reference === 'q0'){
                this.progressUpdate(undefined, data.progress)
            }
            if(data.reference === 'q1'){
                this.progressUpdate(undefined, undefined, data.progress)
            }
        })
        if(this.model._id === null){
            this.model._id = GUID.create()
            console.log('WARNING: you pass in a model without an ID!')
        }
        //this should include full path into the folder where the video datas are stored
        this.model.videoPath = this.model.parentPath + globals.QOriginalFolder + '/'
        this.model.videoPathFile = this.model.videoPath + this.model.fileName
        this.model.videoQ0 = this.model.parentPath + globals.Q0Folder 
        this.model.videoQ1 = this.model.parentPath +  globals.Q1Folder
        this.model.thumbOriginalPath = this.model.parentPath +  globals.thumbFolder + '/original'
        this.model.thumbPath = this.model.parentPath +  globals.thumbFolder + '/compress'
        this.model.thumbPreviewPath = this.model.parentPath + globals.thumbFolder + '/' + globals.thumbPreviewName
        this.getSpecs(()=>{
            //move file to Original folder
            File.move(this.filePath, this.model.videoPathFile, (err)=>{
                if(err){
                    console.log('video file move error ', err)
                    this.callback(err, null)
                    return
                }else{
                    this.encoder.AbsoluteFilePath = this.model.videoPathFile
                    setTimeout(() => {
                        this.asyncTask();
                    }, 1000);
                }
            })
        })
    }
    private getSpecs(callback:()=>void){
        console.log('getting video specs')
        this.encoder.getVideoSpec((err, d)=>{
            this.model.dHorizontal = d.width
            this.model.dVertical = d.height
            console.log('got video specs ', this.model.dHorizontal, ' ', this.model.dVertical)
            callback()
        })
    }
    private asyncTask(){
        console.log('video model ', this.model);
        const Q0T = (callback:(error, data)=>void)=>{
            this.Q0(callback);
        }
        const Q1T = (callback:(error, data)=>void)=>{
            this.Q1(callback);
        }
        const ThumbT = (callback:(error, data)=>void)=>{
            this.thumbnail(callback);
        }
        const prevT = (callback:(error, data)=>void)=>{
            this.thumbPreview(callback);
        }

        //progress points 100 100 20 20 = 240
        async.parallel([Q0T, Q1T, ThumbT, prevT], (error, data)=>{ //Q0T, Q1T, ThumbT
            console.log('async completed ', error, data)
            this.callback(null, this.model) //final callback
        })
    }
    
    //Q0 will be 720p
    private Q0(callback){
        if(this.model.dHorizontal > 1280){
            this.encoder.EncodeFF(
                this.model.videoQ0, 
                '720', 
                1280,
                720,
                undefined,undefined,
                undefined,
                'q0',
                (err, data)=>{
                    this.model.videoQ0File = data
                    callback(null, true);
                }
            )
            this.model.hasQ0 = true;
        }else{
            this.model.hasQ0 = false;
            callback(null, true);
            console.log('Skip due to low resolution 720p')
        }
    }
    //Q0 will be 1080p
    private Q1(callback){
        if(this.model.dHorizontal > 1920){
            this.encoder.EncodeFF(
                this.model.videoQ1, 
                '1080', 
                1920,
                1080,
                undefined,undefined,
                undefined,
                'q1',
                (err, data)=>{
                    this.model.videoQ1File = data
                    callback(null, true);
                }
            )
            this.model.hasQ1 = true;
        }else{
            this.model.hasQ1 = false;
            callback(null, true);
            console.log('Skip due to low resolution 1080p')
        }
    }
    private thumbnail(callback){
        // console.log('thumbnail params ', this.model.thumbOriginalPath, ' ', this.model.dHorizontal, ' ', globals.thumbCount)
        this.encoder.screenshot(this.model.thumbOriginalPath, this.model.dHorizontal, globals.thumbCount, ()=>{
            fs.readdir(this.model.thumbOriginalPath, (err, list)=>{
                if(err){
                    console.log('thumbnail error ', err)
                    callback(err, null)
                }
                let endCounter = 0;
                list.forEach(e=>{
                    const i =  new Image(this.model.thumbOriginalPath + '/' + e)
                    i.CompressAndResize(this.model.thumbPath + '/' + e, globals.thumbHorizontal, ()=>{
                        endCounter++
                        if(endCounter >= list.length){
                            this.progressUpdate(20)
                            callback(null, true)
                        }
                    })
                })
            })
        }, '%00i.png', 'thumbnail')
    }
    private thumbPreview(callback){
        const tempFolder = globals.storage + '/' + GUID.create();
        this.encoder.FramesToVideo(this.model.thumbPreviewPath, globals.panelHorizontal, 25, tempFolder, (err, data)=>{this.progressUpdate(20); callback(err, data)})
    }
    private progressUpdate(points?: number, q0?: number, q1?: number){
        //base on the parameter it will set the current conversion progress and acculate the none conversions progress
        this.conversionPointsQ0 = (q0? q0:this.conversionPointsQ0)
        this.conversionPointsQ1 = (q1? q1:this.conversionPointsQ1)
        this.accuProgress+= (points? points: 0)

        let conversionPoints = 0

        if(this.model.hasQ0.toString() === 'true'){
            conversionPoints = 100
        }
        
        if(this.model.hasQ1.toString() === 'true'){
            console.log('I should not see this')
            conversionPoints = 100
        }

        if(this.model.hasQ0.toString() === 'true' && this.model.hasQ1.toString() === 'true'){
            conversionPoints = 200
        }
        console.log('conversion points ', conversionPoints)

        //this will caculate the actual progress within 0-100 percent
        this.actualProgress = (this.accuProgress + this.conversionPointsQ0 + this.conversionPointsQ1)/(this.noneConversionPoints + conversionPoints) 
        console.log('Progress: ', this.actualProgress)
        
        this.model.status = 'progress ' + ((this.actualProgress)*100).toString().substr(0, 5);
        this.controller.factory().AddOrUpdate({_id: this.model._id}, this.model, (err, data)=>{
            if(err){
                console.log('Progress update error ', err)
            }
        })
    }
}