// import * as fs from 'fs';
// import { File } from './r';
// import * as async from 'async'
// export class NetworkConfig{
//     data: any;
//     static networkConfigPathCentos = '/etc/sysconfig/network-scripts/ifcfg-eth0'
//     constructor(private configPaths:string[]){
//     }
//     write(field: string, value: string, callback:(err,result)=>void){
//         const tasks = []
//         this.configPaths.forEach(e=>{
//             if(fs.existsSync(e)){
//                 const task = (callback:(err, result)=>void)=>{
//                     this.writePrime(e, field, value, callback)
//                 }
//                 tasks.push(task);
//             }else{
//                 console.log('WARN: config file does not exist ', e)
//             }
//         })
//         async.parallel(tasks, callback);
//     }
//     private writePrime(filePath: string, field: string, value: string, callback:(err,result)=>void){
//         fs.readFile(filePath, 'utf8', (err, data)=>{
//             if(err){
//                 callback(err + ' or no data', null);
//                 return;
//             }
//             // console.log('file data ', data)
//             if(!data) data = '{}'
//             let jData = {};
//             try{
//                 jData = JSON.parse(data)
//             }catch (e){
//                 console.log('error config JSON read ', e)
//                 jData = {}
//             }
//             jData[field] = value? value: 'null';
//             fs.writeFile(filePath, JSON.stringify(jData), (err)=>{
//                 if(err){
//                     callback(err, null);
//                     return;
//                 }
//                 callback(null, jData)
//             })
//         })
//     }
//     blank(callback?:(err,result)=>void){
//         const tasks = []
//         this.configPaths.forEach(e=>{
//             if(fs.existsSync(e)){
//                 const task = (callback:(err, result)=>void)=>{
//                     this.blankPrime(e, callback)
//                 }
//                 tasks.push(task);
//             }
//         })
//         async.parallel(tasks, callback);
//     }
//     private blankPrime(filePath: string, callback?:(err,result)=>void){
//         fs.writeFile(filePath, JSON.stringify({}), (err)=>{
//             if(err){
//                 if(callback)callback(err, null);
//                 return;
//             }
//             if(callback)callback(null, true)
//         })
//     }

//     //networkMode is either 'static' or 'dhcp'
//     static setSystemNetwork(ipv4: string, netmask: string, gateway: string, networkMode: string, callback:(err, result)=>void){
//         console.log('centos network settings ', ipv4, netmask, gateway, networkMode)
//         if(process.platform === "win32"){
//             callback('Not linux!', 'Not linux!')
//             return;
//         };
        
//         new Promise((resolve, reject)=>{
//             File.ModifyConfig(NetworkConfig.networkConfigPathCentos, 'NETMASK', netmask, false, (err, result)=>{
//                 if(err) reject(err);
//                 resolve();
//             })
//         })
//         .catch(err => {callback(null, err); return;})
//         .then(()=>{
//             return new Promise((resolve, reject)=>{
//                 File.ModifyConfig(NetworkConfig.networkConfigPathCentos, 'IPADDR', ipv4, false, (err, result)=>{
//                     if(err) reject(err);
//                     resolve();
//                 })
//             })
//         })
//         .catch(err => {callback(null, err); return;})
//         .then(()=>{
//             return new Promise((resolve, reject)=>{
//                 File.ModifyConfig(NetworkConfig.networkConfigPathCentos, 'GATEWAY', gateway, false, (err, result)=>{
//                     if(err) reject(err);
//                     resolve();
//                 })
//             })
//         })
//         .catch(err => {callback(null, err); return;})
//         .then(()=>{
//             return new Promise((resolve, reject)=>{
//                 NetworkConfig.setNetworkMode(networkMode, (err, result)=>{
//                     if(err) reject(err);
//                     resolve();
//                 })
//             })
//         })
//         .catch(err => {callback(null, err); return;})
//         .then(()=>{
//             callback(null, true);
//         })
//     }
//     static setNetworkMode(networkMode: string, callback:(err, result)=>void){
//         File.ModifyConfig(NetworkConfig.networkConfigPathCentos, 'BOOTPROTO', networkMode, false, (err, result)=>{
//             callback(err, result);
//         })
//     }
//     static getNetworkMode(callback:(err, result)=>void){
//         File.ModifyConfig(NetworkConfig.networkConfigPathCentos, 'BOOTPROTO', null, true, (err, result)=>{
//             callback(err, result);
//         })
//     }
//     static setNetmask(ip: string, callback:(err, result)=>void){
//         File.ModifyConfig(NetworkConfig.networkConfigPathCentos, 'NETMASK', ip, false, (err, result)=>{
//             callback(err, result);
//         })
//     }
//     static getNetmask(callback:(err, result)=>void){
//         File.ModifyConfig(NetworkConfig.networkConfigPathCentos, 'NETMASK', null, true, (err, result)=>{
//             callback(err, result);
//         })
//     }

//     static reboot(password: string, callback:(msg)=>void){
//         if(process.platform !== "win32"){
//             require('child_process').exec('echo ' + password + ' | sudo -S /sbin/shutdown -r now', function (msg) { 
//                 callback(msg)
//                 return;
//             });
//         }
//     } 
//     static networkRestart(password: string, callback:(msg)=>void){
//         if(process.platform !== "win32"){
//             require('child_process').exec('echo ' + password + ' | sudo -S service network restart', function (msg) { 
//                 callback(msg)
//                 return;
//             });
//         }
//     }
// }