import { PhotoController } from "./Models/photoController";
import { ServerController } from "./Models/ServerController";
import { KeywordController } from "./Models/keywordController";
import { RequestController } from "./Models/requestController";
import { Ctrl } from "./Server/server";

export class cache{
    static updateKeyword = true;
    static keywords: any;
    static controllers:Ctrl;
}