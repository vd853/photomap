"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const globals_1 = require("./globals");
const videoProcess_1 = require("./videoProcess");
const r_1 = require("./r");
const moment = require("moment");
class Queue {
    constructor(controller, controllers) {
        this.controller = controller;
        this.controllers = controllers;
        this.concurrent = 0;
        this.queues = [];
        setInterval(() => {
            this.queues.forEach(e => {
                if (!e.isActive) {
                    this.process(e);
                }
            });
            if (this.concurrent > 0) {
                console.log('concurrent ', this.concurrent);
            }
            //console.log('check for queues to process ', this.queues)
        }, globals_1.globals.encodingPollTime * 1000);
        //memory leak issue when FindAll is call too many times
        setInterval(() => {
            this.expireDb();
        }, 5000); //time for this is not offically set
    }
    add(model, filePath) {
        if (model.status !== 'uploaded') {
            console.log('ERROR: video was not uploaded and will not be added to queue, id ', model._id);
            return;
        }
        const q = new QueueModel(model, filePath);
        this.queues.push(q);
    }
    remove(model) {
        for (let i = 0; i < this.queues.length; i++) {
            if (this.queues[i].model._id === model._id) {
                this.queues.splice(i, 1);
                console.log('Queue removed, id ', model._id);
                console.log('remaining queue ', this.queues.length);
                return;
            }
        }
        console.log('ERROR: No queue could be removed, id ', model._id);
    }
    //expires a video entry in the db that was created, but video was never uploaded
    expireDb() {
        this.controller.factory().FindAll({}, (err, data) => {
            const list = data;
            list.forEach(e => {
                const condition = new Date() > moment(e.createdDate).add(globals_1.globals.expireVideoDatabase, 's').toDate() && !e.status;
                if (condition) {
                    this.controller.factory().Remove({ _id: e._id }, (err, data1) => {
                        if (err) {
                            console.log('ERROR in expiring db ', err);
                            return;
                        }
                        console.log('Expired db ', e._id);
                    });
                }
            });
        });
    }
    process(queue) {
        if (this.concurrent > globals_1.globals.concurrentEncodingLimit) {
            console.log('Skipping queue, concurrent limit is over ' + this.concurrent);
            return;
        }
        console.log('Queue process begin for ', queue.model._id);
        this.concurrent++;
        queue.isActive = true;
        const process = new videoProcess_1.videoProcessor(queue.filePath, queue.model, this.controller, (err, model) => {
            if (err) {
                console.log('Process error ', err);
                return;
            }
            model.status = 'completed'; //model is mark as ready (ready to view on site)
            //set video as ready in the db
            this.controller.factory().AddOrUpdate({ _id: model._id }, model, (err, data) => {
                if (err) {
                    console.log('set ready error ', err);
                    return;
                }
                console.log(model._id + ' has been process and is ready to be viewed');
                this.controllers.photo.adjustFilePath(model.videoPathFile, model._id, (err, result) => { });
                this.concurrent--;
                //delete the temp folder which should be empty since the file was moved
                // File.deleteFolder(queue.fileFolder, (result)=>{
                //     this.remove(model)
                //     
                // }) 
            });
        });
        process.Start();
    }
}
exports.Queue = Queue;
class QueueModel {
    constructor(model, filePath) {
        this.model = model;
        this.filePath = filePath;
        this.isActive = false;
        this.fileFolder = r_1.File.getFolder(filePath);
    }
}
exports.QueueModel = QueueModel;
//# sourceMappingURL=queue.js.map