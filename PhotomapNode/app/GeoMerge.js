"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("./r");
const async = require("async");
const fs = require("fs");
class GeoMerge {
    constructor(input, output, key) {
        this.input = input;
        this.output = output;
        this.key = key;
        this.nullImage = '../Assets/nullImage.jpg';
        this.tempPath = 'temp';
        this.nullImageCount = 0;
        this.validFolders = [];
        this.invalidFolders = [];
        this.matches = [];
    }
    Start() {
        if (!fs.existsSync(this.input)) {
            throw 'Your input path does not exist';
        }
        if (!fs.existsSync(this.nullImage)) {
            throw 'Your null image does not exist';
        }
        //check if any jpg exist anywhere
        this.folderValidate(this.input);
        if (this.validFolders.length === 0) {
            throw 'No folder or subfolder contains .jpg';
        }
        r_1.File.createFolder(this.tempPath);
        this.createFolder();
        this.geoCreate();
        console.log(this.matches);
    }
    folderValidate(input, prime = true) {
        const folders = r_1.File.getSubfolders(input);
        folders.forEach(e => {
            const actual = input + e; //the actual subfolder path
            //this is use for recursive inner folders and run this function again
            if (r_1.File.getSubfolders(actual).length > 0) {
                r_1.File.getSubfolders(actual).forEach(subs => {
                    let actualSub = actual + '/' + subs + '/';
                    //console.log('subsub ', actualSub)
                    this.folderValidate(actualSub, false);
                });
            }
            //check if subfolder has jpg
            const valid = r_1.File.getAllFiles(actual).some(e1 => {
                if (r_1.File.getExtension(e1) === 'jpg') {
                    return true;
                }
            });
            if (valid) {
                this.validFolders.push(actual);
            }
            else {
                this.invalidFolders.push(actual);
            }
        });
        //check if this folder has photos
        const validPrime = r_1.File.getAllFiles(input).some(e1 => {
            if (r_1.File.getExtension(e1) === 'jpg') {
                return true;
            }
        });
        if (validPrime) {
            if (prime) {
                this.validFolders.push(input);
            }
            else {
                this.validFolders.push(input);
            }
        }
        else {
            if (prime) {
                this.invalidFolders.push(input);
            }
            else {
                this.invalidFolders.push(input);
            }
        }
    }
    createFolder() {
        this.validFolders.forEach(e => {
            const actual = (this.output + e).replace(this.input, '/').replace('//', '/');
            //console.log(actual)
            r_1.File.createFolder(actual);
            this.matches.push({ source: e, target: actual });
        });
    }
    geoCreate() {
        const tasks = [];
        this.matches.forEach(e => {
            const create = (callback) => {
                this.geoMergeBulk(e.source, e.target, callback);
            };
            tasks.push(create);
        });
        console.log('tasks count ', tasks.length);
        async.parallel(tasks, (err, data) => {
            console.log(data);
            console.log(err);
            console.log('all process completed');
            //delete the temp folder
            r_1.File.deleteFolder(this.tempPath, r => {
                if (this.nullImageCount > 0)
                    console.log(this.nullImageCount + ' does not have geotag data.');
                console.log('All task completed');
            });
        });
    }
    //no error callback
    geoMergeBulk(input, output, callback) {
        let taskCounter = 0;
        const subtask = [];
        const files = r_1.File.getAllFiles(input);
        const taskLimit = files.length - 1;
        files.forEach(e => {
            if (r_1.File.getExtension(e) === 'jpg') {
                const create = (callbackc) => {
                    this.geoMergeOne(input + '/' + e, output + '/' + e, e => callbackc(e, true));
                };
                subtask.push(create);
            }
        });
        async.parallel(subtask, callback);
    }
    geoMergeOne(input, output, callback) {
        const i = new r_1.Image(input);
        console.log('Getting Google map for ', input);
        i.GeoLocation((err, dataGeo) => {
            let hasGeotag = true;
            if (err) {
                hasGeotag = false;
            }
            if (!dataGeo) {
                hasGeotag = false;
            }
            const tempfileResized = this.tempPath + '/' + r_1.GUID.create(true) + r_1.File.getFileOnlyOrParentFolderName(input);
            const tempfileRotate = this.tempPath + '/' + r_1.GUID.create(true) + r_1.File.getFileOnlyOrParentFolderName(input);
            console.log('compressing orginal to ', tempfileResized);
            const compressResizer = (image, hasGeo) => {
                image.CompressAndResize(tempfileResized, 640, (err, data1) => {
                    if (err) {
                        console.log('ERROR CompressAndResize', err);
                        callback(err);
                        throw err;
                    }
                    output = output.replace('//', '/');
                    const file = r_1.GUID.create(true) + '.png';
                    const tempfile = this.tempPath + '/' + file;
                    if (hasGeo && dataGeo !== null) {
                        const m = new r_1.GoogleMaps(this.key);
                        m.mapImage2(dataGeo.latitude, dataGeo.longitude, tempfile, (err, dataNull1) => {
                            if (err) {
                                console.log('ERROR mapImage2 ', err);
                                callback(err);
                                throw err;
                            }
                            if (fs.statSync(tempfile).size === 0) {
                                console.log('ERROR mapImage2, a map image did not return');
                                throw 'ERROR mapImage2, a map image did not return. Check your internet connection. Check your API key works for Google Map Static';
                            }
                            console.log('output path ', output);
                            r_1.Image.imageMerge([tempfile, tempfileResized], output, (err, dataNull2) => {
                                if (!err) {
                                    console.log('Image merged (map)');
                                    callback(null);
                                }
                                else {
                                    callback(err);
                                    console.log('Error in image merge (map) ', err, '. Image maybe corrupted.');
                                }
                            });
                        });
                    }
                    else {
                        // console.log('IMAGE')
                        // console.log(this.nullImage)
                        // console.log(tempfileResized)
                        // console.log(output)
                        r_1.Image.imageMerge([this.nullImage, tempfileResized], output, (err, dataNull2) => {
                            if (!err) {
                                console.log('Image merged');
                                callback(null);
                                this.nullImageCount++;
                            }
                            else {
                                callback(err);
                                console.log('Error in image merge ', err, '. Image maybe corrupted.');
                            }
                        });
                    }
                });
            };
            r_1.Image.autoRotate(input, tempfileRotate, (err, data) => {
                if (err) {
                    console.log('Error in auto rotate');
                    callback(err);
                    return;
                }
                const i1 = new r_1.Image(tempfileRotate);
                compressResizer(i1, hasGeotag);
            });
        });
    }
}
exports.GeoMerge = GeoMerge;
//# sourceMappingURL=GeoMerge.js.map