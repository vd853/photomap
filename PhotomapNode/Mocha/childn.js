"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cluster = require("cluster");
if (cluster.isMaster) {
    const numCPUs = require('os').cpus().length;
    console.log('master ', process.pid);
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }
}
if (cluster.isWorker) {
}
//# sourceMappingURL=childn.js.map