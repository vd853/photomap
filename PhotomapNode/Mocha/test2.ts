import * as mergeImg from 'merge-img';
import { GoogleMaps, Image, File, GUID, Encoder, Logger, Cipher, myMath, SSL, CSystem } from '../app/r';
import * as request from 'request'
import * as fs from 'fs'
import { GeoMerge } from '../app/GeoMerge';
import * as async from 'async'
import * as jimp from 'jimp'
import * as size from 'image-size'
import { PhotoModel } from '../app/Models/photo';
import { ServerController } from '../app/Models/ServerController';
import * as zip from 'jszip'
import { PhotoController } from '../app/Models/photoController';
import { KeywordController } from '../app/Models/keywordController';
import { Maintenance } from '../app/maintenance';
import * as _ from 'lodash'
import * as network from 'network';
import { Server } from '../app/Server/server';
import { cache } from '../app/cache';
import * as assert from 'assert';
import { ICFields } from '../app/Models/info';
import { ServerModel } from 'app/Models/server';
// import * as SslValidator from 'cmr1-ssl-validator'

class xclass{
    person: string;
}

describe('test', function(){
    this.timeout(0); //disable any timeout
    xit('1', ()=>{
        //try this on the linux server
        const e = new Encoder('../../Samples/small.mp4')
        e.EncodeFF('../../Samples', 'mp4', 640, 640, null, null, 1000, null, (err, data)=>{
            console.log(data)
        })
    }),
    xit('promise', ()=>{
        new Promise((resolve, reject)=>{
            resolve('return here')
        }).then(e=>{
            console.log('at then ', e)
            return new Promise((resolve, reject)=>{
                reject('rejected') //will only call reject OR resolve
                resolve('123')
            })
        }).then(e=>{
            console.log('at then 2 ', e)
        }).catch(e=>{
            console.log('catch ', e)
        })
    }),
    xit('network ', ()=>{
        network.get_interfaces_list(function(err, ip) {
            console.log('HOSTNAME: ', require('os').hostname())
            console.log(err || ip); // should return your public IP address
        })
    }),
    xit('change hostname ', ()=>{
        CSystem.setHostname('centos03', 'admin', ()=> {
            console.log(require('os').hostname());
        })
    }),
    xit('logger', ()=>{
        const l = new Logger('unittest', './log')
        l.log('test no error', 'labelzzz')
        l.error('test error', 'labelzzz')
        l.warn('test warn', 'labelzzz')
    }),
    

    //set SSL to null so this can run
    xit("dyn attr", ()=>{
        const p = new PhotoController()
        p.factory().FindInArray('attr',['sfesg'], (e, r)=>{
            console.log(r);         
        })
    })
    xit('type checker ', ()=>{
        let kk = new xclass();
        let v = kk.person.toUpperCase();
    })
    xit('error check', ()=>{
        const p = new PhotoController()
        const pm = new PhotoModel(['some', 'atributesrsf', 'hfesjfl'])
        p.factory().AddOrUpdate({_id: pm._id}, pm, (e, r)=>{
            console.log(r);         
        })
    })
    xit('await checking', ()=>{
        const c= new test()
        c.caller();
    })
    xit('async find one', async ()=>{
        const p = new PhotoController()
        const r = await p.factory().FindOneAsync({_id: '0ec24818-ac1f-4182-a7a0-776b7f5e4f49'});
        r.comment = 'new comments'
        await p.factory().AddOrUpdateAsync({_id: r._id}, r);
        
        console.log('code contines');
    }),
    it('update server', async ()=>{
        const s = new ServerController(async ()=>{
            const r = await s.factory().FindOneAsync({_id: 'server'});
            r.captchaByPass = true;
            await s.factory().AddOrUpdateAsync({_id: r._id}, r);
            console.log('server model updated');
        });
    })
})

class test{
    async caller(){
        const time = new Date();
        const result1 = await this.process(1500);
        console.log('r1 ', result1);
        const result2 = await this.process(2000);
        console.log('r2 ', result2);
        const time2 = new Date();
        console.log('elapsed ', time2.getTime() - time.getTime());
    }
    private async process(ms: number){
        return new Promise<string>((resolve, j)=>{
            setTimeout(() => {
                resolve('processed value')
            }, ms);
        })
    }
}
