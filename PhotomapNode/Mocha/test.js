"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../app/r");
const GeoMerge_1 = require("../app/GeoMerge");
const async = require("async");
const photoController_1 = require("../app/Models/photoController");
describe('test', () => {
    xit('1', () => {
        console.log('run 1');
        r_1.Image.imageMerge(['map.png', 'sSmall.jpg'], 'out.png', (e, d) => console.log(e, d));
        // const option = {color:0x000000FF, align: 'center'}
        // mergeImg(['map.png', 'sSmall.jpg'], option)
        // .then((img) => {
        //     // Save image as file
        //     img.write('out.png', () => console.log('done'));
        // })
        // .catch(e=>console.log(e));
    }),
        xit('2', () => {
            const m = new r_1.GoogleMaps('AIzaSyDOEmI24B2Y_LWw6WIW9LNCR1hJwXDciL8');
            const i = new r_1.Image('s.jpg');
            i.GeoLocation((err, data) => {
                m.mapImage(data.latitude, data.longitude, 'map.png', (err, data) => {
                    console.log(err, data);
                });
            });
        });
    xit('3', () => {
        const m = new r_1.GoogleMaps('AIzaSyDOEmI24B2Y_LWw6WIW9LNCR1hJwXDciL8');
        const i = new r_1.Image('s.jpg');
        i.GeoLocation((err, data) => {
            m.mapImage2(data.latitude, data.longitude, 'map2.png', (err, data) => {
                console.log(err, data);
            }, 5, 1000, 400);
        });
    }),
        xit('resize', () => {
            const i = new r_1.Image('s.jpg');
            i.CompressAndResize('sSmall.jpg', 640, (e, d) => {
                console.log(e, d);
            });
        });
    xit('run', () => {
        const g = new GeoMerge_1.GeoMerge('New folder/', 'output/', 'AIzaSyDOEmI24B2Y_LWw6WIW9LNCR1hJwXDciL8');
    }),
        xit('async p', () => {
            const tasks = [];
            for (let i = 0; i < 5; i++) {
                const t = (callback) => {
                    const val = i;
                    setTimeout(() => {
                        console.log('finish ', val);
                        callback(null, true);
                    }, 1000);
                };
                tasks.push(t);
            }
            async.parallel(tasks, (e, d) => {
                console.log(e, d);
            });
        }),
        xit('photo auto rotate', () => {
            r_1.Image.autoRotate('s.jpg', 'fixed.jpg', (e, d) => console.log(e, d));
        }),
        xit('mergers', () => {
            const mapFile = './Working/CI/map.jpg';
            const folder = './Working/CI';
            const outFolder = './Working/Out';
            const mergeFolder = './Working/Merge';
            const files = [];
            r_1.File.getAllFiles(folder).forEach(e => files.push(e));
            console.log(files);
            r_1.File.createFolder(outFolder);
            r_1.File.createFolder(mergeFolder);
            files.forEach(e => {
                const i = new r_1.Image(folder + '/' + e);
                i.CompressAndResize(outFolder + '/' + e, 3000, (err, d) => {
                    console.log('compress and resized ' + d);
                    const mergeFile = mergeFolder + '/' + e;
                    console.log('merge file: ', mergeFile);
                    r_1.Image.imageMerge([d, mapFile], mergeFile, (err, d1) => {
                        console.log('merged ' + mergeFile);
                    });
                });
            });
        }),
        xit('model test', () => {
            const p = new photoController_1.PhotoController();
            const m = p.Model();
            m._id = r_1.GUID.create();
            m.photographer = 'myself';
            p.factory().AddOrUpdate(m, m, () => { });
        }),
        xit('server log', () => {
        });
    xit('clear photo db', () => {
        const p = new photoController_1.PhotoController();
        p.factory().RemoveAll();
    }),
        xit('zip', () => {
            //console.log(File.getAllFilesRecursive('./Working', (err, data)=>console.log(err, data)))
            r_1.File.zipFolder('./Working', 'out.zip', (err, data) => {
                console.log('enter');
            });
        }),
        xit('zip file', () => {
            r_1.File.zipFiles(['./Working/s.jpg', './Working/s1.jpg'], 'out.zip', (err, data) => {
                console.log(data);
            });
        }),
        xit('get image', () => {
            const c = new photoController_1.PhotoController();
            c.factory().FindAllForFields('_id', ['ef2e6bc8-3763-49e4-bd99-7e9cd04579e7', 'bc187c23-25aa-46ad-befa-a2773ef8d68b'], ['folderPath', 'filePath'], (err, data) => {
                console.log(err, data);
            });
        }),
        xit('compress', () => {
            const i = new r_1.Image('./Working/s.jpg');
            i.CompressAndResize('./compress.jpg', 640, (err, result) => {
                console.log(err, result);
            });
        });
    xit('search and sort', () => {
        const p = new photoController_1.PhotoController();
        const searcher = {
            $and: [
                { $or: [{ photographer: 'vd' }, { comment: 'vd' }] }
            ]
        };
        p.factory().FindWhereUnlimitedSort(searcher, [{ field: 'fileName', decending: false }], [], (err, data) => {
            console.log(data);
            data.forEach(e => {
                console.log(e.location, ' ', e.fileName, ' ', e._id);
            });
        });
    }),
        xit('date taken', () => {
            const filePath = './Working/s.jpg';
            r_1.Image.creationTime(filePath, (e, d) => console.log(d));
        }),
        xit('async ', () => {
            const t = (callback) => {
                console.log('t');
                t2(callback);
            };
            const t2 = (callback) => {
                console.log('t2');
                callback(null, true);
            };
            async.parallel([t, t2], (err, data) => {
                console.log('completed');
            });
        }),
        xit('return test', () => {
            const x = 1;
            const y = 123;
            const t = (x) => {
                console.log('in t1 ', x);
                console.log('y ', y);
                if (x === 3)
                    return;
                t(3);
                return;
            };
            console.log(x);
            t(2);
            console.log('end call');
        }),
        xit('stamp color', () => {
            const i = new r_1.Image('../../Samples/1.jpg');
        }),
        xit('test db', () => {
            const f = new photoController_1.PhotoController();
            f.factory().FindOne({ _id: 'NONONON' }, (err, result) => {
                console.log(err, result);
            });
        }),
        xit('undefined and null', () => {
            let a = 'undefined';
            if (!a && a !== 'undefined') {
                console.log('has error?');
            }
        }),
        xit('filename', () => {
            console.log(r_1.File.getFileOnlyOrParentFolderName('./Storage/311bad8e-e5ce-47da-91b9-0f90e9d8e975/compress2_DSCN1345.JPG'));
        }),
        xit('text search ', () => {
            const pc = new photoController_1.PhotoController();
            // pc.factory().RemoveAll();
            // pc.factory().FindText('321 123', (err, result)=>{
            //     console.log(err, result)
            // })
        }),
        xit('get pixel', () => {
            // test dark and light, light portion
            // const origin = {x:252, y:231}
            // const end = {x:258, y: 248}
            // const location = "../../Samples/3.jpg"
            // test dark and light, half half
            // const origin = {x:245, y:232}
            // const end = {x:257, y: 255}
            // const location = "../../Samples/3.jpg"
            const origin = { x: 245, y: 232 };
            const end = { x: 257, y: 255 };
            const location = "../../Samples/3.jpg";
            r_1.Image.shadePortion(location, origin, end, (err, shade) => {
                console.log('shade ', shade);
            });
            // Image.stats(location, (err, dim)=>{
            //     if(err){
            //         return;
            //     }
            //     console.log('Image stats ', dim)
            //     Image.pixelSquare(origin.x, origin.y, end.x, end.y, dim.width, dim.height, (err, n:Array<{n: number, x: number, y: number}>)=>{
            //         console.log('n pixels length ', n.length)
            //         Image.shade(location, n, (err, shade)=>{
            //             if(err){
            //                 return;
            //             }
            //             console.log('shade ',shade)
            //         })
            //     })
            // })
        }),
        xit('pixel square', () => {
        }),
        xit('clean up', () => {
        }),
        xit('keyword process 2', () => {
        }),
        xit('rotate ', () => {
            const location = "../../Samples/s.jpg";
            const out = './sr.jpg';
            r_1.Image.rotate(location, out, 180, () => { });
        }),
        xit('count ', () => {
            const p = new photoController_1.PhotoController();
            p.factory().GetCount({}, (err, result) => {
                console.log('count ', result);
            });
        }),
        xit('promise', () => {
            //immeidate promise function
            const func1 = (resolve, reject) => {
                resolve(1);
            };
            //delayed promise function
            const funcTime = (resolve, myReject) => {
                setTimeout(() => {
                    myReject(3);
                }, 3000);
            };
            //instantiate a new promise using func1
            const func = () => {
                return new Promise(func1);
            };
            let escape = false; //if true, everything except last promise will NOT run
            func()
                .then(e => {
                if (escape)
                    return;
                console.log('here1e', e); //immediate
                return new Promise((resolve, reject) => { resolve(2); });
            })
                .then(e => {
                if (escape)
                    return;
                console.log('here2e', e); //immediately follow
                return new Promise(funcTime);
            })
                .then(e => {
                if (escape)
                    return;
                console.log('here3e', e); //will wait for funcTime
                return new Promise((resolve) => { resolve(4); });
            })
                .catch(err => {
                if (escape)
                    return;
                console.log('here3er', err); //will wait for funcTime
                return new Promise((resolve) => { resolve(4); });
            })
                .then(e => {
                //no breaker so it will run with null if breaker is true
                console.log('here4e', e); //will wait for funcTime and continue from .catch of 3
            });
            console.log('called before func()');
            setTimeout(() => {
                console.log('called during func()');
            }, 2000);
        }),
        xit('waterfall ', () => {
            const func = (a, callback) => {
                console.log(a); //prints abc
                callback(null, '123'); //all waterfall callback has null as first arg
            };
            async.waterfall([
                async.apply(func, 'abc'),
                (fall, callback) => {
                    console.log(fall); //123 from func is falls to this fall, prints 123
                    callback(null, '1', '2'); //1 and 2 falls into fall1 and fall2 of the next function
                },
                (fall1, fall2) => {
                    console.log(fall1, ' ', fall2); //prints 1  2
                }
            ]);
        }),
        xit('AND text search', () => {
            const p = new photoController_1.PhotoController();
            p.factory().FindTextAnd('test test1', [{ field: 'title', decending: true }], ['title', 'photographer'], (err, result) => {
                console.log(result);
            });
            // const s = 'a b c'
            // const t = _.map(s.split(' '), e=> {return '"' + e + '"'}).join(' ')
            // console.log(t.join(' '))
        }),
        xit('get mdoel ', () => {
            const c = new photoController_1.PhotoController();
            c.factory().FindOne({ _id: "bacc7428-7db1-415d-8b30-f6186522b923" }, (err, result) => {
                console.log(result.referencePath[0]);
                console.log(JSON.parse(result.referencePath));
            });
        }),
        xit('search array', () => {
            const p = new photoController_1.PhotoController();
            p.factory().FindInArray('someId', ['d04a3be1-249f-4bf7-b752-09e5f13a9359'], (err, result) => {
                console.log(err, result);
            });
        }),
        xit('file change ', () => {
            const field = 'MMMM123';
            const value = 'ffffsseef';
            const fileName = 'conf.txt';
            r_1.File.ModifyConfig('./conf.txt', 'BOOTPROTO', null, true, (err, result) => {
                console.log(result);
            });
        });
});
//# sourceMappingURL=test.js.map