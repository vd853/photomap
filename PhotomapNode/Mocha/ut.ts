import { SSL, Logger, File, Encoder } from '../app/r';
import * as assert from 'assert';
import * as fs from 'fs';
import * as network from 'network';
describe('Short Unit tests', function(){
    it('validate ssl key',()=>{
        SSL.validatePEM('../app/Keys/bin', '../app/Keys/key.pem', 'photo', true, (err, result)=>{
            if(err) assert.fail(err)
            if(!err) assert.ok(result)
        })
    })
    it("validate ssl cert", ()=>{
        SSL.validatePEM('../app/Keys/bin', '../app/Keys/cert.pem', null, false, (err, result)=>{
            if(err) assert.fail(err)
            if(!err) assert.ok(result)
        })
    })
})
