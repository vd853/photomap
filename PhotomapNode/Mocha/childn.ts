import * as cluster from 'cluster'
if(cluster.isMaster){
    const numCPUs = require('os').cpus().length;
    console.log('master ', process.pid)
    for(let i = 0; i < numCPUs; i++){
        cluster.fork();
    }
}
if(cluster.isWorker)
{

}