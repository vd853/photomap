import { SSL, Logger, File, Encoder } from '../app/r';
import * as assert from 'assert';
import * as fs from 'fs';
describe('Long Unit tests', function(){
    this.timeout(0); //disable any timeout
    it("encoding test", (done)=>{
        const e = new Encoder(File.reparsePath('./test.mp4'), process.platform === "win32")
        e.getVideoSpec((err, data)=>{
            if(err){
                assert.fail(err)
                done();
            }
            if(!data.width){
                assert.fail('no width data')
                done();
            }
            e.EncodeFF(File.reparsePath('./'), 'postfix', data.width, data.height, undefined, undefined, undefined, undefined, (err, data)=>{
                if(err){
                    assert.fail('conversion error');
                    done();
                }
                if(fs.existsSync('./test_postfix.mp4')){
                    fs.unlinkSync('./test_postfix.mp4') //clean up
                    assert.ok('conversion success');
                    done();
                }else{
                    assert.fail('converted file unfound');
                    done();
                }
            })
        })
    })
})
