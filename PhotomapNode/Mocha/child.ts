import * as cluster from 'cluster'
describe('test', ()=>{
    it('1', ()=>{
        if(cluster.isMaster){
            const numCPUs = require('os').cpus().length;
            console.log('master ', process.pid)
            for(let i = 0; i < numCPUs; i++){
                const worker = cluster.fork();
                worker.send('hello');
            }
        }
        if(cluster.isWorker)
        {
            process.on('message', (msg)=>{
                console.log('worker ', msg)
            })
        }
    })
})