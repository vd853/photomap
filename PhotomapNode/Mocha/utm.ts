import { SSL, Logger, File, Encoder, NetworkConfig, ExpressObj, Privilege, GUID, AccountModel, PrivilegeType } from '../app/r';
import * as assert from 'assert';
import * as fs from 'fs';
import * as network from 'network';
import { PhotoController } from '../app/Models/photoController';
import { PhotoModel } from '../app/Models/photo';
const colors = require('colors');
describe('Medium Unit tests', function(){
    this.timeout(0); //disable any timeout
    it("Network lookup", (done)=>{
        if(process.platform === "win32"){
            //@ts-ignore
            console.log('Will not test on win32'.yellow)
            assert.ok(true, 'Will not test on win32')
            done();
        }
        network.get_interfaces_list(function(err, ip) {
            console.log('HOSTNAME: ', require('os').hostname())
            // console.log(err || ip); // should return your public IP address
            const eth0 = ip.find(x => x.name === 'eth0');
            if(!ip){
                assert.fail('No ip was found')    
            }
            if(err){
                assert.fail('error in finding ip ' + err)
            }
            if(eth0){
                console.log(eth0);
                assert.ok(eth0, 'eth0 network was found')
            }else{
                assert.fail('Cannot find eth0')
            }
            done(); process.exit();
        })
    })
    it('Get Network Mask', (done)=>{
        if(process.platform === "win32"){
            //@ts-ignore
            console.log('Will not test on win32'.yellow)
            assert.ok(true, 'Will not test on win32')
            done();
        }
        NetworkConfig.getNetmask((err, result)=>{
            if(result){
                console.log(result);
                assert.ok(result, 'Got a network mask')
            }else{
                assert.fail(err);
            }
            done(); process.exit();
        })
    })
    it('write network config', (done)=>{
        if(process.platform === "win32"){
            //@ts-ignore
            console.log('Will not test on win32'.yellow)
            assert.ok(true, 'Will not test on win32')
            done();
        }
        const done1 = done;
        NetworkConfig.getNetworkMode((err, result)=>{
            if(err){
                assert.fail('Cannot get network mode ' + err)
                done1(); process.exit();
            }
            let setTo: string;
            let setBack: string;
            if(result === 'dhcp'){
                setTo = 'static'
                setBack = 'dhcp'
            }else{
                setTo = 'dhcp'
                setBack = 'static'
            }

            new Promise((resolve, reject)=>{
                NetworkConfig.setNetworkMode(setTo, (err, result)=>{
                    if(err){
                        assert.fail('cannot set to ' + setTo + ':' + err)
                        done1(); process.exit();
                    }
                    resolve();
                })
            }).then(()=>{
                return new Promise((resolve, reject)=>{
                    NetworkConfig.getNetworkMode((err, result)=>{
                        if(err){
                            assert.fail('cannot get as ' + setTo + ':' + err)
                            done1(); process.exit();
                        }
                        if(result !== setTo){
                            assert.fail('did not set to ' + setTo + ':' + err)
                            done1(); process.exit();
                        }
                        resolve();
                    })
                })
            }).then(()=>{
                return new Promise((resolve, reject)=>{
                    NetworkConfig.setNetworkMode(setBack, (err, result)=>{
                        if(err){
                            assert.fail('cannot set back to ' + setBack + ':' + err)
                            done1(); process.exit();
                        }
                        resolve();
                    })
                })
            }).then(()=>{
                return new Promise((resolve, reject)=>{
                    NetworkConfig.getNetworkMode((err, result)=>{
                        if(err){
                            assert.fail('cannot set back to ' + setTo + ':' + err)
                            done1(); process.exit();
                        }
                        if(result !== setBack){
                            assert.fail('did not set back to ' + setBack + ':' + err)
                            done1(); process.exit();
                        }
                        resolve();
                    })
                })
            }).then(()=>{
                assert.ok('Test pass')
                done1(); process.exit();
            })
        })
        
    })
    it('80 port creation', (done)=>{
        const e = new ExpressObj(80, undefined, undefined, undefined)
        e.Start(()=>{
            assert.ok('port created')
            done(); 
            setTimeout(() => {
                e.Stop();
                process.exit();
            }, 1000);
        });
    })
    it('authenticates add/remove', (done)=>{
        const c = new PhotoController();
        const m = c.Model();
        const idTest = GUID.create();
        const am = new AccountModel(GUID.create())
        am.user = 'auth name test';
        m._id = 'unit test only';
        const au = new Privilege(PrivilegeType.none, am);
        au.associatedId = 'assoicated with this id'
        new Promise((resolve, reject)=>{
            c.factory().AddOrUpdate({_id: m._id}, m, (err, result)=>{
                if(err) throw err;
                c.factory().FindOne({_id: m._id}, (err, result)=>{
                    if(err) throw err;
                    if(!result){
                        assert.fail('Failed to add test model');
                        done();
                    }
                    resolve(result)
                })
            })
        }).then((m: PhotoModel)=>{
            return new Promise((resolve, reject)=>{
                let modified = PhotoModel.modifyAuthenticates(m, au);
                modified = PhotoModel.modifyAuthenticates(m, au);
                modified = PhotoModel.modifyAuthenticates(m, au);
                assert.equal(1, modified.authenticates.length);
                if(
                    modified.authenticates[0]._id === au._id &&
                    modified.authenticates[0].associatedId === au.associatedId &&
                    modified.authenticates[0].user === au.user &&
                    modified.authenticates[0].privilege === au.privilege
                ){
                    m = modified;
                    resolve(m);
                }else{
                    assert.fail('Failed to add authenticates')
                    c.factory().Remove({_id: m._id}, ()=>{done(); process.exit()})
                }
            })
        }).then((m: PhotoModel)=>{
            au.toRemove = true;
            m = PhotoModel.modifyAuthenticates(m, au)
            assert.equal(0, m.authenticates.length);
            if(m.authenticates.length > 0){
                assert.fail('Failed to remove authenticates')
                c.factory().Remove({_id: m._id}, ()=>{done(); process.exit()})
            }else{
                assert.ok(true, 'Remove and add authenticates was successful.')
                c.factory().Remove({_id: m._id}, ()=>{done(); process.exit()})
            }
        })
        
    })
})
