"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../app/r");
const assert = require("assert");
describe('Short Unit tests', function () {
    it('validate ssl key', () => {
        r_1.SSL.validatePEM('../app/Keys/bin', '../app/Keys/key.pem', 'photo', true, (err, result) => {
            if (err)
                assert.fail(err);
            if (!err)
                assert.ok(result);
        });
    });
    it("validate ssl cert", () => {
        r_1.SSL.validatePEM('../app/Keys/bin', '../app/Keys/cert.pem', null, false, (err, result) => {
            if (err)
                assert.fail(err);
            if (!err)
                assert.ok(result);
        });
    });
});
//# sourceMappingURL=ut.js.map