"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../app/r");
const assert = require("assert");
const fs = require("fs");
describe('Long Unit tests', function () {
    this.timeout(0); //disable any timeout
    it("encoding test", (done) => {
        const e = new r_1.Encoder(r_1.File.reparsePath('./test.mp4'), process.platform === "win32");
        e.getVideoSpec((err, data) => {
            if (err) {
                assert.fail(err);
                done();
            }
            if (!data.width) {
                assert.fail('no width data');
                done();
            }
            e.EncodeFF(r_1.File.reparsePath('./'), 'postfix', data.width, data.height, undefined, undefined, undefined, undefined, (err, data) => {
                if (err) {
                    assert.fail('conversion error');
                    done();
                }
                if (fs.existsSync('./test_postfix.mp4')) {
                    fs.unlinkSync('./test_postfix.mp4'); //clean up
                    assert.ok('conversion success');
                    done();
                }
                else {
                    assert.fail('converted file unfound');
                    done();
                }
            });
        });
    });
});
//# sourceMappingURL=utl.js.map