"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const r_1 = require("../app/r");
const photo_1 = require("../app/Models/photo");
const ServerController_1 = require("../app/Models/ServerController");
const photoController_1 = require("../app/Models/photoController");
const network = require("network");
// import * as SslValidator from 'cmr1-ssl-validator'
class xclass {
}
describe('test', function () {
    this.timeout(0); //disable any timeout
    xit('1', () => {
        //try this on the linux server
        const e = new r_1.Encoder('../../Samples/small.mp4');
        e.EncodeFF('../../Samples', 'mp4', 640, 640, null, null, 1000, null, (err, data) => {
            console.log(data);
        });
    }),
        xit('promise', () => {
            new Promise((resolve, reject) => {
                resolve('return here');
            }).then(e => {
                console.log('at then ', e);
                return new Promise((resolve, reject) => {
                    reject('rejected'); //will only call reject OR resolve
                    resolve('123');
                });
            }).then(e => {
                console.log('at then 2 ', e);
            }).catch(e => {
                console.log('catch ', e);
            });
        }),
        xit('network ', () => {
            network.get_interfaces_list(function (err, ip) {
                console.log('HOSTNAME: ', require('os').hostname());
                console.log(err || ip); // should return your public IP address
            });
        }),
        xit('change hostname ', () => {
            r_1.CSystem.setHostname('centos03', 'admin', () => {
                console.log(require('os').hostname());
            });
        }),
        xit('logger', () => {
            const l = new r_1.Logger('unittest', './log');
            l.log('test no error', 'labelzzz');
            l.error('test error', 'labelzzz');
            l.warn('test warn', 'labelzzz');
        }),
        //set SSL to null so this can run
        xit("dyn attr", () => {
            const p = new photoController_1.PhotoController();
            p.factory().FindInArray('attr', ['sfesg'], (e, r) => {
                console.log(r);
            });
        });
    xit('type checker ', () => {
        let kk = new xclass();
        let v = kk.person.toUpperCase();
    });
    xit('error check', () => {
        const p = new photoController_1.PhotoController();
        const pm = new photo_1.PhotoModel(['some', 'atributesrsf', 'hfesjfl']);
        p.factory().AddOrUpdate({ _id: pm._id }, pm, (e, r) => {
            console.log(r);
        });
    });
    xit('await checking', () => {
        const c = new test();
        c.caller();
    });
    xit('async find one', () => __awaiter(this, void 0, void 0, function* () {
        const p = new photoController_1.PhotoController();
        const r = yield p.factory().FindOneAsync({ _id: '0ec24818-ac1f-4182-a7a0-776b7f5e4f49' });
        r.comment = 'new comments';
        yield p.factory().AddOrUpdateAsync({ _id: r._id }, r);
        console.log('code contines');
    })),
        it('update server', () => __awaiter(this, void 0, void 0, function* () {
            const s = new ServerController_1.ServerController(() => __awaiter(this, void 0, void 0, function* () {
                const r = yield s.factory().FindOneAsync({ _id: 'server' });
                r.captchaByPass = true;
                yield s.factory().AddOrUpdateAsync({ _id: r._id }, r);
                console.log('server model updated');
            }));
        }));
});
class test {
    caller() {
        return __awaiter(this, void 0, void 0, function* () {
            const time = new Date();
            const result1 = yield this.process(1500);
            console.log('r1 ', result1);
            const result2 = yield this.process(2000);
            console.log('r2 ', result2);
            const time2 = new Date();
            console.log('elapsed ', time2.getTime() - time.getTime());
        });
    }
    process(ms) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, j) => {
                setTimeout(() => {
                    resolve('processed value');
                }, ms);
            });
        });
    }
}
//# sourceMappingURL=test2.js.map