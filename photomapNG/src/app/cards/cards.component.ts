import { Component, OnInit, ViewContainerRef, ViewChild, ComponentFactoryResolver, HostListener, HostBinding } from '@angular/core';
import { CardComponent } from '../card/card.component';
import { ImagePreviewComponent } from '../image-preview/image-preview.component';
import { ServerlinkService } from '../Service/serverlink.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { SearchService } from '../Service/search.service';
import * as _ from 'lodash'
import { GlobalService } from '../Service/global.service';
import { NotificationDirective } from '../Directives/notification.directive';
import { TopnavComponent } from '../topnav/topnav.component';
import { EnclosureComponent } from '../enclosure/enclosure.component';
import {globals as nGlobal, PhotoModel, myMath} from '../r'
import { KeywordLinkService } from '../Service/keyword-link.service';
import { PreviewTextComponent } from '../preview-text/preview-text.component';
import { InfoGetterService } from '../Service/info-getter.service';
import { CacheService } from '../Service/cache.service';
@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
  batchExpanded = false; //similar as batchSelected
  batchSelected = false; //if true, new loaded cards will be automatically select
  topOffset = 100 ; //80 from cards offset (enclosure css) and 20 from column height (cards css)
  buttomOffset = 50;
  headingHeightNoThumbnail = 30 + 5; //use this for headingHeight or the one below
  headingHeightThumbnail = 60 + 5;
  headingHeight = this.headingHeightNoThumbnail; //30 for height, 5 for top offset (heading css)
  maxHeadingOffset = 2; //subtract or add more cards base on final result
  maxHeading = 0;
  maxHeadingFloor = 0; //round down of maxHeading
  totalCardsExpandedHeight = 0; //increases and decreases base on how each cards are expanded or not
  resultEnd = false; //all results are pulling in 
  loadingEnd = false; //all results have been pulled in and loaded
  firstSearch = false;
  
  currentPreviewCardId: string;
  loadingTimer: any;

  previousSelectionId: string;
  innerTest:SafeHtml = ''
  columnClass = 'column'
  columnView = [];
  sortTypes = []
  columnSelecting: string
  thumbIndex: number;
  thumbnailImageRequested = false;
  
  ids: string[];
  children = []
  topNav: TopnavComponent
  parent: EnclosureComponent
  @HostBinding('style.position') position = "absolute"
  @ViewChild(NotificationDirective) notification: NotificationDirective
  @ViewChild('preview') previewImage: ImagePreviewComponent;
  @ViewChild('carder', {read: ViewContainerRef}) carder;
  constructor(
    private resolver: ComponentFactoryResolver, 
    private server: ServerlinkService, 
    private search: SearchService,
    private global: GlobalService,
    private keyword: KeywordLinkService,
    private infoGetter: InfoGetterService,
    private cache: CacheService
  ) { 
    infoGetter.cards = this;
    this.sortTypes = [...nGlobal.sortables, ...this.server.model.fieldDefinitions]
    this.global.cards = this //use for next modify
    this.cache.cards = this;
  }
  ngOnInit() {
    this.columnView = this.server.model.columnView; //current column view
  }

  getChildPosition(id: string) : number{
    for(let i = 0; i < this.children.length; i++){
      if(this.children[i].instance.model._id === id){
        return i;
      }
    }
    console.log('Childen position not found')
  }

  //set which column to set later
  settingColumn(selecting: string){
    this.columnSelecting = selecting;
  }

  //finally set this to the column
  setColumn(set: string){
    //no changes
    if(this.columnSelecting === set){
      return;
    }

    //these line will swap column if needed
    const setIndex = _.findIndex(this.columnView, e=> e === this.columnSelecting) //get index of the current column to set
    const replaceIndex = _.findIndex(this.columnView, e => e === set) //get index of the column that you are changing to
    //debugger;
    this.columnView[setIndex] = set; //set the current column
    this.columnView[replaceIndex] = this.columnSelecting //set the previous column to whatever this column was
    this.checkThumbnailColumnExist()

    //update server with new column setup
    this.server.model.columnView = this.columnView
    this.server.updateServer(this.server.model, (err, data)=>{
    })
  }
  
  checkThumbnailColumnExist(){
    //set new height and get thumbnail if a thumbnail column exist
    this.thumbIndex = _.findIndex(this.columnView, e=> e === 'thumbnail'); 
    if(this.thumbIndex > -1) {
      this.headingHeight = this.headingHeightThumbnail; //with thumbnail height
      this.batchGetImageLow()
    }else{
      this.headingHeight = this.headingHeightNoThumbnail; //non-thumbnail height
    };
  }
  
  batchCreate(ids: string[]){
    this.ids = ids;
    this.firstSearch = true;
    this.initMaxHeading(); //for initualizing the variable
    this.batchCreateLimited();
    // ids.forEach(e=>{
    //   this.createCard(e)
    // })
  }

  checkLoading(){
    if(this.topNav.loading) return;
    const loadingAction = ()=>{
      if(this.children.length >= this.maxHeadingFloor || this.resultEnd){ //condition of NOT loading cards
        this.topNav.loading = false;
      }else{ //if still loading, clear the current timer and create a new one
        clearTimeout(this.loadingTimer);
        this.loadingTimer = setTimeout(loadingAction, 3000);
      }
    }
    this.topNav.loading = true;
    this.loadingTimer = setTimeout(loadingAction, 3000); //set the first checker
  }

  batchCreateLimited(){
    if(!this.ids) return;
    if(this.resultEnd) return;
    this.maxHeadingFloor = Math.floor(this.maxHeading)
    this.checkLoading();
    if(this.children.length < this.maxHeadingFloor){
      console.log('creating cards until ', this.maxHeadingFloor)
      for(let i = this.children.length; i < this.maxHeadingFloor; i++){
        if(!this.ids[i]){
          console.log('no more results')
          this.resultEnd = true;
          this.RecachePhotoModels();
          return
        } 
        this.createCard(this.ids[i])
      }
    }
    this.RecachePhotoModels();
  }

  createCard(id: string){
    const child = this.carder.createComponent(this.resolver.resolveComponentFactory(CardComponent));
    child.instance.parent = this;
    this.children.push(child)
    child.instance.getProfile(id)
    if(this.children.length === this.ids.length){
      this.loadingEnd = true;
      this.notification.toastAM('All results loaded')
    }
    const batchSelected = this.batchSelected
    setTimeout(() => {
      console.log('is batch selected ', batchSelected)
      child.instance.select(batchSelected)
    }, 0);
  }
  hotReload(ids: string[]){
    ids.forEach(e=>{
      this.cache.PhotoModels.get(e).card.autoGetProfile();
    })
  }
  hotReloadAll(){
    this.cache.PhotoModels.forEach(e=>{
      e.card.autoGetProfile();
    })
  }
  //batch process
  batchHighlightReset(){
    this.batchHighlight(null, 0)
    this.parent.backgroundDark(false)
  }
  batchStampFocusReset(){
    this.children.forEach(e=>{
      if(e.instance.stamp) e.instance.stamp.focus = false;
    })
  }
  batchHighlight(exclude:string, mode:number){
    this.children.forEach(e=>{
      switch(mode){
        case 0:
          e.instance.hover.highlightOff()
          break;
        case 1:
          console.log('exlude dark hl')
          e.instance.hover.highlightDark(exclude)
          this.parent.backgroundDark(true);
          break;
        case 2:
          break;
      }
    })
  }
  batchExpand(switchMode: number){
    this.batchExpanded = switchMode === 1? true: false;
    
    this.children.forEach(e=>{
      e.instance.expandImage(switchMode)
    })
  }
  batchSelect(isSelected: boolean, currentOnly = false){
    if(!currentOnly) this.batchSelected = isSelected; //this means any newly loaded card will be selected of all is batched for selection
    this.children.forEach(e=>{
      e.instance.select(isSelected)
    })
    this.updateSelectionNotifier();
  }

  //this is call whenever a small version is needed
  batchGetImageLow(){
    if(!this.thumbnailImageRequested){
      this.thumbnailImageRequested = true;
      this.children.forEach(e=>{
        e.instance.getImage64Low()
      })
    }
  }
  updateSelectionNotifier(){
    setTimeout(() => {
      this.topNav.setSelectionNotifier(this.getSelectedIds().length)
    }, 0);
  }
  getSelectedIds(){
    const ids = []
    const noIds = []
    this.children.forEach(e=>{
      const child = <CardComponent>e.instance
      if(child.heading.selected){
        ids.push(child.model._id)
      }else{
        noIds.push(child.model._id)
      }
    })
    if(this.batchSelected){
      return _.difference(this.ids, noIds)
    }else{
      return ids
    }
  }

  selectDownTo(id: string, isSelect: boolean){ //otherwise would be deselect

    //find current selected
    let positionOfId: number;
    for(let i = 0; i < this.children.length; i++){
      console.log('id checking ', this.children[i].instance.model._id)
      if(this.children[i].instance.model._id === id){
        positionOfId = i;
        i = this.children.length
      }
    }

    //find previous selected
    let previousSelected = 0;
    for(let i = 0; i < this.children.length; i++){
      console.log('id checking ', this.children[i].instance.model._id)
      if(this.children[i].instance.model._id === this.previousSelectionId){
        previousSelected = i;
        i = this.children.length
      }
    }

    myMath.betweenRange(previousSelected, positionOfId).forEach(i=>{
      this.children[i].instance.select(isSelect);
    })
  }

  // selectRange(range1: number, range2: number, isSelect: boolean){
    
  //   // let isDown = true; //direction of select
  //   // if(range1 > range2){
  //   //   isDown = false;
  //   // }
  //   // console.log('isDown ', isDown)
  //   // if(isDown){
  //   //   for(let i = range1; i < range2; i++){
  //   //     console.log(i)
  //   //     this.children[i].instance.select(isSelect)
  //   //   }
  //   // }else{
  //   //   for(let i = range1; i > range2; i--){
  //   //     console.log('up' + i)
  //   //     this.children[i].instance.select(isSelect)
  //   //   }
  //   // }
  // }

  //reset values for new cards to load in
  reset(){
    console.log('card resetting and reloading')
    this.resultEnd = false;
    this.loadingEnd = false;
    this.updateSelectionNotifier()
    this.batchSelected = false;
    this.batchExpanded = false;
    this.resetEffects();
    //you must keep this true to always load the image and load them when server settings is retrieved
    this.thumbnailImageRequested = true; 

    this.maxHeading = 0;
    this.maxHeadingFloor = 0;
    this.totalCardsExpandedHeight = 0;
    this.topNav.reloadableIds = []
  }
  resetEffects(){
    this.batchHighlightReset();
    this.batchStampFocusReset();
  }
  //card deleting
  private deleteCard(id: string){ //use deleteSomeCards outside instead
    for(let i = 0; i < this.children.length; i++){
      if(this.children[i].instance.model._id === id){
        this.children[i].destroy()
        this.children.splice(i, 1)
        i = this.children.length
      }
    }
    console.log('children legnth ', this.children.length)
    this.ids = _.remove(this.ids, e=>e!==id)
    this.topNav.reloadableIds = _.remove(this.topNav.reloadableIds, e=>e!==id)
    this.topNav.idsLoaded = _.remove(this.topNav.idsLoaded, e=>e!==id)
  }
  deleteAllCards(){
    this.children.forEach(e=>{
      e.instance.select(false)
      e.destroy()
      this.updateSelectionNotifier();
      //this is call bay nav.reload which will also reset the children and ids array
    })
    this.cache.PhotoModels.clear();
  }
  RecachePhotoModels(){
    this.cache.PhotoModels.clear();
    setTimeout(() => {
      for(let i = 0; i < this.children.length; i++){
        if(this.children[i].instance.model._id){
          this.cache.PhotoModels.set(this.children[i].instance.model._id, {index: i, model: this.children[i].instance.model, card: this.children[i].instance})
        }
      }
    }, 100);
  }

  //modify (returns deleted ids) > topnav > deletesomecards()
  deleteSomeCards(ids: string[]){
    ids.forEach(e=>{
      this.deleteCard(e)
    })
    this.RecachePhotoModels();
    this.resetEffects();
    this.updateSelectionNotifier();
  }

  //use to get image during a mouse over instant
  getImageLow(id: string){
    let image: string
    let modelId: string
    let isExpanded = false;

    //finds card by id
    const found = this.children.some(e=>{
      if(e.instance.cardId === id){
        if(e.instance.expand){ //test if this card is expanded
          isExpanded = true
          return true
        }
        //if not expanded, return the image64
        //this instance is a card
        image = e.instance.imageLow
        modelId = e.instance.model._id
        return true
      }
    })

    if(found){
      if(isExpanded){
        return 'expanded' //if expanded, use this to indicate it
      }
      return {image, modelId}
    }
    return ''
  }

  //use to fade the columns when scrolling too downward

  @HostListener('window:resize')
  changingMaxHeading(){
    const h = document.documentElement;
    this.parent.checkWindowsWidth(window.innerWidth)
    this.maxHeading = (h.scrollHeight-this.topOffset-this.buttomOffset-this.totalCardsExpandedHeight)/this.headingHeight + this.maxHeadingOffset;
    console.log('scrollheight of change ', h.scrollHeight)
    console.log('total expand height ', this.totalCardsExpandedHeight)
    console.log('changing max headings ', this.maxHeading)
    this.batchCreateLimited();
  }

  //this uses clientHeight instead of scrollHeight
  initMaxHeading(){
    const h = document.documentElement;
    this.maxHeading = (h.clientHeight-this.topOffset-this.buttomOffset-this.totalCardsExpandedHeight)/this.headingHeight + this.maxHeadingOffset;
    console.log('init max headings ', this.maxHeading)
  }

  //scroll check > windows size check > then create card limited
  @HostListener('window:scroll', [])
  onWindowScroll() {
    // console.log('scrolling: ' + window.pageYOffset);
    if(window.pageYOffset > 5){
      this.columnClass = 'column lower'
    }else{
      this.columnClass = 'column'
    }
    const percent = this.getScrollPercent();
    if(percent > 90){
      this.changingMaxHeading()    
    }
    // console.log('scroll percent ', percent)
  }
  getScrollPercent() : number {
    const h = document.documentElement;
    const b = document.body;
    const st = 'scrollTop';
    const sh = 'scrollHeight';
    return (h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight) * 100;
  }

  debug(){
    this.notification.toast('test')
  }

  //use for image preview over a card
  @HostListener('mousemove', ['$event'])
  mouseHandling(event) {
    this.hoverImagePreview(event)
  }
  getChildModelByIndex(index: number){
    return <PhotoModel>this.children[index].instance.model
  }
  hoverImagePreview(mouseEvent: any){
    // console.log('preview event ', event)
    if(!this.server.model) return

    if(!this.server.model.enablePreview) {
      if(this.previewImage.enabled){
        this.previewImage.update(0, 0, false); 
      }
      return;
    }

    //find index with card
    let index = 0
    let isOverCard = false

    // console.log('event ', event.path)

    //don't preview if mouse is over the expand button
    if(mouseEvent.path[0].id === 'expand'){
      this.previewImage.update(0, 0, false)
      return;
    }

    //find up to 6 possible layer for where the card might be
    for(let i = 0; i < 6; i++){
      if(mouseEvent.path[i].id){
        if(mouseEvent.path[i].id.substring(0, 4) === 'card'){
          console.log('is true at ', mouseEvent.path[i].id)
          index = i
          isOverCard = true
          i = 100 //escape loop
        }
      }
    }
    if(isOverCard){
      const actualCardId = mouseEvent.path[index].id
      const cardId = actualCardId.substring(5, mouseEvent.path[index].id.length)
      
      //call the function to get the image64
      let result = this.getImageLow(actualCardId)

      if(result === 'expanded'){ //if result is "expanded", don't show because the card is already expanded
        this.previewImage.update(0, 0, false) //is over card, but card is expanded
      }else{ //if not, then record the image and update the coordinates and show preview
        
        if(this.currentPreviewCardId !== cardId){
          this.currentPreviewCardId = cardId
          this.previewImage.image = result['image'] //this is already a url mapped
          this.previewImage.model = this.cache.PhotoModels.get(result['modelId']).model
        }
        const xOffset = mouseEvent.screenX/window.innerWidth > 0.5? -575: 100

        const percentY = mouseEvent.screenY/window.innerHeight;
        let yOffset = -450 //low
        if(percentY > 0.3 && percentY < 0.7){ //center
          yOffset = -250;
        }else if(percentY > 0 && percentY < 0.3){ //high
          yOffset = -125
        }

        // const yOffset = mouseEvent.pageY > 
        this.previewImage.update(mouseEvent.pageX+xOffset, mouseEvent.screenY+yOffset, true)
      }
    }else{
      this.previewImage.update(0, 0, false)
    }
    
    // console.log('service preview x ', mouseEvent.screenX, ' y ', mouseEvent.screenY)  
    // console.log('service preview x% ', mouseEvent.screenX/window.innerWidth, ' y% ', mouseEvent.screenY/window.innerHeight)  
  }
}
