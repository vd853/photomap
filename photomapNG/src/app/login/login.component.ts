import { Component, OnInit, ViewChild } from '@angular/core';
import { AccountModel, GUID, AccountStatus, globals, AccountRequest, IAuthenticateResponse } from '../r';
import { FormConstructor } from '../Utilities/formConstructor';
import { Router } from '@angular/router';
import { ReCaptchaComponent } from 'angular2-recaptcha';
import { LoginGuard } from '../login.guard';
import { AccountlinkService } from '../Service/accountlink.service';
import { ServerlinkService } from '../Service/serverlink.service';
import { CookieService } from 'ngx-cookie-service';
import { AccountFormComponent } from '../account-manager/account-form/account-form.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  //sitekey: 6LfGLl0UAAAAAEKnQyXJpYPuI7wyiZoWEf2Vz9yr
  @ViewChild(ReCaptchaComponent) captcha: ReCaptchaComponent;
  group = 'form-group col-lg-8 col-md-8 col-sm-8 col-xs-8'
  model: AccountModel = new AccountModel(GUID.create())
  form = new FormConstructor();
  constructor(
    private loginGuard:LoginGuard, 
    private router: Router, 
    private server:ServerlinkService, 
    private cookie: CookieService,
    private account: AccountlinkService
  ) { 
    this.account.login = this;
    this.form.addText('user', 3, 12, true);
    this.form.addText('password', undefined, undefined, true);
    this.form.build();
  }

  ngOnInit() {
    this.autoAuthenticate();
    // this.autologin_debug();
  }

  autologin_debug(){
    this.waitForServer(()=>{
      this.model.user = 'admin';
      this.model.password = 'admin';
      this.submit();
    })
  }

  waitForServer(callback:()=>void){
    const check = setInterval(()=>{
      if(this.server.model){
        clearInterval(check);
        setTimeout(() => {
          callback();
        }, 500);
      }
    }, 100)
  }

  autoAuthenticate(){
    this.waitForServer(()=>{
      const sessionID = this.cookie.get('sessionID')
      const user = this.cookie.get('user')
      console.log('Cookie data ', sessionID, ' : ', user)
      if(sessionID && user){
        console.log('session cookies found');
        this.model.user = user;
        this.model.sessionID = sessionID;
        this.model.status = AccountStatus.auto;
        this.authenticate('', response=>{
          if(!response.verified){
            this.expireCookies();
          }
        }); //session authenticate never use recaptcha
      }
    })
  }

  expireCookies(){
    this.cookie.delete('sessionID')
    this.cookie.delete('user')
  }

  captchaResponse(event: any){
    console.log('captcha response: ', event);
    this.authenticate(event);
  }

  authenticate(captchaData: any, callback?:(result: IAuthenticateResponse)=>void){
    this.loginGuard.authenticate(this.model, captchaData, (err, result)=>{
      if(result.verified){ //register does not return a model
        if(result.model){
          if(result.model.status === AccountStatus.login){
            this.cookie.set('sessionID', result.model.sessionID)
            this.cookie.set('user', result.model.user)
            console.log('Navigating enclosure');
            this.router.navigate(['enclosure'])
          }
        }else{ //account created successfully
          this.accountModify = false;
          this.model = new AccountModel('x');
        }
      }
      if(callback) callback(result);
      this.responseMessage = result.reason;
    })
  }

  submit(){
    console.log('Submit has been pressed');
    this.model.status = AccountStatus.loggingIn;
    this.captchaExecute();
  }
  register(){
    this.model.status = AccountStatus.create;
    this.captchaExecute();
  }
  captchaExecute(){
    if(this.server.model.captchaByPass){
      this.captchaResponse('');
      console.log('captcha is bypassed');
      return;
    }
    this.captcha.execute();
  }
  debug(){
    return JSON.stringify(this.model);
  }

  responseMessage: string = null;
  accountModify = false;
  @ViewChild('accountForm') accountForm: AccountFormComponent;  
  accountModifier(){
    this.accountModify = !this.accountModify
    this.responseMessage = null;
    if(this.accountModify){
      setTimeout(() => {
        this.accountForm.InitCreateMode(this);
      }, 10);
    }
  }
}
