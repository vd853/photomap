import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ImageComponent } from '../image/image.component';
import { ImagelinkService } from '../Service/imagelink.service';
import { CardComponent } from '../card/card.component';
import { MatDialog } from '@angular/material';
import { ModifyComponent } from '../modify/modify.component';
import { PhotoModel, capitalizeEachWord, ModifyModel, maxCharLength, globals, DownloadType, GUID, QueryModel, mediaType} from '../r';
import { DownloadlinkService } from '../Service/downloadlink.service';
import {MatSnackBar} from '@angular/material';
import { GlobalService } from '../Service/global.service';
import { SearchService } from '../Service/search.service';
import { NotificationDirective } from '../Directives/notification.directive';
import * as moment from 'moment'
import { PreviewTextComponent } from '../preview-text/preview-text.component';
import { InfoGetterService } from '../Service/info-getter.service';
import { InfoTableComponent } from '../info-table/info-table.component';
import { QueryType } from '../../../../PhotomapNode/app/Models/query';
import { DatalinkService } from '../Service/datalink.service';
import { CacheService } from '../Service/cache.service';
import { ServerlinkService } from '../Service/serverlink.service';
import { PlayerComponent } from '../Video/player/player.component';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  
  // group = 'col-lg-8 col-md-8 col-sm-8 col-xs-8'
  // liClass = 'list-group-item d-flex justify-content-between align-items-center'
  // spanClass = 'badge badge-primary badge-pill'
  rotationLock = false;
  previous: string;
  typeVideo = false;
  @Input() parent: CardComponent
  @ViewChild('image') image: ImageComponent
  @ViewChild('info') infoTable: InfoTableComponent;
  @ViewChild('player') player: PlayerComponent;
  // @ViewChild('preview') preview :PreviewTextComponent;
  @ViewChild(NotificationDirective) notify: NotificationDirective
  constructor(private data:ImagelinkService, 
    private downloadlink:DownloadlinkService, 
    private dataLink: DatalinkService,
    public dialog: MatDialog, 
    public snackBar: MatSnackBar,
    public infoGetter: InfoGetterService,
    private cache: CacheService,
    private server: ServerlinkService
  ) { 
  }

  ngOnInit() { //parent is loaded here
    if(this.server.model.contraintImageHeightCard) this.image.contraintClass = 'imgContraintHeight' //limit height
  }
  getImage(callback:(err, data)=>void){
    //mediaType does not use callback
    if(this.parent.model.mediaType === mediaType.video){
      this.typeVideo = true;
      setTimeout(() => {
        this.player.init(this.parent.model.someId[0])
      }, 0);
      return;
    }
    console.log('view requesting ', this.parent.model._id)
    this.image.getImage(this.parent.model._id, 0, 0, false, callback)
  }
  rotate(){
    if(this.rotationLock){
      this.notify.toast('Rotating, please wait', 0)
      return;
    }
    this.parent.stamp.notifyEffect(true, 'Rotating...')
    this.rotationLock = true;
    //increase rotation by 90, reload image on view. rotation will be increase on server side
    const q = new QueryModel();
    q.queryType = QueryType.rotation
    q.searchId = this.parent.model._id;
    this.dataLink.getProfile(q, (err, data)=>{
      this.parent.stamp.notifyEffect(false);
      // console.log('rotation result ', data)
      if(data){
        this.parent.reloadViewImage(data.result)
        this.rotationLock = false;
        this.cache.ImageHigh.delete(this.parent.model._id)
      }
    })
  }
  //0 download orginial, 1 reference, 2 merge,
  download(downloadType: number){
    this.snackBar.open('Preparing downloaded','', {
      duration: 2000,
    });
    const modify = new ModifyModel();
    modify.downloadType = downloadType;
    modify.ids = [this.parent.model._id];
    modify.isBatch = false;
    this.downloadlink.download(modify, (err, data)=>{
      console.log('file downloaded')
      this.snackBar.open('File downloaded ' + data,'', {
        duration: 2000,
      });
    })
  }
  openModify(): void {
    let dialogRef = this.dialog.open(ModifyComponent, {
      data: {type: 'update', data: this.parent.model, position: this.parent.parent.getChildPosition(this.parent.model._id)},
      width: '1000px',
      height: '700px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.parent.parent.batchHighlightReset()
      if(!result) return;

      //single delete from the modify
      if(result === 'deleted'){
        console.log('return view for deleted')
        this.parent.destroySelf(true) //if true, only remove card and not the db
        return
      }

      //after model is updated
      if(!result.batch){
        console.log('return modify => view ', result)
        this.updateViewAndModel(result.oneModel)
        if(!this.typeVideo)this.parent.stamp.getReferences();
      }
    });
  }
  updateViewAndModel(model: PhotoModel){
    this.parent.model = model
    // this.parent.stamp.getImage(()=>{});
    console.log('model updated ', model)
  }
  //use only for the view button
  delete(){
    this.notify.confirmDelete('Permanently delete ' + this.parent.model.fileName, '', ()=>{
      this.parent.destroySelf() //this also calls cards.deleteCard(), but after removing it from db first
    })
  }
  checkActive(){
    if(!this.typeVideo) return true;
    if(!this.player) return false;
    return this.player.hasProcessed();
  }

}
