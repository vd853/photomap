import { Component, OnInit } from '@angular/core';
import { PhotoModel } from '../r';

@Component({
  selector: 'app-image-preview',
  templateUrl: './image-preview.component.html',
  styleUrls: ['./image-preview.component.css']
})
export class ImagePreviewComponent implements OnInit {
  model: PhotoModel
  enabled = false
  image: string
  style = {
    'top': 100 + 'px', 
    'left': 100 + 'px'
  }
  constructor() { }
  ngOnInit() {
  }
  update(x: number, y: number, show: boolean){
    this.enabled = show
    this.style['left'] = x + 'px'
    this.style['top'] = y + 'px'
  }
}
