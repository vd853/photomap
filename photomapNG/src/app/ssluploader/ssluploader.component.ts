import { Component, OnInit, ViewChild } from '@angular/core';
import { NotificationDirective } from '../Directives/notification.directive';
import { FileUploader } from 'ng2-file-upload';
import { GlobalService } from '../Service/global.service';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { ServerformComponent } from '../serverform/serverform.component';
@Component({
  selector: 'app-ssluploader',
  templateUrl: './ssluploader.component.html',
  styleUrls: ['./ssluploader.component.css']
})
export class SsluploaderComponent implements OnInit {
  @ViewChild(NotificationDirective) notifier2: NotificationDirective
  pass = '';
  statusAdded: string;
  status: string;
  url = 'upload/0'
  uploader:FileUploader;
  hasStarted = false;
  pemError: string;
  keyStatus: string = 'Not yet validated';
  certStatus: string = 'Not yet validated';
  parent: ServerformComponent;
  constructor(private global: GlobalService) { 
    this.url = global.mainURL + this.url;

    this.uploader = new FileUploader({
      url: this.url,
      maxFileSize: 20000,
      additionalParameter: { //this is for extra parameters
      }
    })

    this.uploader
    this.uploader.onAfterAddingFile = (file)=> { 
      file.withCredentials = false; 
    };
    this.uploader.onBeforeUploadItem = ()=>{
      
    }

    this.uploader.onSuccessItem = (item, response:any, status, headers)=>{
      response = JSON.parse(response)
      this.pemError = null;
      if(response.result){
        if(response.fileName === 'key.pem'){
          this.keyStatus = response.result;
        }
        if(response.fileName === 'cert.pem'){
          this.certStatus = response.result;
        }
      }
      if(response.error){
        if(response.fileName === 'key.pem'){
          this.keyStatus = response.error;
        }
        if(response.fileName === 'cert.pem'){
          this.certStatus = response.error;
        }
        this.pemError = response.error;
      }
      console.log('pem file ', response.fileName)
      console.log('pem error ', response.error)
      console.log('pem response ', response.result)
      this.parent.networkValid();
    }

    this.uploader.onCompleteAll = ()=>{
      this.resetQueue();
    }
    this.uploader.onAfterAddingAll = ()=>{
      this.statusAdded = this.uploader.queue.length.toString();
    }
  }
  ngOnInit() {
    this.onPassInit();
    setTimeout(() => {
      document.getElementById("validator").focus();
    }, 100);
  }

  clearQueue(){
    this.resetQueue();
  }

  onPassInit(){
    console.log('sub instat')
    const input = <HTMLInputElement>document.getElementById('sslpw')
    const onPress = fromEvent(document.getElementById('sslpw'), 'keyup').subscribe(()=>{
      console.log(input.value)
      this.pass = input.value;
    })
  }

  upload(){
    console.log('files number ', this.uploader.queue.length)
    if(this.uploader.queue.length < 1){
      this.pemError = "You must add file(s) first."
    }else{
      this.pemError = ""
    }
    this.uploader.options.additionalParameter.pass = this.pass;
    this.uploader.uploadAll()
    this.hasStarted = true;
  }

  resetQueue(){
    this.statusAdded = null;
    this.uploader.clearQueue();
  }
}
