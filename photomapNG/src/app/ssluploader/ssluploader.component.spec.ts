import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SsluploaderComponent } from './ssluploader.component';

describe('SsluploaderComponent', () => {
  let component: SsluploaderComponent;
  let fixture: ComponentFixture<SsluploaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SsluploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SsluploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
