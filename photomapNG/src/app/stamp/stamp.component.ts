import { Component, OnInit, ViewChild, HostListener, Input } from '@angular/core';
import { ImageComponent } from '../image/image.component';
import { ImagelinkService } from '../Service/imagelink.service';
import { MarkModel, QueryModel, PhotoModel, MarkMode, GUID } from '../r';
import { FormConstructor } from '../Utilities/formConstructor';
import { CardComponent } from '../card/card.component';
import { DatalinkService } from '../Service/datalink.service';
import { StampSwitchComponent } from '../stamp-switch/stamp-switch.component';
import { NotificationDirective } from '../Directives/notification.directive';
import { ServerlinkService } from '../Service/serverlink.service';
import { QueryType } from '../../../../PhotomapNode/app/Models/query';
import { MatDialog } from '@angular/material';
import { ImageLargeComponent } from '../image-large/image-large.component';

@Component({
  selector: 'app-stamp',
  templateUrl: './stamp.component.html',
  styleUrls: ['./stamp.component.css']
})
export class StampComponent implements OnInit {
  @ViewChild('image') image: ImageComponent
  @ViewChild('switch') switch: StampSwitchComponent
  @ViewChild(NotificationDirective) notify: NotificationDirective
  typeVideo = false;
  focus = false;
  infoStampMode = false;
  stampLock = false;
  referencesHaveUpdated = false; //use to update the stamp image cache if references have updated
  group = 'form-group col-lg-8 col-md-8 col-sm-8 col-xs-8 sides'
  groupNoSides = 'form-group col-lg-8 col-md-8 col-sm-8 col-xs-8'
  x: number
  y: number
  sizeButtonClass = ['btn btn-primary', 'btn btn-primary active']
  @Input() parent: CardComponent
  model = new MarkModel(GUID.create())
  form = new FormConstructor()
  constructor(
    private data:ImagelinkService, 
    private profileData: DatalinkService, 
    private server:ServerlinkService, 
    public dialog: MatDialog
  ) { 
    this.model.text = 'X'
    this.model.size = 2
    this.model.isBlack = true;
    this.form.addText('text')
    this.form.addNumber('size')
    this.model.canUndo = false;
    this.form.build()
    this.x = 0;
    this.y = 0;
  }

  ngOnInit() {
    // setTimeout(() => {
    //   this.switch.stamp = this;
    // }, 100);
  }
  getGoogleMap(){
    this.parent.parent.topNav.loading = true;
    const q = new  QueryModel()
    q.searchId = this.parent.model._id
    q.queryType = QueryType.googleMap
    this.notifyEffect(true, 'Mapping...')
    this.profileData.getProfile(q, (err, result)=>{
      this.notifyEffect(false)
      if(result){
        this.parent.model = <PhotoModel>result.result
        this.getReferences()
        //gets the google map, which should be the last reference that was created
        // console.log('map updated last reference path is ', this.parent.model)
        this.image.getImage(this.parent.model._id, 1, this.parent.model.lastReferencePath, true, ()=>{}) 
      }
    })
  }

  //opens a detail window of the currently selected image
  openLargeImage(){
    const getId = this.parent.model.referencePath[this.parent.model.lastReferencePath].referenceId;
    this.data.getImage(getId, 0, 0, (err, imagePath)=>{
      const q = new QueryModel();
      q.searchId = getId;
      this.profileData.getProfile(q, (err, imageInfo)=>{
        let dialogRef = this.dialog.open(ImageLargeComponent, {
          data: {type: 'ImageHigh', data: imagePath.result, model: imageInfo.result},
        });
      })
    })
  }
  init(){
    this.switch.stampImage = this.image
    this.switch.currentPreview = this.parent.model.lastReferencePath? this.parent.model.lastReferencePath: 0;
    this.getReferences()
    if(this.server.model.contraintImageHeightCard) this.image.contraintClass = 'imgContraintHeight' //limit height
  }

  //this is lazy called when card is expanded, it is only called once
  getImage(callback:(err, data)=>void){
    this.init();
    this.image.getImage(this.parent.model._id, 1, this.parent.model.lastReferencePath, true, callback)
  }
  changeSize(size:number){
    this.model.size = size
  }
  stamp(){
    if(this.stampLock) {
      this.notify.toast('Stamping please wait', 0);
      return;
    }
    this.stampLock = true;
    this.model.reset()
    this.model.id = this.parent.model._id
    this.model.x = this.x
    this.model.y = this.y
    if(this.infoStampMode) {
      this.notifyEffect(true,'Stamping info...' );
      this.model.mode = MarkMode.info;
    }else{
      this.notifyEffect(true,'Marking...' );
    }
    this.model.number = this.parent.model.lastReferencePath
    // console.log('mark ', this.model)
    // console.log('model now ', this.parent.model)
    //returns result: {model:.., image64:...}
    this.parent.parent.topNav.loading = true;
    this.data.getMark(this.model, (err, result)=>{
      this.notifyEffect(false)
      if(result){
        //if marked, the model will be updated, and the image64 is return
        this.setNewModelAndReload(result)
        this.stampLock = false;
        this.model.canUndo = true;
        // console.log('stampping ', result)
      }
    })
  }
  notifyEffect(isActive: boolean, message?:string){
    if(isActive){
      this.parent.parent.topNav.loading = true;
      this.parent.parent.topNav.overrideText = message;
    }else{
      this.parent.parent.topNav.loading = false;
      this.parent.parent.topNav.overrideText = null;
    }
  }
  extract(){
    this.model.reset()
    this.model.id = this.parent.model._id
    this.model.x = this.x
    this.model.y = this.y
    this.model.number = this.parent.model.lastReferencePath
    this.model.mode = MarkMode.extract;
    // console.log('mark ', this.model)
    this.data.getMark(this.model, (err, result)=>{
      if(result.result){
        this.notify.toastAM('Extracting into ' + result.result)
      }
    })
  }

  clone(){
    this.model.reset()
    this.model.id = this.parent.model._id
    this.model.x = this.x
    this.model.y = this.y
    this.model.number = this.parent.model.lastReferencePath
    this.model.mode = MarkMode.clone;
    const toCloneIndex = this.parent.model.lastReferencePath+1
    // console.log('mark ', this.model)
    this.data.getMark(this.model, (err, result)=>{
      if(result){
        this.setNewModelAndReload(result)
        this.notify.toastAM('Reference ' + toCloneIndex + ' clone to reference ' + (this.parent.model.lastReferencePath+1))
      }
    })
  }

  clear(){
    this.model.reset()
    this.model.id = this.parent.model._id
    this.model.mode = MarkMode.clear;
    this.model.number = this.parent.model.lastReferencePath
    this.data.getMark(this.model, (err, result)=>{
      if(result){
        //if cleared, the model will be updated, and the image64 is return
        this.parent.image64HighStamp = result.image64
        this.setNewModelAndReload(result)
      }
    })
  }
  deleteButton(){
    if(this.parent.model.referencePath.length < 2){
      this.notify.toast('You must have at least one reference.', 0)
      return
    }
    this.notify.confirmDelete('Permanently delete reference '+ (this.parent.model.lastReferencePath+1) + ' ?','', ()=>{
      this.delete();
    })
  }
  delete(){
    this.model.reset()
    this.model.id = this.parent.model._id
    this.model.mode = MarkMode.delete
    this.model.number = this.parent.model.lastReferencePath
    console.log('mark de ', this.model.number)
    //returns result: {model:.., image64:...}
    this.data.getMark(this.model, (err, result)=>{
      if(result){
        //if marked, the model will be updated, and the image64 is return
        this.setNewModelAndReload(result) //returns the model and image64 of the last one back for update
        console.log('passing this area for mark del ', result)        
      }
    })
  }
  undo(){
    if(this.model.canUndo){
      this.model.reset()
      this.model.id = this.parent.model._id
      this.model.mode = MarkMode.undo
      this.model.number = this.parent.model.lastReferencePath
      this.data.getMark(this.model, (err, result)=>{
        if(result){
          //if marked, the model will be updated, and the image64 is return
          this.setNewModelAndReload(result)
          // console.log('passing this area ', this.parent.model)        
        }
      })
    }
  }

  //creates a new reference base on the view
  mirror(){
    this.model.reset()
    this.model.id = this.parent.model._id
    this.model.mode = MarkMode.mirror
    this.data.getMark(this.model, (err, result)=>{
      if(result){
        //if marked, the model will be updated, and the image64 is return
        this.setNewModelAndReload(result)
        // console.log('passing this area ', this.parent.model)        
      }
    })
  }

  setNewModelAndReload(markResult:{model: PhotoModel}){
    this.parent.model = markResult.model;
    this.getReferences();
  }

  //gets lazy called with getImage()
  getReferences(){
    this.referencesHaveUpdated = true;
    this.switch.setImage()
  }

  infoStamp(){
    this.infoStampMode = !this.infoStampMode;
    this.model.size = this.infoStampMode? 1: 2; //use smaller stamp if info mode is enabled
  }
  getColorStatus(){
    if(!this.server.model.settableStampColor) return 'Auto'
    if(this.model.isBlack){
      return 'Black'
    }else{
      return 'White'
    }
  }
  @HostListener('mousemove', ['$event'])
    mouseHandling(event) {
      const elementId = this.image.elementId //id of the image
      // console.log(event.target.id)
      if(event.target.id === elementId){ //event.target.id === imageId
        event.preventDefault();
        const xImage = 0 //document.getElementById(imageId).getBoundingClientRect().left
        const yImage = 0//document.getElementById(imageId).getBoundingClientRect().top //use 0 for local space
        const wImage = document.getElementById(elementId).getBoundingClientRect().width
        const hImage = document.getElementById(elementId).getBoundingClientRect().height
        let xMouseOnImage = (event.offsetX-xImage) > 0? event.offsetX-xImage: 0
        xMouseOnImage = xMouseOnImage < wImage? xMouseOnImage: wImage
        let yMouseOnImage = (event.offsetY-yImage) > 0? event.offsetY-yImage: 0
        yMouseOnImage = yMouseOnImage < hImage? yMouseOnImage: hImage
        // console.log('OverImage', xMouseOnImage, ' ', yMouseOnImage);
        this.x = xMouseOnImage
        this.y = yMouseOnImage
      }
  }

}
