import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-disable-overlay',
  templateUrl: './disable-overlay.component.html',
  styleUrls: ['./disable-overlay.component.css']
})
export class DisableOverlayComponent implements OnInit {
@Input() screenMode = mode.off;
@Input() message = '';
  constructor() { }

  ngOnInit() {
  }

}

export enum mode {deleteAll = 5, reboot = 4, redirect = 3, upload = 2, screen = 1, off = 0}
