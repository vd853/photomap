import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { CardsComponent } from '../cards/cards.component';
import { TopnavComponent } from '../topnav/topnav.component';
import { ServerlinkService } from '../Service/serverlink.service';
import { DisableOverlayComponent, mode } from './disable-overlay/disable-overlay.component';
import {darkColor} from '../r'
import { GlobalService } from '../Service/global.service';

@Component({
  selector: 'app-enclosure',
  templateUrl: './enclosure.component.html',
  styleUrls: ['./enclosure.component.css']
})
export class EnclosureComponent implements OnInit {
  backgroundColor: string;
  message: string; //is binded to overlay component

  screenMode = mode.off;

  @ViewChild('cards') cards: CardsComponent
  @ViewChild('topNav') topNav: TopnavComponent
  @ViewChild('overlay') overlay: DisableOverlayComponent
  acceptableWidth = 1350
  constructor(private server:ServerlinkService, private global:GlobalService) { //only use to get the first server data setting 
  }
  ngOnInit() {
    this.server.enclosure = this;
    this.topNav.parent = this;
    this.cards.parent = this;
    this.topNav.cards = this.cards
    this.cards.topNav = this.topNav;
    this.global.init();
  }
  backgroundDark(isDark:boolean){
    if(isDark){
      this.backgroundColor = darkColor
      return;
    }
    this.backgroundColor = 'white'
  }

  deleteAllTriggered(){
    this.screenMode = mode.deleteAll;
  }

  redirectUpdate(isDHCP: boolean, port?: number, countdown?: number){
    this.screenMode = mode.redirect;
    if(isDHCP){
      if(!port) throw "port value not set"
      this.message = "You will need to manually redirect your IP at port: " + port;
    }else{
      if(!countdown) throw "countdown value must be set";
      setInterval(()=>{
        this.message = "You will be redirected (" + countdown + ")"
        if(countdown > 0) countdown--; 
      }, 1000)
    }
  }

  reboot(message: string){
    this.screenMode = mode.reboot;
    this.message = message;
    // if(!countdown) throw "countdown value must be set";
    //   setInterval(()=>{
    //     this.message = "You will be redirected (" + countdown + ")"
    //     if(countdown > 0) countdown--; 
    // }, 1000)
  }

  //should only be called by uploader
  updateUpload(value: string){
    if(this.screenMode === mode.upload) this.message = value; //init first value
    if(this.screenMode !== mode.upload){
      this.screenMode = mode.upload;
      this.message = value;
    }
  }
  initWindowCheck(){
    console.log('init check of size')
    this.checkWindowsWidth(window.innerWidth)
  }

  //should only be called by screen
  checkWindowsWidth(width: number){
    if(this.screenMode !== mode.screen) return;
    console.log('width change ', width, ' setting ', this.server.model.disableScreenSizeWarning)
    if(this.server.model.disableScreenSizeWarning) return;
    if(width < this.acceptableWidth){
      console.log('Windows size too small')
      if(this.screenMode !== mode.screen) {
        this.screenMode = mode.screen;
      }
      this.message = (width/this.acceptableWidth*100).toFixed(0) + '%'
    }else{
      this.screenMode = mode.off
    }
  }
  debug(){
  }
}
