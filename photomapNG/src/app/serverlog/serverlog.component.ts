import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ServerlinkService } from '../Service/serverlink.service';
import { DownloadlinkService } from '../Service/downloadlink.service';
import { ModifyModel, RequestType, DownloadType, globals } from '../r';
import { InfoGetterService } from '../Service/info-getter.service';

@Component({
  selector: 'app-serverlog',
  templateUrl: './serverlog.component.html',
  styleUrls: ['./serverlog.component.css']
})
export class ServerlogComponent implements OnInit, OnDestroy  {
  timer: any
  logs = []
  ngOnDestroy(): void {
    console.log('log closed')
    clearInterval(this.timer);
  }
  enabled = false
  constructor(
    public server:ServerlinkService, 
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ServerlogComponent>,
    private downloadLink: DownloadlinkService,
    public snackBar: MatSnackBar,
    public infoGetter: InfoGetterService
  ) { 
    this.enabled = true
  }

  ngOnInit() {
    this.timer = setInterval(()=>{
      this.server.getServer((err, data)=>{
        this.server.model = data.result;
        this.logs = this.server.model.logs.reverse()
        console.log('log reloaded ', data.result)
      })
    }, 1000)
  }
  download(requestId: string){
    const mm = new ModifyModel()
    mm.ids = [requestId];
    mm.downloadType = 3;
    console.log('reqest download ', mm)
    this.snackBar.open('File downloading','', {
      duration: 2000,
    });
    this.downloadLink.download(mm, (err, data)=>{
      console.log('download link return ', data)
      let snack = 'Requested file have expired. Time limit is ' + globals.requestExpirationDays + ' days.'
      if(data){
        snack = 'File downloaded ' + data
      }
      this.snackBar.open(snack,'', {
        duration: 2000,
      });
    })
  }

}


