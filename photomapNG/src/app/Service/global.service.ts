import { Injectable } from '@angular/core';
import { CardsComponent } from '../cards/cards.component';
import { HttpClient } from '@angular/common/http';
import * as jsonfile from 'jsonfile';
@Injectable()
export class GlobalService {
  linkDefined = false;
  isProduction = false;
  staticURL: string;
  mainURL: string;
  cards: CardsComponent;
  linkDependance: linkDependance[] = []
  options = {withCredentials: true}
  constructor(private http: HttpClient) { 

    //silence console.log
    if(this.isProduction) console.log = ()=>{};

    this.init();
  }

  init(){
    //ip info is taken from the assets/config.json file which is modified when the backend server starts
    this.http.get('/assets/config.json').subscribe((data:any)=>{
      console.log('config read as ', data);
      let isSSL = false;
      if(data.ssl === 'true') isSSL = true;
      this.staticURL = (isSSL? 'https://':'http://') + data.ipv4 + ':' + data.port + '/';
      console.log('using staticURL ', this.staticURL)
      this.mainURL = this.staticURL + 'api/'
      setTimeout(() => {
        console.log('dep link ', this.linkDependance)
        this.linkDependance.forEach(e=>{
          if(!e.linkDefined){
            e.OnLink(this.mainURL)
            e.linkDefined = true;
          }
        })
      }, 1);
    })
  }

  //should only be use for stamp-switch thumbnails dropdown in the markup
  //sincee it doesn't use the image component for conversion
  staticMap(linkPath: string){
    return this.staticURL + linkPath
  }
  noLoadedImage(){
    return this.staticURL + 'nullImageSelection.jpg'
  }
}
enum mode {production}

//NOTE: image, section, download link uses isOnLink because their OnLink is not called from this constructor
//do they will will get the mainURL from their own constructor
export interface linkDependance{
  linkDefined: boolean;
  OnLink(mainURL:string)
}