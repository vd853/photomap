import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { QueryModel, PhotoModel } from '../r';
import { GlobalService, linkDependance } from './global.service';
import { LoginGuard } from '../login.guard';

@Injectable()
export class DatalinkService implements linkDependance {
  linkDefined: boolean;
  url = 'profile/'
  OnLink(mainURL:string) {
    this.url = mainURL + this.url;
  }
  constructor(private http: HttpClient, private global:GlobalService, private loginGuard:LoginGuard) { 
    this.global.linkDependance.push(this)
  }
  delProfile(query: QueryModel, callback:(err, data)=>void){
    query.sessionID = this.loginGuard.model.sessionID;
    this.http.delete(this.url + JSON.stringify(query), this.global.options).subscribe(response=>{
      callback(null, response)
    })
  }

  getProfile(query: QueryModel, callback:(err, data)=>void){
    query.sessionID = this.loginGuard.model.sessionID;
    this.http.get(this.url + JSON.stringify(query), this.global.options).subscribe((response:any)=>{
      if(response.result){
        callback(null, response)
      }else{
        console.log('ERROR getProfile ', response.error)
        callback(null, null)
      }
    })
  }

  // //consider using querymodel instead
  // updateProfile(model: PhotoModel, callback:(err, data)=>void){
  //   this.http.post(this.url + '0', model).subscribe(response=>{
  //     callback(null, response)
  //   })
  // }

  updateProfiles(data: {ids: string[], model: PhotoModel}, callback:(err, data)=>void){
    console.log('request model update')
    this.http.post(this.url + this.loginGuard.model.sessionID, data, this.global.options).subscribe(response=>{
      console.log('response update ', response)
      callback(null, response)
    })
  }

  //this might never be used?
  getProfilesFields(query: QueryModel, callback:(err, data)=>void){
    query.sessionID = this.loginGuard.model.sessionID;
    this.http.get(this.url + JSON.stringify(query), this.global.options).subscribe(response=>{
      callback(null, response)
    })
  }

}
