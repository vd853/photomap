import { Injectable } from '@angular/core';
import { linkDependance, GlobalService } from './global.service';
import { HttpClient } from '@angular/common/http';
import { AccountModel, IAuthenticateResponse, AccountStatus } from '../r';
import { LoginGuard } from '../login.guard';
import { LoginComponent } from '../login/login.component';

@Injectable()
export class AccountlinkService implements linkDependance {
  linkDefined: boolean;
  url = 'account/0'
  login: LoginComponent;
  OnLink(mainURL:string) {
    this.url = mainURL + this.url;
    this.getUsersAndId();
  }
  constructor(private http: HttpClient, private global:GlobalService) { 
    this.global.linkDependance.push(this)
  }
  authenticate(model: AccountModel, callback:(err, data: IAuthenticateResponse)=>void){
    this.http.post(this.url, model, this.global.options).subscribe((response:IAuthenticateResponse)=>{
      if(response){
        callback(null, response)
      }else{
        console.log('ERROR authentication ', response)
        callback(null, null)
      }
    })
  }
  getUsersAndId(){
    return new Promise<{user: string, _id: string}[]>(resolve=>{
      this.http.get(this.url.substr(0, this.url.length-1) + 'users').subscribe((response:{results: {user: string, _id: string}[]})=>{
        if(response){
          resolve(response.results);
        }else{
          throw new Error('could not get user accounts list');
        }
      })
    })
  }
  //will not use scramble for this
  logOut(model: AccountModel){
    if(!model) throw Error('Cannot logoff no account model registered.')
    if(model.status === AccountStatus.login){
      model.status = AccountStatus.loggingOut;
      this.authenticate(model, (err, data)=>{
        if(data){
          console.log('logged off ', data)
          this.login.expireCookies();
        }else{
          console.log('ERROR authentication ', data, err)
        }
      })
    }else{
      throw new Error('Account is not seen as login, but you are login.')
    }
  }
}
