import { TestBed, inject } from '@angular/core/testing';

import { AccountlinkService } from './accountlink.service';

describe('AccountlinkService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountlinkService]
    });
  });

  it('should be created', inject([AccountlinkService], (service: AccountlinkService) => {
    expect(service).toBeTruthy();
  }));
});
