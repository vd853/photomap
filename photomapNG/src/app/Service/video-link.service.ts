import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalService, linkDependance } from './global.service';
import { VideoModel, VideoQuery } from '../r';

@Injectable()
export class VideoLinkService implements linkDependance {
  linkDefined: boolean;
  isOnLink = false;
  url = 'video/' //returns video stream directly by VideoQuery
  urlStat = 'videoStats/'
  constructor(private http: HttpClient, private global:GlobalService) { 
    if(!this.isOnLink) this.OnLink(this.global.mainURL);
    this.global.linkDependance.push(this)
  }
  OnLink(mainURL:string) {
    this.url = mainURL + this.url;
    this.urlStat = mainURL + this.urlStat;
    this.isOnLink = true;
  }
  getVideoStats(vQuery: VideoQuery, callback:(data:VideoModel)=>void){
    this.http.get(this.urlStat+JSON.stringify(vQuery), this.global.options).subscribe(d=>{
      console.log('video stats return ', d);
      const data = JSON.parse(JSON.stringify(d))
      callback(<VideoModel>data.model)
    })
  }
  getVideoQueryStreamLink(vQuery: VideoQuery){
    return this.url + JSON.stringify(vQuery)
  }
}
