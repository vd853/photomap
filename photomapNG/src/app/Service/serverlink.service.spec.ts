import { TestBed, inject } from '@angular/core/testing';

import { ServerlinkService } from './serverlink.service';

describe('ServerlinkService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServerlinkService]
    });
  });

  it('should be created', inject([ServerlinkService], (service: ServerlinkService) => {
    expect(service).toBeTruthy();
  }));
});
