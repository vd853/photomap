import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalService, linkDependance } from './global.service';
import { QueryModel, SectionModel } from '../r';
import { query } from '@angular/animations';

@Injectable()
export class SectionlinkService implements linkDependance {
  linkDefined: boolean;
  isOnLink = false;
  url = 'section/'
  OnLink(mainURL:string) {
    this.url = mainURL + this.url;
  }
  constructor(private http: HttpClient, private global:GlobalService) { 
    if(!this.isOnLink) this.OnLink(this.global.mainURL);
    this.global.linkDependance.push(this)
    this.isOnLink = true;
  }
  getSectionsByReference(query: QueryModel, callback:(err, data)=>void){
    this.http.get(this.url + JSON.stringify(query), this.global.options).subscribe((response:any)=>{
      if(response.result){
        callback(null, response)
      }else{
        console.log('ERROR getProfile ', response.error)
        callback(null, null)
      }
    })
  }

  delSection(section: SectionModel, callback:(err, data)=>void){
    const q = new QueryModel();
    q.searchId = section._id;
    this.http.delete(this.url + JSON.stringify(q), this.global.options).subscribe((response:any)=>{
      if(response.result){
        callback(null, response)
      }else{
        console.log('ERROR getProfile ', response.error)
        callback(null, null)
      }
    })
  }

  addSection(section: SectionModel, callback:(err, data)=>void){
    this.http.post(this.url + '0', section, this.global.options).subscribe((response:any)=>{
      if(response.result){
        callback(null, response)
      }else{
        console.log('ERROR getProfile ', response.error)
        callback(null, null)
      }
    })
  }
}
