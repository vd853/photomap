import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {GlobalService, linkDependance} from './global.service'
import { timeout } from 'q';
@Injectable()
export class KeywordLinkService implements linkDependance {
  linkDefined: boolean;
  //for any components that needs keywords on start, otherwise reference keywords directly
  registerComponents: Array<{component: keywordComponents, key: string}> = []
  url = 'keyword/0'
  keywords: any;
  polling = false;
  constructor(private http: HttpClient, private global:GlobalService) { 
    this.global.linkDependance.push(this)
  }
  OnLink(mainURL:string) {
    this.url = mainURL + this.url;
    this.updateKeyword();
  }
  //updated on page reload and after any type of photoform modify
  updateKeyword(callback?:()=>void){
    this.getKeyword((err, data)=>{
      if(data){
        console.log('keyword update')
        this.keywords = data;
        console.log('keywords ', this.keywords)

        //preregisters for components that starts up
        this.registerComponents.forEach(e=>{
          e.component.inputEnabled = false;
          setTimeout(() => {
            e.component.keywords = data[e.key]
            e.component.initAutofill()
            e.component.inputEnabled = true;
          }, 0);
        })
        if(callback) callback();
      }
    })
  }
  private getKeyword(callback:(err, data)=>void){
    this.http.get(this.url, this.global.options).subscribe((response: any)=>{
      if(response.result){
        callback(null, response.result)
      }else{
        console.log('ERROR keyword: ', response.error)
      }
    })
  }
}

export interface keywordComponents{
  initAutofill()
  keywords: string[]
  inputEnabled: boolean
}
