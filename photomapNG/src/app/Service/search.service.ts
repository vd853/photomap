import { Injectable } from '@angular/core';
import * as _ from 'lodash'
import { TopnavComponent } from '../topnav/topnav.component';
import { DomSanitizer } from '@angular/platform-browser';
import { PhotoModel, globals } from '../r';
@Injectable()

//ng g s search
export class SearchService {
  //filterLimit = 50;
  filterDisabled = true; //filter will disable at the cards after 50 cards are loaded, due to performance issue
  topNav: TopnavComponent
  currentSearch = '' //this is updated by some search input field
  constructor(private sanitizer: DomSanitizer) { }

  //this is use by the card
  check(text: Array<string>):Boolean{
    // if(typeof text === 'object'){
    //   return text.some(e=>{
    //     if(_.includes(e, this.currentSearch)) return true
    //   })
    // }
    if(!this.currentSearch) return true; //returns all if text search is blank
    
    //make each term lower case and remove the undefined
    const lowerText = []
    text.forEach(t=>{
      if(t && t !== 'undefined')lowerText.push(t.toString().toLowerCase())
    })

    //if any term matches, the card is shown
    const show = lowerText.some(t=>{
      const regex = new RegExp(this.currentSearch, 'i');
      if(regex.test(t)) return true;
    })
    // console.log('text array ', lowerText)
    // console.log('show ', show)
    return show //_.includes(lowerText, this.currentSearch.toLowerCase())
  }

  highlight(content: string){
    if(this.filterDisabled) return globals.shorten(content);
    if(this.currentSearch === '') return globals.shorten(content);
    
    if(typeof content !== 'string'){
      content = JSON.stringify(content)
    }
    // console.log('highlight mode', typeof content)
    var regex = new RegExp('('+this.currentSearch+')', 'i');
    content = globals.shorten(content).replace(regex, '<span style="background-color:#7aabff">$1</span>');
    return this.sanitizer.bypassSecurityTrustHtml(content);
  }

  // your content will appear by itself inside the div due to the binding
  // <div [innerHTML]="search.highlight('my content')">
  // </div>

  viewCheck(photoModel:PhotoModel){
    if(_.includes(this.topNav.viewSelection, 'all')){
      return true;
    }

    let geotag = false, reference = false, photo = false;

    if(_.includes(this.topNav.viewSelection, 'reference') && photoModel.isReference){
      // console.log('reference includes')
      reference = true;
    }

    if(_.includes(this.topNav.viewSelection, 'geotag') && photoModel.hasGeoData){
      // console.log('geo includes')
      geotag = true;
    }

    if(_.includes(this.topNav.viewSelection, 'photo') && !photoModel.isReference){
      // console.log('geo includes')
      photo = true;
    }

    return reference || geotag || photo
  }
  clear(){
    this.currentSearch = ''
    this.topNav.currentSearch = ''
  }
}

//searcher
// <form [formGroup]="form.formGroup" validate>
//   <mat-form-field>
//     <input type="text" (keyup)="currentSearch = textbox.value" #textbox>
//   </mat-form-field> 
// </form>

//elements
//[hidden]="!search.check(model.component)"
