import { Injectable } from '@angular/core';
import { ServerModel } from '../r';
import { HttpClient } from '@angular/common/http';
import { CardsComponent } from '../cards/cards.component';
import { EnclosureComponent } from '../enclosure/enclosure.component';
import { GlobalService, linkDependance } from './global.service';

@Injectable()
export class ServerlinkService implements linkDependance {
  linkDefined: boolean;
  url = 'server/0'
  enclosure: EnclosureComponent
  model: ServerModel
  constructor(private http: HttpClient, private global:GlobalService){ 
    // this.url = global.mainURL + this.url;
    this.global.linkDependance.push(this);
  }
  OnLink(mainURL:string) {
    this.url = mainURL + this.url;
    console.log('server linking ', this.url)

    //this is the first update
    this.getServer((err, data)=>{
      if(data){
        this.model = <ServerModel>data.result
        console.log('server retrieved ', this.model)
        if(!this.enclosure) return;
        // this.enclosure.cards.initFromServer(this.model.columnView)
        this.enclosure.initWindowCheck();
        if(this.model.loadAllOnStart){
          console.log('loading all')
          this.enclosure.topNav.startSearch();
        }
      }
    })
  }
  getServer(callback:(err, data)=>void, reboot?:()=>void){
    this.http.get(this.url, this.global.options).subscribe(
    data=>{
      if(data){
        callback(null, data)
      }
    },
    err =>{
      reboot();
    })
  }
  updateServer(model: ServerModel, callback:(err, data)=>void){
    this.http.post(this.url, model, this.global.options).subscribe(data=>{
      if(data){
        this.enclosure.cards.batchGetImageLow() //always get the low image
        callback(null, data)
      }else{
        console.error('server update ERROR');
      }
    })
  }
}
