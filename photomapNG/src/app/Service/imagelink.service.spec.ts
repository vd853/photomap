import { TestBed, inject } from '@angular/core/testing';

import { ImagelinkService } from './imagelink.service';

describe('ImagelinkService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImagelinkService]
    });
  });

  it('should be created', inject([ImagelinkService], (service: ImagelinkService) => {
    expect(service).toBeTruthy();
  }));
});
