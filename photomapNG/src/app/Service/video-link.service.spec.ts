import { TestBed, inject } from '@angular/core/testing';

import { VideoLinkService } from './video-link.service';

describe('VideoLinkService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VideoLinkService]
    });
  });

  it('should be created', inject([VideoLinkService], (service: VideoLinkService) => {
    expect(service).toBeTruthy();
  }));
});
