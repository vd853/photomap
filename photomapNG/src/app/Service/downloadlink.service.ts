import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {saveAs as importedSaveAs} from "file-saver";
import { ModifyModel, QueryModel, PhotoModel } from '../r';
import { GlobalService, linkDependance } from './global.service';
import { DatalinkService } from './datalink.service';
import { RequestModel, RequestType, DownloadType } from '../../../../PhotomapNode/app/Models/request';

@Injectable()
export class DownloadlinkService implements linkDependance {
  linkDefined: boolean;
  isOnLink = false;
  url = 'download/'
  constructor(private http: HttpClient, private global:GlobalService, private datalink: DatalinkService) { 
    this.global.linkDependance.push(this)
    if(!this.isOnLink) this.OnLink(this.global.mainURL);
  }
  OnLink(mainURL:string) {
    this.url = mainURL + this.url;
    this.isOnLink = true;
  }
  download(model:ModifyModel, callback:(err, data)=>void){
    //request notification only
    if(model.requestType === RequestType.request){
      this.http.get(this.url + JSON.stringify(model), this.global.options).subscribe((response: any)=>{
        if(response.result){
          return callback(null, response.result);
        }else{
          console.log('Error in request notify ', response.error)
        }
      })
      return;
    }

    //0 download orginial, 1 reference, 2 merge, 3 request
    this.getFileName(model, (err, data)=>{
      if(err){
        console.log(err)
        callback(err, null)
        return;
      }

      //requested downloads from batch, file is expired, so db and file doesn't exist
      if(!data){
        callback(null, null)
        return;
      }

      this.http.get(this.url + JSON.stringify(model), { responseType: 'blob', withCredentials: true }).subscribe((response: any)=>{
        if(response){
          console.log('download resposne ', response)
          this.downloadFile(response, data)
          callback(null, data)
        }
      })
    })
  }

  getFileName(model: ModifyModel, callback:(err, data)=>void){
    if(model.ids.length > 1){
      console.log('ERROR cannot have more than one id, also this must be a request id');
      callback('ERROR cannot have more than one id, also this must be a request id', null)
      return;
    }
    
    //for request download
    if(model.downloadType === DownloadType.requestDownload){
      this.requestDownloadMode(model, callback)
      return;
    }

    //for single file download
    const q = new QueryModel();
    q.searchId = model.ids[0]
    this.datalink.getProfile(q, (err, data:any)=>{
      //0 download orginial, 1 reference, 2 merge, 3 request
      if(data.result){
        const returnModel = <PhotoModel>data.result;
        switch(model.downloadType){
          case DownloadType.high:
            callback(null, returnModel.fileName)
            break;
          case DownloadType.reference:
            const fn1 = returnModel.referencePath[returnModel.lastReferencePath].mark.split('/');
            callback(null, fn1[fn1.length-1])
            break;
          case DownloadType.merge:
            const fn2 = returnModel.mergerPath.split('/');
            console.log('TEST ', fn2[fn2.length-1])
            callback(null,  fn2[fn2.length-1])
            break;
        }
      }else{
        callback(data.error, null)
      }
    })
  }

  requestDownloadMode(model: ModifyModel, callback:(err, data)=>void){
    const m = new ModifyModel();
    m.requestType = RequestType.lookup //should return request id to get the .zip name, otherwise it will be null
    m.ids = [model.ids[0]]
    console.log('requesting lookup')
    this.http.get(this.url + JSON.stringify(m), this.global.options).subscribe((response: any)=>{
      console.log('reponse request download ', response)
      if(response){ 
        callback(null, response.result? response.result._id + '.zip': null)
      }else{
        console.log('lookup ERROR ', response)
      }
    })
  }

  downloadFile(data: any, fileName: string){
    var blob = new Blob([data], { type: "image/jpg" }); //change this if needed
    importedSaveAs(blob, fileName);
  }

}
