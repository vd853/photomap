import { TestBed, inject } from '@angular/core/testing';

import { DownloadlinkService } from './downloadlink.service';

describe('DownloadlinkService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DownloadlinkService]
    });
  });

  it('should be created', inject([DownloadlinkService], (service: DownloadlinkService) => {
    expect(service).toBeTruthy();
  }));
});
