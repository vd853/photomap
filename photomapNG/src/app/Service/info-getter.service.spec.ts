import { TestBed, inject } from '@angular/core/testing';

import { InfoGetterService } from './info-getter.service';

describe('InfoGetterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InfoGetterService]
    });
  });

  it('should be created', inject([InfoGetterService], (service: InfoGetterService) => {
    expect(service).toBeTruthy();
  }));
});
