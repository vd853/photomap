import { TestBed, inject } from '@angular/core/testing';

import { SectionlinkService } from './sectionlink.service';

describe('SectionlinkService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SectionlinkService]
    });
  });

  it('should be created', inject([SectionlinkService], (service: SectionlinkService) => {
    expect(service).toBeTruthy();
  }));
});
