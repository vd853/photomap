import { Injectable } from '@angular/core';
import {globals, PhotoModel} from '../r'
import { CardsComponent } from '../cards/cards.component';
import * as moment from 'moment'
import * as _ from 'lodash';
import { ServerlinkService } from './serverlink.service';
@Injectable()
export class InfoGetterService {
  cards: CardsComponent

  constructor(private serverLink:ServerlinkService) { }
  //base on viewables from node globals
  getViewByKey(model: PhotoModel, key: string){
    let value = model[key]
    return this.getValueByKey(model, key);
  }
  //base on whatever columnView is currently set to, use in heading markup
  getColumnByIndex(model: PhotoModel, n: number){
    if(!model) return '';
    const key = this.cards.columnView[n]
    return this.getValueByKey(model, key);
  }
  public getFormatedDate(validDate: string){
    return moment(validDate).utcOffset('+00:00').calendar(null, {sameElse: 'M/D/YY [at] h:mm A'})
  }
  public getFormatedDateNormal(validDate: string){
    return moment(validDate).calendar(null, {sameElse: 'M/D/YY [at] h:mm A'})
  }
  private getValueByKey(model: PhotoModel, key: string){
    let value = PhotoModel.getAnyFieldValueByKey(key, model);
    return (key === 'date' || key === 'createdDate')? this.getFormatedDate(value) : value
  }
}
