import { Injectable } from '@angular/core';
import { PhotoModel } from '../r';
import { CardComponent } from '../card/card.component';
import { CardsComponent } from '../cards/cards.component';
@Injectable()
export class CacheService {
  cards: CardsComponent;
  ImageHigh = new Map(); //key is the model._id, value is the image64
  PhotoModels = new Map<string, {index: number, model:PhotoModel, card: CardComponent}>(); //get photomodel by photo id
  // PhotoModelsChildrenIndex = new Map(); //get children photomodel by children index
  constructor() { }

}
