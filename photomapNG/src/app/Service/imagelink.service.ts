import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PhotoModel, MarkModel } from '../r';
import { GlobalService, linkDependance } from './global.service';

@Injectable()
export class ImagelinkService implements linkDependance {
  linkDefined: boolean;
  isOnLink = false;
  url = 'image/'
  urlMark = 'mark/'
  constructor(private http: HttpClient, private global:GlobalService) { 
    if(!this.isOnLink) this.OnLink(this.global.mainURL);
    this.global.linkDependance.push(this)
  }
  OnLink(mainURL:string) {
    this.url = mainURL + this.url;
    this.urlMark = mainURL + this.urlMark;
    this.isOnLink = true;
  }
  //type => 0: compress, 1: mark last visited, 2: compress2
  //reference param is use for select which reference instead of using the last visit
  //reference param is ignored when using type 0 and 2
  getImage(id: string, type: number, reference: number, callback:(err, data)=>void){
    this.http.get(this.url + id + '/' + type + '/' + reference, this.global.options).subscribe(response=>{
      console.log('getImage ', response)
      callback(null, response)
    })
  }
  getMark(model: MarkModel, callback:(err, data)=>void){
    this.http.get(this.urlMark + JSON.stringify(model), this.global.options).subscribe((response:any)=>{
      if(!response.error){ //need to return full resposne because it can have .image64, .model, and .response
        callback(null, response)
      }else{
        console.log('getMark ERROR ', response.error)
      }
    })
  }
}
