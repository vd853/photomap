import { TestBed, inject } from '@angular/core/testing';

import { KeywordLinkService } from './keyword-link.service';

describe('KeywordLinkService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KeywordLinkService]
    });
  });

  it('should be created', inject([KeywordLinkService], (service: KeywordLinkService) => {
    expect(service).toBeTruthy();
  }));
});
