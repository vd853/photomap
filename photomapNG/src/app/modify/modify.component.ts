import { Component, OnInit, ViewChild, Inject, HostListener } from '@angular/core';
import { PhotoformComponent } from '../photoform/photoform.component';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { DatalinkService } from '../Service/datalink.service';
import { QueryModel, ModifyModel, PhotoModel, GUID } from '../r';
import { SearchService } from '../Service/search.service';
import { DownloadlinkService } from '../Service/downloadlink.service';
import { ReferenceComponent } from '../reference/reference.component';
import { GlobalService } from '../Service/global.service';
import { NotificationDirective } from '../Directives/notification.directive';
import { KeywordLinkService } from '../Service/keyword-link.service';
import * as r from '../r';
import * as _ from 'lodash';
import { QueryType } from '../../../../PhotomapNode/app/Models/query';
import { CacheService } from '../Service/cache.service';

@Component({
  selector: 'app-modify',
  templateUrl: './modify.component.html',
  styleUrls: ['./modify.component.css']
})
export class ModifyComponent implements OnInit {
  isBatch = true; //only use for downloading functions
  moveForward = true;
  selections = []
  @ViewChild('photoform') photoform: PhotoformComponent
  @ViewChild('reference') reference: ReferenceComponent
  @ViewChild(NotificationDirective) notify: NotificationDirective
  useModel: PhotoModel
  previousFileName: string;

  childPointer = 0 //current children to point model at

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<PhotoformComponent>,
  private datalink: DatalinkService,
  private search: SearchService,
  public snackBar: MatSnackBar,
  private downloadlink: DownloadlinkService,
  private global: GlobalService,
  private keyword: KeywordLinkService,
  private cache: CacheService
  ) { 

  }

  ngOnInit() {
    // this.childPointer = -1; //DEBUG
    // this.nextChild() //DEBUG
    if(this.data.type === 'update'){
      this.useModel = this.photoform.getModel();
      this.useModel = this.data.data //this is from the material data
      this.selections = [this.useModel._id]
      this.isBatch = false;
      this.childPointer = this.data.position-1; //-1 since nextChild() will move to the correct position
      this.nextChild();
    }else{
      this.selections = this.data.data
    }
  }

  nextChild(){
    this.childPointer++;
    if(this.childPointer > this.global.cards.children.length-1){
      this.childPointer = 0;
    }
    // console.log('child pointer ', this.childPointer)
    this.setModel();
  }
  forwardChildAndSave(isForward: boolean){
    this.selections = [this.useModel._id] //set selection to save
    this.previousFileName = this.useModel.fileName; //use for notifcation after save
    this.modify(false) //saves
    this.moveForward = isForward //set variable to move forward or backward after modify is callback
  }
  prevChild(){
    this.childPointer--;
    if(this.childPointer < 0){
      this.childPointer = this.global.cards.children.length-1;
    }
    // console.log('child pointer ', this.childPointer)
    this.setModel();
  }

  //this will cause binding with the instance model to the photoform and useModel
  setModel(){
    this.useModel = this.global.cards.children[this.childPointer].instance.model
    this.photoform.setModel(this.useModel)
  }
  getThumbnail(){
    return this.global.cards.children[this.childPointer].instance.imageLow
  }
  getFileName(){  
    return this.useModel? this.useModel.title? this.short(this.useModel.title): this.short(this.useModel.fileName): null
  }
  short(text: string){
    if(!text) return ''
    const max = 19;
    if(text.length > max){
      return text.substr(0, max) + '...'
    }
    return text
  }

  delete(){
    const deleter = ()=>{
      this.global.cards.topNav.loading = true;
      const q = new QueryModel()
      q.searchIds = this.selections
      //message to wait if too much are being deleted
      // if(this.selections.length > r.globals.modifyLimits/2) this.notify.toast('Please wait, photos are being deleted now.')
      this.datalink.delProfile(q, (err, data)=>{
        if(data.result){
          console.log('deleted ', data.result)
          this.dialogRef.close({delete: data.result}); //returns to the topnav with a list of ids to remove from cards
        }
        this.global.cards.topNav.loading = false;
      })
    }

    if(this.selections.length > 1){
      this.notify.confirmDelete('Permanently delete selected photos? ', '', deleter)
    }else{
      this.notify.confirmDelete('Permanently delete this photo? ', '', deleter)
    }
  }

  download(requestType: number, downloadType: number){
    if(this.selections.length < 1){
      this.snackBar.open('No photos selected','', {
        duration: 2000,
      });
      this.dialogRef.close();
      return;
    }
    this.snackBar.open('Preparing downloaded','', {
      duration: 2000,
    });
    const modify = new ModifyModel();
    modify.requestType = requestType;
    modify.downloadType = downloadType;
    modify.ids = this.selections;
    modify.isBatch = this.isBatch;
    this.downloadlink.download(modify, (err, data)=>{
      console.log(data)
      if(!this.isBatch){
        this.snackBar.open('File downloaded ' + data,'', {
          duration: 2000,
        });
      }else{
        this.snackBar.open('Your request Id is ' + data._id + '. Check log for download link.','dismiss', {
          duration: 10000,
        });
        this.dialogRef.close(true); //cause selection to deselect
      }
    })
  }

  getGoogleMaps(){
    const q = new QueryModel()
    q.searchId = GUID.create(true)
    q.searchIds = this.selections
    q.queryType = QueryType.googleMap
    this.datalink.getProfile(q, (err, result)=>{
      if(result){
        // console.log('google maps ', result)
        this.snackBar.open('Your request Id is ' + result.result + '. Check log for completion.','dismiss', {
          duration: 10000,
        });
        this.dialogRef.close(true); //cause selection to deselect
      }
    })
  }

  batchInfoStamp(){
    const q = new QueryModel()
    q.searchId = GUID.create(true)
    q.searchIds = this.selections
    q.queryType = QueryType.infoStamp
    this.datalink.getProfile(q, (err, result)=>{
      if(result){
        // console.log('google maps ', result)
        this.snackBar.open('Your request Id is ' + result.result + '. Check log for completion.','dismiss', {
          duration: 10000,
        });
        this.dialogRef.close(true); //cause selection to deselect
      }
    })
  }

  batchClearStamp(){
    const q = new QueryModel()
    q.searchId = GUID.create(true)
    q.searchIds = this.selections
    q.queryType = QueryType.clearStamp
    this.datalink.getProfile(q, (err, result)=>{
      if(result){
        // console.log('google maps ', result)
        this.snackBar.open('Your request Id is ' + result.result + '. Check log for completion.','dismiss', {
          duration: 10000,
        });
        this.dialogRef.close(true); //cause selection to deselect
      }
    })
  }

  //modify all selected ids to these settings
  modify(leave = true){
    this.useModel = this.photoform.getModel();
    // console.log('modify model ', this.useModel)
    // console.log('for selections ', this.selections)
    
    //set reference photo to modify if any
    if(this.reference.getCurrentSelection()){
      this.useModel.modifyReferenceId = this.reference.getCurrentSelection();
      this.useModel.modifyReference = true;
    }

    //give notice that modifications are changing now for none-queued and non-single modifications
    // if(this.selections.length < r.globals.modifyLimits && this.selections.length > 2){
    //   this.notify.toast('Please wait, photos are being modified now.')
    // }
    this.datalink.updateProfiles({ids: this.selections, model: this.useModel}, (err, data)=>{ //data is a number of queue
      console.log('modify return data ', data)
      if(data){
        this.search.clear()
        this.keyword.updateKeyword();
        if(leave) {

          //this will happen if too many photos are selected for modification
          if(this.isBatch){ 
            //!data.queue? 1: data.queue if only one is queue, result will be null because server will send back the complete model for just that one (used in non-batch updates), but one will actually be queued.
            this.notify.toast((!data.queue? 1: data.queue) + ' photos have been queued for modifications. Refresh at later time to see changes.', 6000)
          }

          //returns to the nav or view or heading
          this.dialogRef.close({ids: this.selections, batch: this.isBatch, oneModel: !this.isBatch? data.result: null}); 
        }else{
          //this part is use for when the save and next button is trigger
          //do a direct update of the model
          this.global.cards.children[this.childPointer].instance.model = data.result;
          this.snackBar.open(this.previousFileName + ' was saved.','dismiss', {
            duration: 2000,
          });
          if(this.moveForward){
            this.nextChild();
          }else{
            this.prevChild();
          }
          document.activeElement.scrollTop = 0;
          document.getElementById("photoformTop").focus();
        } 
      }
    })

    // if(this.data.type === 'update'){
    //   this.datalink.updateProfile(this.photoform.model, (err, data)=>{
    //     if(data){
    //       this.dialogRef.close(data); //returns to the view
    //     }
    //   })
    //   return
    // }
    // if(this.data.type === 'multiple'){
    //   console.log('multi update ', this.data.data)
    //   this.datalink.updateProfiles({ids: this.data.data, model: this.photoform.model}, (err, data)=>{
    //     if(data){
    //       this.search.clear()
    //       this.dialogRef.close(data); //returns to the nav
    //     }
    //   })
    // }
    
  }

  @HostListener("window:keydown", ['$event'])
  onKeyDown(event:KeyboardEvent) {
    // console.log('key modify ', event)

    if(event.code === 'Enter'){
      if(this.isBatch){
        this.modify();
      }else{
        this.forwardChildAndSave(true)
      }
    }

    if(event.code === 'Esc'){
      this.modify();
    }

    if(this.isBatch) return
    if(document.activeElement.tagName !== 'INPUT' && document.activeElement.tagName !== 'TEXTAREA'){
      if(event.code === 'ArrowRight' || event.code === 'KeyD'){
        console.log('left arrow')
        this.prevChild()
      }
      if(event.code === 'ArrowLeft' || event.code === 'KeyA'){
        console.log('right arrow')
        this.nextChild()
      }
    }
  }
}
