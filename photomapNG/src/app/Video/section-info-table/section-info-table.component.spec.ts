import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionInfoTableComponent } from './section-info-table.component';

describe('SectionInfoTableComponent', () => {
  let component: SectionInfoTableComponent;
  let fixture: ComponentFixture<SectionInfoTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionInfoTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionInfoTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
