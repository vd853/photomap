import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { maxCharLength, globals, capitalizeEachWord, SectionModel } from '../../r';
import { PreviewTextComponent } from '../../preview-text/preview-text.component';
@Component({
  selector: 'app-section-info-table',
  templateUrl: './section-info-table.component.html',
  styleUrls: ['./section-info-table.component.css']
})
export class SectionInfoTableComponent implements OnInit {
  @Input() blockEmpty = false;
  @Input() model: SectionModel
  viewables = globals.sectionViewables
  @ViewChild('preview') preview :PreviewTextComponent;
  constructor(
  ) { }

  ngOnInit() {
  }
  wordCapitalize(word: string){
    return capitalizeEachWord(word)
  }
  checkPreview(text: string, event: any){
    if(!text || !event) return;
    // if(text.length < maxCharLength) return //ignore short text
    // if(!text || text.toString() === 'true') return; //ignore if false or null or like boolean type
    // if(moment(text).isValid()) return //ignore date
    // console.log('showing text preview ', text)
    if(PreviewTextComponent.validator(text)){
      this.preview.text = text
      this.preview.update(event.clientX+20, event.clientY-120, true)
    }
  }

  checkPreviewNull(){
    console.log('null preview')
    this.preview.update(0,0, false)
  }

  unNull(text: any){
    //text input could be a boolean, if this is not use, it will return blank for false booleans
    if(typeof text === 'boolean'){
      if(!text){
        return 'No'
      }else{
        return 'Yes'
      }
    }

    if(!text || text === 'undefined') return ''
    return text
  }

  getModelByKey(key: string){
    if(!this.model) return '';

    if(!key) return 0;

    if(this.model[key]){
      if(key === 'endPercent' || key === 'startPercent'){
        return this.model[key].toFixed(3);
      }else{
        return this.model[key];
      }
    };

    //check for null and number values false returns
    if(key === 'endPercent' || key === 'startPercent'){
      return 0;
    }else{
      return '';
    }
  }
}
