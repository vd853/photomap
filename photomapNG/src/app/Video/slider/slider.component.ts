import { Component, OnInit, HostListener, Input, ElementRef } from '@angular/core';
import { GUID } from '../../r';
import { PlayerComponent } from '../player/player.component';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {
  barId = GUID.create();
  sliderId = 'S1_' + GUID.create();
  sliderId2 = 'S2_' + GUID.create();

  currentSliderId = this.sliderId;

  @Input() isLoop = false;

  mouseHeld = false;

  totalLength: number;
  barWidth: number;
  barOffset: number;
  sliderWidth: number;
  totalLengthOff: number;

  position1 = 0;
  position2 = 0;

  percent1: number;
  percent2: number;
  percent1Static = 0; //use for looping, this only changes by user
  
  @Input() videoLength;

  player: PlayerComponent;

  constructor() { }

  ngOnInit() {
  }
  ngAfterViewInit(): void {
    this.barOffset = document.getElementById(this.barId).clientLeft + document.getElementById(this.barId).getBoundingClientRect().left;
    console.log('bar offset to ', this.barOffset)
    this.barWidth = document.getElementById(this.barId).clientWidth;
    // console.log('bar offset to ', this.barWidth)
    this.sliderWidth = document.getElementById(this.sliderId).clientWidth;
    this.totalLength = this.barWidth;
    this.totalLengthOff = this.totalLength-this.sliderWidth;
    setTimeout(() => {
      this.setPositionByPercent(0, 0.5)
    }, 0);
  }
  

  @HostListener('mousemove', ['$event'])
  onMouseMove(event: any){
    if(!event.buttons){ //if true, the user is not holding any button
      this.onSrub(false, event);
    }
    this.setPositionX(event);
  }

  //sets the position of the handle base on which one is currently held down
  setPositionX(event: any){
    if(!this.mouseHeld) return;
    if(this.mouseHeld){
      if(this.currentSliderId === this.sliderId){ //slider1 in use
        this.position1 = event.clientX - this.barOffset - this.sliderWidth
        if(this.position1 < 0){
          this.position1 = 0
        }
        if(this.position1 > this.barWidth-this.sliderWidth){
          this.position1 =  this.barWidth-this.sliderWidth;
        }
        // console.log('position1 set ', this.position1)
      }else{ //slider2 in use
        this.position2 = event.clientX - this.barOffset - this.sliderWidth
        if(this.position2 < 0){
          this.position2 = 0
        }
        if(this.position2 > this.barWidth-this.sliderWidth){
          this.position2 =  this.barWidth-this.sliderWidth;
        }
        // console.log('position2 set ', this.position2)
      }
    }
    this.setPercentage();
    this.setSeekTime();
  }
  setPercentage(){
    this.percent1 = this.position1/this.totalLengthOff;
    this.percent2 = this.position2/this.totalLengthOff;
  }

  //use by section
  setPositionByPercent(percent1: number, percent2: number){
    if(this.mouseHeld) return;
    console.log('setting position by percent')
    this.position1 = percent1 * this.totalLengthOff;
    this.position2 = percent2 * this.totalLengthOff;
    this.setPercentage();
  }

  //use by player to move whichever handle is currently active
  setPositionByPercentActive(percent: number){
    if(this.currentSliderId === this.sliderId){
      this.setPositionByPercent(percent, this.percent2);
    }else{
      this.setPositionByPercent(this.percent1, percent);
    }
  }
  @HostListener('mousedown', ['$event'])
  onMousedown(event: any){
    if(this.isLoop){
      this.player.notify.toast('You must disable loop first.')
      return;
    }
    // console.log('mousedown ', event)
    this.setSlider(event);
    this.onSrub(true, event);
  }

  setSlider(event: any){
    event.path.forEach(e=>{
      if(e.id === this.sliderId){
        console.log('set slider1')
        this.currentSliderId = this.sliderId
        return;
      }
      if(e.id === this.sliderId2){
        console.log('set slider2')
        this.currentSliderId = this.sliderId2
        return;
      }
    })
  }

  onSrub(mousedown:boolean, anyMouseEvent: any){
    if(mousedown){
      this.setSeekTime(); //causes video to keep changing if mouse is down and scrubbing 
      this.mouseHeld = true;
    }else{
      this.mouseHeld = false;
      this.percent1Static = this.percent1;
      console.log('setting static ', this.percent1Static)
      return;
    }
    this.setPositionX(event);
  }

  setSeekTime(){
    if(this.currentSliderId === this.sliderId){
      this.player.setSeekTimeByPercent(this.percent1);
    }else{
      this.player.setSeekTimeByPercent(this.percent2);
    }
  }
  formatShort(num: Number){
    return num.toFixed(1)
  }
  prepareLoop(){
    this.currentSliderId = this.sliderId;
    if(this.percent1 > this.percent2){
      this.player.setSeekTimeByPercent(this.percent2);
      this.setPositionByPercent(this.percent2, this.percent1);
    }else{
      this.player.setSeekTimeByPercent(this.percent1);
      this.setPositionByPercent(this.percent1, this.percent2);
    }
    this.percent1Static = this.percent1;
    console.log('last pause ', this.player.lastPause)
  }
}
