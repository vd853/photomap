import { Component, OnInit, HostListener, ViewChild, Input } from '@angular/core';
import {VgAPI, VgMedia, VgEvents} from 'videogular2/core';
import { VideoModel, VideoQuery } from '../../r';
import { VideoLinkService } from '../../Service/video-link.service';
import { SliderComponent } from '../slider/slider.component';
import { NotificationDirective } from '../../Directives/notification.directive';
import { ServerlinkService } from '../../Service/serverlink.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {
  isLoop = false;
  api:VgAPI;
  enabled = false
  URL = 'http://localhost:1234/api/video/'
  model: VideoModel
  isQ0 = true;
  showControls = true;
  seek: number;
  seekHold: number; //hold while switching quality
  totalDuration: number;
  lastPause = 0;
  volume: number;
  link0: string;
  link1: string;
  isPlaying = false;
  @Input() noSlider = false;
  size = {'width': '640px', 'height': '480px'}
  @ViewChild('slider') slider:SliderComponent;
  @ViewChild(NotificationDirective) notify: NotificationDirective
  constructor(private videoLink: VideoLinkService, private server: ServerlinkService) { 
    document.addEventListener('webkitfullscreenchange', () => {
      this.fullscreen()
    })
  }

  ngOnInit() {
  }

  //this is use by viewer
  init(photoId: string){
    const q = new VideoQuery()
    q.searchIds = [photoId]
    this.videoLink.getVideoStats(q, (data:VideoModel)=>{
      if(data){
        this.model = data;
        q.quality = 'q0'
        this.link0 = this.videoLink.getVideoQueryStreamLink(q);
        q.quality = 'q1'
        this.link1 = this.videoLink.getVideoQueryStreamLink(q);
        this.enabled = true;
      }else{
        console.log('ERROR cannot find video model')
      }
    })
  }

  //call when viewer id destroy

  getQualityLabel(){
    if(this.isQ0) {
      return '720P'
    }

    //this will be condition for Q1
    //console.log('hasQ1? ', this.model.hasQ1)
    if(this.model.hasQ1.toString() === 'false'){
      return 'Original'
    }else{
      return '1080P'
    }
  }
  onPlayerReady(api:VgAPI) {
    console.log('assigning api')
    this.api = api;
    console.log('subscriptions ', this.api.subscriptions)
    if(this.server.model.autoVideoPlay && this.noSlider)this.play()

    this.api.subscriptions.play.subscribe(()=>{
      this.lastPause = this.api.currentTime/this.totalDuration;
      this.isPlaying = true;
    })

    if(this.noSlider) return;
    //causes slider to follow the timeline
    this.api.subscriptions.loadedMetadata.subscribe(e=>{
      this.slider.videoLength = e.srcElement.duration;
      this.slider.player = this;
      this.totalDuration = e.srcElement.duration;
      setInterval(()=>{
        if(this.isPlaying){
           this.seek = this.api.currentTime/this.totalDuration;
           this.slider.setPositionByPercentActive(this.seek);
           if(this.isLoop){
             if(this.seek > this.slider.percent2){
               console.log('static1 ', this.slider.percent1Static)
               this.setSeekTimeByPercent(this.slider.percent1Static)
             }
           }
        }
      }, 10)
    })
  }
    
  reverse(){
    console.log('reversing to ', this.lastPause)
    this.slider.setPositionByPercentActive(this.lastPause);
    this.api.seekTime(this.lastPause*this.totalDuration);
  }  
  setSeekTimeByPercent(percent: number){
    // console.log('setting seektime to ', percent*this.totalDuration)
    this.api.seekTime(percent*this.totalDuration);
  }
  //switch between quality, there are only two quality
  //controls will be hidden during the switching
  //seek time and volume settings will be preserved
  quality(){
    this.showControls = false;
    this.seekHold = this.api.currentTime;
    this.volume = this.api.volume
    this.isQ0 = !this.isQ0
    setTimeout(() => {
      this.api.seekTime(this.seekHold)
      this.api.volume = this.volume
      this.showControls = true;
    }, 250);
  }
  play(){
    console.log('state ', this.api.state)
    if(this.api.state === 'playing'){
      this.forceStop();
    }else{
      this.forcePlay();
    }
  }
  forcePlay(){
    this.api.play()
    this.isPlaying = true;
  }
  forceStop(){
    this.api.pause()
    this.isPlaying = false;
  }
  qualityCondition(){
    if(this.model){
      if(this.model.hasQ0 || this.model.hasQ1) return true
      return false
    }
    return false
  }
  fullscreen(){
    console.log('fullscreen toggle')
    if(this.api.fsAPI.isFullscreen){
      this.size = {'width': '100%', 'height': '100%'} //max dimension when full screen
    }else{
      this.size = {'width': '640px', 'height': '480px'} //miniture when small
    }
  }
  hasProcessed(){
    if(!this.model) return false;
    if(this.model.status === 'completed'){
      return true
    }
    return false;
  }
  getProgress(){
    if(!this.model) return '';
    return this.model.status; //this value will be like 'progress 47.99'
  }
  debug(){
    console.log(this.api.getDefaultMedia())
  }
  setLoop(){
    this.isLoop = !this.isLoop;
    if(!this.isLoop){
      this.lastPause = this.slider.percent1Static;
      this.reverse();
      this.forceStop();
    }else{
      this.slider.prepareLoop();
    }
  }
}
