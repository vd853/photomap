import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormConstructor } from '../../Utilities/formConstructor';
import { SectionModel, GUID } from '../../r';
import { SectionComponent } from '../section/section.component';
import { SectionlinkService } from '../../Service/sectionlink.service';
import { NotificationDirective } from '../../Directives/notification.directive';
import * as _ from 'lodash';

@Component({
  selector: 'app-section-form',
  templateUrl: './section-form.component.html',
  styleUrls: ['./section-form.component.css']
})
export class SectionFormComponent implements OnInit {
  @Input() parent: SectionComponent;
  @ViewChild(NotificationDirective) notify: NotificationDirective

  //the currently selected model is held by the parent, this.model is a potential new model to add
  model: SectionModel = new SectionModel(GUID.create());
  form = new FormConstructor()
  constructor(private sectionLink: SectionlinkService) { 
    this.form.addText('comment')
    this.form.build(false)
  }

  ngOnInit() {
    this.model.referenceId = this.parent.parent.model._id;
  }
  delete(){
    if(this.parent.sections.length < 1){
      this.notify.toast('No more sections to delete.')
      return;
    }
    const selectedModels: SectionModel[] = _.filter(this.parent.sections, e=> {
      return this.parent.sectionSelection.includes(e._id)
    })
    console.log(selectedModels);
    selectedModels.forEach(e=>{
      this.sectionLink.delSection(e, (err, data)=>{
        if(data){
          this.notify.toast('"' + this.parent.shorten(e.comment) + '"' + ' was deleted.');
          this.parent.player.api.pause();
          this.parent.getSections(true);
        }
      })
    })
  }
  submit(){
    if(!this.form.isValid('comment')){
      this.notify.toast('You must enter a comment.', 0)
      return;
    }
    this.model._id = GUID.create(); //always remake the GUID when sumbit
    this.model.duration = this.parent.player.totalDuration;
    const p1 = this.parent.player.slider.percent1;
    const p2 = this.parent.player.slider.percent2;
    if(p1 < p2){
      this.model.startPercent = p1;
      this.model.endPercent = p2;
    }else{
      this.model.startPercent = p2;
      this.model.endPercent = p1;
    }
    console.log('model send ', this.model)
    this.sectionLink.addSection(this.model, (err, data)=>{
      if(data){
        this.notify.toastAM('"' + this.parent.shorten(this.model.comment) + '"' + ' was added.');
        this.model.comment = null;
        this.parent.getSections();
      }
    })
  }
}
