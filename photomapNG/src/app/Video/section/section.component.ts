import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { SliderComponent } from '../slider/slider.component';
import { CardComponent } from '../../card/card.component';
import { PlayerComponent } from '../player/player.component';
import { SectionModel, QueryModel, QueryType, ModifyModel, DownloadType } from '../../r';
import { SectionFormComponent } from '../section-form/section-form.component';
import { SectionlinkService } from '../../Service/sectionlink.service';
import { DownloadlinkService } from '../../Service/downloadlink.service';
import { RequestType } from '../../../../../PhotomapNode/app/Models/request';
import { NotificationDirective } from '../../Directives/notification.directive';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css']
})
export class SectionComponent implements OnInit {
  model: SectionModel;
  sections: Array<SectionModel> = []
  sectionSelection = [];
  referenceId: string;
  @ViewChild('player') player: PlayerComponent
  @ViewChild('sectionForm') sectionForm: SectionFormComponent
  @Input() parent: CardComponent;
  @ViewChild(NotificationDirective) notify: NotificationDirective
  constructor(private sectionLink:SectionlinkService, private downloadLink:DownloadlinkService) { }

  ngOnInit() {
    this.player.init(this.parent.model.someId[0])
    this.getSections();
  }

  //select section and play on slider1
  setSection(section: SectionModel){
    this.UndoClipDownloadSelection();
    this.model = this.sections[this.indexOf(section)]
    console.log('setting ', this.model)

    //play using slider1
    this.player.slider.currentSliderId = this.player.slider.sliderId;
    this.player.setSeekTimeByPercent(this.model.startPercent)
    this.player.slider.setPositionByPercent(this.model.startPercent, this.model.endPercent);
    this.player.slider.percent1Static = this.model.startPercent;
    this.player.isLoop = true;
    this.player.forcePlay();
  }

  //prevents selection from changing when the play button is press
  UndoClipDownloadSelection(){
    //clipDownloadSelections binding is done after
    const resetClipDownloadSelections = this.sectionSelection;
    setTimeout(() => {
      this.sectionSelection = resetClipDownloadSelections;
    }, 1);
  }

  indexOf(section: SectionModel){
    return this.sections.indexOf(section);
  }
  getSections(resetPosition = false){
    const q = new QueryModel();
    q.queryType = QueryType.sectionReference;
    q.searchId = this.parent.model._id;
    this.sectionLink.getSectionsByReference(q, (err, data)=>{
      if(data){
        // console.log('section ', data)
        this.sections = []
        data.result.forEach(e=>{
          this.sections.push(e);
        })

        //set model if not already set;
        if(!this.model || resetPosition){
          this.model = this.sections[this.sections.length-1];
        }
        console.log('new sections ', this.sections)
      }
    })
  }
  downloadClip(){
    const m = new ModifyModel();
    m.downloadType = DownloadType.clipDownload;
    m.requestType = RequestType.request;
    this.sectionSelection.forEach(e=>{
      m.ids.push(e);
    })
    console.log('modify model ', m);
    this.downloadLink.download(m, (err, data)=>{
      if(data){
        this.notify.toastAM('Your request ID is ' + data._id);
        console.log(data);
      }
    })
  }
  shorten(text: string){
    if(text.length > 24){
      return text.substr(0, 24) + '...';
    }
    return text;
  }
}
