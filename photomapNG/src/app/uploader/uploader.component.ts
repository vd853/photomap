import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { PhotoformComponent } from '../photoform/photoform.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GUID, PhotoModel } from '../r';
import { GlobalService } from '../Service/global.service';
import { DigitNotifierComponent } from '../topnav/digit-notifier/digit-notifier.component';
import { NotificationDirective } from '../Directives/notification.directive';
import { LoginGuard } from '../login.guard';


@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.css']
})
export class UploaderComponent implements OnInit {
  @ViewChild('photo') photoform: PhotoformComponent;
  @ViewChild(NotificationDirective) notifier2: NotificationDirective
  statusAdded: string;
  status: string;
  url = 'upload/0'
  useModel: PhotoModel
  uploader:FileUploader;
  hasStarted = false;
  notifier: DigitNotifierComponent
  uploadCompleted = false;
  isLongUpload = false;
  constructor(public dialogRef: MatDialogRef<UploaderComponent>, private global: GlobalService,
    @Inject(MAT_DIALOG_DATA) public data: any, private loginGuard: LoginGuard)
    {
      this.notifier = data;
      this.url = global.mainURL + this.url;
      this.uploader = new FileUploader({
        url: this.url,
        maxFileSize: 2000000000,
        additionalParameter: { //this is for extra parameters
          comments: 'Metadata',
          photoModel: null,
          accountModel: JSON.stringify(loginGuard.model)
        }
      })
      this.uploader.onProgressAll = (p) => {
        this.notifier.setValue(p + '%')
        if(this.isLongUpload){
          this.global.cards.parent.updateUpload(p + '%')
        }
      }
      this.uploader.onAfterAddingFile = (file)=> { file.withCredentials = false; };
      this.uploader.onBeforeUploadItem = ()=>{
        
      }

      this.uploader.onCompleteAll = ()=>{
        this.dialogRef.close(); 
        this.notifier.signalCompleted('Uploads completed');
        this.uploadCompleted = true;
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      }
      this.uploader.onAfterAddingAll = ()=>{
        this.statusAdded = this.uploader.queue.length.toString();
      }
  }
  ngOnInit() {
  }
  upload(){
    this.useModel = this.photoform.getModel()
    if(!this.useModel.isReference) this.useModel.isReference = false;
    console.log('out model ', this.useModel)
    this.useModel.requestId = GUID.create(true)
    this.uploader.options.additionalParameter['photoModel'] = JSON.stringify(this.useModel)
    this.uploader.uploadAll()
    this.hasStarted = true;
    this.notifier.enabled = true; 
    setTimeout(() => {
      if(!this.uploadCompleted){
        this.notifier2.toastTop('Please wait, do not close or reload this page until upload completes.', 0)
        setTimeout(() => {
          this.notifier2.toastTop('This page will automatically reload when uploads are completed.', 0)
          setTimeout(() => {
            this.isLongUpload = true;
          }, 2000);
        }, 1500);
      }
    }, 1500);
    this.dialogRef.close();
  }
  debug(){
    this.dialogRef.close('I just closed');
  }
}
