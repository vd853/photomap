import { Component, OnInit, ViewChild } from '@angular/core';
import { ImageComponent } from '../image/image.component';
import { DatalinkService } from '../Service/datalink.service';
import { QueryModel } from '../r';
import { ImagelinkService } from '../Service/imagelink.service';
import blank from './blank'
import { MatMenuTrigger } from '@angular/material';
import { QueryType } from '../../../../PhotomapNode/app/Models/query';
import { GlobalService } from '../Service/global.service';
@Component({
  selector: 'app-reference',
  templateUrl: './reference.component.html',
  styleUrls: ['./reference.component.css']
})
export class ReferenceComponent implements OnInit {
  @ViewChild('image') image: ImageComponent
  references: Array<{index: number, data: string, id: string, fileName: string, title: string}> = []
  ids: string[]
  currentSelection: number;
  currentPreview: number;
  constructor(private datalink:DatalinkService, private imageLink: ImagelinkService, private globalLink: GlobalService) { 
    this.getReferenceIds()

    //this push the null image
    this.references.push({index: this.references.length, data: blank, id: null, fileName: 'No Selection', title: null})
  }
  getReferenceIds(){
    const q = new QueryModel()
    q.queryType = QueryType.referencing
    this.datalink.getProfile(q, (err, data)=>{
      if(data){
        if(data.length < 1){
          console.log('There are no reference image found.')
        }else{
          this.ids = data.result;
          this.getAllImage();
        }
      }else{
        console.log('ERROR retreiving image')
      }
    })
  }
  getCurrentSelection(): string{
    if(this.currentSelection === 0) return null
    return this.references[this.currentSelection].id
  }
  getAllImage(){
    this.ids.forEach(id=>{
      console.log('getting id ', id['_id'])
      this.getImage(id['_id'])
    })
  }
  getImage(id: string){
    this.imageLink.getImage(id, 2, -1, (err, data)=>{
      console.log('reference ', data)
      this.references.push({index: this.references.length, data: data.result, id: id, fileName: data.fileName, title: data.title})
    })
  }
  prevImage(index: number){
    this.currentPreview = index;
    if(!index){
      this.image.reload(this.references[index].data, true) //this will load the blank image which is a image64
      return
    }
    this.image.reload(this.references[index].data)
  }
  offPrevImage(){
    console.log('mouse leave')
    if(this.currentPreview !== this.currentSelection){
      this.prevImage(this.currentSelection)
    }
  }
  setImage(){
    this.currentSelection = this.currentPreview;
  }

  ngOnInit() {
    this.image.contraintClass = 'imgContraintThumb'
    this.image.shadow = true;
    this.prevImage(0)
    this.setImage()
  }
  
  debug(){
    console.log(this.references[1].fileName)
  }
}
