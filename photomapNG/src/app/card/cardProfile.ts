import { CardComponent } from "./card.component";
import { QueryModel, PhotoModel, mediaType } from "../r";

export class CardProfile{
    static getProfile(id: string, source: CardComponent, callback?:()=>void){
        const q = new QueryModel()
        q.searchId = id;
        source.data.getProfile(q, (err, data)=>{
        if(data){
            source.model = <PhotoModel>data.result
            console.log('card model updated ', source.model)
            if(source.model.mediaType === mediaType.video){
                source.isVideo = true;
            }
            //set all variables for inner card components like heading, view, stamp
            setTimeout(() => {
            if(source.serverLink.model.enablePreview || source.parent.thumbnailImageRequested){
                source.getImageLow()
            }
            }, 0);
            if(callback) callback();
        }
        })
    }
}