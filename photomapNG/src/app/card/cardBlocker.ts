import { CardComponent } from "./card.component";
import { globals } from "../r";

export class CardBlocker{
    static searchBlock(card: CardComponent){ //card will show if false is return
        if(card.search.filterDisabled) return false;
        if(!card.search.currentSearch) return false;
        // console.log('source view', source.view)
        let date = null;
        if(card.view){
            if(card.model.date){
                date = card.view.infoGetter.getFormatedDate(card.model.date.toString())
            }
        }
        const searchTerms: string[] = []
        globals.filterable.forEach(key=>{
            searchTerms.push(card.model[key])
        })
        searchTerms.push(date) //note, date is not included in keywordable
        const exposed = !card.search.check(searchTerms)
        card.heading.selectable = !exposed
        return exposed //if true, card will be hidden
    }
    static viewBlock(source: CardComponent){
        const exposed = !source.search.viewCheck(source.model)
        source.heading.selectable = !exposed
        return exposed
    } 
}