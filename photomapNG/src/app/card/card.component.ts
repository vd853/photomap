import { Component, OnInit, ViewChild, Input, HostListener, OnDestroy } from '@angular/core';
import { PhotoModel, QueryModel, GUID, mediaType } from '../r';
import { DatalinkService } from '../Service/datalink.service';
import { HeadingComponent } from '../heading/heading.component';
import { ViewComponent } from '../view/view.component';
import { StampComponent } from '../stamp/stamp.component';
import { CardsComponent } from '../cards/cards.component';
import { SearchService } from '../Service//search.service';
import { ImagelinkService } from '../Service//imagelink.service';
import { ServerlinkService } from '../Service//serverlink.service';
import * as async from 'async'
import { CardProfile } from './cardProfile';
import { CardBlocker } from './cardBlocker';
import { CardSelect } from './cardSelect';
import { HoverActiveDirective } from '../Directives/hover-active.directive';
import { GlobalService } from '../Service//global.service';
import { CacheService } from '../Service//cache.service';
import { SectionComponent } from '../Video/section/section.component';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit, OnDestroy {
  destroying = false;
  cardId = "card_" + GUID.create() //"card" is use to search during hover preview
  expandedBodyHeight = 0;
  imageLoaded = false
  imageLoadedLow = false //for the image64Low

  imageLow: string
  imageHigh: string
  image64HighStamp: string

  @ViewChild(HoverActiveDirective) hover: HoverActiveDirective;
  @ViewChild('heading') heading: HeadingComponent
  @ViewChild('view') view: ViewComponent
  @ViewChild('stamp') stamp: StampComponent 
  @ViewChild('section') section: SectionComponent
  parent: CardsComponent
  @Input() isVideo = false;
  expand = false;
  model = new PhotoModel(this.serverLink.model.fieldDefinitions)
  highlightStyle = {
    'outline-color': 'rgb(139, 182, 255)',
    'outline-style': 'solid',
    'outline-width': '3px',
    'background-color': 'white'
  }
  selectedStyle = {
    'outline-color': '#F56300',
    'outline-style': 'solid',
    'outline-width': '3px',
    'background-color': 'white'
  }
  currentStyle = {
  };
  constructor(
    public cache: CacheService,
    public data:DatalinkService, 
    public search:SearchService, 
    public dataLinkImage: ImagelinkService,
    public serverLink: ServerlinkService,
    public globalLink: GlobalService
  ) { 
  }

  //invoke by any type of card delete
  ngOnDestroy(): void {
    this.cache.PhotoModels.delete(this.model._id)
    this.removeHeight();
    console.log('set destroying')
    this.destroying = true;
  }
  getProfile(id: string){
    CardProfile.getProfile(id, this)
  }

  //reload model for this card
  autoGetProfile(callback?:()=>void){
    CardProfile.getProfile(this.model._id, this, callback)
  }
  destroySelf(cardOnly = false){
    if(cardOnly){
      this.parent.deleteSomeCards([this.model._id])
      return
    }
    const q = new QueryModel()
    q.searchIds = [this.model._id]
    this.data.delProfile(q, (err, data)=>{
      if(data.result){
        console.log(data)
        this.parent.deleteSomeCards([data.result[0]]) //the deleted id of just one, modifies handles the batch delete
      }
    })
  }
  ngOnInit() {
    //this is true if the user selected expand all. New cards will automatically expand in this case
    if(this.parent.batchExpanded){ //by default they don't expand on created
      this.expandImage(1)
    }
  }
  getImageLow(){
    if(!this.imageLoadedLow){
      this.imageLoadedLow = true;
      this.dataLinkImage.getImage(this.model._id, 2, 0, (err, data)=>{
        if(data.result){
          this.imageLow = this.globalLink.staticMap(data.result)
          this.heading.imageActive = true;
          // console.log('low64 retreived ', data.result)
        }else{
          console.log('low64 get error ', false)
        }
      })
    }
  }

  //0 collapse, 1 expand, 2 toggle
  expandImage(expandMode: number){
    let changed: boolean;
    switch(expandMode){
      case 0:
        changed = this.expand !== false;
        this.expand = false;
        break;
      case 1:
        changed = this.expand !== true;
        this.expand = true;
        break;
      case 2:
        changed = true;
        this.expand = !this.expand
        break;
    }

    //is this the best solution?
    if(this.expand){
      this.initExpansion(changed)
    }else{
      this.setTotalHeight(changed);
    }
    this.hover.highlightOff();
  }

  //changed mean it expand was !expand
  initExpansion(changed: boolean){
    this.parent.batchHighlight(this.model._id, 1)
    this.parent.parent.backgroundDark(true)
    if(this.model.mediaType === mediaType.photo){
      this.setImageExpansion(changed);
    }else{
      this.setVideoExpansion(changed);
    }
  }
  setVideoExpansion(changed: boolean){
    setTimeout(() => {
      this.view.getImage(null)
    }, 0);
  }
  setImageExpansion(changed: boolean){
    setTimeout(()=>{ //view and stamp with thier image should be instantiated here and parent already set
      if(!this.imageLoaded){
        this.imageLoaded = true
        this.loadImage(err=>{
          console.log('reload all image')
          if(!err){
            this.setTotalHeight(changed);
            this.imageHigh = this.view.image.image64
            this.image64HighStamp = this.stamp.image.image64
          }else{
            console.log('error in loading image for ', this.model._id)
          }
        })
      }else{
        //image is reloaded for cache
        console.log('reloading image')
        this.setTotalHeight(changed);
        this.reloadViewImage(this.imageHigh)

        if(this.stamp.referencesHaveUpdated){
          this.stamp.referencesHaveUpdated = false
          this.stamp.getImage(err=>{
            if(err){
              console.log('ERROR updating stamp cache')
            }
            this.image64HighStamp = this.stamp.image.image64 //update the cache
            this.stamp.init();
          })
        }else{
          this.stamp.image.reload(this.image64HighStamp, true)
          this.stamp.init();
        }
      }
    }, 0)
  }

  setTotalHeight(changed: boolean){
    if(this.destroying) return;
    if(changed){
      setTimeout(() => {
        if(this.destroying) return;
        this.recaculateHeight();
      }, 500);
    }
  }
  recaculateHeight(){
    if(this.expandedBodyHeight === 0){
      const ele = document.getElementById(this.cardId)
      this.expandedBodyHeight = ele.clientHeight - this.parent.headingHeight; //defines expandedBodyHeight when expanded
    }
    if(this.expand){
      this.parent.totalCardsExpandedHeight+=this.expandedBodyHeight;
    }else{
      this.parent.totalCardsExpandedHeight-=this.expandedBodyHeight;
    }
    console.log('total expanded height  ', this.parent.totalCardsExpandedHeight);
    console.log('card height ', this.expandedBodyHeight)
  }

  //removes expandedBodyHeight on delete if size exist
  removeHeight(){
    this.parent.totalCardsExpandedHeight-=this.expandedBodyHeight;
    console.log('total expanded height  ', this.parent.totalCardsExpandedHeight);
  }
  //use during rotation and re-expanding
  reloadViewImage(image64: string){
    this.imageHigh = image64;
    this.view.image.reload(image64, true)
  }

  loadImage(callback:(err)=>void){
    const view = (callback1:(err, data)=>void)=>{
      this.view.getImage(err=>callback1(err, null))
    }
    const stamp = (callback2:(err, data)=>void)=>{
      this.stamp.getImage(err=>callback2(err, null))
    }
    async.parallel([view, stamp], (err, data)=>{
      console.log('image loaded')
      callback(err)
    })
  }

  select(toggle: boolean){
    setTimeout(() => {
      CardSelect.select(toggle, this)
    }, 1);
  }

  //used in heading
  highlight(isOver: boolean){
    CardSelect.highlight(isOver, this)
  }
  
  selectedCheck(triggerMode?:number){
    if(triggerMode){
      if(triggerMode === 1){
        this.currentStyle = {};
      }else{
        this.currentStyle = this.selectedStyle;
      }
      return;
    }
    
    if(this.heading.selected){
      this.currentStyle = this.selectedStyle;
      // console.log('selected highlight ', this.currentStyle)
      return true;
    }else{
      return false;
    }
  }
  searchBlock(){
    return CardBlocker.searchBlock(this)
  }
  viewBlock(){
    return CardBlocker.viewBlock(this)
  }
  getModel(){
    return JSON.stringify(this.model)
  }
}
