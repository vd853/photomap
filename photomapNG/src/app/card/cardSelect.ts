import { CardComponent } from "./card.component";

export class CardSelect{
    static select(toggle: boolean, source: CardComponent){
        if(source.heading.selectable){
            source.heading.selected = toggle
            source.highlight(false) //for hover over hightlights
        }
    }

    //turn on or turn off the selected outline effect
    static highlight(isOver: boolean, source: CardComponent){
        if(source.selectedCheck()) return;
        if(isOver){
            source.currentStyle = source.highlightStyle;
        }else{
            source.currentStyle = {}
        }
    }
}