import { Component, OnInit, ViewChild, Input } from '@angular/core';
import * as moment from 'moment'
import { PreviewTextComponent } from '../preview-text/preview-text.component';
import { maxCharLength, PhotoModel, globals, capitalizeEachWord } from '../r';
import { InfoGetterService } from '../Service/info-getter.service';
import { SearchService } from '../Service/search.service';
import { ServerlinkService } from '../Service/serverlink.service';
@Component({
  selector: 'app-info-table',
  templateUrl: './info-table.component.html',
  styleUrls: ['./info-table.component.css']
})
export class InfoTableComponent implements OnInit {
  @Input() blockEmpty = false;
  @Input() model: PhotoModel
  viewables = globals.viewables
  @ViewChild('preview') preview :PreviewTextComponent;
  constructor(
    public infoGetter: InfoGetterService,
    public search: SearchService,
    public serverLink: ServerlinkService
  ) { 
    this.viewables = this.viewables.concat(this.serverLink.model.fieldDefinitions);
    console.log('viewables ', this.viewables);
  }

  ngOnInit() {
  }
  wordCapitalize(word: string){
    return capitalizeEachWord(word)
  }
  checkPreview(key: string, event: any){
    const text = this.infoGetter.getViewByKey(this.model, key)
    // if(text.length < maxCharLength) return //ignore short text
    // if(!text || text.toString() === 'true') return; //ignore if false or null or like boolean type
    // if(moment(text).isValid()) return //ignore date
    // console.log('showing text preview ', text)
    if(PreviewTextComponent.validator(text)){
      this.preview.text = text
      this.preview.update(event.clientX+20, event.clientY-120, true)
    }
  }

  checkPreviewNull(){
    console.log('null preview')
    this.preview.update(0,0, false)
  }

  unNull(text: any){
    //text input could be a boolean, if this is not use, it will return blank for false booleans
    if(typeof text === 'boolean'){
      if(!text){
        return 'No'
      }else{
        return 'Yes'
      }
    }

    if(!text || text === 'undefined') return ''
    return text
  }
  isReference(model: PhotoModel, key: string){
    if(key !== 'isReference') return null;
    if(model[key]){
      return 'gReferenceType'
    }else{
      return null
    }
  }
}
