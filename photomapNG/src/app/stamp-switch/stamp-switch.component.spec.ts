import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StampSwitchComponent } from './stamp-switch.component';

describe('StampSwitchComponent', () => {
  let component: StampSwitchComponent;
  let fixture: ComponentFixture<StampSwitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StampSwitchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StampSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
