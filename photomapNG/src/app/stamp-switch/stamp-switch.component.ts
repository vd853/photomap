import { Component, OnInit, HostListener, Input } from '@angular/core';
import { ImagelinkService } from '../Service/imagelink.service';
import { PhotoModel, GUID } from '../r';
import { ImageComponent } from '../image/image.component';
import { DatalinkService } from '../Service/datalink.service';
import { CardComponent } from '../card/card.component';
import { StampComponent } from '../stamp/stamp.component';
import { CacheService } from '../Service/cache.service';
import { GlobalService } from '../Service/global.service';

@Component({
  selector: 'app-stamp-switch',
  templateUrl: './stamp-switch.component.html',
  styleUrls: ['./stamp-switch.component.css']
})
export class StampSwitchComponent implements OnInit {
  scrollLock = false;
  currentPreview = 0;
  @Input() card: CardComponent;
  @Input() stamp: StampComponent;
  references: Array<{index: number, data: string, referenceId: string}> = []
  stampImage: ImageComponent
  private isPick = false; //this is set true if user picks
  constructor(private photoLink:ImagelinkService, private dataLink: DatalinkService, private globalLink: GlobalService) { 
  }

  ngOnInit() {
  }

  //updates the db as the current reference
  setImage(){
    this.scrollLock = true;
    console.log('setImage ', this.currentPreview, '  ',this.card.model.lastReferencePath)
    if(this.isPick){
      this.isPick = false;
      this.card.model.lastReferencePath = this.currentPreview
      this.dataLink.updateProfiles({ids: [this.card.model._id], model: this.card.model}, (err, data)=>{
        if(data){
          this.getReferencesPhoto();
        }
      })
    }else{
      this.currentPreview = this.card.model.lastReferencePath
    }
  }
  //mouse hover
  prevImage(index: number){
    this.currentPreview = index;
    this.isPick = true;
    console.log('current preview ', this.currentPreview)
  }

  nextImageScroll(){
    if(this.scrollLock) return;
    if(!this.currentPreview) this.currentPreview = this.card.model.lastReferencePath
    console.log('scroll next')
    this.currentPreview++;
    if(this.currentPreview > this.card.model.referencePath.length-1) this.currentPreview = 0; //cycle back
    this.isPick = true;
    this.setImage()
  }
  prevImageScroll(){
    if(this.scrollLock) return;
    if(!this.currentPreview) this.currentPreview = this.card.model.lastReferencePath
    console.log('scroll prev')
    this.currentPreview--;
    if(this.currentPreview < 0) this.currentPreview = this.card.model.referencePath.length-1; //cycle forward
    this.isPick = true;
    this.setImage() 
  }

  applyImage(){
    // console.log('applying ', this.image64s[this.currentPreview].data)
    this.stampImage.reload(this.references[this.currentPreview].data, true)
  }

  //this gets all the reference images, there are only 640px version of them
  getReferencesPhoto(){
    let i = 0;
    this.references = []
    this.photoLink.getImage(this.card.model._id, 3, 0, (err, data)=>{
      // console.log('get reference from model ', this.parent.model)
      if(data.result){
        console.log('reference photos returns ', data)
        data.result.forEach(set=>{
          this.references.push({index: i, data: set.base64, referenceId: set.referenceId});
          i++;
        })

        //since this.currentpreview becomes null after expanded
        this.currentPreview = this.currentPreview? this.currentPreview: this.card.model.lastReferencePath;

        // console.log('lastreferencepath ', this.parent.model.lastReferencePath, ' current', this.currentPreview)
        
        this.applyImage();
      }else{
        console.log('Error getting reference images')
      }
      this.scrollLock = false;
    })
  }

  @HostListener("window:keydown", ['$event'])
  onKeyDown(event:KeyboardEvent) {
    // console.log('key modify ', event)
    // console.log('focusx ', this.stamp.focus)
    if(this.stamp.focus){
      if(event.code === 'ArrowRight' || event.code === 'KeyD'){
        console.log('right arrow')
        this.nextImageScroll()
      }
      if(event.code === 'ArrowLeft' || event.code === 'KeyA'){
        console.log('left arrow')
        this.prevImageScroll()
      }
    }
  }
  
}
