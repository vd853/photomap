import { Component, OnInit, Input, ViewChild } from '@angular/core';
import {MatDialog, MatMenuTrigger} from '@angular/material';
import { UploaderComponent } from '../uploader/uploader.component';
import { EnclosureComponent } from '../enclosure/enclosure.component';
import { DatalinkService } from '../Service/datalink.service';
import { QueryModel, capitalizeEachWord, ServerModel, ServerTriggers, AccountModel} from '../r';
import { ModifyComponent } from '../modify/modify.component';
import { CardsComponent } from '../cards/cards.component';
import { ServerformComponent } from '../serverform/serverform.component';
import { ServerlogComponent } from '../serverlog/serverlog.component';
import { SearchService } from '../Service/search.service';
import { GlobalService } from '../Service/global.service';
import { FormControl } from '@angular/forms';
import { FormConstructor } from '../Utilities/formConstructor';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {globals} from '../r'
import { KeywordLinkService, keywordComponents } from '../Service/keyword-link.service';
import { NotificationDirective } from '../Directives/notification.directive';
import { DigitNotifierComponent } from './digit-notifier/digit-notifier.component';
import * as _ from 'lodash';
import { QueryType } from '../../../../PhotomapNode/app/Models/query';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ServerlinkService } from '../Service/serverlink.service';
import { AccountlinkService } from '../Service/accountlink.service';
import { LoginGuard } from '../login.guard';
import { AccountSetComponent } from '../account-manager/account-set/account-set.component';
import { AccountManagerComponent } from '../account-manager/account-manager.component';

@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.css']
})
export class TopnavComponent implements OnInit, keywordComponents {
  isExpandSearch = false;
  overrideText: string;
  reloadableIds:string[] = []
  inputEnabled = false;
  loading = false;
  form = new FormConstructor()
  previousSearch = ''
  currentSearch = ''
  decending = true
  idsLoaded: string[] //all photo _id that are loaded
  sortSelected = 'createdDate'
  viewTypes = [{value: 'all', viewValue: 'All'}, {value: 'photo', viewValue: 'Photo'}, {value: 'reference', viewValue: 'Reference'}, {value: 'geotag', viewValue: 'Geotag'}]
  viewSelection = ['all', 'photo', 'reference', 'geotag']
  globals = globals //sort types are the same as searchables from globals in Node
  @Input() parent: EnclosureComponent
  @ViewChild('un') uploaderNotifier: DigitNotifierComponent
  @ViewChild('sn') selectionNotifier: DigitNotifierComponent
  cards: CardsComponent
  keywords: string[]
  @ViewChild(NotificationDirective) notifier: NotificationDirective

  constructor(
    public dialog: MatDialog, 
    private data:DatalinkService, 
    public search:SearchService, 
    private keyword: KeywordLinkService,
    private global: GlobalService,
    private sanitizer: DomSanitizer,
    private server: ServerlinkService,
    private account: AccountlinkService,
    public loginGuard: LoginGuard
  ) {
    this.search.topNav = this;
    this.form.addGeneric('view', false)
    this.form.build()
    this.keyword.registerComponents.push({component: this, key: 'all'})
  }

  formatter = (result: string) => result;
  searcher: any
  initAutofill(){
    // console.log('key update ', this.keywords)
    this.searcher= (text$: Observable<string>) =>
    text$
    .debounceTime(200)
    .distinctUntilChanged()
    .map(term => term === '' ? []
      : this.keywords.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
  }

  ngOnInit() {
  }
  startSearch(){
    this.loading = true;
    this.previousSearch = this.currentSearch
    const q = new QueryModel();
    q.search = this.currentSearch;
    q.isDescending = this.decending
    q.getFields = [this.sortSelected];
    if(this.isExpandSearch) q.queryType = QueryType.expandedSearch;
    this.data.getProfile(q, (err, data)=>{
      // console.log(err, data)
      if(data.result){
        if(this.previousSearch === ''){
          this.notifier.toast(data.result.length + ' results found for all photos')
        }else{
          this.notifier.toast(data.result.length + ' results found for "' + this.previousSearch + '"')
        }
        this.reload(_.map(data.result, '_id')) //does not reload the whole page
        this.resetView()
        this.UpdateSearch(); //invoke hightlight after search, otherwise only 1 char is highlighted
        setTimeout(() => {
          this.loading = false;
        }, 2000);
      }
    })
  }
  UpdateSearch(){
    this.search.currentSearch = this.currentSearch;
  }
  reload(ids?:string[]){
    this.cards.batchSelected = false;
    if(ids){
      this.idsLoaded = ids
    }
    if(this.cards.children.length > 0){
      this.cards.deleteAllCards()
    }
    this.cards.children = []
    this.cards.reset()
    this.cards.batchCreate(this.idsLoaded)
  }
  wordCapitalize(word: string){
    return capitalizeEachWord(word)
  }

  //changes sort type and sorts
  setSortType(type: string){
    this.sortSelected = type; //for UI binding
    console.log('sort set ', type)
    console.log('sort search ', this.previousSearch)
    const q = new QueryModel()
    q.search = this.previousSearch;
    q.getFields = [type]
    q.isDescending = this.decending
    q.queryType = QueryType.sorting;
    this.data.getProfile(q, (err, data)=>{
      console.log(err, data)
      if(data.result){
        this.reload(_.map(data.result, '_id'))
      }
    })
  }

  //returns a label for the sort button
  decendingLabel(){return this.decending? 'Decending': 'Accending'}

  //use for the sort button, it call sort
  reorder(){
    this.decending = !this.decending
    this.setSortType(this.sortSelected)
  }

  openUpload(): void {
    let dialogRef = this.dialog.open(UploaderComponent, {
      width: '800px',
      height: '600px',
      data: this.uploaderNotifier
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
    });
  }

  openAccount(model?: AccountModel, accountManager?: AccountManagerComponent){
    if(!this.loginGuard.model && !model) throw new Error('No user login is detected in loginGuard.')
    let dialogRef = this.dialog.open(AccountSetComponent, {
      width: '800px',
      height: '600px',
      data: {
        model: model? model: this.loginGuard.model,
        isManagement: model? true: false,
        accountManager: accountManager? accountManager: null
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
    });
  }

  openAccountManager(){
    let dialogRef = this.dialog.open(AccountManagerComponent, {
      width: '800px',
      height: '600px',
      data: {
        topNav: this
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
    });
  }
  hotReload(){
    if(this.reloadableIds.length < 1) return;
    this.cards.hotReload(this.reloadableIds);
    this.reloadableIds = [];
  }
  openModify(): void {
    const selected = this.cards.getSelectedIds();
    if(selected.length === 0){
      this.notifier.toast('No photos selected for modification')
      return;
    }
    console.log('current selection ', selected.length, ' total search', this.cards.ids.length)
    let dialogRef = this.dialog.open(ModifyComponent, {
      data: {type: 'multiple', data: selected},//list of ids to modify
      width: '1000px',
      height: '700px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.currentSearch = this.previousSearch; 
      console.log('return to nav')
      if(result){
        this.batchSelectButton('none') //clear selection if something was modified
        if(result.delete){
          // console.log('to remove cards ', result)
          this.cards.deleteSomeCards(result.delete)
        }else{
          // console.log('modifed ids ', result)
          this.reloadableIds = _.union(this.reloadableIds, result.ids)
          // console.log('reloadables ', this.reloadableIds)
        }
      } 
    });
  }
  getSearchPlaceholder(){
    if(this.overrideText) return this.overrideText;

    if(this.loading){
      return 'Loading results...';
    }
    return this.search.filterDisabled? 'Search': 'Search and Filter'
  }
  toggleFilter(){
    this.search.filterDisabled = !this.search.filterDisabled
  }
  toggleExpandedSearch(){
    this.isExpandSearch = !this.isExpandSearch;
  }
  openServer(): void {
    let dialogRef = this.dialog.open(ServerformComponent, {
      width: '800px',
      height: '600px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed for topnav', result);
    });
  }

  openLog(): void {
    let dialogRef = this.dialog.open(ServerlogComponent, {
      width: '800px',
      height: '500px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed for topnav (log)', result);
    });
  }
  clear(){
    this.currentSearch = ''
    this.UpdateSearch()
  }
  reloadPage(){
    window.location.reload();
  }

  //should be invoke whenever something is selected
  setSelectionNotifier(digit: number){
    // console.log('set selection notfier to ', digit)
    if(digit > 0){
      this.selectionNotifier.enabled = true;
      setTimeout(() => {
        this.selectionNotifier.setValue(digit.toString() + '/' + this.parent.cards.ids.length.toString())
      }, 0);
    }else{
      this.selectionNotifier.enabled = false;
    }
  }
  debug(){
    this.account.logOut(this.loginGuard.model);
    // window.location.replace("http://wwww.zstackoverflow.com");
  }
  batchSelectButton(type: string){
    if(!this.parent.cards.ids){
      this.notifier.toast('No search results to select')
      return;
    }
    switch(type){
      case 'current':
        this.cards.batchSelect(true, true)
        break;
      case 'all':
        this.cards.batchSelect(true)
        break;
      case 'none':
        this.cards.batchSelect(false)
        break;
    }
  }

  //set view defaults here
  resetView(){
    this.viewSelection = ['all']
  }
}
