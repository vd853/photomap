import { Component, OnInit, ViewChild } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { NotificationDirective } from '../../Directives/notification.directive';

@Component({
  selector: 'app-uploader-notifier',
  templateUrl: './uploader-notifier.component.html',
  styleUrls: ['./uploader-notifier.component.css']
})
export class UploaderNotifierComponent implements OnInit {
  enabled = false;
  value = 0;
  @ViewChild(NotificationDirective) notifier: NotificationDirective
  constructor() { 
  }

  ngOnInit() {
  }
  setValue(value: number){
    this.value = value;
  }
  getValue(){
    return this.value? this.value.toFixed(0): 0
  }
  signalCompleted(){
    this.enabled = false;
    this.notifier.toast('Upload completed', 0)
  }

}
