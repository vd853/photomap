import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploaderNotifierComponent } from './uploader-notifier.component';

describe('UploaderNotifierComponent', () => {
  let component: UploaderNotifierComponent;
  let fixture: ComponentFixture<UploaderNotifierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploaderNotifierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploaderNotifierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
