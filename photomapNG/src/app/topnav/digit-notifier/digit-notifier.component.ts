import { Component, OnInit, ViewChild } from '@angular/core';
import { NotificationDirective } from '../../Directives/notification.directive';

@Component({
  selector: 'app-digit-notifier',
  templateUrl: './digit-notifier.component.html',
  styleUrls: ['./digit-notifier.component.css']
})
export class DigitNotifierComponent implements OnInit {
  enabled = false;
  value = '';
  @ViewChild(NotificationDirective) notifier: NotificationDirective
  constructor() { 
    this.value = ''
  }

  ngOnInit() {
    this.value = ''
  }
  setValue(value: string){
    this.value = value;
  }
  // getValue(){
  //   return this.value;
  // }
  signalCompleted(message: string){
    this.enabled = false;
    this.notifier.toast(message, 0)
  }
}
