import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DigitNotifierComponent } from './digit-notifier.component';

describe('DigitNotifierComponent', () => {
  let component: DigitNotifierComponent;
  let fixture: ComponentFixture<DigitNotifierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DigitNotifierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DigitNotifierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
