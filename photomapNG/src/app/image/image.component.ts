import { Component, OnInit, Input, Inject } from '@angular/core';
import { ImagelinkService } from '../Service/imagelink.service';
import { GUID } from '../r';
import { GlobalService } from '../Service/global.service';
@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
  contraintClass = 'imgContraint'
  shadow = false;
  image64: string = this.global.noLoadedImage() //this could also be a link image
  got = true;
  id: string
  elementId: string
  blankImage: string;
  @Input() constraint = false; //small contraint only
  constructor(
    private data: ImagelinkService,
    public global: GlobalService
  ) { 
    this.elementId = this.id + GUID.create();
  }

  //reference will return the markPath photo
  getImage(getId: string, type: number, reference: number, is64: boolean, callback:(err, data)=>void){
    this.id = getId
    this.data.getImage(getId,type, reference, (err, datar)=>{
      if(datar){
        this.reload(datar.result, is64)
        callback(null, true)
      }else{
        callback(err, null)
      }
    })
  }
  ngOnInit() {
  }
  reload(image64: string, noConversion = false){
    this.got = false
    if(noConversion){
      this.image64 = image64
    }else{
      this.image64 = this.global.staticURL + image64
    }
    this.got = true
  }
}
