import { Component, OnInit, Inject, Query } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { LoginGuard } from '../login.guard';
import { PhotoModel, Privilege, Data, PrivilegeType, QueryModel, QueryType } from '../r';
import { AccountlinkService } from '../Service/accountlink.service';
import { DatalinkService } from '../Service/datalink.service';
import { CardComponent } from '../card/card.component';
@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.css']
})
export class ShareComponent implements OnInit {
  userShares: Privilege[];
  dataRetrieved = false;
  privilageList = []
  private model: PhotoModel;
  private card: CardComponent;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: CardComponent,
    public dialogRef: MatDialogRef<ShareComponent>,
    private loginGuard: LoginGuard,
    private accountLink: AccountlinkService,
    private datalink: DatalinkService
  ) { 
    this.card = data;
    this.model = this.card.model;
    for (let index = 0; index < Object.keys(PrivilegeType).length/2; index++) {
      this.privilageList.push({value: index, viewValue: Data.EnumIndexToWordOrdered(PrivilegeType, index)})
    }
  }

  ngOnInit() {
    if(!this.model.authenticates) throw new Error('Photomodel must have authenticates')
    this.InitAsync();
  }
  async InitAsync(){
    this.userShares = this.model.authenticates;
    const allUsers = await this.accountLink.getUsersAndId();
    const userSharesUsers = this.userShares.map(e=>e.user);
    const allNoneUsers = allUsers.filter(e=>!userSharesUsers.includes(e.user));
    allNoneUsers.forEach(e=>{
      const p = new Privilege();
      p.user = e.user;
      p.associatedId = e._id;
      this.userShares.push(p)
    })
    this.dataRetrieved = true;
  }
  debug(){
    return JSON.stringify(this.model);
  }
  getPrivilageType(index: number){
    const x = Data.EnumIndexToWordOrdered(PrivilegeType, index);
    return Data.EnumIndexToWordOrdered(PrivilegeType, index);
  }
  privilageChange(associatedId: string, user: string, privilage: number){
    console.log('privilage changed ', associatedId, privilage);
    const p = new Privilege(privilage);
    p.user = user;
    p.associatedId = associatedId;
    const q = new QueryModel();
    q.searchIds = [this.model._id];
    q.queryType = QueryType.setPrivilege;
    q.package = p;
    this.datalink.getProfile(q, (err, result)=>{
      this.card.autoGetProfile(()=>{
        this.InitAsync();
      });
      console.log('privilage change result');
    })
  }
}
