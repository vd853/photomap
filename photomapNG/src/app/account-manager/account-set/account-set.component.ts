import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AccountManagerComponent } from '../account-manager.component';
import { LoginGuard } from '../../login.guard';
import { AccountModel , globals, AccountRequest, AccountLevel, StringData} from '../../r';
import { FormConstructor } from '../../Utilities/formConstructor';
import { NotificationDirective } from '../../Directives/notification.directive';
import { AccountFormComponent } from '../account-form/account-form.component';

@Component({
  selector: 'app-account-set',
  templateUrl: './account-set.component.html',
  styleUrls: ['./account-set.component.css']
})
export class AccountSetComponent implements OnInit {
  //This class is use to modify the current account settings
  group = 'form-group col-lg-8 col-md-8 col-sm-8 col-xs-8'
  changePasswordTrigger = false;
  form = new FormConstructor();
  model: AccountModel;
  dataRetrieved = false;
  userDataKeys: string[];
  list: {value: number, viewValue:string}[] = []
  isManagement: boolean;
  // responseMessage: string;
  accountManager: AccountManagerComponent;
  cc = StringData.CamelCaseToWords;
  // @ViewChild(NotificationDirective) notifier: NotificationDirective;
  @ViewChild('accountForm') accountForm: AccountFormComponent;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AccountSetComponent>,
    private loginGuard: LoginGuard,
  ) { 
    this.list = AccountSetComponent.generateAccountModelKeys();
  }
  public static generateAccountModelKeys(){
    const list = [];
    let count = 0;
    const keys = Object.keys(AccountLevel);
    keys.splice(0, keys.length/2)
    keys.forEach(e=>{
      list.push({value: count, viewValue: e});
      count++;
    })
    return list;
  }
  ngOnInit() {
    if(!this.data) throw new Error('No model was passed in.')
    if(this.data.model){
      this.isManagement = this.data.isManagement;
      this.userDataKeys = this.isManagement? globals.managementRetrievable: globals.userRetrievable;
      this.accountManager = this.isManagement? this.data.accountManager: null;
      this.model = this.data.model;
      this.model.sessionIDManagement = this.isManagement? this.loginGuard.model.sessionID: null;
      console.log('accountset model ', this.model);
      this.dataRetrieved = true;

      this.accountForm.InitConfigMode(this.isManagement, this.accountManager)
    }
  }
  getKeyData(key: string){
    let returner;

    if(key === 'authenticated'){
      return this.model['authenticated']? 'Authenticated': 'Not authenticated';
    }

    if(key === 'lockoutTime'){
      return AccountModel.lockoutTimeMessage(this.model);
    }
    if(key === 'level' && this.list[this.model['level']]){
      return this.list[this.model[key]].viewValue;
    }else{
      returner = '';
    }
    returner = this.model[key];
    return returner? returner: '';
  }
  
  // changePassword(){
  //   this.model.request = AccountRequest.passwordChange;
  //   this.loginGuard.authenticate(this.model, '', (err, result)=>{
  //     if(err) throw new Error(err);
  //     if(result.verified) {
  //       this.changePasswordTrigger = false;
  //       this.model.passwordNew = null;
  //       this.model.password = ''
  //       this.notifier.toast('Your password is changed.')
  //     }else{
  //       this.notifier.toast('Your password is invalid.')
  //     }
  //     this.responseMessage = result.reason;
  //   })
  // }
  // userValidator(){
  //   if(this.model.user === 'admin' && this.model.level === AccountLevel.root){
  //     return true;
  //   }else{
  //     if(this.model.user !== 'admin' && this.model.level !== AccountLevel.root){
  //       return true;
  //     }
  //     return false;
  //   }
  // }
  // reason(key: string){
  //   switch(key){
  //     case 'user':
  //       if(!this.form.isValid('user')) return 'User name must be between 3-12 characters.';
  //       if(!this.userValidator()) return 'Root user can only be name "admin".';
  //       return null;
  //     case 'email':
  //       if(!this.form.isValid('email')) return 'Email is invalid.'; 
  //     default:
  //       return null;
  //   }
  // }
}
