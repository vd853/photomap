import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormConstructor } from '../../Utilities/formConstructor';
import { AccountModel, AccountLevel, GUID, AccountRequest } from '../../r';
import { LoginGuard } from '../../login.guard';
import { NotificationDirective } from '../../Directives/notification.directive';
import { AccountManagerComponent } from '../account-manager.component';
import { LoginComponent } from '../../login/login.component';
import { AccountSetComponent } from '../account-set/account-set.component';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.css']
})
export class AccountFormComponent implements OnInit {
  private originalUser: string;
  private isManagement: boolean;
  private accountManager: AccountManagerComponent;
  private list: {value: number, viewValue:string}[] = [];
  isCreate: boolean;

  private login: LoginComponent;

  @Input() responseMessage: string;
  form = new FormConstructor();
  deleteAccountTrigger = false;
  changePasswordTrigger = false;
  @ViewChild(NotificationDirective) notifier: NotificationDirective;
  @Input() model: AccountModel;
  constructor(private loginGuard: LoginGuard, private cookie: CookieService) { 
    this.form.addText('user', 2, 12, true);
    this.form.addText('info')
    this.form.addGeneric('level');
    this.form.addEmail('email', false); //allow empty emails for now
    this.form.addText('password', undefined, undefined, true);
    this.form.addText('passwordNew')
    this.form.build();
  }

  ngOnInit() {
    this.list = AccountSetComponent.generateAccountModelKeys();
  }
  InitConfigMode(isManagement: boolean, accountManager: AccountManagerComponent){
    this.isManagement = isManagement;
    this.accountManager = accountManager;
    setTimeout(() => {
      this.originalUser = this.model.user;
    }, 10);
  }

  InitCreateMode(login: LoginComponent){
    this.isCreate = true;
    this.login = login;
  }

  InitCreateManagement(accountManager: AccountManagerComponent){
    this.accountManager = accountManager;
    this.isCreate = true;
    this.isManagement = true;
  }

  userValidator(){
    if(this.model.user === 'admin' && this.model.level === AccountLevel.root){
      return true;
    }else{
      if(this.model.user !== 'admin' && this.model.level !== AccountLevel.root){
        return true;
      }
      return false;
    }
  }
  reason(key: string){
    let reason = null;
    switch(key){
      case 'user':
        if(!this.form.isValid('user')) reason = 'User name must be between 2-12 characters.';
        if(!this.userValidator()) reason = 'Root user can only be name "admin".';
        return reason
      case 'email':
        // if(!this.form.isValid('email')) reason = 'Email is invalid.'; 
        // this.formValid = reason? false: true;
        // return reason
      case 'password':
        if(!this.form.isValid('password')) reason = 'Password is required.';
        return reason
    }
  }
  validate(){
    return this.form.isValid('user') && 
            this.userValidator() &&
            this.form.isValid('password')
  }
  changePassword(){
    this.model.request = AccountRequest.passwordChange;
    this.loginGuard.authenticate(this.model, '', (err, result)=>{
      if(err) throw new Error(err);
      if(result.verified) {
        this.changePasswordTrigger = false;
        this.model.passwordNew = null;
        this.model.password = ''
        this.notifier.toast(this.model.user + ' password is changed.')
      }else{
        this.notifier.toast(this.model.user + ' password is invalid.', 0)
      }
      this.responseMessage = result.reason;
    })
  }
  update(){
    this.model.request = AccountRequest.updateSelf;
    delete this.model.password;
    this.loginGuard.authenticate(this.model, '', (err, result)=>{
      if(err) throw new Error(err);
      if(this.model.user !== this.originalUser && !this.isManagement) window.location.reload();
      if(this.model.user !== this.originalUser && this.isManagement) this.accountManager.getUsers();
      if(result.verified) this.notifier.toast(this.model.user + ' profile was updated.')
      console.log(result);
    })
  }
  create(){
    this.form.touchAll();
    if(!this.validate()){
      this.responseMessage = "Some inputs are invalid"
      return;
    }else{
      this.responseMessage = null;
    }
    if(!this.isManagement){
      this.login.register();
      return;
    }
    this.createByManagement()
  }
  createByManagement(){
    this.model.request = AccountRequest.create;
    this.model.sessionIDManagement = this.loginGuard.model.sessionID;
    this.loginGuard.authenticate(this.model, '', (err, result)=>{
      if(err) throw new Error(err);
      if(result.verified){
        this.notifier.toast(this.model.user + ' was created.')
        this.model = new AccountModel(GUID.create())
        this.accountManager.getUsers();
        this.accountManager.accountModify = false;
      }
    })
  }
  //not for management
  delete(){
    this.model.request = AccountRequest.delete;
    this.loginGuard.authenticate(this.model, '', (err, result)=>{
      if(err) throw new Error(err);
      if(result.verified){
        this.notifier.toastTop(this.model.user + ' was deleted. You will be redirected to login page.', 0)
        setTimeout(() => {
          this.cookie.deleteAll();
          window.location.reload();
        }, 5000);
      }
      this.notifier.toast(result.reason, 0)
    })
  }
}
