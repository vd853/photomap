import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { AccountModel, AccountRequest, GUID} from '../r';
import { LoginGuard } from '../login.guard';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { TopnavComponent } from '../topnav/topnav.component';
import { AccountFormComponent } from './account-form/account-form.component';
import { NotificationDirective } from '../Directives/notification.directive';

@Component({
  selector: 'app-account-manager',
  templateUrl: './account-manager.component.html',
  styleUrls: ['./account-manager.component.css']
})
export class AccountManagerComponent implements OnInit {
  RequestRemodel: AccountModel = new AccountModel('anything');
  newModel: AccountModel;
  dataRetrieved = false;
  userData:AccountModel[];
  topNav: TopnavComponent;
  loginGuard: LoginGuard;
  responseMessage: string;
  @ViewChild(NotificationDirective) notifier: NotificationDirective;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AccountManagerComponent>,
    ) { 
  }

  ngOnInit() {
    if(!this.data.topNav){
      throw new Error('Account manager requires a topNav reference.')
    }
    this.topNav = this.data.topNav;
    this.loginGuard = this.topNav.loginGuard;
    if(this.loginGuard.model){
      this.RequestRemodel.sessionIDManagement = this.loginGuard.model.sessionID;
      this.RequestRemodel.user = this.loginGuard.model.user;
    }else{
      throw new Error('Expected model in LoginGuard does not exist.')
    }
    this.getUsers();
  }
  getUsers(){
    this.RequestRemodel.request = AccountRequest.getUsers;
    this.loginGuard.authenticate(this.RequestRemodel, '', (err, result)=>{
      if(err) throw err;
      if(result){
        this.userData = result.package
        this.dataRetrieved = true;
        console.log(this.userData);
      }
    })
  }
  openSettings(user: string){
    console.log('Finding ', user);
    const model = this.userData.find(e=>e.user === user);
    if(!model) throw new Error('Could not find ' + user + ' for editing.')
    this.topNav.openAccount(model, this);
  }
  deleteUser(user: string){
    const model = this.userData.find(e=>e.user === user);
    model.request = AccountRequest.delete;
    model.sessionIDManagement = this.loginGuard.model.sessionID;
    this.loginGuard.authenticate(model, '', (err, result)=>{
      if(err) throw err;
      if(result){
        this.notifier.toast(result.reason)
        this.getUsers();
      }
    })
  }

  accountModify = false;
  @ViewChild('accountForm') accountForm: AccountFormComponent;
  accountModifier(){
    this.accountModify = !this.accountModify
    this.responseMessage = null;
    if(this.accountModify){
      this.newModel = new AccountModel(GUID.create());
      setTimeout(() => {
        this.accountForm.InitCreateManagement(this);
      }, 10);
    }
  }
}
