import { Component, OnInit, Input } from '@angular/core';
import { maxCharLength } from '../r';
import * as moment from 'moment'

@Component({
  selector: 'app-preview-text',
  templateUrl: './preview-text.component.html',
  styleUrls: ['./preview-text.component.css']
})
export class PreviewTextComponent implements OnInit {
  enabled = false
  @Input() index = 0;
  text = 'some Really'
  style = {
    'top': 100 + 'px', 
    'left': 100 + 'px'
  }
  constructor() { }

  ngOnInit() {
  }
  update(x: number, y: number, show: boolean){
    // console.log('text preview show')
    this.enabled = show
    this.style['left'] = x + 'px'
    this.style['top'] = (y+150) + 'px' 
  }
  
  static validator(text: string){
    if(!text) return false;
    if(text.toString() === 'true') return false;
    if(text.length < maxCharLength) return false;
    if(moment(text).isValid()) return false;
    return true;
  }

}
