import { Component, OnInit, Input } from '@angular/core';
import { PhotoModel, capitalizeEachWord, GUID, globals, CFields } from '../r';
import { FormConstructor } from '../Utilities/formConstructor';
import * as _ from 'lodash'
import { Observable } from 'rxjs/Observable';
import { KeywordLinkService } from '../Service/keyword-link.service';
import { ServerlinkService } from '../Service/serverlink.service';
@Component({
  selector: 'app-photoform',
  templateUrl: './photoform.component.html',
  styleUrls: ['./photoform.component.css']
})
export class PhotoformComponent implements OnInit {
  public model: PhotoModel //only public for HTML binding, otherwise use getModel()
  @Input() isUpload = false; //this disables the date since it is process automatically
  group = 'form-group col-lg-8 col-md-8 col-sm-8 col-xs-8'
  groupCheck = 'form-check'
  form = new FormConstructor()
  attrTemp:any = {};
  initBinds = false;
  constructor(private keywordLink:KeywordLinkService, private serverLink:ServerlinkService) {
    this.model = new PhotoModel(serverLink.model.fieldDefinitions);
    this.model.isReference = false
    this.form.addText('photographer')
    this.form.addText('location')
    this.form.addText('campus')
    this.form.addText('building')
    this.form.addText('component')
    this.form.addText('conduit')
    this.form.addText('comment')
    this.form.addText('area')
    this.form.addText('title')
    this.form.addGeneric('isReference')
    this.form.addGeneric('date')
    serverLink.model.fieldDefinitions.forEach(e=>{
      this.attrTemp[e] = '';
      this.form.addText(e)
    })
    this.form.build(false)
   }

  formatter = (result: string) => result.toUpperCase();
  //from globals keywordable = ['location','photographer', 'building', 'campus', 'component', 'comment', 'conduit', 'fileName']
  //only these fields can be autocompleted
  searcher = new Map() //searcher are identifiable by their key
  initAutofill(){
    console.log('init autofill')
    const searchables = globals.keywordable.concat(this.serverLink.model.fieldDefinitions)
    searchables.forEach(e=>{
    const terms = this.keywordLink.keywords[e]? this.keywordLink.keywords[e]: []
    this.searcher[e] = (text$: Observable<string>) =>
      text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term === '' ? []
        : terms.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
    })
  }

  ngOnInit() {
    this.initAutofill()

    //wait for any incoming model, particularly from modify component
    setTimeout(() => {
      this.serverLink.model.fieldDefinitions.forEach(e=>{
        this.fieldReverseBindWithModel(e);
      })
      console.log('generated model ', this.model);
      this.initBinds = true;
    }, 100);
  }

  //don't show 'undefined' text in the input field
  blankUndefines(model:PhotoModel):PhotoModel{
    globals.formablesText.forEach(e=>{
      // console.log('form check ', e)
      if(!model[e] || model[e] === 'undefined'){
        console.log('setting')
        model[e] = ''
      }
    })
    return model;
  }
  isDateDirty(){
    return this.form.formGroup.controls['date'].dirty
  }
  setModel(model: PhotoModel){
    setTimeout(() => {
      this.model = this.blankUndefines(model);
    }, 1);
  }
  getModel(){
    // console.log('diry1 ', this.form.formGroup.controls['date'].dirty, ' diry2 ', this.form.formGroup.controls['isReference'].dirty)
    if(!this.form.formGroup.controls['date'].dirty){
      delete this.model.date; 
    }
    if(!this.form.formGroup.controls['isReference'].dirty){
      delete this.model.isReference;
    }
    return this.model;
  }
  formatLabel(text: string){
    return capitalizeEachWord(text);
  }
  fieldBindWithModel(field: string){
    if(!this.initBinds) return;
    console.log('modeling binding');
    const element = this.model.attr.find(e=>e.name === field)
    if(element){
      this.model.attr[this.model.attr.indexOf(element)].value = this.attrTemp[field];
    }else{
      const newEle = new CFields(GUID.create())
      newEle.value = this.attrTemp[field]
      newEle.name = field;
      this.model.attr.push(newEle);
    }
    console.log('model now ', this.model);
  }
  fieldReverseBindWithModel(field: string){
    const element = this.model.attr.find(e=>e.name === field)
    console.log('reverse bindings ', element);
    if(element){
      console.log('reverse bind ', element);
      this.attrTemp[field] = this.model.attr[this.model.attr.indexOf(element)].value;
    }else{
      console.log('no revesre binding');
    }
  }
  debug(){
    return JSON.stringify(this.attrTemp);
  }
  debug2(){
    return JSON.stringify(this.model);
  }
}
