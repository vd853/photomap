import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageLargeComponent } from './image-large.component';

describe('ImageLargeComponent', () => {
  let component: ImageLargeComponent;
  let fixture: ComponentFixture<ImageLargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageLargeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageLargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
