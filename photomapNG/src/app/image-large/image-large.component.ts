import { Component, OnInit, ViewChild, Inject, HostListener } from '@angular/core';
import { ImageComponent } from '../image/image.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PhotoModel } from '../r';
import { InfoTableComponent } from '../info-table/info-table.component';
import { HeadingComponent } from '../heading/heading.component';

@Component({
  selector: 'app-image-large',
  templateUrl: './image-large.component.html',
  styleUrls: ['./image-large.component.css']
})
export class ImageLargeComponent implements OnInit {
  model:PhotoModel
  @ViewChild('image') image: ImageComponent
  @ViewChild('info') infoTable: InfoTableComponent;
  constructor(@Inject(MAT_DIALOG_DATA) public dialogData: any, public dialogRef: MatDialogRef<HeadingComponent>,) { }

  ngOnInit() {
    //use for enlarge image when thumbnail is clicked
    if(this.dialogData){
      if(this.dialogData.type === 'ImageHigh'){
        this.model = this.dialogData.model
        this.image.reload(this.dialogData.data)
      }
    }
  }
  @HostListener("window:keydown", ['$event'])
  onKeyDown(event:KeyboardEvent) {
    // console.log('key modify ', event)

    //returns to heading
    if(event.code === 'ArrowRight' || event.code === 'KeyD'){
      this.dialogRef.close(2)
    }
    if(event.code === 'ArrowLeft' || event.code === 'KeyA'){
      this.dialogRef.close(1)
    }
  }
}
