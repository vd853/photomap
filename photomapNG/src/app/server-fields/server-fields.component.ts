import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ServerlinkService } from '../Service/serverlink.service';
import { ServerTriggers } from '../r';
import { KeywordLinkService } from '../Service/keyword-link.service';
import { CacheService } from '../Service/cache.service';

@Component({
  selector: 'app-server-fields',
  templateUrl: './server-fields.component.html',
  styleUrls: ['./server-fields.component.css']
})
export class ServerFieldsComponent implements OnInit {
  newField: string = '';
  responseMessage: string = '';
  @ViewChild('box') box:ElementRef;
  constructor(private serverLink: ServerlinkService, private keywordLink: KeywordLinkService, private cache: CacheService) { 
    
  }

  ngOnInit() {
  }
  addNewField(){
    if(!this.newField){
      this.responseMessage = 'You must enter a field name.'
      return;
    }
    if(this.serverLink.model.fieldDefinitions.indexOf(this.newField) !== -1){
      this.responseMessage = 'Field name already exist.'
      return;
    }
    this.serverLink.model.fieldDefinitions.push(this.newField)
    this.serverLink.model.triggers = ServerTriggers.fieldUpdate;
    this.serverLink.updateServer(this.serverLink.model, (err, data)=>{
      if(data){
        this.serverLink.model = data.model;
        this.updateFields();
        this.responseMessage = 'Added: ' + this.newField;
        this.box.nativeElement.value = '';
      }else{
        this.responseMessage = 'Error updating server.'
      }
    })
  }
  removeField(field: string){
    if(!field) throw 'Error, no field to remove'
    if(this.serverLink.model.fieldDefinitions.indexOf(field) === -1) throw 'Field does not exist'
    this.serverLink.model.fieldDefinitions.splice(this.serverLink.model.fieldDefinitions.indexOf(field), 1)
    this.serverLink.model.triggers = ServerTriggers.fieldUpdate;
    this.serverLink.updateServer(this.serverLink.model, (err, data)=>{
      if(data){
        this.serverLink.model = data.model;
        this.updateFields();
        this.responseMessage = 'Removed: ' + field;
      }else{
        this.responseMessage = 'Error updating server.'
      }
    })
  }

  //changes may not immediately appear for large media counts
  updateFields(){
    this.keywordLink.updateKeyword(()=>{
      this.cache.cards.hotReloadAll();
    });
  }
}


