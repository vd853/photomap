import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerFieldsComponent } from './server-fields.component';

describe('ServerFieldsComponent', () => {
  let component: ServerFieldsComponent;
  let fixture: ComponentFixture<ServerFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServerFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
