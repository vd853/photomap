import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { CardComponent } from '../card/card.component';
import { SearchService } from '../Service/search.service';
import * as moment from 'moment'
import { MatDialog } from '@angular/material';
import { ModifyComponent } from '../modify/modify.component';
import { PhotoModel, maxCharLength } from '../r';
import { ImagelinkService } from '../Service/imagelink.service';
import { CacheService } from '../Service/cache.service';
import { ImageLargeComponent } from '../image-large/image-large.component';
import { PreviewTextComponent } from '../preview-text/preview-text.component';
import { InfoGetterService } from '../Service/info-getter.service';
import { ShareComponent } from '../share/share.component';
@Component({
  selector: 'app-heading',
  templateUrl: './heading.component.html',
  styleUrls: ['./heading.component.css']
})
export class HeadingComponent implements OnInit {
  imageActive = false;
  hover = false
  selected = false;
  selectable = true;
  @Input() parent: CardComponent
  columnCount = [0,1,2,3,4]
  currentN;
  @ViewChild('preview') preview :PreviewTextComponent;
  constructor(private infoGetter: InfoGetterService, private search: SearchService, public dialog: MatDialog, private imageLink: ImagelinkService, private cache: CacheService) { }

  ngOnInit() {
    
  }

  select(isSelected: boolean){
    this.selected = isSelected
    this.parent.selectedCheck(this.selected? 2:1)
  }
  checkSelect(event: any){
    if(event.ctrlKey || event.altKey) { //alt key will cause deselect
      this.parent.parent.selectDownTo(this.parent.model._id, event.ctrlKey? true: false)
      console.log('shifted')
    } 
    else 
    {
      //reverse boolean due to binding delay
      this.parent.parent.previousSelectionId = this.parent.model._id
      console.log('not shifted')
    }
    this.parent.selectedCheck(!this.selected? 2:1)
    this.parent.parent.updateSelectionNotifier();
  }
  unNull(text: any, n: number){
    //this is use to keep the text part blank, otherwise 'false' will show up
    //the text part cannot be hidden or the flex will mess up
    if(this.parent.parent.thumbIndex === n){
      return ''
    }

    //text input could be a boolean, if this is not use, it will return blank for false booleans
    if(typeof text === 'boolean'){
      if(!text){
        return 'No'
      }else{
        return 'Yes'
      }
    }

    if(!text || text === 'undefined') return ''
    return text
  }
  

  reset(){
  }

  //similar to info-table, but NOT the same
  checkPreview(n: number, event: any){
    const key = this.parent.parent.columnView[n]
    const value = PhotoModel.getAnyFieldValueByKey(key, this.parent.model);
    if(PreviewTextComponent.validator(value)){
      this.preview.text = value;
      this.preview.update(event.clientX+20, event.clientY-130, true)
    }
  }

  checkPreviewNull(){
    // console.log('null preview')
    this.preview.update(0,0, false)
  }

  checkImageActive(n: number): boolean{
    if(!this.imageActive){
      return false;
    }
    return this.parent.parent.thumbIndex === n;
  }

  getHighImage(id?: string, model?: PhotoModel){
    if(!id){
      id = this.parent.model._id;
      model = this.parent.model;
    }
    //use cache image
    if(this.cache.ImageHigh.get(id)){
      console.log('getting from cache ', this.cache.ImageHigh.size)
      this.openLargeImage(this.cache.ImageHigh.get(id), model)
      return;
    }

    //no cache image so get it, display, and cache
    this.imageLink.getImage(id,4, -1, (err, datar)=>{
      if(datar){
        console.log('getting high resolution ')
        this.cache.ImageHigh.set(id, datar.result)
        this.openLargeImage(datar.result, model)
      }
    })
  }
  openLargeImage(image64: string, model: PhotoModel){
    let dialogRef = this.dialog.open(ImageLargeComponent, {
      data: {type: 'ImageHigh', data: image64, model: model},
    });

    dialogRef.afterClosed().subscribe(result =>{
      //2 is right, 1 is left, 0 is nothing
      if(result === 2){
        this.findNext(model._id)
        console.log('next')
      }
      if(result === 1){
        this.findNext(model._id, false)
      }
    })
  }

  //use the current id to set the next popup image, or the previous
  //scrolling is loop
  findNext(currentId: string, isNext = true){
    console.log('lookup id ', currentId)
    let index = this.cache.PhotoModels.get(currentId).index;
    let useModel: PhotoModel
    const totalLength = this.parent.parent.children.length;
    console.log('index at ', index, ' totallength ', totalLength)
    if(isNext){
      if(index === totalLength-1){
        this.getHighImage(this.parent.parent.getChildModelByIndex(0)._id, this.parent.parent.getChildModelByIndex(0))
      }else{
        this.getHighImage(this.parent.parent.getChildModelByIndex(index+1)._id, this.parent.parent.getChildModelByIndex(index+1))
      }
    }else{
      if(index === 0){
        this.getHighImage(this.parent.parent.getChildModelByIndex(totalLength-1)._id, this.parent.parent.getChildModelByIndex(totalLength-1))
      }else{
        this.getHighImage(this.parent.parent.getChildModelByIndex(index-1)._id, this.parent.parent.getChildModelByIndex(index-1))
      }
    }
  }
  openModify(): void {
    let dialogRef = this.dialog.open(ModifyComponent, {
      data: {type: 'update', data: this.parent.model, position: this.parent.parent.getChildPosition(this.parent.model._id)},
      width: '1000px',
      height: '700px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(!result) return;

      //single delete from the modify
      if(result.delete){
        console.log('deleted')
        this.parent.destroySelf(true) //if true, only remove card and not the db
        return
      }

      //after model is updated
      if(!result.batch){
        console.log('return modify => heading')
        console.log('model updated ', result)

        //this model was already updated by the modify before returning here
        //You cannot update through other components since they maybe unexpanded
        //Do a direct update
        this.parent.model = result.oneModel; 
      }
    });
  }
  openShare(){
    let dialogRef = this.dialog.open(ShareComponent, {
      data: this.parent,
      width: '1000px',
      height: '700px'
    });
  }
}
