import {UploadModel} from '../../../PhotomapNode/app/Models/upload'; 
export {UploadModel}
import {PhotoModel} from '../../../PhotomapNode/app/Models/photo'
export {PhotoModel}
import {MarkModel, MarkMode} from '../../../PhotomapNode/app/Models/mark'
export {MarkModel, MarkMode}
import {QueryModel, QueryType} from '../../../PhotomapNode/app/Models/query'
export {QueryModel, QueryType}
import {ServerModel, ServerTriggers} from '../../../PhotomapNode/app/Models/server'
export {ServerModel, ServerTriggers}
import {ModifyModel} from '../../../PhotomapNode/app/Models/modify'
export {ModifyModel}
import {GUID} from '../../../../uNode/u/GUID'
export {GUID}
import {Cipher} from '../../../../uNode/u/Cipher'
export {Cipher}
import {globals} from '../../../PhotomapNode/app/globals'
export {globals}
import {VideoModel} from '../../../../uNode/m/VideoModel'
export {VideoModel}
import {SectionModel} from '../../../PhotomapNode/app/Models/section'
export {SectionModel}
import {VideoQuery} from '../../../PhotomapNode/app/Models/videoQuery'
export {VideoQuery}
import {mediaType} from '../../../PhotomapNode/app/Models/reference'
export {mediaType}
import {AccountModel, IAuthenticateResponse, AccountStatus, AccountRequest, AccountLevel, Privilege, PrivilegeType} from '../../../../uNode/m/AccountModel'
export {AccountModel, IAuthenticateResponse, AccountStatus, AccountRequest, AccountLevel, Privilege, PrivilegeType}
import {StringData, myMath, Data} from '../../../../uNode/u/Data'
export {StringData, myMath, Data}
import {CFields} from '../../../PhotomapNode/app/Models/info'
export {CFields}


//problem exporting and importing enum, so this is the copy and paste version of request in the node model
export enum RequestType {request = 0, lookup = 1, singleDownload = 2} //assume request to be multiple
export enum DownloadType {high = 0, reference = 1, merge = 2, requestDownload = 3, clipDownload = 4}

import * as shortid from 'shortid'
export function capitalizeEachWord(text: string){
    return text.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
export const darkColor = 'gray'
export const maxCharLength = 20;
// export class GUID{
//     static create(isShort = false)  
//     {  
//        if(isShort){
//            return shortid.generate()
//        } 
//        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {  
//           var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);  
//           return v.toString(16);  
//        });  
//     } 
// }
