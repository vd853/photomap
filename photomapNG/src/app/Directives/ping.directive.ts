import { Directive } from '@angular/core';
import { GlobalService } from '../Service/global.service';
import {Observable} from 'rxjs/Observable';
import { ServerlinkService } from '../Service/serverlink.service';

@Directive({
  selector: '[appPing]'
})
export class PingDirective {

  constructor(private global: GlobalService, private server: ServerlinkService) { 
  }
  ping(){
    this.server.enclosure.reboot("Waiting for server to turn off.");
    let waitingOff = true;
    return new Observable(obs=>{
      const checker = setInterval(()=>{
        this.server.getServer(
          (err, data)=>{
            if(!waitingOff){
              clearInterval(checker);
              window.location.reload();
              obs.complete(); //server is now back online
            }
            obs.next('waitOff'); //server is still online
          },
          ()=>{
            waitingOff = false;
            this.server.enclosure.reboot("Waiting for server to turn on.");
            obs.next('waitOn'); //server has just went offline
          }
      )
      }, waitingOff? 250: 1000)
    })
  }
}
