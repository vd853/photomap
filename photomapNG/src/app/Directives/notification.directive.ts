import { Directive } from '@angular/core';
import { Ng2IzitoastService } from 'ng2-izitoast';
import { MatSnackBar } from '@angular/material';
import { ServerlinkService } from '../Service/serverlink.service';

@Directive({
  selector: '[appNotification]'
})
export class NotificationDirective {
  defaultColor = '#40A73F'
  warnColor = '#F56300'
  constructor(public iziToast: Ng2IzitoastService, public snackBar: MatSnackBar, private server: ServerlinkService) { }
  toast(message:string, color: number = -1, time = 4000){
    let colorValue = this.defaultColor
    switch(color){
      case 0:
        colorValue = this.warnColor
        break;
      case 1:
        break;
    }
    this.iziToast.show({
      message: message,
      timeout: time,
      color: colorValue,
      messageColor: 'white',
      progressBar: false,
      close: false
    });
  }
  toastTop(message: string, color: number = -1){
    let colorValue = this.defaultColor
    switch(color){
      case 0:
        colorValue = this.warnColor
        break;
      case 1:
        break;
    }
    this.iziToast.show({
      message: message,
      timeout: 5000,
      color: colorValue,
      messageColor: 'white',
      progressBar: true,
      position: 'topCenter',
      close: false
    });
  }
  toastAM(message: string, subMessage = ''){
    this.snackBar.open(message,subMessage, {
      duration: 4000,
    });
  }
  confirmDelete(message: string, title: string, onYes:()=>void){
    if(this.server.model.noConfirmDelete){
      onYes();
      return;
    }
    this.iziToast.question({
      timeout: 10000,
      close: false,
      overlay: true,
      toastOnce: true,
      title: title,
      message: message,
      messageColor: 'white',
      id: 'question',
      position: 'center',
      color: this.warnColor,
      zindex: '99999',
      buttons: [
          ['<button><b>YES</b></button>', function (instance, toast) {
              onYes();
              instance.hide(toast, { transitionOut: 'fadeOut' }, 'YES');
          }, true],
          ['<button>NO</button>', function (instance, toast) {
              instance.hide(toast, { transitionOut: 'fadeOut' }, 'NO');
          }],
      ]
    });
  }
}
