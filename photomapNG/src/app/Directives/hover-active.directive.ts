import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { CardComponent } from '../card/card.component';
import { darkColor } from '../r';

@Directive({
  selector: '[appHoverActive]'
})
export class HoverActiveDirective {
  @Input() card: CardComponent
  static current: string;
  constructor(private el: ElementRef) {
    
  }
  highlightDark(excludeId:string){
    if(!this.card) return;
    if(this.card.model._id !== excludeId){
      this.el.nativeElement.style.backgroundColor = darkColor;
    }
  }
  highlightOff(){
    this.el.nativeElement.style.backgroundColor = null;
  }
  hightlightOn(){
    this.el.nativeElement.style.backgroundColor = '#ADD2FF';
  }
  @HostListener('mouseenter') onMouseEnter() {
    HoverActiveDirective.current = this.el.nativeElement.id
    // console.log('hover id ', HoverActiveDirective.current)
    // console.log('expanded ', this.card.expand)
    if(!this.card.expand){
      this.hightlightOn();
    }else{
      this.highlightOff();
      this.card.parent.batchHighlight(this.card.model._id, 1)
    }
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.card.parent.resetEffects()
  }
}
