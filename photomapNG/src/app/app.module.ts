import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { UploaderComponent } from './uploader/uploader.component';
import { TestComponent } from './test/test.component';
import { FileUploadModule } from 'ng2-file-upload';
import { ReactiveFormsModule } from '@angular/forms';
import { PhotoformComponent } from './photoform/photoform.component';
import { ImageComponent } from './image/image.component';
import { ImagelinkService } from './Service/imagelink.service';
import { StampComponent } from './stamp/stamp.component';
import { CardComponent } from './card/card.component';
import { ViewComponent } from './view/view.component';
import { HeadingComponent } from './heading/heading.component';
import { DatalinkService } from './Service/datalink.service';
import { CardsComponent } from './cards/cards.component';
import { TopnavComponent } from './topnav/topnav.component';
import { EnclosureComponent } from './enclosure/enclosure.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatNativeDateModule, MatInputModule, MatSelectModule} from '@angular/material';
import {MatMenuModule} from '@angular/material/menu';
import {MatDialogModule} from '@angular/material/dialog';
import { ModifyComponent } from './modify/modify.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms';
import { ImagePreviewComponent } from './image-preview/image-preview.component';
import { ServerlinkService } from './Service/serverlink.service';
import { ServerformComponent } from './serverform/serverform.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { ServerlogComponent } from './serverlog/serverlog.component';
import {MatTableModule} from '@angular/material/table';
import { DownloadlinkService } from './Service/downloadlink.service';
import { SearchService } from './Service/search.service';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ReferenceComponent } from './reference/reference.component';
import { StampSwitchComponent } from './stamp-switch/stamp-switch.component';
import { TestdDirective } from './testd.directive';
import {MatTooltipModule} from '@angular/material/tooltip';
import { KeywordLinkService } from './Service/keyword-link.service';
import { Ng2IziToastModule } from 'ng2-izitoast';
import { NotificationDirective } from './Directives/notification.directive';
import { DigitNotifierComponent } from './topnav/digit-notifier/digit-notifier.component';
import { DisableOverlayComponent } from './enclosure/disable-overlay/disable-overlay.component';
import { HoverActiveDirective } from './Directives/hover-active.directive';//<-- this line
import { CacheService } from './Service/cache.service';
import { ImageLargeComponent } from './image-large/image-large.component';
import { PreviewTextComponent } from './preview-text/preview-text.component';
import { InfoGetterService } from './Service/info-getter.service';
import { InfoTableComponent } from './info-table/info-table.component';
import { SliderComponent } from './Video/slider/slider.component';
import { PlayerComponent } from './Video/player/player.component';
import {VgCoreModule} from 'videogular2/core';
import {VgControlsModule} from 'videogular2/controls';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VgBufferingModule} from 'videogular2/buffering';
import { VideoLinkService } from './Service/video-link.service';
import { SectionComponent } from './Video/section/section.component';
import { SectionFormComponent } from './Video/section-form/section-form.component';
import { SectionlinkService } from './Service/sectionlink.service';
import { SectionInfoTableComponent } from './Video/section-info-table/section-info-table.component';
import { SsluploaderComponent } from './ssluploader/ssluploader.component';
import { PingDirective } from './Directives/ping.directive';
import { ServerFieldsComponent } from './server-fields/server-fields.component';
import { LoginComponent } from './login/login.component';
import { ReCaptchaModule } from 'angular2-recaptcha';
import { LoginGuard } from './login.guard';
import { AccountlinkService } from './Service/accountlink.service';
import { RouterModule, Routes } from '@angular/router';
import { GlobalService } from './Service/global.service';
import { CookieService } from 'ngx-cookie-service';
import { AccountManagerComponent } from './account-manager/account-manager.component';
import { AccountSetComponent } from './account-manager/account-set/account-set.component';
import { AccountFormComponent } from './account-manager/account-form/account-form.component';
import { ShareComponent } from './share/share.component';

const routes:Routes = [
  {path: 'enclosure', component: EnclosureComponent, canActivate: [LoginGuard]},
  {path: '**', component: LoginComponent},
]


@NgModule({
  declarations: [
    AppComponent,
    UploaderComponent,
    TestComponent,
    PhotoformComponent,
    ImageComponent,
    StampComponent,
    CardComponent,
    ViewComponent,
    HeadingComponent,
    CardsComponent,
    TopnavComponent,
    EnclosureComponent,
    ModifyComponent,
    ImagePreviewComponent,
    ServerformComponent,
    ServerlogComponent,
    ReferenceComponent,
    StampSwitchComponent,
    TestdDirective,
    NotificationDirective,
    DigitNotifierComponent,
    DisableOverlayComponent,
    HoverActiveDirective,
    ImageLargeComponent,
    PreviewTextComponent,
    InfoTableComponent,
    SliderComponent,
    PlayerComponent,
    SectionComponent,
    SectionFormComponent,
    SectionInfoTableComponent,
    SsluploaderComponent,
    PingDirective,
    ServerFieldsComponent,
    LoginComponent,
    AccountManagerComponent,
    AccountSetComponent,
    AccountFormComponent,
    ShareComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FileUploadModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    MatButtonModule,
    MatMenuModule,
    MatDialogModule,
    MatCheckboxModule,
    FormsModule,
    MatSnackBarModule,
    MatTableModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    Ng2IziToastModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    ReCaptchaModule
  ],
  providers: [
    ImagelinkService, 
    DatalinkService, 
    ServerlinkService, 
    InfoGetterService, 
    DownloadlinkService, 
    SearchService, 
    GlobalService, 
    KeywordLinkService, 
    CacheService,
    VideoLinkService,
    SectionlinkService,
    LoginGuard,
    AccountlinkService,
    CookieService
  ],
  bootstrap: [AppComponent],
  entryComponents:[
    UploaderComponent, 
    CardComponent, 
    ModifyComponent, 
    ServerformComponent, 
    ServerlogComponent, 
    ImageLargeComponent,
    AccountSetComponent,
    AccountManagerComponent,
    ShareComponent
  ]
})
export class AppModule { }
