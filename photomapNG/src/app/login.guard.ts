import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {Cipher, AccountModel, GUID, IAuthenticateResponse} from './r'
import { AccountlinkService } from './Service/accountlink.service';

@Injectable()
export class LoginGuard implements CanActivate {
  private authenticated = false;
  private static scrambleKey = 'photomap';
  model: AccountModel;
  constructor(private router: Router, private accountLink: AccountlinkService){

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(!this.authenticated) {this.router.navigate(['**']); return;};
    return this.authenticated;
  }
  test(){
    this.accountLink.authenticate(null, (err, result:IAuthenticateResponse)=>{
      console.log('test return ', err, result);
    })
  }
  authenticate(model: AccountModel, captchaString: string, callback:(err, result:IAuthenticateResponse)=>void){
    this.accountLink.authenticate(LoginGuard.scramble(model, captchaString), (err, result)=>{
      if(result){
        console.log('Reponse: ', result);
        if(result.verified){
          this.authenticated = true
          this.model = this.model? this.model: result.model; //this can only be set once
          callback(null, result);
        }else{
          callback(null, result);
        }
      }else{
        callback(err, result);
      }
    })
  }
  static scramble(model: AccountModel, captchaString: string): AccountModel{
    console.log('Model within scramble ', model);
    const c = new Cipher(this.scrambleKey);
    const scrambleModel = JSON.stringify({model: model, cap: captchaString});
    const remodel = new AccountModel(GUID.create());
    remodel.authenticationBundle = c.Encrypt(scrambleModel)
    return remodel;
  }
}
