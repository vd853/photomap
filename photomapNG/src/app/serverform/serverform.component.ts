import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormConstructor } from '../Utilities/formConstructor';
import { ServerModel, QueryModel, StringData, ServerTriggers } from '../r';
import { ServerlinkService } from '../Service/serverlink.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import {MatSnackBar} from '@angular/material';
import { DatalinkService } from '../Service/datalink.service';
import { NotificationDirective } from '../Directives/notification.directive';
import { QueryType } from '../../../../PhotomapNode/app/Models/query';
import { SsluploaderComponent } from '../ssluploader/ssluploader.component';
import { PingDirective } from '../Directives/ping.directive';

@Component({
  selector: 'app-serverform',
  templateUrl: './serverform.component.html',
  styleUrls: ['./serverform.component.css']
})
export class ServerformComponent implements OnInit {
  @ViewChild(NotificationDirective) notify : NotificationDirective
  @ViewChild('ssluploader') ssluploader: SsluploaderComponent;
  @ViewChild(PingDirective) ping: PingDirective
  form = new FormConstructor()
  totalPhoto: number;
  networkConfigTrigger: boolean = false;
  networkIsDHCP = true;
  networkValidationStyle = {}
  networkInvalidStyle = {'box-shadow': '0 0 0 5px red'}
  ipFields = ['ipv4', 'netmask', 'gateway']
  constructor(
    public server:ServerlinkService, 
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ServerformComponent>,
    public snackBar: MatSnackBar,
    private datalink: DatalinkService
  ) { 

    this.form.addGeneric('networkConfigTrigger')
    this.form.addText('ipv4');
    this.form.addText('netmask');
    this.form.addText('gateway');
    this.form.addNumber('port');
    this.form.addGeneric('ssl');
    this.form.addGeneric('networkIsDHCP');
    this.form.addGeneric('hostname');


    this.form.addText('enablePreview')
    this.form.addGeneric('autoGenerateMaps')
    this.form.addGeneric('loadAllOnStart')
    this.form.addGeneric('noConfirmDelete')
    this.form.addGeneric('disableScreenSizeWarning')
    this.form.addGeneric('settableStampColor')
    this.form.addGeneric('contraintImageHeightCard')
    this.form.addGeneric('autoVideoPlay')
    this.form.build(false)
    this.getTotalPhoto();
  }
  sslValid(){
    if(this.server.model.certValid && this.server.model.keyValid){
      return true;
    }
    return false;
  }
  networkValid(){
    console.log('netwokr validate')

    if(this.networkIsDHCP && !this.server.model.ssl){
      return true;
    }

    //check if all ip are valid
    let invalid = this.ipFields.some(e=>{
      if(!StringData.isIp(this.server.model[e])){
        return true;
      }
    })

    if(this.server.model.ssl && this.ssluploader){
      if(this.ssluploader.certStatus === 'validated' && this.ssluploader.keyStatus === 'validated'){
        invalid = false;
      }else{
        invalid = true;
      }
      this.server.model.certValid = !invalid;
      this.server.model.keyValid = !invalid;
    }
    
    if(invalid){
      this.networkValidationStyle = this.networkInvalidStyle;
      return false;
    }else{
      this.networkValidationStyle = {};
      return true;
    }
  }
  getTotalPhoto(){
    const q = new QueryModel();
    q.queryType = QueryType.photoCount;
    this.datalink.getProfile(q, (err, result)=>{
      this.totalPhoto = result? result.result: 0;
    })
  }
  ngOnInit() {
    setTimeout(() => {
      if(this.server.model.networkMode === 'static') this.networkIsDHCP = false;
      this.initSsluploader();
    }, 1);
  }
  initSsluploader(){
    setTimeout(() => { //time to update after this.ssluploader has loaded
      if(!this.ssluploader){
        this.networkValid();
        return;      
      };
      this.ssluploader.parent = this;
      if(this.server.model.keyValid) this.ssluploader.keyStatus = 'validated';
      if(this.server.model.certValid) this.ssluploader.certStatus = 'validated';
      this.networkValid(); //validate again after checking key and cert
    }, 1);
  }

  //null will enable the input control [attr.disabled]="isStatic()"
  //true will disable it
  isStatic(){
    return this.networkIsDHCP? true: null;
  }

  modelGet(){
    return JSON.stringify(this.server.model)
  }

  //awaitOnly is only for waiting, does not send any info to the server
  restartServer(awaitOnly = false){
    this.server.model.triggers = ServerTriggers.reboot;
    const sub = this.ping.ping().subscribe({
      next(response){
        if(response === 'waitOn'){
          console.log('waiting to be online');
        }
        if(response === 'waitOff'){
          console.log('waiting to be shutdown')
        }
      },
      complete(){
        console.log('is now back online')
        sub.unsubscribe();
      }
    })
    if(!awaitOnly) this.apply(true);
  }

  deleteAll(){
    this.notify.confirmDelete('Permanently delete all data and reset server settings? ', 'Critical', ()=>{
      const q = new QueryModel()
      q.destroyAll = true;
      this.datalink.delProfile(q, (err, result)=>{
        if(result.result){
          this.server.enclosure.deleteAllTriggered();
        }
        this.snackBar.open('All data will be removed','', {
          duration: 2000,
        });
        this.dialogRef.close(); //returns to the view
      })
    })
  }
  //modify all selected ids to these settings
  apply(close = false){
    if(this.networkConfigTrigger){
      if(this.networkValid()){
        this.server.model.networkMode = this.networkIsDHCP? 'dhcp': 'static'
        this.server.model.triggers = ServerTriggers.networkUpdate;
      }else{
        this.notify.toast('Network is setting is invalid', 1)
        return;
      }
    }

    this.server.updateServer(this.server.model, (err, data)=>{
      if(data){
        if(this.networkConfigTrigger){
          if(this.networkIsDHCP){
            this.server.enclosure.redirectUpdate(true, this.server.model.port);
            // this.notify.toastTop('Network has been configure as DHCP.' + ' Port: ' + this.server.model.port + '. You will need to manually redirect your address.', 0)
          }else{
            this.server.enclosure.redirectUpdate(false, null, 10);
            // this.notify.toastTop('Network has been configure as static. You will be redirect in 10 seconds.', 0)
            setTimeout(() => {
              const relink = (this.server.model.ssl? 'https://':'http://') + this.server.model.ipv4 + ':' + this.server.model.port + '/';
              window.location.replace(relink);
            }, 10000);
          }
          
        }
        console.log('server data saved')
        this.snackBar.open('Settings saved','', {
          duration: 2000,
        });
        if(close){
          this.dialogRef.close(); //returns to the view
        }
      }
    })
  }
}
